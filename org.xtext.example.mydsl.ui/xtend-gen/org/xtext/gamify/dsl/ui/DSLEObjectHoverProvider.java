package org.xtext.gamify.dsl.ui;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.ui.editor.hover.html.DefaultEObjectHoverProvider;
import org.xtext.gamify.dsl.model.Achievement;
import org.xtext.gamify.dsl.model.Condition;
import org.xtext.gamify.dsl.model.Event;
import org.xtext.gamify.dsl.model.GameDynamic;
import org.xtext.gamify.dsl.model.GameMechanic;
import org.xtext.gamify.dsl.model.Gamify;
import org.xtext.gamify.dsl.model.Item;
import org.xtext.gamify.dsl.model.Restriction;
import org.xtext.gamify.dsl.model.Reward;
import org.xtext.gamify.dsl.model.System;

import com.google.inject.Inject;
 
public class DSLEObjectHoverProvider extends DefaultEObjectHoverProvider {
	@Inject
	private IQualifiedNameProvider nameProvider;
	//try overriding hasHover to always true
	@Override
	protected boolean hasHover(EObject o) {
		if(o instanceof Object) {
			if((Object) o instanceof Integer) {
				return true;
			}
			if((Object) o instanceof String) {
				return true;
			}
		}
		if(o instanceof Keyword) {
			return true;
		}
		return nameProvider.getFullyQualifiedName(o) !=null;
	}
    @Override
    protected String getFirstLine(EObject o) {
        if (o instanceof Gamify) {
            return "Example:<br>"
            		+ "Gamify{<br>"
            		+ "&nbsp;&nbsp;&nbsp;&nbsp;<b>Name:</b> ProjectName <br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>System{</b><br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...<br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>}</b><br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>GameDynamics{</b><br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...<br>" +
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>}</b><br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>Items{</b><br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...<br>" +
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>}</b><br>" + 
            		
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>Information:</b> \"Example of important information.\"<br>" + 
            		"<b>}</b>";//"Project name: " + ((Gamify)o).getName() + "\nAdditional information for the developers: " + ((Gamify)o).getInformation();
        }
        if (o instanceof Item) {
            return "Example:<br>"
            		+ "<b>Prize{</b><br>"
            		+ "&nbsp;&nbsp;&nbsp;&nbsp;<b>Name:</b> PrizeExample<br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>RewardDescription:</b><b>\"</b>Example of a description of a reward<b>\"</b><br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>PrizeType:</b> VIRTUAL<br>" +  
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>Information:</b> <b>\"</b>Example of important information.<b>\"</b><br>" + 
            		"<b>}</b>";
            }
        if(o instanceof System) {
        	return "Example:"
            		+ "<br><b>System{</b><br>"
            		+ "&nbsp;&nbsp;&nbsp;&nbsp;<b>Attributes:</b> ESTABLISHED<br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>UserType:</b> CUSTOMER<br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>UserAttributes:</b> ACHIEVERS<br>" + 
            		"<b>}</b>";
        }
        if (o instanceof GameDynamic) {
            return "Example:"
            		+ "<br><b>GameDynamic{</b><br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>Name:</b>NewThresholds<br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>DynamicType:</b>REVIEW<br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>GameMechanics{</b><br>" +
            		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...<br>" +
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>}</b><br>" +
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>Achievements{</b><br>" +
            		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...<br>" +
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>}</b><br>" +
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>Information:</b>\"Implement Dynamic\" <br>" + 
            		"<b>}</b>";
        }
        if (o instanceof GameMechanic) {
            return "Example:"
            		+ "<br><b>GameMechanic{</b><br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>Name:</b>NewThresholds<br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>MechanicType:</b>PROGRESSBAR<br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>Events{</b><br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Event{</b><br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Name:</b> Running<br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>EventType:</b>MSUCCESS<br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>PointGain:</b>10<br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>TriggerConditions(</b>BeginnerRunner.PointSoaker, JourneymanRunner.FurtherBeyond<b>)</b><br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Information:</b>\"Insert a progression event to further reward users\" <br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>}</b><br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>}</b><br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>Information:</b>\"Link game mechanic with service code\" <br>" + 
            		"<b>}</b>";
        }
        if (o instanceof Achievement) {
            return "Example:<br>"
            		+ "<b>Achievement{</b><br>"
            		+ "&nbsp;&nbsp;&nbsp;&nbsp;<b>Name:</b> JourneymanRunner<br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>Hidden:</b>true<br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>RequiredAchievements(</b>BeginnerRunner, Dasher<b>)</b><br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>Rewards{</b><br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...<br>" +
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>}</b><br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>Conditions{</b><br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...<br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>}</b><br>" +
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>Information:</b>\"Implement Achievement\"<br>" + 
            		"<b>}</b>";
        }
        if (o instanceof Event) {
            return "Example:<br>"+
            		"<b>Event{</b><br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>Name:</b> Running<br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>EventType:</b>MSUCCESS<br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>PointGain:</b>10<br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>TriggerConditions(</b>BeginnerRunner.PointSoaker, JourneymanRunner.FurtherBeyond<b>)</b><br>\r\n" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>Information:</b>\"Insert a progression event to further reward users\" <br>\r\n" + 
            		"<b>}</b><br>";
        }
        if (o instanceof Condition) {
            return "Example:<br>"
            		+ "Condition{<br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;Name:FurtherBeyond<br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;AmountRequired:500<br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;ConditionType:EQUAL<br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;Information:\"Achieved 500 meters of distance\"<br>" + 
            		"}";
        }
        if (o instanceof Restriction) {
            return "\n\nExample:<br>"
            		+ "<b>Restriction{</b><br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>Name:</b>RestrictionExample<br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>RestrictionType:</b>ACTIONLIMIT<br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>Amount:</b>10<br>" + 
            		"&nbsp;&nbsp;&nbsp;&nbsp;<b>Information:</b>\"Restriction information\"<br>" +  
            		"<b>}</b>";
        }
        if (o instanceof Reward) {
            return "Example:<br>"
            		+ "<b>FixedReward{</b><br>"
            		+ "&nbsp;&nbsp;&nbsp;&nbsp;<b>Item:</b> ItemReference<br>" + 
            		"<b>}</b>";
        }
      //"Project name: " + ((Gamify)o).getName() + "\nAdditional information for the developers: " + ((Gamify)o).getInformation();
        
        return super.getFirstLine(o);
    }
 
}