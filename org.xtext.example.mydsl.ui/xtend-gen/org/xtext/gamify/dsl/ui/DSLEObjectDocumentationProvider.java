package org.xtext.gamify.dsl.ui;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.documentation.IEObjectDocumentationProvider;
import org.xtext.gamify.dsl.model.Achievement;
import org.xtext.gamify.dsl.model.Condition;
import org.xtext.gamify.dsl.model.Event;
import org.xtext.gamify.dsl.model.GameDynamic;
import org.xtext.gamify.dsl.model.GameMechanic;
import org.xtext.gamify.dsl.model.Gamify;
import org.xtext.gamify.dsl.model.Item;
import org.xtext.gamify.dsl.model.Restriction;
import org.xtext.gamify.dsl.model.Reward;
import org.xtext.gamify.dsl.model.System;
 
public class DSLEObjectDocumentationProvider implements IEObjectDocumentationProvider {
 
    @Override
    public String getDocumentation(EObject o) {
        if (o instanceof Gamify) {
            return "Gamify is the root of the gamification strategy."
            		+ "<br>In this element, the users should specify the name of their project, as well as any additional information that may be useful for the developers."
            		+ "<br>From this element, the various items which can be used as rewards in a later stage of the design should also be added here, allowing them to be referred to."
            		+ "<br>Further information about the system to be gamified can also be provided through this element, which can be used later when developing strategies."
            		+ "Attribute Details:"
            		+ "<br>"
            		+ "<b>Name</b> - Provide a simple name for the project you wish to create, no spaces or symbols are allowed.<br>"
            		+ "<b>System</b> - Allows you to describe the system to gamify.<br>"
            		+ "<b>GameDynamics</b> - Define the various dynamics to be customized.<br>"
            		+ "<b>Items</b> - Add the various items to be linked as rewards in a later stage of the strategy.<br>"
            		+ "<b>Information</b> - Additional information about the overall strategy for the developers who will be working with the generated code. Use quotation marks.<br>"
            		+ "<br>"
            		+ "Strategy Guidelines:<br>"
            		+ "It is recommended to add items before customizing game dynamics since they are used later when designing achievements.";

        }
        if (o instanceof System) {
            return "System gathers the information about the application to be gamified, including important aspects about its users and how well established in its respective community it is."
            		+ " The purpose of this element is to guide users through the design process of the strategy at hand by adding quick tips when hovering over other strategy elements."
            		+ "<br>"
            		+ "The following are small descriptions of each option within the UserAttributes' list:<br>"
            		+ "ACHIEVERS - User-base consists of users who value completing the tasks set before them. Badges locked behind difficult tasks may maintain these users engaged.<br>"
            		+ "EXPLORERS - User-base consists of users who value new content. Adding different dynamics linked to user progression should increase engagement. <br>"
            		+ "KILLERS - User-base consists of users who value domination over others. Interaction and competition between users is paramount to maintain interest.<br>"
            		+ "SOCIALIZERS - User-base consists of users who value working cooperatively. Tasks should allow users to work together to achieve completion.<br>"
            		+ "<br>"
            		+ "The following are small descriptions of each option within the Attributes' list:<br>"
            		+ "ESTABLISHED - The main service is already used, gamification is only to increase attractiveness of the main service over the competition. Complex strategies are not necessary. <br>"
            		+ "NEW - The main service is not used and gamification is a big part of it. Complex strategies are necessary to maintain interest.<br>"
            		+ "GROWING - The main service is attracting more and more users, gamification is used to increase overall attractiveness. Complexity of strategies varies. <br>"
            		+ "<br>"
            		+ "The following are small descriptions of each option within the UserTypes' list:<br>"
            		+ "CUSTOMER - User-base consists of customers.<br>"
            		+ "EMPLOYEE - User-base consists of employees.<br>";
        }
        if (o instanceof GameDynamic) {
            return "GameDynamic is where overall dynamics can be defined to later have its components customized.<br>"
            		+ "GameDynamic provides a list of dynamics to be chosen from. These dynamics are used to later inform developers what kind of dynamic they are expected to develop."
            		+ "From this element, you can define the dynamic type to be implemented, as well as add all of its related achievements and game mechanics."
            		+ "<br>"
            		+ "Attribute Details:"
            		+ "<br>"
            		+ "<b>Name</b> - Provide a simple name for the <b>GameDynamic</b> you wish to create, no spaces or symbols are allowed (e.g. PuzzleDynamic).<br>"
            		+ "<b>DynamicType</b> - There are some types of game dynamics at your disposal, each being quite self-explanatory. However, the <b>CUSTOM</b> dynamic can be used when the rest of the options aren't quite what you wanted.<br>"
            		+ "The following are the available options: TAKEHOLD, TREASUREHUNT, REVIEW, BET, PRIZESHOP, LOTTERY, PROFILE.<br>"
            		+ "<b>GameMechanics</b> - Allows you to add various game mechanics to the dynamic in question.<br>"
            		+ "<b>Achievements</b> - Allows you to add various achievements related to the dynamic in question.<br>"
            		+ "<b>Information</b> - Additional information about the <b>GameDynamic</b> for the developers who will be working with the generated code. Use quotation marks.<br>"
            		+ "<br>"
            		+ "Strategy Guidelines:<br>"
            		+ "A dynamic should represent the overall functionality of a portion of the strategy in question. As such, should you choose a Puzzle dynamic, the mechanics, the conditions and other factors, should work in tandem with the dynamic chosen."
            		+ "<br>";
        }
        if (o instanceof GameMechanic) {
            return "GameMechanic is where game mechanics can be specified and customized."
            		+ "A list of game mechanics are provided to you to let the developers know what kind of mechanics are expected to be implemented on the gamified service. "
            		+ "These mechanics are not generated since it highly depends on how the base application was developed."
            		+ "Each mechanic should be linked to one or more events, to define the methods behind triggering said mechanic."
            		+ "Attribute Details:"
            		+ "<br>"
            		+ "Strategy Guidelines:<br>"
            		+ "Always remember to maintain a balance between the game mechanic's complexity and the reward behind it. If a user has to go through a hassle to receive an insignificant reward, they'll simply not do it.<br>"
            		+ "Adding a sense of progress may be important to maintain users interested, progress bars or leaderboards may assist in this matter, which can later be used to increase the difficulty of the mechanics, as well as the rewards.";
        }
        if (o instanceof Item) {
            return "An <b>Item</b> is where you may design the rewards for your users.<br>"
            		+ "An <b>Item</b> can be defined as <b>Points</b>, <b>Prize</b>, or <b>Badge</b>.<br>"
            		+ "Depending on the initial choice you've made, the system will provide you with different choices in attributes for the Item you desire to create by pressing <b>CTRL+SPACE</b>.<br>"
            		+ "<br>"
            		+ "Attribute Details:"
            		+ "<br>"
            		+ "<b>Name</b> - Provide a simple name for the <b>Item</b> you wish to create, no spaces or symbols are allowed (e.g. for an Item of type Prize, an example could be a Car).<br>"
            		+ "<b>RewardDescription</b> - This description is read by users who wish to know more about what the reward in question entails. Use quotation marks.<br>"
            		+ "<b>Information</b> - Additional information about the <b>Item</b> for the developers who will be working with the generated code. Use quotation marks.<br>"
            		+ "<b>PrizeType</b> - Applies only for Items of type <b>Prize</b>. Clarifies the type of prize to be rewarded, it can be either <b>VIRTUAL</b> or <b>PHYSICAL</b>.<br>"
            		+ "<b>BadgeLevel</b> - Applies only for Items of type <b>Badge</b>. Insert a number to function to establish how meaningful the badge is, the higher, the more meaningful it is. <br>"
            		+ "<b>BadgeImage</b> - Applies only for Items of type <b>Badge</b>. Add a link pointing to where the developers may find the appropriate image for the badge. Use quotation marks.<br>"
            		+ "<b>Amount</b> - Applies only for Items of type <b>Points</b>. Insert the amount of points a user may receive.<br>"
            		+ "<b>Conditions</b> - Applies only for Items of type <b>Points</b>. Insert the conditions that will be affected by the rewarded amount of points previously set. It's recommended to already have conditions prepared in order to list them.<br>"
            		+ "<b>isCollectible</b> - Applies only for Items of type <b>Points</b>. Establishes whether or not the user is being rewarded with pieces of a greater reward or simply with points. Can either be <b>true</b> or <b>false</b>.<br>"
            		+ "<br>"
            		+ "Strategy Guidelines:<br>"
            		+ "Rewards are fundamental in any gamification strategy, the <b>Item</b> element is used to create these rewards so you can later link them to various other elements of type <b>Achievement</b>.<br>"
            		+ "The types of rewards recommended differ depending on the user-base in question. If the users in question give importance to their status in comparison with others, rewards linked with specific leaderboards are advised. These rewards should be meaningful to motivate users in maintaining/improving their positions."
            		+ "<br>Should the users be considered as achievers, then a set of achievements providing small rewards leading to a heftier reward may be more appropriate.";
        }
        if (o instanceof Achievement) {
            return "Achievement aggregates rewards and conditions.<br>"
            		+ "In each Achievement, you can create several chains of achievements and conditions leading to a reward.<br>"
            		+ "<br>"
            		+ "Attribute Details:"
            		+ "<br>"
            		+ "<b>Name</b> - Provides a simple name for the <b>Achievement</b> you wish to create, no spaces or symbols are allowed (e.g. BeginnerAchiever).<br>"
            		+ "<b>Hidden</b> - Define the Achievement's visibility for the users. Use the keywords <b>TRUE</b> or <b>FALSE</b> to do so.<br>"
            		+ "<b>RequiredAchievements</b> - Add a list of previously created Achievements as requirements before this Achievement can be updated.<br>"
            		+ "<b>Rewards</b> - Add a list of rewards for completing the Achievement.<br>"
            		+ "<b>Conditions</b> - Add a list of conditions to complete the Achievement.<br>"
            		+ "<b>Information</b> - Additional information about the <b>Achievement</b> for the developers who will be working with the generated code. Use quotation marks.<br>"
            		+ "<br>"
            		+ "Strategy Guidelines:<br>"
            		+ "<br>"
            		+ "Use the Hidden attribute to create Easter Eggs. The attribute RequiredAchievements is useful to create a chain of Achievements, emphasizing the user's progression (from BeginnerAchiever to ExpertAchiever).";
        }
        if (o instanceof Event) {
            return "An Event defines what interacts with a game mechanic as well as how it interacts with said mechanic."
            		+ "\nIn this element, the users should specify the name of their project, as well as any additional information that may be useful for the developers."
            		+ "\nFrom this element, the various items which can be used as rewards in a later stage of the design should also be added here, allowing them to be referred to."
            		+ "\nFurther information about the system to be gamified can also be provided through this element, which can be used in combination with the <b>Generate</b> element."
            		+ " This combination allows the generation of a new file with a preset depending on the attributes set."
            		+ "<br>"
            		+ "Attribute Details:"
            		+ "<br>"
            		+ "<b>Name</b> - Provide a simple name for the <b>Event</b> you wish to create, no spaces or symbols are allowed (e.g. Click).<br>"
            		+ "<b>EventType</b> - Events can be actions from users as well as responses from the system. Many of the types are self-explanatory, but the following list explains some of the event types that are system-related:<br>"
            		+ "&nbsp;&nbsp;&nbsp;&nbsp;<b>DSUCCESS</b> - This type signifies that the user was successful in completing the dynamic in question.<br>"
            		+ "&nbsp;&nbsp;&nbsp;&nbsp;<b>MSUCCESS</b> - This type signifies that the user was successful in completing the mechanic in question.<br>"
            		+ "&nbsp;&nbsp;&nbsp;&nbsp;<b>DFAILURE</b> - This type signifies that the user was not successful in completing the dynamic in question.<br>"
            		+ "&nbsp;&nbsp;&nbsp;&nbsp;<b>FAILURE</b> - This type signifies that the user was not successful in completing the mechanic in question.<br>"
            		+ "<b>TriggerConditions</b> - Link a list of Conditions that the Event will interact with.<br>"
            		+ "<b>PointGain</b> - Establish the number of points the conditions will receive should the event be triggered.<br>"
            		+ "<b>Restrictions</b> - Add restrictions to this Event.<br>"
            		+ "<b>Information</b> - Additional information about the <b>Event</b> for the developers who will be working with the generated code. Use quotation marks.<br>"
            		+ "<br>"
            		+ "Strategy Guidelines:<br>"
            		+ "<br>"
            		+ "The events should follow the lead set by the mechanic and the dynamic previously defined, as an example, you don't want to create an Event related to running on a Puzzle dynamic."
            		+ "Use EventTypes such as DSUCCESS and MSUCCESS to increase the amount of points gained to a specific condition.";
        }
        if (o instanceof Condition) {
            return "<b>Condition</b> allows you to design a condition that must be completed to beat an Achievement.<br>"
            		+ "<br>"
            		+ "Attribute Details:"
            		+ "<br>"
            		+ "<b>Name</b> - Provide a simple name for the <b>Condition</b> you wish to create, no spaces or symbols are allowed (e.g. Outrun).<br>"
            		+ "<b>AmountRequired</b> - Set the threshold desired for the condition to be completed.<br>"
            		+ "<b>PreRequirements</b> - Cross-reference to other Conditions necessary for this Condition to update.<br>"
            		+ "<b>ConditionType</b> - Specify the type of condition in relation to the AmountRequired variable. It can be either <b>EQUAL</b>, <b>MORE</b> or <b>LESS</b>.<br>"
            		+ "<b>Information</b> - Additional information about the <b>Condition</b> for the developers who will be working with the generated code. Use quotation marks.<br>"
            		+ "<br>"
            		+ "Strategy Guidelines:<br>"
            		+ "Create chains of conditions using the PreRequirements Attribute, it can be used to increase the difficulty of tasks as well as to create more complex (and in turn more rewarding) Achievements.<br>";
        }
        if (o instanceof Restriction) {
            return "Restriction is used to affect a specific event in different ways."
            		+ "To increase the difficulty of completing any condition, restrictions may be added. A restriction can limit the number of actions, as well as add a time limit to perform the action defined in the event.<br>"
            		+ "<br>"
            		+ "Attribute Details:"
            		+ "<br>"
            		+ "<b>Name</b> - Provide a simple name for the <b>Restriction</b> you wish to create, no spaces or symbols are allowed (e.g. TimeLimit).<br>"
            		+ "<b>RuleDescription</b> - Inform the users about what the restriction entails. Use quotation marks.<br>"
            		+ "<b>RestrictionType</b> - There are three different types of restrictions: <b>TIMELIMIT</b>,<b>POINTLIMIT</b>,<b>ACTIONLIMIT</b>. Each of the types restrict the time to act, the points gathered, or the number of actions allowed, respectivelly.<br>"
            		+ "<b>Amount</b> - Insert the value of the restriction related to the type of restriction chosen.<br>"
            		+ "<b>Information</b> - Additional information about the <b>Restriction</b> for the developers who will be working with the generated code. Use quotation marks.<br>"
            		+ "<br>"
            		+ "Strategy Guidelines:<br>"
            		+ "<br>"
            		+ "Restrictions must be handled with care, specially in the cases when the user-base are employees. As before, difficult tasks should feel all the more rewarding.";
        }
        if (o instanceof Reward) {
            return "Reward specifies which item you wish to give your users for completing achievements."
            		+ "In this simple element, you only need to choose between the reward type you wish to create, being its either a fixed reward or a random reward linked to a customizable chance."
            		+ "To do so, you only need to create a Reward of type FixedReward or of type RandomReward."
            		+ "<br>"
            		+ "Attribute Details:"
            		+ "<br>"
            		+ "<b>Item</b> - Allows you to reference a previously created Item, to define it as a Reward for the Achievement in question.<br>"
            		+ "<b>DropRate</b> - Add a drop chance to the Reward in question. Values should be from 0 to 1. Only for Rewards of type RandomReward.<br>"
            		+ "<b>IsVisible</b> - Establish if the users can know the possible rewards that await them. Only for Rewards of type RandomReward.<br>"
            		+ "<br>"
            		+ "Strategy Guidelines:<br>"
            		+ "<br>"
            		+ "Random rewards can be useful to keep users going, as such they can be used after completing small achievements."
            		+ "However, they can also be used as rewards for completing complex achievements, but make sure you only provide valuable rewards, or else the users may feel cheated."
            		+ "Knowing this, it may be wise to accompany a random reward with a fixed reward.";
        }

        return null;
    }
 
}