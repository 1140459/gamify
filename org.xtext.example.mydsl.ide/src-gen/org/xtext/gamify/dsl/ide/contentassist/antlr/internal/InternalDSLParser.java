package org.xtext.gamify.dsl.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.xtext.gamify.dsl.services.DSLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDSLParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_DOUBLE", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'true'", "'false'", "'ESTABLISHED'", "'GROWING'", "'NEW'", "'EMPLOYEE'", "'CUSTOMER'", "'KILLERS'", "'SOCIALIZERS'", "'EXPLORERS'", "'ACHIEVERS'", "'TAKEHOLD'", "'CUSTOM'", "'TREASUREHUNT'", "'REVIEW'", "'BET'", "'PRIZESHOP'", "'LOTTERY'", "'PROFILE'", "'LEADERBOARD'", "'QUEST'", "'PROGRESSBAR'", "'COOPERATION'", "'OPPONENT'", "'COLLECTIBLE'", "'INVENTORY'", "'SOCIALGIFT'", "'AVATAR'", "'CLICK'", "'HOVER'", "'VIEW'", "'DRAG'", "'DROP'", "'DSUCCESS'", "'MSUCCESS'", "'DFAILURE'", "'MFAILURE'", "'EQUAL'", "'MORE'", "'LESS'", "'TIMELIMIT'", "'ACTIONLIMIT'", "'POINTLIMIT'", "'VIRTUAL'", "'PHYSICAL'", "'Gamify'", "'{'", "'GameDynamics'", "'}'", "'Items'", "'Name:'", "'System'", "','", "'Information:'", "'.'", "'Attributes:'", "'UserType:'", "'UserAttributes:'", "'GameDynamic'", "'GameMechanics'", "'Achievements'", "'DynamicType:'", "'GameMechanic'", "'MechanicType:'", "'Events'", "'Achievement'", "'Conditions'", "'Hidden:'", "'RequiredAchievements'", "'('", "')'", "'Rewards'", "'Event'", "'EventType:'", "'PointGain:'", "'TriggerConditions'", "'Restrictions'", "'Condition'", "'AmountRequired:'", "'ConditionType:'", "'PreRequirements'", "'Restriction'", "'RuleDescription:'", "'RestrictionType:'", "'Amount:'", "'RandomReward'", "'DropRate:'", "'IsVisible:'", "'Item:'", "'FixedReward'", "'Badge'", "'RewardDescription:'", "'BadgeLevel:'", "'BadgeImage:'", "'Points'", "'IsCollectible:'", "'Prize'", "'PrizeType:'"
    };
    public static final int T__50=50;
    public static final int T__59=59;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=4;
    public static final int RULE_INT=6;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__67=67;
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__64=64;
    public static final int T__65=65;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__91=91;
    public static final int T__100=100;
    public static final int T__92=92;
    public static final int T__93=93;
    public static final int T__102=102;
    public static final int T__94=94;
    public static final int T__101=101;
    public static final int T__90=90;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__99=99;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__95=95;
    public static final int T__96=96;
    public static final int T__97=97;
    public static final int T__98=98;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=9;
    public static final int RULE_DOUBLE=7;
    public static final int T__77=77;
    public static final int T__78=78;
    public static final int T__79=79;
    public static final int T__73=73;
    public static final int EOF=-1;
    public static final int T__74=74;
    public static final int T__75=75;
    public static final int T__76=76;
    public static final int T__80=80;
    public static final int T__81=81;
    public static final int T__82=82;
    public static final int T__83=83;
    public static final int RULE_WS=10;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__88=88;
    public static final int T__108=108;
    public static final int T__89=89;
    public static final int T__107=107;
    public static final int T__109=109;
    public static final int T__84=84;
    public static final int T__104=104;
    public static final int T__85=85;
    public static final int T__103=103;
    public static final int T__86=86;
    public static final int T__106=106;
    public static final int T__87=87;
    public static final int T__105=105;

    // delegates
    // delegators


        public InternalDSLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDSLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDSLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalDSL.g"; }


    	private DSLGrammarAccess grammarAccess;

    	public void setGrammarAccess(DSLGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleGamify"
    // InternalDSL.g:53:1: entryRuleGamify : ruleGamify EOF ;
    public final void entryRuleGamify() throws RecognitionException {
        try {
            // InternalDSL.g:54:1: ( ruleGamify EOF )
            // InternalDSL.g:55:1: ruleGamify EOF
            {
             before(grammarAccess.getGamifyRule()); 
            pushFollow(FOLLOW_1);
            ruleGamify();

            state._fsp--;

             after(grammarAccess.getGamifyRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGamify"


    // $ANTLR start "ruleGamify"
    // InternalDSL.g:62:1: ruleGamify : ( ( rule__Gamify__Group__0 ) ) ;
    public final void ruleGamify() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:66:2: ( ( ( rule__Gamify__Group__0 ) ) )
            // InternalDSL.g:67:2: ( ( rule__Gamify__Group__0 ) )
            {
            // InternalDSL.g:67:2: ( ( rule__Gamify__Group__0 ) )
            // InternalDSL.g:68:3: ( rule__Gamify__Group__0 )
            {
             before(grammarAccess.getGamifyAccess().getGroup()); 
            // InternalDSL.g:69:3: ( rule__Gamify__Group__0 )
            // InternalDSL.g:69:4: rule__Gamify__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Gamify__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getGamifyAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGamify"


    // $ANTLR start "entryRuleEString"
    // InternalDSL.g:78:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // InternalDSL.g:79:1: ( ruleEString EOF )
            // InternalDSL.g:80:1: ruleEString EOF
            {
             before(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEStringRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalDSL.g:87:1: ruleEString : ( RULE_ID ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:91:2: ( ( RULE_ID ) )
            // InternalDSL.g:92:2: ( RULE_ID )
            {
            // InternalDSL.g:92:2: ( RULE_ID )
            // InternalDSL.g:93:3: RULE_ID
            {
             before(grammarAccess.getEStringAccess().getIDTerminalRuleCall()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getEStringAccess().getIDTerminalRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleFQNString"
    // InternalDSL.g:103:1: entryRuleFQNString : ruleFQNString EOF ;
    public final void entryRuleFQNString() throws RecognitionException {
        try {
            // InternalDSL.g:104:1: ( ruleFQNString EOF )
            // InternalDSL.g:105:1: ruleFQNString EOF
            {
             before(grammarAccess.getFQNStringRule()); 
            pushFollow(FOLLOW_1);
            ruleFQNString();

            state._fsp--;

             after(grammarAccess.getFQNStringRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFQNString"


    // $ANTLR start "ruleFQNString"
    // InternalDSL.g:112:1: ruleFQNString : ( ( rule__FQNString__Group__0 ) ) ;
    public final void ruleFQNString() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:116:2: ( ( ( rule__FQNString__Group__0 ) ) )
            // InternalDSL.g:117:2: ( ( rule__FQNString__Group__0 ) )
            {
            // InternalDSL.g:117:2: ( ( rule__FQNString__Group__0 ) )
            // InternalDSL.g:118:3: ( rule__FQNString__Group__0 )
            {
             before(grammarAccess.getFQNStringAccess().getGroup()); 
            // InternalDSL.g:119:3: ( rule__FQNString__Group__0 )
            // InternalDSL.g:119:4: rule__FQNString__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__FQNString__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFQNStringAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFQNString"


    // $ANTLR start "entryRuleItem"
    // InternalDSL.g:128:1: entryRuleItem : ruleItem EOF ;
    public final void entryRuleItem() throws RecognitionException {
        try {
            // InternalDSL.g:129:1: ( ruleItem EOF )
            // InternalDSL.g:130:1: ruleItem EOF
            {
             before(grammarAccess.getItemRule()); 
            pushFollow(FOLLOW_1);
            ruleItem();

            state._fsp--;

             after(grammarAccess.getItemRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleItem"


    // $ANTLR start "ruleItem"
    // InternalDSL.g:137:1: ruleItem : ( ( rule__Item__Alternatives ) ) ;
    public final void ruleItem() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:141:2: ( ( ( rule__Item__Alternatives ) ) )
            // InternalDSL.g:142:2: ( ( rule__Item__Alternatives ) )
            {
            // InternalDSL.g:142:2: ( ( rule__Item__Alternatives ) )
            // InternalDSL.g:143:3: ( rule__Item__Alternatives )
            {
             before(grammarAccess.getItemAccess().getAlternatives()); 
            // InternalDSL.g:144:3: ( rule__Item__Alternatives )
            // InternalDSL.g:144:4: rule__Item__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Item__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getItemAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleItem"


    // $ANTLR start "entryRuleReward"
    // InternalDSL.g:153:1: entryRuleReward : ruleReward EOF ;
    public final void entryRuleReward() throws RecognitionException {
        try {
            // InternalDSL.g:154:1: ( ruleReward EOF )
            // InternalDSL.g:155:1: ruleReward EOF
            {
             before(grammarAccess.getRewardRule()); 
            pushFollow(FOLLOW_1);
            ruleReward();

            state._fsp--;

             after(grammarAccess.getRewardRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleReward"


    // $ANTLR start "ruleReward"
    // InternalDSL.g:162:1: ruleReward : ( ( rule__Reward__Alternatives ) ) ;
    public final void ruleReward() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:166:2: ( ( ( rule__Reward__Alternatives ) ) )
            // InternalDSL.g:167:2: ( ( rule__Reward__Alternatives ) )
            {
            // InternalDSL.g:167:2: ( ( rule__Reward__Alternatives ) )
            // InternalDSL.g:168:3: ( rule__Reward__Alternatives )
            {
             before(grammarAccess.getRewardAccess().getAlternatives()); 
            // InternalDSL.g:169:3: ( rule__Reward__Alternatives )
            // InternalDSL.g:169:4: rule__Reward__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Reward__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getRewardAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleReward"


    // $ANTLR start "entryRuleSystem"
    // InternalDSL.g:178:1: entryRuleSystem : ruleSystem EOF ;
    public final void entryRuleSystem() throws RecognitionException {
        try {
            // InternalDSL.g:179:1: ( ruleSystem EOF )
            // InternalDSL.g:180:1: ruleSystem EOF
            {
             before(grammarAccess.getSystemRule()); 
            pushFollow(FOLLOW_1);
            ruleSystem();

            state._fsp--;

             after(grammarAccess.getSystemRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSystem"


    // $ANTLR start "ruleSystem"
    // InternalDSL.g:187:1: ruleSystem : ( ( rule__System__Group__0 ) ) ;
    public final void ruleSystem() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:191:2: ( ( ( rule__System__Group__0 ) ) )
            // InternalDSL.g:192:2: ( ( rule__System__Group__0 ) )
            {
            // InternalDSL.g:192:2: ( ( rule__System__Group__0 ) )
            // InternalDSL.g:193:3: ( rule__System__Group__0 )
            {
             before(grammarAccess.getSystemAccess().getGroup()); 
            // InternalDSL.g:194:3: ( rule__System__Group__0 )
            // InternalDSL.g:194:4: rule__System__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__System__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSystemAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSystem"


    // $ANTLR start "entryRuleGameDynamic"
    // InternalDSL.g:203:1: entryRuleGameDynamic : ruleGameDynamic EOF ;
    public final void entryRuleGameDynamic() throws RecognitionException {
        try {
            // InternalDSL.g:204:1: ( ruleGameDynamic EOF )
            // InternalDSL.g:205:1: ruleGameDynamic EOF
            {
             before(grammarAccess.getGameDynamicRule()); 
            pushFollow(FOLLOW_1);
            ruleGameDynamic();

            state._fsp--;

             after(grammarAccess.getGameDynamicRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGameDynamic"


    // $ANTLR start "ruleGameDynamic"
    // InternalDSL.g:212:1: ruleGameDynamic : ( ( rule__GameDynamic__Group__0 ) ) ;
    public final void ruleGameDynamic() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:216:2: ( ( ( rule__GameDynamic__Group__0 ) ) )
            // InternalDSL.g:217:2: ( ( rule__GameDynamic__Group__0 ) )
            {
            // InternalDSL.g:217:2: ( ( rule__GameDynamic__Group__0 ) )
            // InternalDSL.g:218:3: ( rule__GameDynamic__Group__0 )
            {
             before(grammarAccess.getGameDynamicAccess().getGroup()); 
            // InternalDSL.g:219:3: ( rule__GameDynamic__Group__0 )
            // InternalDSL.g:219:4: rule__GameDynamic__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getGameDynamicAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGameDynamic"


    // $ANTLR start "entryRuleGameMechanic"
    // InternalDSL.g:228:1: entryRuleGameMechanic : ruleGameMechanic EOF ;
    public final void entryRuleGameMechanic() throws RecognitionException {
        try {
            // InternalDSL.g:229:1: ( ruleGameMechanic EOF )
            // InternalDSL.g:230:1: ruleGameMechanic EOF
            {
             before(grammarAccess.getGameMechanicRule()); 
            pushFollow(FOLLOW_1);
            ruleGameMechanic();

            state._fsp--;

             after(grammarAccess.getGameMechanicRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGameMechanic"


    // $ANTLR start "ruleGameMechanic"
    // InternalDSL.g:237:1: ruleGameMechanic : ( ( rule__GameMechanic__Group__0 ) ) ;
    public final void ruleGameMechanic() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:241:2: ( ( ( rule__GameMechanic__Group__0 ) ) )
            // InternalDSL.g:242:2: ( ( rule__GameMechanic__Group__0 ) )
            {
            // InternalDSL.g:242:2: ( ( rule__GameMechanic__Group__0 ) )
            // InternalDSL.g:243:3: ( rule__GameMechanic__Group__0 )
            {
             before(grammarAccess.getGameMechanicAccess().getGroup()); 
            // InternalDSL.g:244:3: ( rule__GameMechanic__Group__0 )
            // InternalDSL.g:244:4: rule__GameMechanic__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__GameMechanic__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getGameMechanicAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGameMechanic"


    // $ANTLR start "entryRuleAchievement"
    // InternalDSL.g:253:1: entryRuleAchievement : ruleAchievement EOF ;
    public final void entryRuleAchievement() throws RecognitionException {
        try {
            // InternalDSL.g:254:1: ( ruleAchievement EOF )
            // InternalDSL.g:255:1: ruleAchievement EOF
            {
             before(grammarAccess.getAchievementRule()); 
            pushFollow(FOLLOW_1);
            ruleAchievement();

            state._fsp--;

             after(grammarAccess.getAchievementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAchievement"


    // $ANTLR start "ruleAchievement"
    // InternalDSL.g:262:1: ruleAchievement : ( ( rule__Achievement__Group__0 ) ) ;
    public final void ruleAchievement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:266:2: ( ( ( rule__Achievement__Group__0 ) ) )
            // InternalDSL.g:267:2: ( ( rule__Achievement__Group__0 ) )
            {
            // InternalDSL.g:267:2: ( ( rule__Achievement__Group__0 ) )
            // InternalDSL.g:268:3: ( rule__Achievement__Group__0 )
            {
             before(grammarAccess.getAchievementAccess().getGroup()); 
            // InternalDSL.g:269:3: ( rule__Achievement__Group__0 )
            // InternalDSL.g:269:4: rule__Achievement__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Achievement__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAchievementAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAchievement"


    // $ANTLR start "entryRuleEvent"
    // InternalDSL.g:278:1: entryRuleEvent : ruleEvent EOF ;
    public final void entryRuleEvent() throws RecognitionException {
        try {
            // InternalDSL.g:279:1: ( ruleEvent EOF )
            // InternalDSL.g:280:1: ruleEvent EOF
            {
             before(grammarAccess.getEventRule()); 
            pushFollow(FOLLOW_1);
            ruleEvent();

            state._fsp--;

             after(grammarAccess.getEventRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEvent"


    // $ANTLR start "ruleEvent"
    // InternalDSL.g:287:1: ruleEvent : ( ( rule__Event__Group__0 ) ) ;
    public final void ruleEvent() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:291:2: ( ( ( rule__Event__Group__0 ) ) )
            // InternalDSL.g:292:2: ( ( rule__Event__Group__0 ) )
            {
            // InternalDSL.g:292:2: ( ( rule__Event__Group__0 ) )
            // InternalDSL.g:293:3: ( rule__Event__Group__0 )
            {
             before(grammarAccess.getEventAccess().getGroup()); 
            // InternalDSL.g:294:3: ( rule__Event__Group__0 )
            // InternalDSL.g:294:4: rule__Event__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Event__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEventAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEvent"


    // $ANTLR start "entryRuleCondition"
    // InternalDSL.g:303:1: entryRuleCondition : ruleCondition EOF ;
    public final void entryRuleCondition() throws RecognitionException {
        try {
            // InternalDSL.g:304:1: ( ruleCondition EOF )
            // InternalDSL.g:305:1: ruleCondition EOF
            {
             before(grammarAccess.getConditionRule()); 
            pushFollow(FOLLOW_1);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getConditionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCondition"


    // $ANTLR start "ruleCondition"
    // InternalDSL.g:312:1: ruleCondition : ( ( rule__Condition__Group__0 ) ) ;
    public final void ruleCondition() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:316:2: ( ( ( rule__Condition__Group__0 ) ) )
            // InternalDSL.g:317:2: ( ( rule__Condition__Group__0 ) )
            {
            // InternalDSL.g:317:2: ( ( rule__Condition__Group__0 ) )
            // InternalDSL.g:318:3: ( rule__Condition__Group__0 )
            {
             before(grammarAccess.getConditionAccess().getGroup()); 
            // InternalDSL.g:319:3: ( rule__Condition__Group__0 )
            // InternalDSL.g:319:4: rule__Condition__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Condition__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getConditionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCondition"


    // $ANTLR start "entryRuleRestriction"
    // InternalDSL.g:328:1: entryRuleRestriction : ruleRestriction EOF ;
    public final void entryRuleRestriction() throws RecognitionException {
        try {
            // InternalDSL.g:329:1: ( ruleRestriction EOF )
            // InternalDSL.g:330:1: ruleRestriction EOF
            {
             before(grammarAccess.getRestrictionRule()); 
            pushFollow(FOLLOW_1);
            ruleRestriction();

            state._fsp--;

             after(grammarAccess.getRestrictionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRestriction"


    // $ANTLR start "ruleRestriction"
    // InternalDSL.g:337:1: ruleRestriction : ( ( rule__Restriction__Group__0 ) ) ;
    public final void ruleRestriction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:341:2: ( ( ( rule__Restriction__Group__0 ) ) )
            // InternalDSL.g:342:2: ( ( rule__Restriction__Group__0 ) )
            {
            // InternalDSL.g:342:2: ( ( rule__Restriction__Group__0 ) )
            // InternalDSL.g:343:3: ( rule__Restriction__Group__0 )
            {
             before(grammarAccess.getRestrictionAccess().getGroup()); 
            // InternalDSL.g:344:3: ( rule__Restriction__Group__0 )
            // InternalDSL.g:344:4: rule__Restriction__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Restriction__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRestrictionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRestriction"


    // $ANTLR start "entryRuleBoolean"
    // InternalDSL.g:353:1: entryRuleBoolean : ruleBoolean EOF ;
    public final void entryRuleBoolean() throws RecognitionException {
        try {
            // InternalDSL.g:354:1: ( ruleBoolean EOF )
            // InternalDSL.g:355:1: ruleBoolean EOF
            {
             before(grammarAccess.getBooleanRule()); 
            pushFollow(FOLLOW_1);
            ruleBoolean();

            state._fsp--;

             after(grammarAccess.getBooleanRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBoolean"


    // $ANTLR start "ruleBoolean"
    // InternalDSL.g:362:1: ruleBoolean : ( ( rule__Boolean__Alternatives ) ) ;
    public final void ruleBoolean() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:366:2: ( ( ( rule__Boolean__Alternatives ) ) )
            // InternalDSL.g:367:2: ( ( rule__Boolean__Alternatives ) )
            {
            // InternalDSL.g:367:2: ( ( rule__Boolean__Alternatives ) )
            // InternalDSL.g:368:3: ( rule__Boolean__Alternatives )
            {
             before(grammarAccess.getBooleanAccess().getAlternatives()); 
            // InternalDSL.g:369:3: ( rule__Boolean__Alternatives )
            // InternalDSL.g:369:4: rule__Boolean__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Boolean__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getBooleanAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBoolean"


    // $ANTLR start "entryRuleRandomReward"
    // InternalDSL.g:378:1: entryRuleRandomReward : ruleRandomReward EOF ;
    public final void entryRuleRandomReward() throws RecognitionException {
        try {
            // InternalDSL.g:379:1: ( ruleRandomReward EOF )
            // InternalDSL.g:380:1: ruleRandomReward EOF
            {
             before(grammarAccess.getRandomRewardRule()); 
            pushFollow(FOLLOW_1);
            ruleRandomReward();

            state._fsp--;

             after(grammarAccess.getRandomRewardRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRandomReward"


    // $ANTLR start "ruleRandomReward"
    // InternalDSL.g:387:1: ruleRandomReward : ( ( rule__RandomReward__Group__0 ) ) ;
    public final void ruleRandomReward() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:391:2: ( ( ( rule__RandomReward__Group__0 ) ) )
            // InternalDSL.g:392:2: ( ( rule__RandomReward__Group__0 ) )
            {
            // InternalDSL.g:392:2: ( ( rule__RandomReward__Group__0 ) )
            // InternalDSL.g:393:3: ( rule__RandomReward__Group__0 )
            {
             before(grammarAccess.getRandomRewardAccess().getGroup()); 
            // InternalDSL.g:394:3: ( rule__RandomReward__Group__0 )
            // InternalDSL.g:394:4: rule__RandomReward__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__RandomReward__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRandomRewardAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRandomReward"


    // $ANTLR start "entryRuleFixedReward"
    // InternalDSL.g:403:1: entryRuleFixedReward : ruleFixedReward EOF ;
    public final void entryRuleFixedReward() throws RecognitionException {
        try {
            // InternalDSL.g:404:1: ( ruleFixedReward EOF )
            // InternalDSL.g:405:1: ruleFixedReward EOF
            {
             before(grammarAccess.getFixedRewardRule()); 
            pushFollow(FOLLOW_1);
            ruleFixedReward();

            state._fsp--;

             after(grammarAccess.getFixedRewardRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFixedReward"


    // $ANTLR start "ruleFixedReward"
    // InternalDSL.g:412:1: ruleFixedReward : ( ( rule__FixedReward__Group__0 ) ) ;
    public final void ruleFixedReward() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:416:2: ( ( ( rule__FixedReward__Group__0 ) ) )
            // InternalDSL.g:417:2: ( ( rule__FixedReward__Group__0 ) )
            {
            // InternalDSL.g:417:2: ( ( rule__FixedReward__Group__0 ) )
            // InternalDSL.g:418:3: ( rule__FixedReward__Group__0 )
            {
             before(grammarAccess.getFixedRewardAccess().getGroup()); 
            // InternalDSL.g:419:3: ( rule__FixedReward__Group__0 )
            // InternalDSL.g:419:4: rule__FixedReward__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__FixedReward__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFixedRewardAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFixedReward"


    // $ANTLR start "entryRuleBadge"
    // InternalDSL.g:428:1: entryRuleBadge : ruleBadge EOF ;
    public final void entryRuleBadge() throws RecognitionException {
        try {
            // InternalDSL.g:429:1: ( ruleBadge EOF )
            // InternalDSL.g:430:1: ruleBadge EOF
            {
             before(grammarAccess.getBadgeRule()); 
            pushFollow(FOLLOW_1);
            ruleBadge();

            state._fsp--;

             after(grammarAccess.getBadgeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBadge"


    // $ANTLR start "ruleBadge"
    // InternalDSL.g:437:1: ruleBadge : ( ( rule__Badge__Group__0 ) ) ;
    public final void ruleBadge() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:441:2: ( ( ( rule__Badge__Group__0 ) ) )
            // InternalDSL.g:442:2: ( ( rule__Badge__Group__0 ) )
            {
            // InternalDSL.g:442:2: ( ( rule__Badge__Group__0 ) )
            // InternalDSL.g:443:3: ( rule__Badge__Group__0 )
            {
             before(grammarAccess.getBadgeAccess().getGroup()); 
            // InternalDSL.g:444:3: ( rule__Badge__Group__0 )
            // InternalDSL.g:444:4: rule__Badge__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Badge__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBadgeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBadge"


    // $ANTLR start "entryRulePoints"
    // InternalDSL.g:453:1: entryRulePoints : rulePoints EOF ;
    public final void entryRulePoints() throws RecognitionException {
        try {
            // InternalDSL.g:454:1: ( rulePoints EOF )
            // InternalDSL.g:455:1: rulePoints EOF
            {
             before(grammarAccess.getPointsRule()); 
            pushFollow(FOLLOW_1);
            rulePoints();

            state._fsp--;

             after(grammarAccess.getPointsRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePoints"


    // $ANTLR start "rulePoints"
    // InternalDSL.g:462:1: rulePoints : ( ( rule__Points__Group__0 ) ) ;
    public final void rulePoints() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:466:2: ( ( ( rule__Points__Group__0 ) ) )
            // InternalDSL.g:467:2: ( ( rule__Points__Group__0 ) )
            {
            // InternalDSL.g:467:2: ( ( rule__Points__Group__0 ) )
            // InternalDSL.g:468:3: ( rule__Points__Group__0 )
            {
             before(grammarAccess.getPointsAccess().getGroup()); 
            // InternalDSL.g:469:3: ( rule__Points__Group__0 )
            // InternalDSL.g:469:4: rule__Points__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Points__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPointsAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePoints"


    // $ANTLR start "entryRulePrize"
    // InternalDSL.g:478:1: entryRulePrize : rulePrize EOF ;
    public final void entryRulePrize() throws RecognitionException {
        try {
            // InternalDSL.g:479:1: ( rulePrize EOF )
            // InternalDSL.g:480:1: rulePrize EOF
            {
             before(grammarAccess.getPrizeRule()); 
            pushFollow(FOLLOW_1);
            rulePrize();

            state._fsp--;

             after(grammarAccess.getPrizeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrize"


    // $ANTLR start "rulePrize"
    // InternalDSL.g:487:1: rulePrize : ( ( rule__Prize__Group__0 ) ) ;
    public final void rulePrize() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:491:2: ( ( ( rule__Prize__Group__0 ) ) )
            // InternalDSL.g:492:2: ( ( rule__Prize__Group__0 ) )
            {
            // InternalDSL.g:492:2: ( ( rule__Prize__Group__0 ) )
            // InternalDSL.g:493:3: ( rule__Prize__Group__0 )
            {
             before(grammarAccess.getPrizeAccess().getGroup()); 
            // InternalDSL.g:494:3: ( rule__Prize__Group__0 )
            // InternalDSL.g:494:4: rule__Prize__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Prize__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPrizeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrize"


    // $ANTLR start "ruleSystemAttributes"
    // InternalDSL.g:503:1: ruleSystemAttributes : ( ( rule__SystemAttributes__Alternatives ) ) ;
    public final void ruleSystemAttributes() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:507:1: ( ( ( rule__SystemAttributes__Alternatives ) ) )
            // InternalDSL.g:508:2: ( ( rule__SystemAttributes__Alternatives ) )
            {
            // InternalDSL.g:508:2: ( ( rule__SystemAttributes__Alternatives ) )
            // InternalDSL.g:509:3: ( rule__SystemAttributes__Alternatives )
            {
             before(grammarAccess.getSystemAttributesAccess().getAlternatives()); 
            // InternalDSL.g:510:3: ( rule__SystemAttributes__Alternatives )
            // InternalDSL.g:510:4: rule__SystemAttributes__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__SystemAttributes__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getSystemAttributesAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSystemAttributes"


    // $ANTLR start "ruleUserTypes"
    // InternalDSL.g:519:1: ruleUserTypes : ( ( rule__UserTypes__Alternatives ) ) ;
    public final void ruleUserTypes() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:523:1: ( ( ( rule__UserTypes__Alternatives ) ) )
            // InternalDSL.g:524:2: ( ( rule__UserTypes__Alternatives ) )
            {
            // InternalDSL.g:524:2: ( ( rule__UserTypes__Alternatives ) )
            // InternalDSL.g:525:3: ( rule__UserTypes__Alternatives )
            {
             before(grammarAccess.getUserTypesAccess().getAlternatives()); 
            // InternalDSL.g:526:3: ( rule__UserTypes__Alternatives )
            // InternalDSL.g:526:4: rule__UserTypes__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__UserTypes__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getUserTypesAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUserTypes"


    // $ANTLR start "ruleUserBaseAttributes"
    // InternalDSL.g:535:1: ruleUserBaseAttributes : ( ( rule__UserBaseAttributes__Alternatives ) ) ;
    public final void ruleUserBaseAttributes() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:539:1: ( ( ( rule__UserBaseAttributes__Alternatives ) ) )
            // InternalDSL.g:540:2: ( ( rule__UserBaseAttributes__Alternatives ) )
            {
            // InternalDSL.g:540:2: ( ( rule__UserBaseAttributes__Alternatives ) )
            // InternalDSL.g:541:3: ( rule__UserBaseAttributes__Alternatives )
            {
             before(grammarAccess.getUserBaseAttributesAccess().getAlternatives()); 
            // InternalDSL.g:542:3: ( rule__UserBaseAttributes__Alternatives )
            // InternalDSL.g:542:4: rule__UserBaseAttributes__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__UserBaseAttributes__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getUserBaseAttributesAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUserBaseAttributes"


    // $ANTLR start "ruleDynamicTypes"
    // InternalDSL.g:551:1: ruleDynamicTypes : ( ( rule__DynamicTypes__Alternatives ) ) ;
    public final void ruleDynamicTypes() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:555:1: ( ( ( rule__DynamicTypes__Alternatives ) ) )
            // InternalDSL.g:556:2: ( ( rule__DynamicTypes__Alternatives ) )
            {
            // InternalDSL.g:556:2: ( ( rule__DynamicTypes__Alternatives ) )
            // InternalDSL.g:557:3: ( rule__DynamicTypes__Alternatives )
            {
             before(grammarAccess.getDynamicTypesAccess().getAlternatives()); 
            // InternalDSL.g:558:3: ( rule__DynamicTypes__Alternatives )
            // InternalDSL.g:558:4: rule__DynamicTypes__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__DynamicTypes__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getDynamicTypesAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDynamicTypes"


    // $ANTLR start "ruleGameMechanicTypes"
    // InternalDSL.g:567:1: ruleGameMechanicTypes : ( ( rule__GameMechanicTypes__Alternatives ) ) ;
    public final void ruleGameMechanicTypes() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:571:1: ( ( ( rule__GameMechanicTypes__Alternatives ) ) )
            // InternalDSL.g:572:2: ( ( rule__GameMechanicTypes__Alternatives ) )
            {
            // InternalDSL.g:572:2: ( ( rule__GameMechanicTypes__Alternatives ) )
            // InternalDSL.g:573:3: ( rule__GameMechanicTypes__Alternatives )
            {
             before(grammarAccess.getGameMechanicTypesAccess().getAlternatives()); 
            // InternalDSL.g:574:3: ( rule__GameMechanicTypes__Alternatives )
            // InternalDSL.g:574:4: rule__GameMechanicTypes__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__GameMechanicTypes__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getGameMechanicTypesAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGameMechanicTypes"


    // $ANTLR start "ruleEventTypes"
    // InternalDSL.g:583:1: ruleEventTypes : ( ( rule__EventTypes__Alternatives ) ) ;
    public final void ruleEventTypes() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:587:1: ( ( ( rule__EventTypes__Alternatives ) ) )
            // InternalDSL.g:588:2: ( ( rule__EventTypes__Alternatives ) )
            {
            // InternalDSL.g:588:2: ( ( rule__EventTypes__Alternatives ) )
            // InternalDSL.g:589:3: ( rule__EventTypes__Alternatives )
            {
             before(grammarAccess.getEventTypesAccess().getAlternatives()); 
            // InternalDSL.g:590:3: ( rule__EventTypes__Alternatives )
            // InternalDSL.g:590:4: rule__EventTypes__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__EventTypes__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEventTypesAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEventTypes"


    // $ANTLR start "ruleConditionTypes"
    // InternalDSL.g:599:1: ruleConditionTypes : ( ( rule__ConditionTypes__Alternatives ) ) ;
    public final void ruleConditionTypes() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:603:1: ( ( ( rule__ConditionTypes__Alternatives ) ) )
            // InternalDSL.g:604:2: ( ( rule__ConditionTypes__Alternatives ) )
            {
            // InternalDSL.g:604:2: ( ( rule__ConditionTypes__Alternatives ) )
            // InternalDSL.g:605:3: ( rule__ConditionTypes__Alternatives )
            {
             before(grammarAccess.getConditionTypesAccess().getAlternatives()); 
            // InternalDSL.g:606:3: ( rule__ConditionTypes__Alternatives )
            // InternalDSL.g:606:4: rule__ConditionTypes__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__ConditionTypes__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getConditionTypesAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConditionTypes"


    // $ANTLR start "ruleRestrictionTypes"
    // InternalDSL.g:615:1: ruleRestrictionTypes : ( ( rule__RestrictionTypes__Alternatives ) ) ;
    public final void ruleRestrictionTypes() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:619:1: ( ( ( rule__RestrictionTypes__Alternatives ) ) )
            // InternalDSL.g:620:2: ( ( rule__RestrictionTypes__Alternatives ) )
            {
            // InternalDSL.g:620:2: ( ( rule__RestrictionTypes__Alternatives ) )
            // InternalDSL.g:621:3: ( rule__RestrictionTypes__Alternatives )
            {
             before(grammarAccess.getRestrictionTypesAccess().getAlternatives()); 
            // InternalDSL.g:622:3: ( rule__RestrictionTypes__Alternatives )
            // InternalDSL.g:622:4: rule__RestrictionTypes__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__RestrictionTypes__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getRestrictionTypesAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRestrictionTypes"


    // $ANTLR start "rulePrizeTypes"
    // InternalDSL.g:631:1: rulePrizeTypes : ( ( rule__PrizeTypes__Alternatives ) ) ;
    public final void rulePrizeTypes() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:635:1: ( ( ( rule__PrizeTypes__Alternatives ) ) )
            // InternalDSL.g:636:2: ( ( rule__PrizeTypes__Alternatives ) )
            {
            // InternalDSL.g:636:2: ( ( rule__PrizeTypes__Alternatives ) )
            // InternalDSL.g:637:3: ( rule__PrizeTypes__Alternatives )
            {
             before(grammarAccess.getPrizeTypesAccess().getAlternatives()); 
            // InternalDSL.g:638:3: ( rule__PrizeTypes__Alternatives )
            // InternalDSL.g:638:4: rule__PrizeTypes__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__PrizeTypes__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPrizeTypesAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrizeTypes"


    // $ANTLR start "rule__Item__Alternatives"
    // InternalDSL.g:646:1: rule__Item__Alternatives : ( ( ruleBadge ) | ( rulePoints ) | ( rulePrize ) );
    public final void rule__Item__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:650:1: ( ( ruleBadge ) | ( rulePoints ) | ( rulePrize ) )
            int alt1=3;
            switch ( input.LA(1) ) {
            case 102:
                {
                alt1=1;
                }
                break;
            case 106:
                {
                alt1=2;
                }
                break;
            case 108:
                {
                alt1=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalDSL.g:651:2: ( ruleBadge )
                    {
                    // InternalDSL.g:651:2: ( ruleBadge )
                    // InternalDSL.g:652:3: ruleBadge
                    {
                     before(grammarAccess.getItemAccess().getBadgeParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleBadge();

                    state._fsp--;

                     after(grammarAccess.getItemAccess().getBadgeParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDSL.g:657:2: ( rulePoints )
                    {
                    // InternalDSL.g:657:2: ( rulePoints )
                    // InternalDSL.g:658:3: rulePoints
                    {
                     before(grammarAccess.getItemAccess().getPointsParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    rulePoints();

                    state._fsp--;

                     after(grammarAccess.getItemAccess().getPointsParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalDSL.g:663:2: ( rulePrize )
                    {
                    // InternalDSL.g:663:2: ( rulePrize )
                    // InternalDSL.g:664:3: rulePrize
                    {
                     before(grammarAccess.getItemAccess().getPrizeParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    rulePrize();

                    state._fsp--;

                     after(grammarAccess.getItemAccess().getPrizeParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Item__Alternatives"


    // $ANTLR start "rule__Reward__Alternatives"
    // InternalDSL.g:673:1: rule__Reward__Alternatives : ( ( ruleRandomReward ) | ( ruleFixedReward ) );
    public final void rule__Reward__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:677:1: ( ( ruleRandomReward ) | ( ruleFixedReward ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==97) ) {
                alt2=1;
            }
            else if ( (LA2_0==101) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalDSL.g:678:2: ( ruleRandomReward )
                    {
                    // InternalDSL.g:678:2: ( ruleRandomReward )
                    // InternalDSL.g:679:3: ruleRandomReward
                    {
                     before(grammarAccess.getRewardAccess().getRandomRewardParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleRandomReward();

                    state._fsp--;

                     after(grammarAccess.getRewardAccess().getRandomRewardParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDSL.g:684:2: ( ruleFixedReward )
                    {
                    // InternalDSL.g:684:2: ( ruleFixedReward )
                    // InternalDSL.g:685:3: ruleFixedReward
                    {
                     before(grammarAccess.getRewardAccess().getFixedRewardParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleFixedReward();

                    state._fsp--;

                     after(grammarAccess.getRewardAccess().getFixedRewardParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reward__Alternatives"


    // $ANTLR start "rule__Boolean__Alternatives"
    // InternalDSL.g:694:1: rule__Boolean__Alternatives : ( ( 'true' ) | ( 'false' ) );
    public final void rule__Boolean__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:698:1: ( ( 'true' ) | ( 'false' ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==12) ) {
                alt3=1;
            }
            else if ( (LA3_0==13) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalDSL.g:699:2: ( 'true' )
                    {
                    // InternalDSL.g:699:2: ( 'true' )
                    // InternalDSL.g:700:3: 'true'
                    {
                     before(grammarAccess.getBooleanAccess().getTrueKeyword_0()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getBooleanAccess().getTrueKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDSL.g:705:2: ( 'false' )
                    {
                    // InternalDSL.g:705:2: ( 'false' )
                    // InternalDSL.g:706:3: 'false'
                    {
                     before(grammarAccess.getBooleanAccess().getFalseKeyword_1()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getBooleanAccess().getFalseKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Boolean__Alternatives"


    // $ANTLR start "rule__SystemAttributes__Alternatives"
    // InternalDSL.g:715:1: rule__SystemAttributes__Alternatives : ( ( ( 'ESTABLISHED' ) ) | ( ( 'GROWING' ) ) | ( ( 'NEW' ) ) );
    public final void rule__SystemAttributes__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:719:1: ( ( ( 'ESTABLISHED' ) ) | ( ( 'GROWING' ) ) | ( ( 'NEW' ) ) )
            int alt4=3;
            switch ( input.LA(1) ) {
            case 14:
                {
                alt4=1;
                }
                break;
            case 15:
                {
                alt4=2;
                }
                break;
            case 16:
                {
                alt4=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalDSL.g:720:2: ( ( 'ESTABLISHED' ) )
                    {
                    // InternalDSL.g:720:2: ( ( 'ESTABLISHED' ) )
                    // InternalDSL.g:721:3: ( 'ESTABLISHED' )
                    {
                     before(grammarAccess.getSystemAttributesAccess().getESTABLISHEDEnumLiteralDeclaration_0()); 
                    // InternalDSL.g:722:3: ( 'ESTABLISHED' )
                    // InternalDSL.g:722:4: 'ESTABLISHED'
                    {
                    match(input,14,FOLLOW_2); 

                    }

                     after(grammarAccess.getSystemAttributesAccess().getESTABLISHEDEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDSL.g:726:2: ( ( 'GROWING' ) )
                    {
                    // InternalDSL.g:726:2: ( ( 'GROWING' ) )
                    // InternalDSL.g:727:3: ( 'GROWING' )
                    {
                     before(grammarAccess.getSystemAttributesAccess().getGROWINGEnumLiteralDeclaration_1()); 
                    // InternalDSL.g:728:3: ( 'GROWING' )
                    // InternalDSL.g:728:4: 'GROWING'
                    {
                    match(input,15,FOLLOW_2); 

                    }

                     after(grammarAccess.getSystemAttributesAccess().getGROWINGEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalDSL.g:732:2: ( ( 'NEW' ) )
                    {
                    // InternalDSL.g:732:2: ( ( 'NEW' ) )
                    // InternalDSL.g:733:3: ( 'NEW' )
                    {
                     before(grammarAccess.getSystemAttributesAccess().getNEWEnumLiteralDeclaration_2()); 
                    // InternalDSL.g:734:3: ( 'NEW' )
                    // InternalDSL.g:734:4: 'NEW'
                    {
                    match(input,16,FOLLOW_2); 

                    }

                     after(grammarAccess.getSystemAttributesAccess().getNEWEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SystemAttributes__Alternatives"


    // $ANTLR start "rule__UserTypes__Alternatives"
    // InternalDSL.g:742:1: rule__UserTypes__Alternatives : ( ( ( 'EMPLOYEE' ) ) | ( ( 'CUSTOMER' ) ) );
    public final void rule__UserTypes__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:746:1: ( ( ( 'EMPLOYEE' ) ) | ( ( 'CUSTOMER' ) ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==17) ) {
                alt5=1;
            }
            else if ( (LA5_0==18) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalDSL.g:747:2: ( ( 'EMPLOYEE' ) )
                    {
                    // InternalDSL.g:747:2: ( ( 'EMPLOYEE' ) )
                    // InternalDSL.g:748:3: ( 'EMPLOYEE' )
                    {
                     before(grammarAccess.getUserTypesAccess().getEMPLOYEEEnumLiteralDeclaration_0()); 
                    // InternalDSL.g:749:3: ( 'EMPLOYEE' )
                    // InternalDSL.g:749:4: 'EMPLOYEE'
                    {
                    match(input,17,FOLLOW_2); 

                    }

                     after(grammarAccess.getUserTypesAccess().getEMPLOYEEEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDSL.g:753:2: ( ( 'CUSTOMER' ) )
                    {
                    // InternalDSL.g:753:2: ( ( 'CUSTOMER' ) )
                    // InternalDSL.g:754:3: ( 'CUSTOMER' )
                    {
                     before(grammarAccess.getUserTypesAccess().getCUSTOMEREnumLiteralDeclaration_1()); 
                    // InternalDSL.g:755:3: ( 'CUSTOMER' )
                    // InternalDSL.g:755:4: 'CUSTOMER'
                    {
                    match(input,18,FOLLOW_2); 

                    }

                     after(grammarAccess.getUserTypesAccess().getCUSTOMEREnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserTypes__Alternatives"


    // $ANTLR start "rule__UserBaseAttributes__Alternatives"
    // InternalDSL.g:763:1: rule__UserBaseAttributes__Alternatives : ( ( ( 'KILLERS' ) ) | ( ( 'SOCIALIZERS' ) ) | ( ( 'EXPLORERS' ) ) | ( ( 'ACHIEVERS' ) ) );
    public final void rule__UserBaseAttributes__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:767:1: ( ( ( 'KILLERS' ) ) | ( ( 'SOCIALIZERS' ) ) | ( ( 'EXPLORERS' ) ) | ( ( 'ACHIEVERS' ) ) )
            int alt6=4;
            switch ( input.LA(1) ) {
            case 19:
                {
                alt6=1;
                }
                break;
            case 20:
                {
                alt6=2;
                }
                break;
            case 21:
                {
                alt6=3;
                }
                break;
            case 22:
                {
                alt6=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalDSL.g:768:2: ( ( 'KILLERS' ) )
                    {
                    // InternalDSL.g:768:2: ( ( 'KILLERS' ) )
                    // InternalDSL.g:769:3: ( 'KILLERS' )
                    {
                     before(grammarAccess.getUserBaseAttributesAccess().getKILLERSEnumLiteralDeclaration_0()); 
                    // InternalDSL.g:770:3: ( 'KILLERS' )
                    // InternalDSL.g:770:4: 'KILLERS'
                    {
                    match(input,19,FOLLOW_2); 

                    }

                     after(grammarAccess.getUserBaseAttributesAccess().getKILLERSEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDSL.g:774:2: ( ( 'SOCIALIZERS' ) )
                    {
                    // InternalDSL.g:774:2: ( ( 'SOCIALIZERS' ) )
                    // InternalDSL.g:775:3: ( 'SOCIALIZERS' )
                    {
                     before(grammarAccess.getUserBaseAttributesAccess().getSOCIALIZERSEnumLiteralDeclaration_1()); 
                    // InternalDSL.g:776:3: ( 'SOCIALIZERS' )
                    // InternalDSL.g:776:4: 'SOCIALIZERS'
                    {
                    match(input,20,FOLLOW_2); 

                    }

                     after(grammarAccess.getUserBaseAttributesAccess().getSOCIALIZERSEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalDSL.g:780:2: ( ( 'EXPLORERS' ) )
                    {
                    // InternalDSL.g:780:2: ( ( 'EXPLORERS' ) )
                    // InternalDSL.g:781:3: ( 'EXPLORERS' )
                    {
                     before(grammarAccess.getUserBaseAttributesAccess().getEXPLORERSEnumLiteralDeclaration_2()); 
                    // InternalDSL.g:782:3: ( 'EXPLORERS' )
                    // InternalDSL.g:782:4: 'EXPLORERS'
                    {
                    match(input,21,FOLLOW_2); 

                    }

                     after(grammarAccess.getUserBaseAttributesAccess().getEXPLORERSEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalDSL.g:786:2: ( ( 'ACHIEVERS' ) )
                    {
                    // InternalDSL.g:786:2: ( ( 'ACHIEVERS' ) )
                    // InternalDSL.g:787:3: ( 'ACHIEVERS' )
                    {
                     before(grammarAccess.getUserBaseAttributesAccess().getACHIEVERSEnumLiteralDeclaration_3()); 
                    // InternalDSL.g:788:3: ( 'ACHIEVERS' )
                    // InternalDSL.g:788:4: 'ACHIEVERS'
                    {
                    match(input,22,FOLLOW_2); 

                    }

                     after(grammarAccess.getUserBaseAttributesAccess().getACHIEVERSEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UserBaseAttributes__Alternatives"


    // $ANTLR start "rule__DynamicTypes__Alternatives"
    // InternalDSL.g:796:1: rule__DynamicTypes__Alternatives : ( ( ( 'TAKEHOLD' ) ) | ( ( 'CUSTOM' ) ) | ( ( 'TREASUREHUNT' ) ) | ( ( 'REVIEW' ) ) | ( ( 'BET' ) ) | ( ( 'PRIZESHOP' ) ) | ( ( 'LOTTERY' ) ) | ( ( 'PROFILE' ) ) );
    public final void rule__DynamicTypes__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:800:1: ( ( ( 'TAKEHOLD' ) ) | ( ( 'CUSTOM' ) ) | ( ( 'TREASUREHUNT' ) ) | ( ( 'REVIEW' ) ) | ( ( 'BET' ) ) | ( ( 'PRIZESHOP' ) ) | ( ( 'LOTTERY' ) ) | ( ( 'PROFILE' ) ) )
            int alt7=8;
            switch ( input.LA(1) ) {
            case 23:
                {
                alt7=1;
                }
                break;
            case 24:
                {
                alt7=2;
                }
                break;
            case 25:
                {
                alt7=3;
                }
                break;
            case 26:
                {
                alt7=4;
                }
                break;
            case 27:
                {
                alt7=5;
                }
                break;
            case 28:
                {
                alt7=6;
                }
                break;
            case 29:
                {
                alt7=7;
                }
                break;
            case 30:
                {
                alt7=8;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }

            switch (alt7) {
                case 1 :
                    // InternalDSL.g:801:2: ( ( 'TAKEHOLD' ) )
                    {
                    // InternalDSL.g:801:2: ( ( 'TAKEHOLD' ) )
                    // InternalDSL.g:802:3: ( 'TAKEHOLD' )
                    {
                     before(grammarAccess.getDynamicTypesAccess().getTAKEHOLDEnumLiteralDeclaration_0()); 
                    // InternalDSL.g:803:3: ( 'TAKEHOLD' )
                    // InternalDSL.g:803:4: 'TAKEHOLD'
                    {
                    match(input,23,FOLLOW_2); 

                    }

                     after(grammarAccess.getDynamicTypesAccess().getTAKEHOLDEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDSL.g:807:2: ( ( 'CUSTOM' ) )
                    {
                    // InternalDSL.g:807:2: ( ( 'CUSTOM' ) )
                    // InternalDSL.g:808:3: ( 'CUSTOM' )
                    {
                     before(grammarAccess.getDynamicTypesAccess().getCUSTOMEnumLiteralDeclaration_1()); 
                    // InternalDSL.g:809:3: ( 'CUSTOM' )
                    // InternalDSL.g:809:4: 'CUSTOM'
                    {
                    match(input,24,FOLLOW_2); 

                    }

                     after(grammarAccess.getDynamicTypesAccess().getCUSTOMEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalDSL.g:813:2: ( ( 'TREASUREHUNT' ) )
                    {
                    // InternalDSL.g:813:2: ( ( 'TREASUREHUNT' ) )
                    // InternalDSL.g:814:3: ( 'TREASUREHUNT' )
                    {
                     before(grammarAccess.getDynamicTypesAccess().getTREASUREHUNTEnumLiteralDeclaration_2()); 
                    // InternalDSL.g:815:3: ( 'TREASUREHUNT' )
                    // InternalDSL.g:815:4: 'TREASUREHUNT'
                    {
                    match(input,25,FOLLOW_2); 

                    }

                     after(grammarAccess.getDynamicTypesAccess().getTREASUREHUNTEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalDSL.g:819:2: ( ( 'REVIEW' ) )
                    {
                    // InternalDSL.g:819:2: ( ( 'REVIEW' ) )
                    // InternalDSL.g:820:3: ( 'REVIEW' )
                    {
                     before(grammarAccess.getDynamicTypesAccess().getREVIEWEnumLiteralDeclaration_3()); 
                    // InternalDSL.g:821:3: ( 'REVIEW' )
                    // InternalDSL.g:821:4: 'REVIEW'
                    {
                    match(input,26,FOLLOW_2); 

                    }

                     after(grammarAccess.getDynamicTypesAccess().getREVIEWEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalDSL.g:825:2: ( ( 'BET' ) )
                    {
                    // InternalDSL.g:825:2: ( ( 'BET' ) )
                    // InternalDSL.g:826:3: ( 'BET' )
                    {
                     before(grammarAccess.getDynamicTypesAccess().getBETEnumLiteralDeclaration_4()); 
                    // InternalDSL.g:827:3: ( 'BET' )
                    // InternalDSL.g:827:4: 'BET'
                    {
                    match(input,27,FOLLOW_2); 

                    }

                     after(grammarAccess.getDynamicTypesAccess().getBETEnumLiteralDeclaration_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalDSL.g:831:2: ( ( 'PRIZESHOP' ) )
                    {
                    // InternalDSL.g:831:2: ( ( 'PRIZESHOP' ) )
                    // InternalDSL.g:832:3: ( 'PRIZESHOP' )
                    {
                     before(grammarAccess.getDynamicTypesAccess().getPRIZESHOPEnumLiteralDeclaration_5()); 
                    // InternalDSL.g:833:3: ( 'PRIZESHOP' )
                    // InternalDSL.g:833:4: 'PRIZESHOP'
                    {
                    match(input,28,FOLLOW_2); 

                    }

                     after(grammarAccess.getDynamicTypesAccess().getPRIZESHOPEnumLiteralDeclaration_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalDSL.g:837:2: ( ( 'LOTTERY' ) )
                    {
                    // InternalDSL.g:837:2: ( ( 'LOTTERY' ) )
                    // InternalDSL.g:838:3: ( 'LOTTERY' )
                    {
                     before(grammarAccess.getDynamicTypesAccess().getLOTTERYEnumLiteralDeclaration_6()); 
                    // InternalDSL.g:839:3: ( 'LOTTERY' )
                    // InternalDSL.g:839:4: 'LOTTERY'
                    {
                    match(input,29,FOLLOW_2); 

                    }

                     after(grammarAccess.getDynamicTypesAccess().getLOTTERYEnumLiteralDeclaration_6()); 

                    }


                    }
                    break;
                case 8 :
                    // InternalDSL.g:843:2: ( ( 'PROFILE' ) )
                    {
                    // InternalDSL.g:843:2: ( ( 'PROFILE' ) )
                    // InternalDSL.g:844:3: ( 'PROFILE' )
                    {
                     before(grammarAccess.getDynamicTypesAccess().getPROFILEEnumLiteralDeclaration_7()); 
                    // InternalDSL.g:845:3: ( 'PROFILE' )
                    // InternalDSL.g:845:4: 'PROFILE'
                    {
                    match(input,30,FOLLOW_2); 

                    }

                     after(grammarAccess.getDynamicTypesAccess().getPROFILEEnumLiteralDeclaration_7()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DynamicTypes__Alternatives"


    // $ANTLR start "rule__GameMechanicTypes__Alternatives"
    // InternalDSL.g:853:1: rule__GameMechanicTypes__Alternatives : ( ( ( 'LEADERBOARD' ) ) | ( ( 'QUEST' ) ) | ( ( 'PROGRESSBAR' ) ) | ( ( 'COOPERATION' ) ) | ( ( 'OPPONENT' ) ) | ( ( 'COLLECTIBLE' ) ) | ( ( 'INVENTORY' ) ) | ( ( 'SOCIALGIFT' ) ) | ( ( 'AVATAR' ) ) );
    public final void rule__GameMechanicTypes__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:857:1: ( ( ( 'LEADERBOARD' ) ) | ( ( 'QUEST' ) ) | ( ( 'PROGRESSBAR' ) ) | ( ( 'COOPERATION' ) ) | ( ( 'OPPONENT' ) ) | ( ( 'COLLECTIBLE' ) ) | ( ( 'INVENTORY' ) ) | ( ( 'SOCIALGIFT' ) ) | ( ( 'AVATAR' ) ) )
            int alt8=9;
            switch ( input.LA(1) ) {
            case 31:
                {
                alt8=1;
                }
                break;
            case 32:
                {
                alt8=2;
                }
                break;
            case 33:
                {
                alt8=3;
                }
                break;
            case 34:
                {
                alt8=4;
                }
                break;
            case 35:
                {
                alt8=5;
                }
                break;
            case 36:
                {
                alt8=6;
                }
                break;
            case 37:
                {
                alt8=7;
                }
                break;
            case 38:
                {
                alt8=8;
                }
                break;
            case 39:
                {
                alt8=9;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // InternalDSL.g:858:2: ( ( 'LEADERBOARD' ) )
                    {
                    // InternalDSL.g:858:2: ( ( 'LEADERBOARD' ) )
                    // InternalDSL.g:859:3: ( 'LEADERBOARD' )
                    {
                     before(grammarAccess.getGameMechanicTypesAccess().getLEADERBOARDEnumLiteralDeclaration_0()); 
                    // InternalDSL.g:860:3: ( 'LEADERBOARD' )
                    // InternalDSL.g:860:4: 'LEADERBOARD'
                    {
                    match(input,31,FOLLOW_2); 

                    }

                     after(grammarAccess.getGameMechanicTypesAccess().getLEADERBOARDEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDSL.g:864:2: ( ( 'QUEST' ) )
                    {
                    // InternalDSL.g:864:2: ( ( 'QUEST' ) )
                    // InternalDSL.g:865:3: ( 'QUEST' )
                    {
                     before(grammarAccess.getGameMechanicTypesAccess().getQUESTEnumLiteralDeclaration_1()); 
                    // InternalDSL.g:866:3: ( 'QUEST' )
                    // InternalDSL.g:866:4: 'QUEST'
                    {
                    match(input,32,FOLLOW_2); 

                    }

                     after(grammarAccess.getGameMechanicTypesAccess().getQUESTEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalDSL.g:870:2: ( ( 'PROGRESSBAR' ) )
                    {
                    // InternalDSL.g:870:2: ( ( 'PROGRESSBAR' ) )
                    // InternalDSL.g:871:3: ( 'PROGRESSBAR' )
                    {
                     before(grammarAccess.getGameMechanicTypesAccess().getPROGRESSBAREnumLiteralDeclaration_2()); 
                    // InternalDSL.g:872:3: ( 'PROGRESSBAR' )
                    // InternalDSL.g:872:4: 'PROGRESSBAR'
                    {
                    match(input,33,FOLLOW_2); 

                    }

                     after(grammarAccess.getGameMechanicTypesAccess().getPROGRESSBAREnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalDSL.g:876:2: ( ( 'COOPERATION' ) )
                    {
                    // InternalDSL.g:876:2: ( ( 'COOPERATION' ) )
                    // InternalDSL.g:877:3: ( 'COOPERATION' )
                    {
                     before(grammarAccess.getGameMechanicTypesAccess().getCOOPERATIONEnumLiteralDeclaration_3()); 
                    // InternalDSL.g:878:3: ( 'COOPERATION' )
                    // InternalDSL.g:878:4: 'COOPERATION'
                    {
                    match(input,34,FOLLOW_2); 

                    }

                     after(grammarAccess.getGameMechanicTypesAccess().getCOOPERATIONEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalDSL.g:882:2: ( ( 'OPPONENT' ) )
                    {
                    // InternalDSL.g:882:2: ( ( 'OPPONENT' ) )
                    // InternalDSL.g:883:3: ( 'OPPONENT' )
                    {
                     before(grammarAccess.getGameMechanicTypesAccess().getOPPONENTEnumLiteralDeclaration_4()); 
                    // InternalDSL.g:884:3: ( 'OPPONENT' )
                    // InternalDSL.g:884:4: 'OPPONENT'
                    {
                    match(input,35,FOLLOW_2); 

                    }

                     after(grammarAccess.getGameMechanicTypesAccess().getOPPONENTEnumLiteralDeclaration_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalDSL.g:888:2: ( ( 'COLLECTIBLE' ) )
                    {
                    // InternalDSL.g:888:2: ( ( 'COLLECTIBLE' ) )
                    // InternalDSL.g:889:3: ( 'COLLECTIBLE' )
                    {
                     before(grammarAccess.getGameMechanicTypesAccess().getCOLLECTIBLEEnumLiteralDeclaration_5()); 
                    // InternalDSL.g:890:3: ( 'COLLECTIBLE' )
                    // InternalDSL.g:890:4: 'COLLECTIBLE'
                    {
                    match(input,36,FOLLOW_2); 

                    }

                     after(grammarAccess.getGameMechanicTypesAccess().getCOLLECTIBLEEnumLiteralDeclaration_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalDSL.g:894:2: ( ( 'INVENTORY' ) )
                    {
                    // InternalDSL.g:894:2: ( ( 'INVENTORY' ) )
                    // InternalDSL.g:895:3: ( 'INVENTORY' )
                    {
                     before(grammarAccess.getGameMechanicTypesAccess().getINVENTORYEnumLiteralDeclaration_6()); 
                    // InternalDSL.g:896:3: ( 'INVENTORY' )
                    // InternalDSL.g:896:4: 'INVENTORY'
                    {
                    match(input,37,FOLLOW_2); 

                    }

                     after(grammarAccess.getGameMechanicTypesAccess().getINVENTORYEnumLiteralDeclaration_6()); 

                    }


                    }
                    break;
                case 8 :
                    // InternalDSL.g:900:2: ( ( 'SOCIALGIFT' ) )
                    {
                    // InternalDSL.g:900:2: ( ( 'SOCIALGIFT' ) )
                    // InternalDSL.g:901:3: ( 'SOCIALGIFT' )
                    {
                     before(grammarAccess.getGameMechanicTypesAccess().getSOCIALGIFTEnumLiteralDeclaration_7()); 
                    // InternalDSL.g:902:3: ( 'SOCIALGIFT' )
                    // InternalDSL.g:902:4: 'SOCIALGIFT'
                    {
                    match(input,38,FOLLOW_2); 

                    }

                     after(grammarAccess.getGameMechanicTypesAccess().getSOCIALGIFTEnumLiteralDeclaration_7()); 

                    }


                    }
                    break;
                case 9 :
                    // InternalDSL.g:906:2: ( ( 'AVATAR' ) )
                    {
                    // InternalDSL.g:906:2: ( ( 'AVATAR' ) )
                    // InternalDSL.g:907:3: ( 'AVATAR' )
                    {
                     before(grammarAccess.getGameMechanicTypesAccess().getAVATAREnumLiteralDeclaration_8()); 
                    // InternalDSL.g:908:3: ( 'AVATAR' )
                    // InternalDSL.g:908:4: 'AVATAR'
                    {
                    match(input,39,FOLLOW_2); 

                    }

                     after(grammarAccess.getGameMechanicTypesAccess().getAVATAREnumLiteralDeclaration_8()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanicTypes__Alternatives"


    // $ANTLR start "rule__EventTypes__Alternatives"
    // InternalDSL.g:916:1: rule__EventTypes__Alternatives : ( ( ( 'CLICK' ) ) | ( ( 'HOVER' ) ) | ( ( 'VIEW' ) ) | ( ( 'DRAG' ) ) | ( ( 'DROP' ) ) | ( ( 'DSUCCESS' ) ) | ( ( 'MSUCCESS' ) ) | ( ( 'DFAILURE' ) ) | ( ( 'MFAILURE' ) ) );
    public final void rule__EventTypes__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:920:1: ( ( ( 'CLICK' ) ) | ( ( 'HOVER' ) ) | ( ( 'VIEW' ) ) | ( ( 'DRAG' ) ) | ( ( 'DROP' ) ) | ( ( 'DSUCCESS' ) ) | ( ( 'MSUCCESS' ) ) | ( ( 'DFAILURE' ) ) | ( ( 'MFAILURE' ) ) )
            int alt9=9;
            switch ( input.LA(1) ) {
            case 40:
                {
                alt9=1;
                }
                break;
            case 41:
                {
                alt9=2;
                }
                break;
            case 42:
                {
                alt9=3;
                }
                break;
            case 43:
                {
                alt9=4;
                }
                break;
            case 44:
                {
                alt9=5;
                }
                break;
            case 45:
                {
                alt9=6;
                }
                break;
            case 46:
                {
                alt9=7;
                }
                break;
            case 47:
                {
                alt9=8;
                }
                break;
            case 48:
                {
                alt9=9;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // InternalDSL.g:921:2: ( ( 'CLICK' ) )
                    {
                    // InternalDSL.g:921:2: ( ( 'CLICK' ) )
                    // InternalDSL.g:922:3: ( 'CLICK' )
                    {
                     before(grammarAccess.getEventTypesAccess().getCLICKEnumLiteralDeclaration_0()); 
                    // InternalDSL.g:923:3: ( 'CLICK' )
                    // InternalDSL.g:923:4: 'CLICK'
                    {
                    match(input,40,FOLLOW_2); 

                    }

                     after(grammarAccess.getEventTypesAccess().getCLICKEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDSL.g:927:2: ( ( 'HOVER' ) )
                    {
                    // InternalDSL.g:927:2: ( ( 'HOVER' ) )
                    // InternalDSL.g:928:3: ( 'HOVER' )
                    {
                     before(grammarAccess.getEventTypesAccess().getHOVEREnumLiteralDeclaration_1()); 
                    // InternalDSL.g:929:3: ( 'HOVER' )
                    // InternalDSL.g:929:4: 'HOVER'
                    {
                    match(input,41,FOLLOW_2); 

                    }

                     after(grammarAccess.getEventTypesAccess().getHOVEREnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalDSL.g:933:2: ( ( 'VIEW' ) )
                    {
                    // InternalDSL.g:933:2: ( ( 'VIEW' ) )
                    // InternalDSL.g:934:3: ( 'VIEW' )
                    {
                     before(grammarAccess.getEventTypesAccess().getVIEWEnumLiteralDeclaration_2()); 
                    // InternalDSL.g:935:3: ( 'VIEW' )
                    // InternalDSL.g:935:4: 'VIEW'
                    {
                    match(input,42,FOLLOW_2); 

                    }

                     after(grammarAccess.getEventTypesAccess().getVIEWEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalDSL.g:939:2: ( ( 'DRAG' ) )
                    {
                    // InternalDSL.g:939:2: ( ( 'DRAG' ) )
                    // InternalDSL.g:940:3: ( 'DRAG' )
                    {
                     before(grammarAccess.getEventTypesAccess().getDRAGEnumLiteralDeclaration_3()); 
                    // InternalDSL.g:941:3: ( 'DRAG' )
                    // InternalDSL.g:941:4: 'DRAG'
                    {
                    match(input,43,FOLLOW_2); 

                    }

                     after(grammarAccess.getEventTypesAccess().getDRAGEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalDSL.g:945:2: ( ( 'DROP' ) )
                    {
                    // InternalDSL.g:945:2: ( ( 'DROP' ) )
                    // InternalDSL.g:946:3: ( 'DROP' )
                    {
                     before(grammarAccess.getEventTypesAccess().getDROPEnumLiteralDeclaration_4()); 
                    // InternalDSL.g:947:3: ( 'DROP' )
                    // InternalDSL.g:947:4: 'DROP'
                    {
                    match(input,44,FOLLOW_2); 

                    }

                     after(grammarAccess.getEventTypesAccess().getDROPEnumLiteralDeclaration_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalDSL.g:951:2: ( ( 'DSUCCESS' ) )
                    {
                    // InternalDSL.g:951:2: ( ( 'DSUCCESS' ) )
                    // InternalDSL.g:952:3: ( 'DSUCCESS' )
                    {
                     before(grammarAccess.getEventTypesAccess().getDSUCCESSEnumLiteralDeclaration_5()); 
                    // InternalDSL.g:953:3: ( 'DSUCCESS' )
                    // InternalDSL.g:953:4: 'DSUCCESS'
                    {
                    match(input,45,FOLLOW_2); 

                    }

                     after(grammarAccess.getEventTypesAccess().getDSUCCESSEnumLiteralDeclaration_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalDSL.g:957:2: ( ( 'MSUCCESS' ) )
                    {
                    // InternalDSL.g:957:2: ( ( 'MSUCCESS' ) )
                    // InternalDSL.g:958:3: ( 'MSUCCESS' )
                    {
                     before(grammarAccess.getEventTypesAccess().getMSUCCESSEnumLiteralDeclaration_6()); 
                    // InternalDSL.g:959:3: ( 'MSUCCESS' )
                    // InternalDSL.g:959:4: 'MSUCCESS'
                    {
                    match(input,46,FOLLOW_2); 

                    }

                     after(grammarAccess.getEventTypesAccess().getMSUCCESSEnumLiteralDeclaration_6()); 

                    }


                    }
                    break;
                case 8 :
                    // InternalDSL.g:963:2: ( ( 'DFAILURE' ) )
                    {
                    // InternalDSL.g:963:2: ( ( 'DFAILURE' ) )
                    // InternalDSL.g:964:3: ( 'DFAILURE' )
                    {
                     before(grammarAccess.getEventTypesAccess().getDFAILUREEnumLiteralDeclaration_7()); 
                    // InternalDSL.g:965:3: ( 'DFAILURE' )
                    // InternalDSL.g:965:4: 'DFAILURE'
                    {
                    match(input,47,FOLLOW_2); 

                    }

                     after(grammarAccess.getEventTypesAccess().getDFAILUREEnumLiteralDeclaration_7()); 

                    }


                    }
                    break;
                case 9 :
                    // InternalDSL.g:969:2: ( ( 'MFAILURE' ) )
                    {
                    // InternalDSL.g:969:2: ( ( 'MFAILURE' ) )
                    // InternalDSL.g:970:3: ( 'MFAILURE' )
                    {
                     before(grammarAccess.getEventTypesAccess().getMFAILUREEnumLiteralDeclaration_8()); 
                    // InternalDSL.g:971:3: ( 'MFAILURE' )
                    // InternalDSL.g:971:4: 'MFAILURE'
                    {
                    match(input,48,FOLLOW_2); 

                    }

                     after(grammarAccess.getEventTypesAccess().getMFAILUREEnumLiteralDeclaration_8()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EventTypes__Alternatives"


    // $ANTLR start "rule__ConditionTypes__Alternatives"
    // InternalDSL.g:979:1: rule__ConditionTypes__Alternatives : ( ( ( 'EQUAL' ) ) | ( ( 'MORE' ) ) | ( ( 'LESS' ) ) );
    public final void rule__ConditionTypes__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:983:1: ( ( ( 'EQUAL' ) ) | ( ( 'MORE' ) ) | ( ( 'LESS' ) ) )
            int alt10=3;
            switch ( input.LA(1) ) {
            case 49:
                {
                alt10=1;
                }
                break;
            case 50:
                {
                alt10=2;
                }
                break;
            case 51:
                {
                alt10=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // InternalDSL.g:984:2: ( ( 'EQUAL' ) )
                    {
                    // InternalDSL.g:984:2: ( ( 'EQUAL' ) )
                    // InternalDSL.g:985:3: ( 'EQUAL' )
                    {
                     before(grammarAccess.getConditionTypesAccess().getEQUALEnumLiteralDeclaration_0()); 
                    // InternalDSL.g:986:3: ( 'EQUAL' )
                    // InternalDSL.g:986:4: 'EQUAL'
                    {
                    match(input,49,FOLLOW_2); 

                    }

                     after(grammarAccess.getConditionTypesAccess().getEQUALEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDSL.g:990:2: ( ( 'MORE' ) )
                    {
                    // InternalDSL.g:990:2: ( ( 'MORE' ) )
                    // InternalDSL.g:991:3: ( 'MORE' )
                    {
                     before(grammarAccess.getConditionTypesAccess().getMOREEnumLiteralDeclaration_1()); 
                    // InternalDSL.g:992:3: ( 'MORE' )
                    // InternalDSL.g:992:4: 'MORE'
                    {
                    match(input,50,FOLLOW_2); 

                    }

                     after(grammarAccess.getConditionTypesAccess().getMOREEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalDSL.g:996:2: ( ( 'LESS' ) )
                    {
                    // InternalDSL.g:996:2: ( ( 'LESS' ) )
                    // InternalDSL.g:997:3: ( 'LESS' )
                    {
                     before(grammarAccess.getConditionTypesAccess().getLESSEnumLiteralDeclaration_2()); 
                    // InternalDSL.g:998:3: ( 'LESS' )
                    // InternalDSL.g:998:4: 'LESS'
                    {
                    match(input,51,FOLLOW_2); 

                    }

                     after(grammarAccess.getConditionTypesAccess().getLESSEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionTypes__Alternatives"


    // $ANTLR start "rule__RestrictionTypes__Alternatives"
    // InternalDSL.g:1006:1: rule__RestrictionTypes__Alternatives : ( ( ( 'TIMELIMIT' ) ) | ( ( 'ACTIONLIMIT' ) ) | ( ( 'POINTLIMIT' ) ) );
    public final void rule__RestrictionTypes__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1010:1: ( ( ( 'TIMELIMIT' ) ) | ( ( 'ACTIONLIMIT' ) ) | ( ( 'POINTLIMIT' ) ) )
            int alt11=3;
            switch ( input.LA(1) ) {
            case 52:
                {
                alt11=1;
                }
                break;
            case 53:
                {
                alt11=2;
                }
                break;
            case 54:
                {
                alt11=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }

            switch (alt11) {
                case 1 :
                    // InternalDSL.g:1011:2: ( ( 'TIMELIMIT' ) )
                    {
                    // InternalDSL.g:1011:2: ( ( 'TIMELIMIT' ) )
                    // InternalDSL.g:1012:3: ( 'TIMELIMIT' )
                    {
                     before(grammarAccess.getRestrictionTypesAccess().getTIMELIMITEnumLiteralDeclaration_0()); 
                    // InternalDSL.g:1013:3: ( 'TIMELIMIT' )
                    // InternalDSL.g:1013:4: 'TIMELIMIT'
                    {
                    match(input,52,FOLLOW_2); 

                    }

                     after(grammarAccess.getRestrictionTypesAccess().getTIMELIMITEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDSL.g:1017:2: ( ( 'ACTIONLIMIT' ) )
                    {
                    // InternalDSL.g:1017:2: ( ( 'ACTIONLIMIT' ) )
                    // InternalDSL.g:1018:3: ( 'ACTIONLIMIT' )
                    {
                     before(grammarAccess.getRestrictionTypesAccess().getACTIONLIMITEnumLiteralDeclaration_1()); 
                    // InternalDSL.g:1019:3: ( 'ACTIONLIMIT' )
                    // InternalDSL.g:1019:4: 'ACTIONLIMIT'
                    {
                    match(input,53,FOLLOW_2); 

                    }

                     after(grammarAccess.getRestrictionTypesAccess().getACTIONLIMITEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalDSL.g:1023:2: ( ( 'POINTLIMIT' ) )
                    {
                    // InternalDSL.g:1023:2: ( ( 'POINTLIMIT' ) )
                    // InternalDSL.g:1024:3: ( 'POINTLIMIT' )
                    {
                     before(grammarAccess.getRestrictionTypesAccess().getPOINTLIMITEnumLiteralDeclaration_2()); 
                    // InternalDSL.g:1025:3: ( 'POINTLIMIT' )
                    // InternalDSL.g:1025:4: 'POINTLIMIT'
                    {
                    match(input,54,FOLLOW_2); 

                    }

                     after(grammarAccess.getRestrictionTypesAccess().getPOINTLIMITEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RestrictionTypes__Alternatives"


    // $ANTLR start "rule__PrizeTypes__Alternatives"
    // InternalDSL.g:1033:1: rule__PrizeTypes__Alternatives : ( ( ( 'VIRTUAL' ) ) | ( ( 'PHYSICAL' ) ) );
    public final void rule__PrizeTypes__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1037:1: ( ( ( 'VIRTUAL' ) ) | ( ( 'PHYSICAL' ) ) )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==55) ) {
                alt12=1;
            }
            else if ( (LA12_0==56) ) {
                alt12=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // InternalDSL.g:1038:2: ( ( 'VIRTUAL' ) )
                    {
                    // InternalDSL.g:1038:2: ( ( 'VIRTUAL' ) )
                    // InternalDSL.g:1039:3: ( 'VIRTUAL' )
                    {
                     before(grammarAccess.getPrizeTypesAccess().getVIRTUALEnumLiteralDeclaration_0()); 
                    // InternalDSL.g:1040:3: ( 'VIRTUAL' )
                    // InternalDSL.g:1040:4: 'VIRTUAL'
                    {
                    match(input,55,FOLLOW_2); 

                    }

                     after(grammarAccess.getPrizeTypesAccess().getVIRTUALEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDSL.g:1044:2: ( ( 'PHYSICAL' ) )
                    {
                    // InternalDSL.g:1044:2: ( ( 'PHYSICAL' ) )
                    // InternalDSL.g:1045:3: ( 'PHYSICAL' )
                    {
                     before(grammarAccess.getPrizeTypesAccess().getPHYSICALEnumLiteralDeclaration_1()); 
                    // InternalDSL.g:1046:3: ( 'PHYSICAL' )
                    // InternalDSL.g:1046:4: 'PHYSICAL'
                    {
                    match(input,56,FOLLOW_2); 

                    }

                     after(grammarAccess.getPrizeTypesAccess().getPHYSICALEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrizeTypes__Alternatives"


    // $ANTLR start "rule__Gamify__Group__0"
    // InternalDSL.g:1054:1: rule__Gamify__Group__0 : rule__Gamify__Group__0__Impl rule__Gamify__Group__1 ;
    public final void rule__Gamify__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1058:1: ( rule__Gamify__Group__0__Impl rule__Gamify__Group__1 )
            // InternalDSL.g:1059:2: rule__Gamify__Group__0__Impl rule__Gamify__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Gamify__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Gamify__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__0"


    // $ANTLR start "rule__Gamify__Group__0__Impl"
    // InternalDSL.g:1066:1: rule__Gamify__Group__0__Impl : ( 'Gamify' ) ;
    public final void rule__Gamify__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1070:1: ( ( 'Gamify' ) )
            // InternalDSL.g:1071:1: ( 'Gamify' )
            {
            // InternalDSL.g:1071:1: ( 'Gamify' )
            // InternalDSL.g:1072:2: 'Gamify'
            {
             before(grammarAccess.getGamifyAccess().getGamifyKeyword_0()); 
            match(input,57,FOLLOW_2); 
             after(grammarAccess.getGamifyAccess().getGamifyKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__0__Impl"


    // $ANTLR start "rule__Gamify__Group__1"
    // InternalDSL.g:1081:1: rule__Gamify__Group__1 : rule__Gamify__Group__1__Impl rule__Gamify__Group__2 ;
    public final void rule__Gamify__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1085:1: ( rule__Gamify__Group__1__Impl rule__Gamify__Group__2 )
            // InternalDSL.g:1086:2: rule__Gamify__Group__1__Impl rule__Gamify__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Gamify__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Gamify__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__1"


    // $ANTLR start "rule__Gamify__Group__1__Impl"
    // InternalDSL.g:1093:1: rule__Gamify__Group__1__Impl : ( '{' ) ;
    public final void rule__Gamify__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1097:1: ( ( '{' ) )
            // InternalDSL.g:1098:1: ( '{' )
            {
            // InternalDSL.g:1098:1: ( '{' )
            // InternalDSL.g:1099:2: '{'
            {
             before(grammarAccess.getGamifyAccess().getLeftCurlyBracketKeyword_1()); 
            match(input,58,FOLLOW_2); 
             after(grammarAccess.getGamifyAccess().getLeftCurlyBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__1__Impl"


    // $ANTLR start "rule__Gamify__Group__2"
    // InternalDSL.g:1108:1: rule__Gamify__Group__2 : rule__Gamify__Group__2__Impl rule__Gamify__Group__3 ;
    public final void rule__Gamify__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1112:1: ( rule__Gamify__Group__2__Impl rule__Gamify__Group__3 )
            // InternalDSL.g:1113:2: rule__Gamify__Group__2__Impl rule__Gamify__Group__3
            {
            pushFollow(FOLLOW_4);
            rule__Gamify__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Gamify__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__2"


    // $ANTLR start "rule__Gamify__Group__2__Impl"
    // InternalDSL.g:1120:1: rule__Gamify__Group__2__Impl : ( ( rule__Gamify__Group_2__0 )? ) ;
    public final void rule__Gamify__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1124:1: ( ( ( rule__Gamify__Group_2__0 )? ) )
            // InternalDSL.g:1125:1: ( ( rule__Gamify__Group_2__0 )? )
            {
            // InternalDSL.g:1125:1: ( ( rule__Gamify__Group_2__0 )? )
            // InternalDSL.g:1126:2: ( rule__Gamify__Group_2__0 )?
            {
             before(grammarAccess.getGamifyAccess().getGroup_2()); 
            // InternalDSL.g:1127:2: ( rule__Gamify__Group_2__0 )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==62) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalDSL.g:1127:3: rule__Gamify__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Gamify__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getGamifyAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__2__Impl"


    // $ANTLR start "rule__Gamify__Group__3"
    // InternalDSL.g:1135:1: rule__Gamify__Group__3 : rule__Gamify__Group__3__Impl rule__Gamify__Group__4 ;
    public final void rule__Gamify__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1139:1: ( rule__Gamify__Group__3__Impl rule__Gamify__Group__4 )
            // InternalDSL.g:1140:2: rule__Gamify__Group__3__Impl rule__Gamify__Group__4
            {
            pushFollow(FOLLOW_4);
            rule__Gamify__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Gamify__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__3"


    // $ANTLR start "rule__Gamify__Group__3__Impl"
    // InternalDSL.g:1147:1: rule__Gamify__Group__3__Impl : ( ( rule__Gamify__Group_3__0 )? ) ;
    public final void rule__Gamify__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1151:1: ( ( ( rule__Gamify__Group_3__0 )? ) )
            // InternalDSL.g:1152:1: ( ( rule__Gamify__Group_3__0 )? )
            {
            // InternalDSL.g:1152:1: ( ( rule__Gamify__Group_3__0 )? )
            // InternalDSL.g:1153:2: ( rule__Gamify__Group_3__0 )?
            {
             before(grammarAccess.getGamifyAccess().getGroup_3()); 
            // InternalDSL.g:1154:2: ( rule__Gamify__Group_3__0 )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==63) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalDSL.g:1154:3: rule__Gamify__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Gamify__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getGamifyAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__3__Impl"


    // $ANTLR start "rule__Gamify__Group__4"
    // InternalDSL.g:1162:1: rule__Gamify__Group__4 : rule__Gamify__Group__4__Impl rule__Gamify__Group__5 ;
    public final void rule__Gamify__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1166:1: ( rule__Gamify__Group__4__Impl rule__Gamify__Group__5 )
            // InternalDSL.g:1167:2: rule__Gamify__Group__4__Impl rule__Gamify__Group__5
            {
            pushFollow(FOLLOW_3);
            rule__Gamify__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Gamify__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__4"


    // $ANTLR start "rule__Gamify__Group__4__Impl"
    // InternalDSL.g:1174:1: rule__Gamify__Group__4__Impl : ( 'GameDynamics' ) ;
    public final void rule__Gamify__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1178:1: ( ( 'GameDynamics' ) )
            // InternalDSL.g:1179:1: ( 'GameDynamics' )
            {
            // InternalDSL.g:1179:1: ( 'GameDynamics' )
            // InternalDSL.g:1180:2: 'GameDynamics'
            {
             before(grammarAccess.getGamifyAccess().getGameDynamicsKeyword_4()); 
            match(input,59,FOLLOW_2); 
             after(grammarAccess.getGamifyAccess().getGameDynamicsKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__4__Impl"


    // $ANTLR start "rule__Gamify__Group__5"
    // InternalDSL.g:1189:1: rule__Gamify__Group__5 : rule__Gamify__Group__5__Impl rule__Gamify__Group__6 ;
    public final void rule__Gamify__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1193:1: ( rule__Gamify__Group__5__Impl rule__Gamify__Group__6 )
            // InternalDSL.g:1194:2: rule__Gamify__Group__5__Impl rule__Gamify__Group__6
            {
            pushFollow(FOLLOW_5);
            rule__Gamify__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Gamify__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__5"


    // $ANTLR start "rule__Gamify__Group__5__Impl"
    // InternalDSL.g:1201:1: rule__Gamify__Group__5__Impl : ( '{' ) ;
    public final void rule__Gamify__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1205:1: ( ( '{' ) )
            // InternalDSL.g:1206:1: ( '{' )
            {
            // InternalDSL.g:1206:1: ( '{' )
            // InternalDSL.g:1207:2: '{'
            {
             before(grammarAccess.getGamifyAccess().getLeftCurlyBracketKeyword_5()); 
            match(input,58,FOLLOW_2); 
             after(grammarAccess.getGamifyAccess().getLeftCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__5__Impl"


    // $ANTLR start "rule__Gamify__Group__6"
    // InternalDSL.g:1216:1: rule__Gamify__Group__6 : rule__Gamify__Group__6__Impl rule__Gamify__Group__7 ;
    public final void rule__Gamify__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1220:1: ( rule__Gamify__Group__6__Impl rule__Gamify__Group__7 )
            // InternalDSL.g:1221:2: rule__Gamify__Group__6__Impl rule__Gamify__Group__7
            {
            pushFollow(FOLLOW_6);
            rule__Gamify__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Gamify__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__6"


    // $ANTLR start "rule__Gamify__Group__6__Impl"
    // InternalDSL.g:1228:1: rule__Gamify__Group__6__Impl : ( ( rule__Gamify__GamedynamicsAssignment_6 ) ) ;
    public final void rule__Gamify__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1232:1: ( ( ( rule__Gamify__GamedynamicsAssignment_6 ) ) )
            // InternalDSL.g:1233:1: ( ( rule__Gamify__GamedynamicsAssignment_6 ) )
            {
            // InternalDSL.g:1233:1: ( ( rule__Gamify__GamedynamicsAssignment_6 ) )
            // InternalDSL.g:1234:2: ( rule__Gamify__GamedynamicsAssignment_6 )
            {
             before(grammarAccess.getGamifyAccess().getGamedynamicsAssignment_6()); 
            // InternalDSL.g:1235:2: ( rule__Gamify__GamedynamicsAssignment_6 )
            // InternalDSL.g:1235:3: rule__Gamify__GamedynamicsAssignment_6
            {
            pushFollow(FOLLOW_2);
            rule__Gamify__GamedynamicsAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getGamifyAccess().getGamedynamicsAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__6__Impl"


    // $ANTLR start "rule__Gamify__Group__7"
    // InternalDSL.g:1243:1: rule__Gamify__Group__7 : rule__Gamify__Group__7__Impl rule__Gamify__Group__8 ;
    public final void rule__Gamify__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1247:1: ( rule__Gamify__Group__7__Impl rule__Gamify__Group__8 )
            // InternalDSL.g:1248:2: rule__Gamify__Group__7__Impl rule__Gamify__Group__8
            {
            pushFollow(FOLLOW_6);
            rule__Gamify__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Gamify__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__7"


    // $ANTLR start "rule__Gamify__Group__7__Impl"
    // InternalDSL.g:1255:1: rule__Gamify__Group__7__Impl : ( ( rule__Gamify__Group_7__0 )* ) ;
    public final void rule__Gamify__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1259:1: ( ( ( rule__Gamify__Group_7__0 )* ) )
            // InternalDSL.g:1260:1: ( ( rule__Gamify__Group_7__0 )* )
            {
            // InternalDSL.g:1260:1: ( ( rule__Gamify__Group_7__0 )* )
            // InternalDSL.g:1261:2: ( rule__Gamify__Group_7__0 )*
            {
             before(grammarAccess.getGamifyAccess().getGroup_7()); 
            // InternalDSL.g:1262:2: ( rule__Gamify__Group_7__0 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==64) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalDSL.g:1262:3: rule__Gamify__Group_7__0
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__Gamify__Group_7__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getGamifyAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__7__Impl"


    // $ANTLR start "rule__Gamify__Group__8"
    // InternalDSL.g:1270:1: rule__Gamify__Group__8 : rule__Gamify__Group__8__Impl rule__Gamify__Group__9 ;
    public final void rule__Gamify__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1274:1: ( rule__Gamify__Group__8__Impl rule__Gamify__Group__9 )
            // InternalDSL.g:1275:2: rule__Gamify__Group__8__Impl rule__Gamify__Group__9
            {
            pushFollow(FOLLOW_8);
            rule__Gamify__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Gamify__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__8"


    // $ANTLR start "rule__Gamify__Group__8__Impl"
    // InternalDSL.g:1282:1: rule__Gamify__Group__8__Impl : ( '}' ) ;
    public final void rule__Gamify__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1286:1: ( ( '}' ) )
            // InternalDSL.g:1287:1: ( '}' )
            {
            // InternalDSL.g:1287:1: ( '}' )
            // InternalDSL.g:1288:2: '}'
            {
             before(grammarAccess.getGamifyAccess().getRightCurlyBracketKeyword_8()); 
            match(input,60,FOLLOW_2); 
             after(grammarAccess.getGamifyAccess().getRightCurlyBracketKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__8__Impl"


    // $ANTLR start "rule__Gamify__Group__9"
    // InternalDSL.g:1297:1: rule__Gamify__Group__9 : rule__Gamify__Group__9__Impl rule__Gamify__Group__10 ;
    public final void rule__Gamify__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1301:1: ( rule__Gamify__Group__9__Impl rule__Gamify__Group__10 )
            // InternalDSL.g:1302:2: rule__Gamify__Group__9__Impl rule__Gamify__Group__10
            {
            pushFollow(FOLLOW_3);
            rule__Gamify__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Gamify__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__9"


    // $ANTLR start "rule__Gamify__Group__9__Impl"
    // InternalDSL.g:1309:1: rule__Gamify__Group__9__Impl : ( 'Items' ) ;
    public final void rule__Gamify__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1313:1: ( ( 'Items' ) )
            // InternalDSL.g:1314:1: ( 'Items' )
            {
            // InternalDSL.g:1314:1: ( 'Items' )
            // InternalDSL.g:1315:2: 'Items'
            {
             before(grammarAccess.getGamifyAccess().getItemsKeyword_9()); 
            match(input,61,FOLLOW_2); 
             after(grammarAccess.getGamifyAccess().getItemsKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__9__Impl"


    // $ANTLR start "rule__Gamify__Group__10"
    // InternalDSL.g:1324:1: rule__Gamify__Group__10 : rule__Gamify__Group__10__Impl rule__Gamify__Group__11 ;
    public final void rule__Gamify__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1328:1: ( rule__Gamify__Group__10__Impl rule__Gamify__Group__11 )
            // InternalDSL.g:1329:2: rule__Gamify__Group__10__Impl rule__Gamify__Group__11
            {
            pushFollow(FOLLOW_9);
            rule__Gamify__Group__10__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Gamify__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__10"


    // $ANTLR start "rule__Gamify__Group__10__Impl"
    // InternalDSL.g:1336:1: rule__Gamify__Group__10__Impl : ( '{' ) ;
    public final void rule__Gamify__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1340:1: ( ( '{' ) )
            // InternalDSL.g:1341:1: ( '{' )
            {
            // InternalDSL.g:1341:1: ( '{' )
            // InternalDSL.g:1342:2: '{'
            {
             before(grammarAccess.getGamifyAccess().getLeftCurlyBracketKeyword_10()); 
            match(input,58,FOLLOW_2); 
             after(grammarAccess.getGamifyAccess().getLeftCurlyBracketKeyword_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__10__Impl"


    // $ANTLR start "rule__Gamify__Group__11"
    // InternalDSL.g:1351:1: rule__Gamify__Group__11 : rule__Gamify__Group__11__Impl rule__Gamify__Group__12 ;
    public final void rule__Gamify__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1355:1: ( rule__Gamify__Group__11__Impl rule__Gamify__Group__12 )
            // InternalDSL.g:1356:2: rule__Gamify__Group__11__Impl rule__Gamify__Group__12
            {
            pushFollow(FOLLOW_6);
            rule__Gamify__Group__11__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Gamify__Group__12();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__11"


    // $ANTLR start "rule__Gamify__Group__11__Impl"
    // InternalDSL.g:1363:1: rule__Gamify__Group__11__Impl : ( ( rule__Gamify__ItemsAssignment_11 ) ) ;
    public final void rule__Gamify__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1367:1: ( ( ( rule__Gamify__ItemsAssignment_11 ) ) )
            // InternalDSL.g:1368:1: ( ( rule__Gamify__ItemsAssignment_11 ) )
            {
            // InternalDSL.g:1368:1: ( ( rule__Gamify__ItemsAssignment_11 ) )
            // InternalDSL.g:1369:2: ( rule__Gamify__ItemsAssignment_11 )
            {
             before(grammarAccess.getGamifyAccess().getItemsAssignment_11()); 
            // InternalDSL.g:1370:2: ( rule__Gamify__ItemsAssignment_11 )
            // InternalDSL.g:1370:3: rule__Gamify__ItemsAssignment_11
            {
            pushFollow(FOLLOW_2);
            rule__Gamify__ItemsAssignment_11();

            state._fsp--;


            }

             after(grammarAccess.getGamifyAccess().getItemsAssignment_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__11__Impl"


    // $ANTLR start "rule__Gamify__Group__12"
    // InternalDSL.g:1378:1: rule__Gamify__Group__12 : rule__Gamify__Group__12__Impl rule__Gamify__Group__13 ;
    public final void rule__Gamify__Group__12() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1382:1: ( rule__Gamify__Group__12__Impl rule__Gamify__Group__13 )
            // InternalDSL.g:1383:2: rule__Gamify__Group__12__Impl rule__Gamify__Group__13
            {
            pushFollow(FOLLOW_6);
            rule__Gamify__Group__12__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Gamify__Group__13();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__12"


    // $ANTLR start "rule__Gamify__Group__12__Impl"
    // InternalDSL.g:1390:1: rule__Gamify__Group__12__Impl : ( ( rule__Gamify__Group_12__0 )* ) ;
    public final void rule__Gamify__Group__12__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1394:1: ( ( ( rule__Gamify__Group_12__0 )* ) )
            // InternalDSL.g:1395:1: ( ( rule__Gamify__Group_12__0 )* )
            {
            // InternalDSL.g:1395:1: ( ( rule__Gamify__Group_12__0 )* )
            // InternalDSL.g:1396:2: ( rule__Gamify__Group_12__0 )*
            {
             before(grammarAccess.getGamifyAccess().getGroup_12()); 
            // InternalDSL.g:1397:2: ( rule__Gamify__Group_12__0 )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==64) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalDSL.g:1397:3: rule__Gamify__Group_12__0
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__Gamify__Group_12__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

             after(grammarAccess.getGamifyAccess().getGroup_12()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__12__Impl"


    // $ANTLR start "rule__Gamify__Group__13"
    // InternalDSL.g:1405:1: rule__Gamify__Group__13 : rule__Gamify__Group__13__Impl rule__Gamify__Group__14 ;
    public final void rule__Gamify__Group__13() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1409:1: ( rule__Gamify__Group__13__Impl rule__Gamify__Group__14 )
            // InternalDSL.g:1410:2: rule__Gamify__Group__13__Impl rule__Gamify__Group__14
            {
            pushFollow(FOLLOW_10);
            rule__Gamify__Group__13__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Gamify__Group__14();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__13"


    // $ANTLR start "rule__Gamify__Group__13__Impl"
    // InternalDSL.g:1417:1: rule__Gamify__Group__13__Impl : ( '}' ) ;
    public final void rule__Gamify__Group__13__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1421:1: ( ( '}' ) )
            // InternalDSL.g:1422:1: ( '}' )
            {
            // InternalDSL.g:1422:1: ( '}' )
            // InternalDSL.g:1423:2: '}'
            {
             before(grammarAccess.getGamifyAccess().getRightCurlyBracketKeyword_13()); 
            match(input,60,FOLLOW_2); 
             after(grammarAccess.getGamifyAccess().getRightCurlyBracketKeyword_13()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__13__Impl"


    // $ANTLR start "rule__Gamify__Group__14"
    // InternalDSL.g:1432:1: rule__Gamify__Group__14 : rule__Gamify__Group__14__Impl rule__Gamify__Group__15 ;
    public final void rule__Gamify__Group__14() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1436:1: ( rule__Gamify__Group__14__Impl rule__Gamify__Group__15 )
            // InternalDSL.g:1437:2: rule__Gamify__Group__14__Impl rule__Gamify__Group__15
            {
            pushFollow(FOLLOW_10);
            rule__Gamify__Group__14__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Gamify__Group__15();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__14"


    // $ANTLR start "rule__Gamify__Group__14__Impl"
    // InternalDSL.g:1444:1: rule__Gamify__Group__14__Impl : ( ( rule__Gamify__Group_14__0 )? ) ;
    public final void rule__Gamify__Group__14__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1448:1: ( ( ( rule__Gamify__Group_14__0 )? ) )
            // InternalDSL.g:1449:1: ( ( rule__Gamify__Group_14__0 )? )
            {
            // InternalDSL.g:1449:1: ( ( rule__Gamify__Group_14__0 )? )
            // InternalDSL.g:1450:2: ( rule__Gamify__Group_14__0 )?
            {
             before(grammarAccess.getGamifyAccess().getGroup_14()); 
            // InternalDSL.g:1451:2: ( rule__Gamify__Group_14__0 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==65) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalDSL.g:1451:3: rule__Gamify__Group_14__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Gamify__Group_14__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getGamifyAccess().getGroup_14()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__14__Impl"


    // $ANTLR start "rule__Gamify__Group__15"
    // InternalDSL.g:1459:1: rule__Gamify__Group__15 : rule__Gamify__Group__15__Impl ;
    public final void rule__Gamify__Group__15() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1463:1: ( rule__Gamify__Group__15__Impl )
            // InternalDSL.g:1464:2: rule__Gamify__Group__15__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Gamify__Group__15__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__15"


    // $ANTLR start "rule__Gamify__Group__15__Impl"
    // InternalDSL.g:1470:1: rule__Gamify__Group__15__Impl : ( '}' ) ;
    public final void rule__Gamify__Group__15__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1474:1: ( ( '}' ) )
            // InternalDSL.g:1475:1: ( '}' )
            {
            // InternalDSL.g:1475:1: ( '}' )
            // InternalDSL.g:1476:2: '}'
            {
             before(grammarAccess.getGamifyAccess().getRightCurlyBracketKeyword_15()); 
            match(input,60,FOLLOW_2); 
             after(grammarAccess.getGamifyAccess().getRightCurlyBracketKeyword_15()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group__15__Impl"


    // $ANTLR start "rule__Gamify__Group_2__0"
    // InternalDSL.g:1486:1: rule__Gamify__Group_2__0 : rule__Gamify__Group_2__0__Impl rule__Gamify__Group_2__1 ;
    public final void rule__Gamify__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1490:1: ( rule__Gamify__Group_2__0__Impl rule__Gamify__Group_2__1 )
            // InternalDSL.g:1491:2: rule__Gamify__Group_2__0__Impl rule__Gamify__Group_2__1
            {
            pushFollow(FOLLOW_11);
            rule__Gamify__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Gamify__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group_2__0"


    // $ANTLR start "rule__Gamify__Group_2__0__Impl"
    // InternalDSL.g:1498:1: rule__Gamify__Group_2__0__Impl : ( 'Name:' ) ;
    public final void rule__Gamify__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1502:1: ( ( 'Name:' ) )
            // InternalDSL.g:1503:1: ( 'Name:' )
            {
            // InternalDSL.g:1503:1: ( 'Name:' )
            // InternalDSL.g:1504:2: 'Name:'
            {
             before(grammarAccess.getGamifyAccess().getNameKeyword_2_0()); 
            match(input,62,FOLLOW_2); 
             after(grammarAccess.getGamifyAccess().getNameKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group_2__0__Impl"


    // $ANTLR start "rule__Gamify__Group_2__1"
    // InternalDSL.g:1513:1: rule__Gamify__Group_2__1 : rule__Gamify__Group_2__1__Impl ;
    public final void rule__Gamify__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1517:1: ( rule__Gamify__Group_2__1__Impl )
            // InternalDSL.g:1518:2: rule__Gamify__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Gamify__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group_2__1"


    // $ANTLR start "rule__Gamify__Group_2__1__Impl"
    // InternalDSL.g:1524:1: rule__Gamify__Group_2__1__Impl : ( ( rule__Gamify__NameAssignment_2_1 ) ) ;
    public final void rule__Gamify__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1528:1: ( ( ( rule__Gamify__NameAssignment_2_1 ) ) )
            // InternalDSL.g:1529:1: ( ( rule__Gamify__NameAssignment_2_1 ) )
            {
            // InternalDSL.g:1529:1: ( ( rule__Gamify__NameAssignment_2_1 ) )
            // InternalDSL.g:1530:2: ( rule__Gamify__NameAssignment_2_1 )
            {
             before(grammarAccess.getGamifyAccess().getNameAssignment_2_1()); 
            // InternalDSL.g:1531:2: ( rule__Gamify__NameAssignment_2_1 )
            // InternalDSL.g:1531:3: rule__Gamify__NameAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__Gamify__NameAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getGamifyAccess().getNameAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group_2__1__Impl"


    // $ANTLR start "rule__Gamify__Group_3__0"
    // InternalDSL.g:1540:1: rule__Gamify__Group_3__0 : rule__Gamify__Group_3__0__Impl rule__Gamify__Group_3__1 ;
    public final void rule__Gamify__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1544:1: ( rule__Gamify__Group_3__0__Impl rule__Gamify__Group_3__1 )
            // InternalDSL.g:1545:2: rule__Gamify__Group_3__0__Impl rule__Gamify__Group_3__1
            {
            pushFollow(FOLLOW_3);
            rule__Gamify__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Gamify__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group_3__0"


    // $ANTLR start "rule__Gamify__Group_3__0__Impl"
    // InternalDSL.g:1552:1: rule__Gamify__Group_3__0__Impl : ( 'System' ) ;
    public final void rule__Gamify__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1556:1: ( ( 'System' ) )
            // InternalDSL.g:1557:1: ( 'System' )
            {
            // InternalDSL.g:1557:1: ( 'System' )
            // InternalDSL.g:1558:2: 'System'
            {
             before(grammarAccess.getGamifyAccess().getSystemKeyword_3_0()); 
            match(input,63,FOLLOW_2); 
             after(grammarAccess.getGamifyAccess().getSystemKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group_3__0__Impl"


    // $ANTLR start "rule__Gamify__Group_3__1"
    // InternalDSL.g:1567:1: rule__Gamify__Group_3__1 : rule__Gamify__Group_3__1__Impl ;
    public final void rule__Gamify__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1571:1: ( rule__Gamify__Group_3__1__Impl )
            // InternalDSL.g:1572:2: rule__Gamify__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Gamify__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group_3__1"


    // $ANTLR start "rule__Gamify__Group_3__1__Impl"
    // InternalDSL.g:1578:1: rule__Gamify__Group_3__1__Impl : ( ( rule__Gamify__SystemAssignment_3_1 ) ) ;
    public final void rule__Gamify__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1582:1: ( ( ( rule__Gamify__SystemAssignment_3_1 ) ) )
            // InternalDSL.g:1583:1: ( ( rule__Gamify__SystemAssignment_3_1 ) )
            {
            // InternalDSL.g:1583:1: ( ( rule__Gamify__SystemAssignment_3_1 ) )
            // InternalDSL.g:1584:2: ( rule__Gamify__SystemAssignment_3_1 )
            {
             before(grammarAccess.getGamifyAccess().getSystemAssignment_3_1()); 
            // InternalDSL.g:1585:2: ( rule__Gamify__SystemAssignment_3_1 )
            // InternalDSL.g:1585:3: rule__Gamify__SystemAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Gamify__SystemAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getGamifyAccess().getSystemAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group_3__1__Impl"


    // $ANTLR start "rule__Gamify__Group_7__0"
    // InternalDSL.g:1594:1: rule__Gamify__Group_7__0 : rule__Gamify__Group_7__0__Impl rule__Gamify__Group_7__1 ;
    public final void rule__Gamify__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1598:1: ( rule__Gamify__Group_7__0__Impl rule__Gamify__Group_7__1 )
            // InternalDSL.g:1599:2: rule__Gamify__Group_7__0__Impl rule__Gamify__Group_7__1
            {
            pushFollow(FOLLOW_5);
            rule__Gamify__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Gamify__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group_7__0"


    // $ANTLR start "rule__Gamify__Group_7__0__Impl"
    // InternalDSL.g:1606:1: rule__Gamify__Group_7__0__Impl : ( ',' ) ;
    public final void rule__Gamify__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1610:1: ( ( ',' ) )
            // InternalDSL.g:1611:1: ( ',' )
            {
            // InternalDSL.g:1611:1: ( ',' )
            // InternalDSL.g:1612:2: ','
            {
             before(grammarAccess.getGamifyAccess().getCommaKeyword_7_0()); 
            match(input,64,FOLLOW_2); 
             after(grammarAccess.getGamifyAccess().getCommaKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group_7__0__Impl"


    // $ANTLR start "rule__Gamify__Group_7__1"
    // InternalDSL.g:1621:1: rule__Gamify__Group_7__1 : rule__Gamify__Group_7__1__Impl ;
    public final void rule__Gamify__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1625:1: ( rule__Gamify__Group_7__1__Impl )
            // InternalDSL.g:1626:2: rule__Gamify__Group_7__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Gamify__Group_7__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group_7__1"


    // $ANTLR start "rule__Gamify__Group_7__1__Impl"
    // InternalDSL.g:1632:1: rule__Gamify__Group_7__1__Impl : ( ( rule__Gamify__GamedynamicsAssignment_7_1 ) ) ;
    public final void rule__Gamify__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1636:1: ( ( ( rule__Gamify__GamedynamicsAssignment_7_1 ) ) )
            // InternalDSL.g:1637:1: ( ( rule__Gamify__GamedynamicsAssignment_7_1 ) )
            {
            // InternalDSL.g:1637:1: ( ( rule__Gamify__GamedynamicsAssignment_7_1 ) )
            // InternalDSL.g:1638:2: ( rule__Gamify__GamedynamicsAssignment_7_1 )
            {
             before(grammarAccess.getGamifyAccess().getGamedynamicsAssignment_7_1()); 
            // InternalDSL.g:1639:2: ( rule__Gamify__GamedynamicsAssignment_7_1 )
            // InternalDSL.g:1639:3: rule__Gamify__GamedynamicsAssignment_7_1
            {
            pushFollow(FOLLOW_2);
            rule__Gamify__GamedynamicsAssignment_7_1();

            state._fsp--;


            }

             after(grammarAccess.getGamifyAccess().getGamedynamicsAssignment_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group_7__1__Impl"


    // $ANTLR start "rule__Gamify__Group_12__0"
    // InternalDSL.g:1648:1: rule__Gamify__Group_12__0 : rule__Gamify__Group_12__0__Impl rule__Gamify__Group_12__1 ;
    public final void rule__Gamify__Group_12__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1652:1: ( rule__Gamify__Group_12__0__Impl rule__Gamify__Group_12__1 )
            // InternalDSL.g:1653:2: rule__Gamify__Group_12__0__Impl rule__Gamify__Group_12__1
            {
            pushFollow(FOLLOW_9);
            rule__Gamify__Group_12__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Gamify__Group_12__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group_12__0"


    // $ANTLR start "rule__Gamify__Group_12__0__Impl"
    // InternalDSL.g:1660:1: rule__Gamify__Group_12__0__Impl : ( ',' ) ;
    public final void rule__Gamify__Group_12__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1664:1: ( ( ',' ) )
            // InternalDSL.g:1665:1: ( ',' )
            {
            // InternalDSL.g:1665:1: ( ',' )
            // InternalDSL.g:1666:2: ','
            {
             before(grammarAccess.getGamifyAccess().getCommaKeyword_12_0()); 
            match(input,64,FOLLOW_2); 
             after(grammarAccess.getGamifyAccess().getCommaKeyword_12_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group_12__0__Impl"


    // $ANTLR start "rule__Gamify__Group_12__1"
    // InternalDSL.g:1675:1: rule__Gamify__Group_12__1 : rule__Gamify__Group_12__1__Impl ;
    public final void rule__Gamify__Group_12__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1679:1: ( rule__Gamify__Group_12__1__Impl )
            // InternalDSL.g:1680:2: rule__Gamify__Group_12__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Gamify__Group_12__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group_12__1"


    // $ANTLR start "rule__Gamify__Group_12__1__Impl"
    // InternalDSL.g:1686:1: rule__Gamify__Group_12__1__Impl : ( ( rule__Gamify__ItemsAssignment_12_1 ) ) ;
    public final void rule__Gamify__Group_12__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1690:1: ( ( ( rule__Gamify__ItemsAssignment_12_1 ) ) )
            // InternalDSL.g:1691:1: ( ( rule__Gamify__ItemsAssignment_12_1 ) )
            {
            // InternalDSL.g:1691:1: ( ( rule__Gamify__ItemsAssignment_12_1 ) )
            // InternalDSL.g:1692:2: ( rule__Gamify__ItemsAssignment_12_1 )
            {
             before(grammarAccess.getGamifyAccess().getItemsAssignment_12_1()); 
            // InternalDSL.g:1693:2: ( rule__Gamify__ItemsAssignment_12_1 )
            // InternalDSL.g:1693:3: rule__Gamify__ItemsAssignment_12_1
            {
            pushFollow(FOLLOW_2);
            rule__Gamify__ItemsAssignment_12_1();

            state._fsp--;


            }

             after(grammarAccess.getGamifyAccess().getItemsAssignment_12_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group_12__1__Impl"


    // $ANTLR start "rule__Gamify__Group_14__0"
    // InternalDSL.g:1702:1: rule__Gamify__Group_14__0 : rule__Gamify__Group_14__0__Impl rule__Gamify__Group_14__1 ;
    public final void rule__Gamify__Group_14__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1706:1: ( rule__Gamify__Group_14__0__Impl rule__Gamify__Group_14__1 )
            // InternalDSL.g:1707:2: rule__Gamify__Group_14__0__Impl rule__Gamify__Group_14__1
            {
            pushFollow(FOLLOW_12);
            rule__Gamify__Group_14__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Gamify__Group_14__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group_14__0"


    // $ANTLR start "rule__Gamify__Group_14__0__Impl"
    // InternalDSL.g:1714:1: rule__Gamify__Group_14__0__Impl : ( 'Information:' ) ;
    public final void rule__Gamify__Group_14__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1718:1: ( ( 'Information:' ) )
            // InternalDSL.g:1719:1: ( 'Information:' )
            {
            // InternalDSL.g:1719:1: ( 'Information:' )
            // InternalDSL.g:1720:2: 'Information:'
            {
             before(grammarAccess.getGamifyAccess().getInformationKeyword_14_0()); 
            match(input,65,FOLLOW_2); 
             after(grammarAccess.getGamifyAccess().getInformationKeyword_14_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group_14__0__Impl"


    // $ANTLR start "rule__Gamify__Group_14__1"
    // InternalDSL.g:1729:1: rule__Gamify__Group_14__1 : rule__Gamify__Group_14__1__Impl ;
    public final void rule__Gamify__Group_14__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1733:1: ( rule__Gamify__Group_14__1__Impl )
            // InternalDSL.g:1734:2: rule__Gamify__Group_14__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Gamify__Group_14__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group_14__1"


    // $ANTLR start "rule__Gamify__Group_14__1__Impl"
    // InternalDSL.g:1740:1: rule__Gamify__Group_14__1__Impl : ( ( rule__Gamify__InformationAssignment_14_1 ) ) ;
    public final void rule__Gamify__Group_14__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1744:1: ( ( ( rule__Gamify__InformationAssignment_14_1 ) ) )
            // InternalDSL.g:1745:1: ( ( rule__Gamify__InformationAssignment_14_1 ) )
            {
            // InternalDSL.g:1745:1: ( ( rule__Gamify__InformationAssignment_14_1 ) )
            // InternalDSL.g:1746:2: ( rule__Gamify__InformationAssignment_14_1 )
            {
             before(grammarAccess.getGamifyAccess().getInformationAssignment_14_1()); 
            // InternalDSL.g:1747:2: ( rule__Gamify__InformationAssignment_14_1 )
            // InternalDSL.g:1747:3: rule__Gamify__InformationAssignment_14_1
            {
            pushFollow(FOLLOW_2);
            rule__Gamify__InformationAssignment_14_1();

            state._fsp--;


            }

             after(grammarAccess.getGamifyAccess().getInformationAssignment_14_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__Group_14__1__Impl"


    // $ANTLR start "rule__FQNString__Group__0"
    // InternalDSL.g:1756:1: rule__FQNString__Group__0 : rule__FQNString__Group__0__Impl rule__FQNString__Group__1 ;
    public final void rule__FQNString__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1760:1: ( rule__FQNString__Group__0__Impl rule__FQNString__Group__1 )
            // InternalDSL.g:1761:2: rule__FQNString__Group__0__Impl rule__FQNString__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__FQNString__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FQNString__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQNString__Group__0"


    // $ANTLR start "rule__FQNString__Group__0__Impl"
    // InternalDSL.g:1768:1: rule__FQNString__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__FQNString__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1772:1: ( ( RULE_ID ) )
            // InternalDSL.g:1773:1: ( RULE_ID )
            {
            // InternalDSL.g:1773:1: ( RULE_ID )
            // InternalDSL.g:1774:2: RULE_ID
            {
             before(grammarAccess.getFQNStringAccess().getIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFQNStringAccess().getIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQNString__Group__0__Impl"


    // $ANTLR start "rule__FQNString__Group__1"
    // InternalDSL.g:1783:1: rule__FQNString__Group__1 : rule__FQNString__Group__1__Impl rule__FQNString__Group__2 ;
    public final void rule__FQNString__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1787:1: ( rule__FQNString__Group__1__Impl rule__FQNString__Group__2 )
            // InternalDSL.g:1788:2: rule__FQNString__Group__1__Impl rule__FQNString__Group__2
            {
            pushFollow(FOLLOW_11);
            rule__FQNString__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FQNString__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQNString__Group__1"


    // $ANTLR start "rule__FQNString__Group__1__Impl"
    // InternalDSL.g:1795:1: rule__FQNString__Group__1__Impl : ( '.' ) ;
    public final void rule__FQNString__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1799:1: ( ( '.' ) )
            // InternalDSL.g:1800:1: ( '.' )
            {
            // InternalDSL.g:1800:1: ( '.' )
            // InternalDSL.g:1801:2: '.'
            {
             before(grammarAccess.getFQNStringAccess().getFullStopKeyword_1()); 
            match(input,66,FOLLOW_2); 
             after(grammarAccess.getFQNStringAccess().getFullStopKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQNString__Group__1__Impl"


    // $ANTLR start "rule__FQNString__Group__2"
    // InternalDSL.g:1810:1: rule__FQNString__Group__2 : rule__FQNString__Group__2__Impl ;
    public final void rule__FQNString__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1814:1: ( rule__FQNString__Group__2__Impl )
            // InternalDSL.g:1815:2: rule__FQNString__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FQNString__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQNString__Group__2"


    // $ANTLR start "rule__FQNString__Group__2__Impl"
    // InternalDSL.g:1821:1: rule__FQNString__Group__2__Impl : ( RULE_ID ) ;
    public final void rule__FQNString__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1825:1: ( ( RULE_ID ) )
            // InternalDSL.g:1826:1: ( RULE_ID )
            {
            // InternalDSL.g:1826:1: ( RULE_ID )
            // InternalDSL.g:1827:2: RULE_ID
            {
             before(grammarAccess.getFQNStringAccess().getIDTerminalRuleCall_2()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFQNStringAccess().getIDTerminalRuleCall_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FQNString__Group__2__Impl"


    // $ANTLR start "rule__System__Group__0"
    // InternalDSL.g:1837:1: rule__System__Group__0 : rule__System__Group__0__Impl rule__System__Group__1 ;
    public final void rule__System__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1841:1: ( rule__System__Group__0__Impl rule__System__Group__1 )
            // InternalDSL.g:1842:2: rule__System__Group__0__Impl rule__System__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__System__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__System__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__Group__0"


    // $ANTLR start "rule__System__Group__0__Impl"
    // InternalDSL.g:1849:1: rule__System__Group__0__Impl : ( () ) ;
    public final void rule__System__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1853:1: ( ( () ) )
            // InternalDSL.g:1854:1: ( () )
            {
            // InternalDSL.g:1854:1: ( () )
            // InternalDSL.g:1855:2: ()
            {
             before(grammarAccess.getSystemAccess().getSystemAction_0()); 
            // InternalDSL.g:1856:2: ()
            // InternalDSL.g:1856:3: 
            {
            }

             after(grammarAccess.getSystemAccess().getSystemAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__Group__0__Impl"


    // $ANTLR start "rule__System__Group__1"
    // InternalDSL.g:1864:1: rule__System__Group__1 : rule__System__Group__1__Impl rule__System__Group__2 ;
    public final void rule__System__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1868:1: ( rule__System__Group__1__Impl rule__System__Group__2 )
            // InternalDSL.g:1869:2: rule__System__Group__1__Impl rule__System__Group__2
            {
            pushFollow(FOLLOW_14);
            rule__System__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__System__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__Group__1"


    // $ANTLR start "rule__System__Group__1__Impl"
    // InternalDSL.g:1876:1: rule__System__Group__1__Impl : ( '{' ) ;
    public final void rule__System__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1880:1: ( ( '{' ) )
            // InternalDSL.g:1881:1: ( '{' )
            {
            // InternalDSL.g:1881:1: ( '{' )
            // InternalDSL.g:1882:2: '{'
            {
             before(grammarAccess.getSystemAccess().getLeftCurlyBracketKeyword_1()); 
            match(input,58,FOLLOW_2); 
             after(grammarAccess.getSystemAccess().getLeftCurlyBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__Group__1__Impl"


    // $ANTLR start "rule__System__Group__2"
    // InternalDSL.g:1891:1: rule__System__Group__2 : rule__System__Group__2__Impl rule__System__Group__3 ;
    public final void rule__System__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1895:1: ( rule__System__Group__2__Impl rule__System__Group__3 )
            // InternalDSL.g:1896:2: rule__System__Group__2__Impl rule__System__Group__3
            {
            pushFollow(FOLLOW_14);
            rule__System__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__System__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__Group__2"


    // $ANTLR start "rule__System__Group__2__Impl"
    // InternalDSL.g:1903:1: rule__System__Group__2__Impl : ( ( rule__System__Group_2__0 )? ) ;
    public final void rule__System__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1907:1: ( ( ( rule__System__Group_2__0 )? ) )
            // InternalDSL.g:1908:1: ( ( rule__System__Group_2__0 )? )
            {
            // InternalDSL.g:1908:1: ( ( rule__System__Group_2__0 )? )
            // InternalDSL.g:1909:2: ( rule__System__Group_2__0 )?
            {
             before(grammarAccess.getSystemAccess().getGroup_2()); 
            // InternalDSL.g:1910:2: ( rule__System__Group_2__0 )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==67) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalDSL.g:1910:3: rule__System__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__System__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSystemAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__Group__2__Impl"


    // $ANTLR start "rule__System__Group__3"
    // InternalDSL.g:1918:1: rule__System__Group__3 : rule__System__Group__3__Impl rule__System__Group__4 ;
    public final void rule__System__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1922:1: ( rule__System__Group__3__Impl rule__System__Group__4 )
            // InternalDSL.g:1923:2: rule__System__Group__3__Impl rule__System__Group__4
            {
            pushFollow(FOLLOW_14);
            rule__System__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__System__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__Group__3"


    // $ANTLR start "rule__System__Group__3__Impl"
    // InternalDSL.g:1930:1: rule__System__Group__3__Impl : ( ( rule__System__Group_3__0 )? ) ;
    public final void rule__System__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1934:1: ( ( ( rule__System__Group_3__0 )? ) )
            // InternalDSL.g:1935:1: ( ( rule__System__Group_3__0 )? )
            {
            // InternalDSL.g:1935:1: ( ( rule__System__Group_3__0 )? )
            // InternalDSL.g:1936:2: ( rule__System__Group_3__0 )?
            {
             before(grammarAccess.getSystemAccess().getGroup_3()); 
            // InternalDSL.g:1937:2: ( rule__System__Group_3__0 )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==68) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalDSL.g:1937:3: rule__System__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__System__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSystemAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__Group__3__Impl"


    // $ANTLR start "rule__System__Group__4"
    // InternalDSL.g:1945:1: rule__System__Group__4 : rule__System__Group__4__Impl rule__System__Group__5 ;
    public final void rule__System__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1949:1: ( rule__System__Group__4__Impl rule__System__Group__5 )
            // InternalDSL.g:1950:2: rule__System__Group__4__Impl rule__System__Group__5
            {
            pushFollow(FOLLOW_14);
            rule__System__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__System__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__Group__4"


    // $ANTLR start "rule__System__Group__4__Impl"
    // InternalDSL.g:1957:1: rule__System__Group__4__Impl : ( ( rule__System__Group_4__0 )? ) ;
    public final void rule__System__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1961:1: ( ( ( rule__System__Group_4__0 )? ) )
            // InternalDSL.g:1962:1: ( ( rule__System__Group_4__0 )? )
            {
            // InternalDSL.g:1962:1: ( ( rule__System__Group_4__0 )? )
            // InternalDSL.g:1963:2: ( rule__System__Group_4__0 )?
            {
             before(grammarAccess.getSystemAccess().getGroup_4()); 
            // InternalDSL.g:1964:2: ( rule__System__Group_4__0 )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==69) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalDSL.g:1964:3: rule__System__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__System__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSystemAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__Group__4__Impl"


    // $ANTLR start "rule__System__Group__5"
    // InternalDSL.g:1972:1: rule__System__Group__5 : rule__System__Group__5__Impl ;
    public final void rule__System__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1976:1: ( rule__System__Group__5__Impl )
            // InternalDSL.g:1977:2: rule__System__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__System__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__Group__5"


    // $ANTLR start "rule__System__Group__5__Impl"
    // InternalDSL.g:1983:1: rule__System__Group__5__Impl : ( '}' ) ;
    public final void rule__System__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:1987:1: ( ( '}' ) )
            // InternalDSL.g:1988:1: ( '}' )
            {
            // InternalDSL.g:1988:1: ( '}' )
            // InternalDSL.g:1989:2: '}'
            {
             before(grammarAccess.getSystemAccess().getRightCurlyBracketKeyword_5()); 
            match(input,60,FOLLOW_2); 
             after(grammarAccess.getSystemAccess().getRightCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__Group__5__Impl"


    // $ANTLR start "rule__System__Group_2__0"
    // InternalDSL.g:1999:1: rule__System__Group_2__0 : rule__System__Group_2__0__Impl rule__System__Group_2__1 ;
    public final void rule__System__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2003:1: ( rule__System__Group_2__0__Impl rule__System__Group_2__1 )
            // InternalDSL.g:2004:2: rule__System__Group_2__0__Impl rule__System__Group_2__1
            {
            pushFollow(FOLLOW_15);
            rule__System__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__System__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__Group_2__0"


    // $ANTLR start "rule__System__Group_2__0__Impl"
    // InternalDSL.g:2011:1: rule__System__Group_2__0__Impl : ( 'Attributes:' ) ;
    public final void rule__System__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2015:1: ( ( 'Attributes:' ) )
            // InternalDSL.g:2016:1: ( 'Attributes:' )
            {
            // InternalDSL.g:2016:1: ( 'Attributes:' )
            // InternalDSL.g:2017:2: 'Attributes:'
            {
             before(grammarAccess.getSystemAccess().getAttributesKeyword_2_0()); 
            match(input,67,FOLLOW_2); 
             after(grammarAccess.getSystemAccess().getAttributesKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__Group_2__0__Impl"


    // $ANTLR start "rule__System__Group_2__1"
    // InternalDSL.g:2026:1: rule__System__Group_2__1 : rule__System__Group_2__1__Impl ;
    public final void rule__System__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2030:1: ( rule__System__Group_2__1__Impl )
            // InternalDSL.g:2031:2: rule__System__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__System__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__Group_2__1"


    // $ANTLR start "rule__System__Group_2__1__Impl"
    // InternalDSL.g:2037:1: rule__System__Group_2__1__Impl : ( ( rule__System__AttributesAssignment_2_1 ) ) ;
    public final void rule__System__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2041:1: ( ( ( rule__System__AttributesAssignment_2_1 ) ) )
            // InternalDSL.g:2042:1: ( ( rule__System__AttributesAssignment_2_1 ) )
            {
            // InternalDSL.g:2042:1: ( ( rule__System__AttributesAssignment_2_1 ) )
            // InternalDSL.g:2043:2: ( rule__System__AttributesAssignment_2_1 )
            {
             before(grammarAccess.getSystemAccess().getAttributesAssignment_2_1()); 
            // InternalDSL.g:2044:2: ( rule__System__AttributesAssignment_2_1 )
            // InternalDSL.g:2044:3: rule__System__AttributesAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__System__AttributesAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getSystemAccess().getAttributesAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__Group_2__1__Impl"


    // $ANTLR start "rule__System__Group_3__0"
    // InternalDSL.g:2053:1: rule__System__Group_3__0 : rule__System__Group_3__0__Impl rule__System__Group_3__1 ;
    public final void rule__System__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2057:1: ( rule__System__Group_3__0__Impl rule__System__Group_3__1 )
            // InternalDSL.g:2058:2: rule__System__Group_3__0__Impl rule__System__Group_3__1
            {
            pushFollow(FOLLOW_16);
            rule__System__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__System__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__Group_3__0"


    // $ANTLR start "rule__System__Group_3__0__Impl"
    // InternalDSL.g:2065:1: rule__System__Group_3__0__Impl : ( 'UserType:' ) ;
    public final void rule__System__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2069:1: ( ( 'UserType:' ) )
            // InternalDSL.g:2070:1: ( 'UserType:' )
            {
            // InternalDSL.g:2070:1: ( 'UserType:' )
            // InternalDSL.g:2071:2: 'UserType:'
            {
             before(grammarAccess.getSystemAccess().getUserTypeKeyword_3_0()); 
            match(input,68,FOLLOW_2); 
             after(grammarAccess.getSystemAccess().getUserTypeKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__Group_3__0__Impl"


    // $ANTLR start "rule__System__Group_3__1"
    // InternalDSL.g:2080:1: rule__System__Group_3__1 : rule__System__Group_3__1__Impl ;
    public final void rule__System__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2084:1: ( rule__System__Group_3__1__Impl )
            // InternalDSL.g:2085:2: rule__System__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__System__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__Group_3__1"


    // $ANTLR start "rule__System__Group_3__1__Impl"
    // InternalDSL.g:2091:1: rule__System__Group_3__1__Impl : ( ( rule__System__UserTypeAssignment_3_1 ) ) ;
    public final void rule__System__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2095:1: ( ( ( rule__System__UserTypeAssignment_3_1 ) ) )
            // InternalDSL.g:2096:1: ( ( rule__System__UserTypeAssignment_3_1 ) )
            {
            // InternalDSL.g:2096:1: ( ( rule__System__UserTypeAssignment_3_1 ) )
            // InternalDSL.g:2097:2: ( rule__System__UserTypeAssignment_3_1 )
            {
             before(grammarAccess.getSystemAccess().getUserTypeAssignment_3_1()); 
            // InternalDSL.g:2098:2: ( rule__System__UserTypeAssignment_3_1 )
            // InternalDSL.g:2098:3: rule__System__UserTypeAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__System__UserTypeAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getSystemAccess().getUserTypeAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__Group_3__1__Impl"


    // $ANTLR start "rule__System__Group_4__0"
    // InternalDSL.g:2107:1: rule__System__Group_4__0 : rule__System__Group_4__0__Impl rule__System__Group_4__1 ;
    public final void rule__System__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2111:1: ( rule__System__Group_4__0__Impl rule__System__Group_4__1 )
            // InternalDSL.g:2112:2: rule__System__Group_4__0__Impl rule__System__Group_4__1
            {
            pushFollow(FOLLOW_17);
            rule__System__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__System__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__Group_4__0"


    // $ANTLR start "rule__System__Group_4__0__Impl"
    // InternalDSL.g:2119:1: rule__System__Group_4__0__Impl : ( 'UserAttributes:' ) ;
    public final void rule__System__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2123:1: ( ( 'UserAttributes:' ) )
            // InternalDSL.g:2124:1: ( 'UserAttributes:' )
            {
            // InternalDSL.g:2124:1: ( 'UserAttributes:' )
            // InternalDSL.g:2125:2: 'UserAttributes:'
            {
             before(grammarAccess.getSystemAccess().getUserAttributesKeyword_4_0()); 
            match(input,69,FOLLOW_2); 
             after(grammarAccess.getSystemAccess().getUserAttributesKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__Group_4__0__Impl"


    // $ANTLR start "rule__System__Group_4__1"
    // InternalDSL.g:2134:1: rule__System__Group_4__1 : rule__System__Group_4__1__Impl ;
    public final void rule__System__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2138:1: ( rule__System__Group_4__1__Impl )
            // InternalDSL.g:2139:2: rule__System__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__System__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__Group_4__1"


    // $ANTLR start "rule__System__Group_4__1__Impl"
    // InternalDSL.g:2145:1: rule__System__Group_4__1__Impl : ( ( rule__System__UserAttributesAssignment_4_1 ) ) ;
    public final void rule__System__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2149:1: ( ( ( rule__System__UserAttributesAssignment_4_1 ) ) )
            // InternalDSL.g:2150:1: ( ( rule__System__UserAttributesAssignment_4_1 ) )
            {
            // InternalDSL.g:2150:1: ( ( rule__System__UserAttributesAssignment_4_1 ) )
            // InternalDSL.g:2151:2: ( rule__System__UserAttributesAssignment_4_1 )
            {
             before(grammarAccess.getSystemAccess().getUserAttributesAssignment_4_1()); 
            // InternalDSL.g:2152:2: ( rule__System__UserAttributesAssignment_4_1 )
            // InternalDSL.g:2152:3: rule__System__UserAttributesAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__System__UserAttributesAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getSystemAccess().getUserAttributesAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__Group_4__1__Impl"


    // $ANTLR start "rule__GameDynamic__Group__0"
    // InternalDSL.g:2161:1: rule__GameDynamic__Group__0 : rule__GameDynamic__Group__0__Impl rule__GameDynamic__Group__1 ;
    public final void rule__GameDynamic__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2165:1: ( rule__GameDynamic__Group__0__Impl rule__GameDynamic__Group__1 )
            // InternalDSL.g:2166:2: rule__GameDynamic__Group__0__Impl rule__GameDynamic__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__GameDynamic__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__0"


    // $ANTLR start "rule__GameDynamic__Group__0__Impl"
    // InternalDSL.g:2173:1: rule__GameDynamic__Group__0__Impl : ( 'GameDynamic' ) ;
    public final void rule__GameDynamic__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2177:1: ( ( 'GameDynamic' ) )
            // InternalDSL.g:2178:1: ( 'GameDynamic' )
            {
            // InternalDSL.g:2178:1: ( 'GameDynamic' )
            // InternalDSL.g:2179:2: 'GameDynamic'
            {
             before(grammarAccess.getGameDynamicAccess().getGameDynamicKeyword_0()); 
            match(input,70,FOLLOW_2); 
             after(grammarAccess.getGameDynamicAccess().getGameDynamicKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__0__Impl"


    // $ANTLR start "rule__GameDynamic__Group__1"
    // InternalDSL.g:2188:1: rule__GameDynamic__Group__1 : rule__GameDynamic__Group__1__Impl rule__GameDynamic__Group__2 ;
    public final void rule__GameDynamic__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2192:1: ( rule__GameDynamic__Group__1__Impl rule__GameDynamic__Group__2 )
            // InternalDSL.g:2193:2: rule__GameDynamic__Group__1__Impl rule__GameDynamic__Group__2
            {
            pushFollow(FOLLOW_18);
            rule__GameDynamic__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__1"


    // $ANTLR start "rule__GameDynamic__Group__1__Impl"
    // InternalDSL.g:2200:1: rule__GameDynamic__Group__1__Impl : ( '{' ) ;
    public final void rule__GameDynamic__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2204:1: ( ( '{' ) )
            // InternalDSL.g:2205:1: ( '{' )
            {
            // InternalDSL.g:2205:1: ( '{' )
            // InternalDSL.g:2206:2: '{'
            {
             before(grammarAccess.getGameDynamicAccess().getLeftCurlyBracketKeyword_1()); 
            match(input,58,FOLLOW_2); 
             after(grammarAccess.getGameDynamicAccess().getLeftCurlyBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__1__Impl"


    // $ANTLR start "rule__GameDynamic__Group__2"
    // InternalDSL.g:2215:1: rule__GameDynamic__Group__2 : rule__GameDynamic__Group__2__Impl rule__GameDynamic__Group__3 ;
    public final void rule__GameDynamic__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2219:1: ( rule__GameDynamic__Group__2__Impl rule__GameDynamic__Group__3 )
            // InternalDSL.g:2220:2: rule__GameDynamic__Group__2__Impl rule__GameDynamic__Group__3
            {
            pushFollow(FOLLOW_18);
            rule__GameDynamic__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__2"


    // $ANTLR start "rule__GameDynamic__Group__2__Impl"
    // InternalDSL.g:2227:1: rule__GameDynamic__Group__2__Impl : ( ( rule__GameDynamic__Group_2__0 )? ) ;
    public final void rule__GameDynamic__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2231:1: ( ( ( rule__GameDynamic__Group_2__0 )? ) )
            // InternalDSL.g:2232:1: ( ( rule__GameDynamic__Group_2__0 )? )
            {
            // InternalDSL.g:2232:1: ( ( rule__GameDynamic__Group_2__0 )? )
            // InternalDSL.g:2233:2: ( rule__GameDynamic__Group_2__0 )?
            {
             before(grammarAccess.getGameDynamicAccess().getGroup_2()); 
            // InternalDSL.g:2234:2: ( rule__GameDynamic__Group_2__0 )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==62) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalDSL.g:2234:3: rule__GameDynamic__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__GameDynamic__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getGameDynamicAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__2__Impl"


    // $ANTLR start "rule__GameDynamic__Group__3"
    // InternalDSL.g:2242:1: rule__GameDynamic__Group__3 : rule__GameDynamic__Group__3__Impl rule__GameDynamic__Group__4 ;
    public final void rule__GameDynamic__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2246:1: ( rule__GameDynamic__Group__3__Impl rule__GameDynamic__Group__4 )
            // InternalDSL.g:2247:2: rule__GameDynamic__Group__3__Impl rule__GameDynamic__Group__4
            {
            pushFollow(FOLLOW_18);
            rule__GameDynamic__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__3"


    // $ANTLR start "rule__GameDynamic__Group__3__Impl"
    // InternalDSL.g:2254:1: rule__GameDynamic__Group__3__Impl : ( ( rule__GameDynamic__Group_3__0 )? ) ;
    public final void rule__GameDynamic__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2258:1: ( ( ( rule__GameDynamic__Group_3__0 )? ) )
            // InternalDSL.g:2259:1: ( ( rule__GameDynamic__Group_3__0 )? )
            {
            // InternalDSL.g:2259:1: ( ( rule__GameDynamic__Group_3__0 )? )
            // InternalDSL.g:2260:2: ( rule__GameDynamic__Group_3__0 )?
            {
             before(grammarAccess.getGameDynamicAccess().getGroup_3()); 
            // InternalDSL.g:2261:2: ( rule__GameDynamic__Group_3__0 )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==73) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalDSL.g:2261:3: rule__GameDynamic__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__GameDynamic__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getGameDynamicAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__3__Impl"


    // $ANTLR start "rule__GameDynamic__Group__4"
    // InternalDSL.g:2269:1: rule__GameDynamic__Group__4 : rule__GameDynamic__Group__4__Impl rule__GameDynamic__Group__5 ;
    public final void rule__GameDynamic__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2273:1: ( rule__GameDynamic__Group__4__Impl rule__GameDynamic__Group__5 )
            // InternalDSL.g:2274:2: rule__GameDynamic__Group__4__Impl rule__GameDynamic__Group__5
            {
            pushFollow(FOLLOW_3);
            rule__GameDynamic__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__4"


    // $ANTLR start "rule__GameDynamic__Group__4__Impl"
    // InternalDSL.g:2281:1: rule__GameDynamic__Group__4__Impl : ( 'GameMechanics' ) ;
    public final void rule__GameDynamic__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2285:1: ( ( 'GameMechanics' ) )
            // InternalDSL.g:2286:1: ( 'GameMechanics' )
            {
            // InternalDSL.g:2286:1: ( 'GameMechanics' )
            // InternalDSL.g:2287:2: 'GameMechanics'
            {
             before(grammarAccess.getGameDynamicAccess().getGameMechanicsKeyword_4()); 
            match(input,71,FOLLOW_2); 
             after(grammarAccess.getGameDynamicAccess().getGameMechanicsKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__4__Impl"


    // $ANTLR start "rule__GameDynamic__Group__5"
    // InternalDSL.g:2296:1: rule__GameDynamic__Group__5 : rule__GameDynamic__Group__5__Impl rule__GameDynamic__Group__6 ;
    public final void rule__GameDynamic__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2300:1: ( rule__GameDynamic__Group__5__Impl rule__GameDynamic__Group__6 )
            // InternalDSL.g:2301:2: rule__GameDynamic__Group__5__Impl rule__GameDynamic__Group__6
            {
            pushFollow(FOLLOW_19);
            rule__GameDynamic__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__5"


    // $ANTLR start "rule__GameDynamic__Group__5__Impl"
    // InternalDSL.g:2308:1: rule__GameDynamic__Group__5__Impl : ( '{' ) ;
    public final void rule__GameDynamic__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2312:1: ( ( '{' ) )
            // InternalDSL.g:2313:1: ( '{' )
            {
            // InternalDSL.g:2313:1: ( '{' )
            // InternalDSL.g:2314:2: '{'
            {
             before(grammarAccess.getGameDynamicAccess().getLeftCurlyBracketKeyword_5()); 
            match(input,58,FOLLOW_2); 
             after(grammarAccess.getGameDynamicAccess().getLeftCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__5__Impl"


    // $ANTLR start "rule__GameDynamic__Group__6"
    // InternalDSL.g:2323:1: rule__GameDynamic__Group__6 : rule__GameDynamic__Group__6__Impl rule__GameDynamic__Group__7 ;
    public final void rule__GameDynamic__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2327:1: ( rule__GameDynamic__Group__6__Impl rule__GameDynamic__Group__7 )
            // InternalDSL.g:2328:2: rule__GameDynamic__Group__6__Impl rule__GameDynamic__Group__7
            {
            pushFollow(FOLLOW_6);
            rule__GameDynamic__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__6"


    // $ANTLR start "rule__GameDynamic__Group__6__Impl"
    // InternalDSL.g:2335:1: rule__GameDynamic__Group__6__Impl : ( ( rule__GameDynamic__GamemechanicsAssignment_6 ) ) ;
    public final void rule__GameDynamic__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2339:1: ( ( ( rule__GameDynamic__GamemechanicsAssignment_6 ) ) )
            // InternalDSL.g:2340:1: ( ( rule__GameDynamic__GamemechanicsAssignment_6 ) )
            {
            // InternalDSL.g:2340:1: ( ( rule__GameDynamic__GamemechanicsAssignment_6 ) )
            // InternalDSL.g:2341:2: ( rule__GameDynamic__GamemechanicsAssignment_6 )
            {
             before(grammarAccess.getGameDynamicAccess().getGamemechanicsAssignment_6()); 
            // InternalDSL.g:2342:2: ( rule__GameDynamic__GamemechanicsAssignment_6 )
            // InternalDSL.g:2342:3: rule__GameDynamic__GamemechanicsAssignment_6
            {
            pushFollow(FOLLOW_2);
            rule__GameDynamic__GamemechanicsAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getGameDynamicAccess().getGamemechanicsAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__6__Impl"


    // $ANTLR start "rule__GameDynamic__Group__7"
    // InternalDSL.g:2350:1: rule__GameDynamic__Group__7 : rule__GameDynamic__Group__7__Impl rule__GameDynamic__Group__8 ;
    public final void rule__GameDynamic__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2354:1: ( rule__GameDynamic__Group__7__Impl rule__GameDynamic__Group__8 )
            // InternalDSL.g:2355:2: rule__GameDynamic__Group__7__Impl rule__GameDynamic__Group__8
            {
            pushFollow(FOLLOW_6);
            rule__GameDynamic__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__7"


    // $ANTLR start "rule__GameDynamic__Group__7__Impl"
    // InternalDSL.g:2362:1: rule__GameDynamic__Group__7__Impl : ( ( rule__GameDynamic__Group_7__0 )* ) ;
    public final void rule__GameDynamic__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2366:1: ( ( ( rule__GameDynamic__Group_7__0 )* ) )
            // InternalDSL.g:2367:1: ( ( rule__GameDynamic__Group_7__0 )* )
            {
            // InternalDSL.g:2367:1: ( ( rule__GameDynamic__Group_7__0 )* )
            // InternalDSL.g:2368:2: ( rule__GameDynamic__Group_7__0 )*
            {
             before(grammarAccess.getGameDynamicAccess().getGroup_7()); 
            // InternalDSL.g:2369:2: ( rule__GameDynamic__Group_7__0 )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==64) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // InternalDSL.g:2369:3: rule__GameDynamic__Group_7__0
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__GameDynamic__Group_7__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

             after(grammarAccess.getGameDynamicAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__7__Impl"


    // $ANTLR start "rule__GameDynamic__Group__8"
    // InternalDSL.g:2377:1: rule__GameDynamic__Group__8 : rule__GameDynamic__Group__8__Impl rule__GameDynamic__Group__9 ;
    public final void rule__GameDynamic__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2381:1: ( rule__GameDynamic__Group__8__Impl rule__GameDynamic__Group__9 )
            // InternalDSL.g:2382:2: rule__GameDynamic__Group__8__Impl rule__GameDynamic__Group__9
            {
            pushFollow(FOLLOW_20);
            rule__GameDynamic__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__8"


    // $ANTLR start "rule__GameDynamic__Group__8__Impl"
    // InternalDSL.g:2389:1: rule__GameDynamic__Group__8__Impl : ( '}' ) ;
    public final void rule__GameDynamic__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2393:1: ( ( '}' ) )
            // InternalDSL.g:2394:1: ( '}' )
            {
            // InternalDSL.g:2394:1: ( '}' )
            // InternalDSL.g:2395:2: '}'
            {
             before(grammarAccess.getGameDynamicAccess().getRightCurlyBracketKeyword_8()); 
            match(input,60,FOLLOW_2); 
             after(grammarAccess.getGameDynamicAccess().getRightCurlyBracketKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__8__Impl"


    // $ANTLR start "rule__GameDynamic__Group__9"
    // InternalDSL.g:2404:1: rule__GameDynamic__Group__9 : rule__GameDynamic__Group__9__Impl rule__GameDynamic__Group__10 ;
    public final void rule__GameDynamic__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2408:1: ( rule__GameDynamic__Group__9__Impl rule__GameDynamic__Group__10 )
            // InternalDSL.g:2409:2: rule__GameDynamic__Group__9__Impl rule__GameDynamic__Group__10
            {
            pushFollow(FOLLOW_3);
            rule__GameDynamic__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__9"


    // $ANTLR start "rule__GameDynamic__Group__9__Impl"
    // InternalDSL.g:2416:1: rule__GameDynamic__Group__9__Impl : ( 'Achievements' ) ;
    public final void rule__GameDynamic__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2420:1: ( ( 'Achievements' ) )
            // InternalDSL.g:2421:1: ( 'Achievements' )
            {
            // InternalDSL.g:2421:1: ( 'Achievements' )
            // InternalDSL.g:2422:2: 'Achievements'
            {
             before(grammarAccess.getGameDynamicAccess().getAchievementsKeyword_9()); 
            match(input,72,FOLLOW_2); 
             after(grammarAccess.getGameDynamicAccess().getAchievementsKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__9__Impl"


    // $ANTLR start "rule__GameDynamic__Group__10"
    // InternalDSL.g:2431:1: rule__GameDynamic__Group__10 : rule__GameDynamic__Group__10__Impl rule__GameDynamic__Group__11 ;
    public final void rule__GameDynamic__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2435:1: ( rule__GameDynamic__Group__10__Impl rule__GameDynamic__Group__11 )
            // InternalDSL.g:2436:2: rule__GameDynamic__Group__10__Impl rule__GameDynamic__Group__11
            {
            pushFollow(FOLLOW_21);
            rule__GameDynamic__Group__10__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__10"


    // $ANTLR start "rule__GameDynamic__Group__10__Impl"
    // InternalDSL.g:2443:1: rule__GameDynamic__Group__10__Impl : ( '{' ) ;
    public final void rule__GameDynamic__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2447:1: ( ( '{' ) )
            // InternalDSL.g:2448:1: ( '{' )
            {
            // InternalDSL.g:2448:1: ( '{' )
            // InternalDSL.g:2449:2: '{'
            {
             before(grammarAccess.getGameDynamicAccess().getLeftCurlyBracketKeyword_10()); 
            match(input,58,FOLLOW_2); 
             after(grammarAccess.getGameDynamicAccess().getLeftCurlyBracketKeyword_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__10__Impl"


    // $ANTLR start "rule__GameDynamic__Group__11"
    // InternalDSL.g:2458:1: rule__GameDynamic__Group__11 : rule__GameDynamic__Group__11__Impl rule__GameDynamic__Group__12 ;
    public final void rule__GameDynamic__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2462:1: ( rule__GameDynamic__Group__11__Impl rule__GameDynamic__Group__12 )
            // InternalDSL.g:2463:2: rule__GameDynamic__Group__11__Impl rule__GameDynamic__Group__12
            {
            pushFollow(FOLLOW_6);
            rule__GameDynamic__Group__11__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group__12();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__11"


    // $ANTLR start "rule__GameDynamic__Group__11__Impl"
    // InternalDSL.g:2470:1: rule__GameDynamic__Group__11__Impl : ( ( rule__GameDynamic__AchievementsAssignment_11 ) ) ;
    public final void rule__GameDynamic__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2474:1: ( ( ( rule__GameDynamic__AchievementsAssignment_11 ) ) )
            // InternalDSL.g:2475:1: ( ( rule__GameDynamic__AchievementsAssignment_11 ) )
            {
            // InternalDSL.g:2475:1: ( ( rule__GameDynamic__AchievementsAssignment_11 ) )
            // InternalDSL.g:2476:2: ( rule__GameDynamic__AchievementsAssignment_11 )
            {
             before(grammarAccess.getGameDynamicAccess().getAchievementsAssignment_11()); 
            // InternalDSL.g:2477:2: ( rule__GameDynamic__AchievementsAssignment_11 )
            // InternalDSL.g:2477:3: rule__GameDynamic__AchievementsAssignment_11
            {
            pushFollow(FOLLOW_2);
            rule__GameDynamic__AchievementsAssignment_11();

            state._fsp--;


            }

             after(grammarAccess.getGameDynamicAccess().getAchievementsAssignment_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__11__Impl"


    // $ANTLR start "rule__GameDynamic__Group__12"
    // InternalDSL.g:2485:1: rule__GameDynamic__Group__12 : rule__GameDynamic__Group__12__Impl rule__GameDynamic__Group__13 ;
    public final void rule__GameDynamic__Group__12() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2489:1: ( rule__GameDynamic__Group__12__Impl rule__GameDynamic__Group__13 )
            // InternalDSL.g:2490:2: rule__GameDynamic__Group__12__Impl rule__GameDynamic__Group__13
            {
            pushFollow(FOLLOW_6);
            rule__GameDynamic__Group__12__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group__13();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__12"


    // $ANTLR start "rule__GameDynamic__Group__12__Impl"
    // InternalDSL.g:2497:1: rule__GameDynamic__Group__12__Impl : ( ( rule__GameDynamic__Group_12__0 )* ) ;
    public final void rule__GameDynamic__Group__12__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2501:1: ( ( ( rule__GameDynamic__Group_12__0 )* ) )
            // InternalDSL.g:2502:1: ( ( rule__GameDynamic__Group_12__0 )* )
            {
            // InternalDSL.g:2502:1: ( ( rule__GameDynamic__Group_12__0 )* )
            // InternalDSL.g:2503:2: ( rule__GameDynamic__Group_12__0 )*
            {
             before(grammarAccess.getGameDynamicAccess().getGroup_12()); 
            // InternalDSL.g:2504:2: ( rule__GameDynamic__Group_12__0 )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==64) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalDSL.g:2504:3: rule__GameDynamic__Group_12__0
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__GameDynamic__Group_12__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);

             after(grammarAccess.getGameDynamicAccess().getGroup_12()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__12__Impl"


    // $ANTLR start "rule__GameDynamic__Group__13"
    // InternalDSL.g:2512:1: rule__GameDynamic__Group__13 : rule__GameDynamic__Group__13__Impl rule__GameDynamic__Group__14 ;
    public final void rule__GameDynamic__Group__13() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2516:1: ( rule__GameDynamic__Group__13__Impl rule__GameDynamic__Group__14 )
            // InternalDSL.g:2517:2: rule__GameDynamic__Group__13__Impl rule__GameDynamic__Group__14
            {
            pushFollow(FOLLOW_10);
            rule__GameDynamic__Group__13__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group__14();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__13"


    // $ANTLR start "rule__GameDynamic__Group__13__Impl"
    // InternalDSL.g:2524:1: rule__GameDynamic__Group__13__Impl : ( '}' ) ;
    public final void rule__GameDynamic__Group__13__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2528:1: ( ( '}' ) )
            // InternalDSL.g:2529:1: ( '}' )
            {
            // InternalDSL.g:2529:1: ( '}' )
            // InternalDSL.g:2530:2: '}'
            {
             before(grammarAccess.getGameDynamicAccess().getRightCurlyBracketKeyword_13()); 
            match(input,60,FOLLOW_2); 
             after(grammarAccess.getGameDynamicAccess().getRightCurlyBracketKeyword_13()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__13__Impl"


    // $ANTLR start "rule__GameDynamic__Group__14"
    // InternalDSL.g:2539:1: rule__GameDynamic__Group__14 : rule__GameDynamic__Group__14__Impl rule__GameDynamic__Group__15 ;
    public final void rule__GameDynamic__Group__14() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2543:1: ( rule__GameDynamic__Group__14__Impl rule__GameDynamic__Group__15 )
            // InternalDSL.g:2544:2: rule__GameDynamic__Group__14__Impl rule__GameDynamic__Group__15
            {
            pushFollow(FOLLOW_10);
            rule__GameDynamic__Group__14__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group__15();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__14"


    // $ANTLR start "rule__GameDynamic__Group__14__Impl"
    // InternalDSL.g:2551:1: rule__GameDynamic__Group__14__Impl : ( ( rule__GameDynamic__Group_14__0 )? ) ;
    public final void rule__GameDynamic__Group__14__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2555:1: ( ( ( rule__GameDynamic__Group_14__0 )? ) )
            // InternalDSL.g:2556:1: ( ( rule__GameDynamic__Group_14__0 )? )
            {
            // InternalDSL.g:2556:1: ( ( rule__GameDynamic__Group_14__0 )? )
            // InternalDSL.g:2557:2: ( rule__GameDynamic__Group_14__0 )?
            {
             before(grammarAccess.getGameDynamicAccess().getGroup_14()); 
            // InternalDSL.g:2558:2: ( rule__GameDynamic__Group_14__0 )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==65) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalDSL.g:2558:3: rule__GameDynamic__Group_14__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__GameDynamic__Group_14__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getGameDynamicAccess().getGroup_14()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__14__Impl"


    // $ANTLR start "rule__GameDynamic__Group__15"
    // InternalDSL.g:2566:1: rule__GameDynamic__Group__15 : rule__GameDynamic__Group__15__Impl ;
    public final void rule__GameDynamic__Group__15() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2570:1: ( rule__GameDynamic__Group__15__Impl )
            // InternalDSL.g:2571:2: rule__GameDynamic__Group__15__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group__15__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__15"


    // $ANTLR start "rule__GameDynamic__Group__15__Impl"
    // InternalDSL.g:2577:1: rule__GameDynamic__Group__15__Impl : ( '}' ) ;
    public final void rule__GameDynamic__Group__15__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2581:1: ( ( '}' ) )
            // InternalDSL.g:2582:1: ( '}' )
            {
            // InternalDSL.g:2582:1: ( '}' )
            // InternalDSL.g:2583:2: '}'
            {
             before(grammarAccess.getGameDynamicAccess().getRightCurlyBracketKeyword_15()); 
            match(input,60,FOLLOW_2); 
             after(grammarAccess.getGameDynamicAccess().getRightCurlyBracketKeyword_15()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group__15__Impl"


    // $ANTLR start "rule__GameDynamic__Group_2__0"
    // InternalDSL.g:2593:1: rule__GameDynamic__Group_2__0 : rule__GameDynamic__Group_2__0__Impl rule__GameDynamic__Group_2__1 ;
    public final void rule__GameDynamic__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2597:1: ( rule__GameDynamic__Group_2__0__Impl rule__GameDynamic__Group_2__1 )
            // InternalDSL.g:2598:2: rule__GameDynamic__Group_2__0__Impl rule__GameDynamic__Group_2__1
            {
            pushFollow(FOLLOW_11);
            rule__GameDynamic__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group_2__0"


    // $ANTLR start "rule__GameDynamic__Group_2__0__Impl"
    // InternalDSL.g:2605:1: rule__GameDynamic__Group_2__0__Impl : ( 'Name:' ) ;
    public final void rule__GameDynamic__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2609:1: ( ( 'Name:' ) )
            // InternalDSL.g:2610:1: ( 'Name:' )
            {
            // InternalDSL.g:2610:1: ( 'Name:' )
            // InternalDSL.g:2611:2: 'Name:'
            {
             before(grammarAccess.getGameDynamicAccess().getNameKeyword_2_0()); 
            match(input,62,FOLLOW_2); 
             after(grammarAccess.getGameDynamicAccess().getNameKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group_2__0__Impl"


    // $ANTLR start "rule__GameDynamic__Group_2__1"
    // InternalDSL.g:2620:1: rule__GameDynamic__Group_2__1 : rule__GameDynamic__Group_2__1__Impl ;
    public final void rule__GameDynamic__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2624:1: ( rule__GameDynamic__Group_2__1__Impl )
            // InternalDSL.g:2625:2: rule__GameDynamic__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group_2__1"


    // $ANTLR start "rule__GameDynamic__Group_2__1__Impl"
    // InternalDSL.g:2631:1: rule__GameDynamic__Group_2__1__Impl : ( ( rule__GameDynamic__NameAssignment_2_1 ) ) ;
    public final void rule__GameDynamic__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2635:1: ( ( ( rule__GameDynamic__NameAssignment_2_1 ) ) )
            // InternalDSL.g:2636:1: ( ( rule__GameDynamic__NameAssignment_2_1 ) )
            {
            // InternalDSL.g:2636:1: ( ( rule__GameDynamic__NameAssignment_2_1 ) )
            // InternalDSL.g:2637:2: ( rule__GameDynamic__NameAssignment_2_1 )
            {
             before(grammarAccess.getGameDynamicAccess().getNameAssignment_2_1()); 
            // InternalDSL.g:2638:2: ( rule__GameDynamic__NameAssignment_2_1 )
            // InternalDSL.g:2638:3: rule__GameDynamic__NameAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__GameDynamic__NameAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getGameDynamicAccess().getNameAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group_2__1__Impl"


    // $ANTLR start "rule__GameDynamic__Group_3__0"
    // InternalDSL.g:2647:1: rule__GameDynamic__Group_3__0 : rule__GameDynamic__Group_3__0__Impl rule__GameDynamic__Group_3__1 ;
    public final void rule__GameDynamic__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2651:1: ( rule__GameDynamic__Group_3__0__Impl rule__GameDynamic__Group_3__1 )
            // InternalDSL.g:2652:2: rule__GameDynamic__Group_3__0__Impl rule__GameDynamic__Group_3__1
            {
            pushFollow(FOLLOW_22);
            rule__GameDynamic__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group_3__0"


    // $ANTLR start "rule__GameDynamic__Group_3__0__Impl"
    // InternalDSL.g:2659:1: rule__GameDynamic__Group_3__0__Impl : ( 'DynamicType:' ) ;
    public final void rule__GameDynamic__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2663:1: ( ( 'DynamicType:' ) )
            // InternalDSL.g:2664:1: ( 'DynamicType:' )
            {
            // InternalDSL.g:2664:1: ( 'DynamicType:' )
            // InternalDSL.g:2665:2: 'DynamicType:'
            {
             before(grammarAccess.getGameDynamicAccess().getDynamicTypeKeyword_3_0()); 
            match(input,73,FOLLOW_2); 
             after(grammarAccess.getGameDynamicAccess().getDynamicTypeKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group_3__0__Impl"


    // $ANTLR start "rule__GameDynamic__Group_3__1"
    // InternalDSL.g:2674:1: rule__GameDynamic__Group_3__1 : rule__GameDynamic__Group_3__1__Impl ;
    public final void rule__GameDynamic__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2678:1: ( rule__GameDynamic__Group_3__1__Impl )
            // InternalDSL.g:2679:2: rule__GameDynamic__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group_3__1"


    // $ANTLR start "rule__GameDynamic__Group_3__1__Impl"
    // InternalDSL.g:2685:1: rule__GameDynamic__Group_3__1__Impl : ( ( rule__GameDynamic__DynamicTypeAssignment_3_1 ) ) ;
    public final void rule__GameDynamic__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2689:1: ( ( ( rule__GameDynamic__DynamicTypeAssignment_3_1 ) ) )
            // InternalDSL.g:2690:1: ( ( rule__GameDynamic__DynamicTypeAssignment_3_1 ) )
            {
            // InternalDSL.g:2690:1: ( ( rule__GameDynamic__DynamicTypeAssignment_3_1 ) )
            // InternalDSL.g:2691:2: ( rule__GameDynamic__DynamicTypeAssignment_3_1 )
            {
             before(grammarAccess.getGameDynamicAccess().getDynamicTypeAssignment_3_1()); 
            // InternalDSL.g:2692:2: ( rule__GameDynamic__DynamicTypeAssignment_3_1 )
            // InternalDSL.g:2692:3: rule__GameDynamic__DynamicTypeAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__GameDynamic__DynamicTypeAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getGameDynamicAccess().getDynamicTypeAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group_3__1__Impl"


    // $ANTLR start "rule__GameDynamic__Group_7__0"
    // InternalDSL.g:2701:1: rule__GameDynamic__Group_7__0 : rule__GameDynamic__Group_7__0__Impl rule__GameDynamic__Group_7__1 ;
    public final void rule__GameDynamic__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2705:1: ( rule__GameDynamic__Group_7__0__Impl rule__GameDynamic__Group_7__1 )
            // InternalDSL.g:2706:2: rule__GameDynamic__Group_7__0__Impl rule__GameDynamic__Group_7__1
            {
            pushFollow(FOLLOW_19);
            rule__GameDynamic__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group_7__0"


    // $ANTLR start "rule__GameDynamic__Group_7__0__Impl"
    // InternalDSL.g:2713:1: rule__GameDynamic__Group_7__0__Impl : ( ',' ) ;
    public final void rule__GameDynamic__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2717:1: ( ( ',' ) )
            // InternalDSL.g:2718:1: ( ',' )
            {
            // InternalDSL.g:2718:1: ( ',' )
            // InternalDSL.g:2719:2: ','
            {
             before(grammarAccess.getGameDynamicAccess().getCommaKeyword_7_0()); 
            match(input,64,FOLLOW_2); 
             after(grammarAccess.getGameDynamicAccess().getCommaKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group_7__0__Impl"


    // $ANTLR start "rule__GameDynamic__Group_7__1"
    // InternalDSL.g:2728:1: rule__GameDynamic__Group_7__1 : rule__GameDynamic__Group_7__1__Impl ;
    public final void rule__GameDynamic__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2732:1: ( rule__GameDynamic__Group_7__1__Impl )
            // InternalDSL.g:2733:2: rule__GameDynamic__Group_7__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group_7__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group_7__1"


    // $ANTLR start "rule__GameDynamic__Group_7__1__Impl"
    // InternalDSL.g:2739:1: rule__GameDynamic__Group_7__1__Impl : ( ( rule__GameDynamic__GamemechanicsAssignment_7_1 ) ) ;
    public final void rule__GameDynamic__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2743:1: ( ( ( rule__GameDynamic__GamemechanicsAssignment_7_1 ) ) )
            // InternalDSL.g:2744:1: ( ( rule__GameDynamic__GamemechanicsAssignment_7_1 ) )
            {
            // InternalDSL.g:2744:1: ( ( rule__GameDynamic__GamemechanicsAssignment_7_1 ) )
            // InternalDSL.g:2745:2: ( rule__GameDynamic__GamemechanicsAssignment_7_1 )
            {
             before(grammarAccess.getGameDynamicAccess().getGamemechanicsAssignment_7_1()); 
            // InternalDSL.g:2746:2: ( rule__GameDynamic__GamemechanicsAssignment_7_1 )
            // InternalDSL.g:2746:3: rule__GameDynamic__GamemechanicsAssignment_7_1
            {
            pushFollow(FOLLOW_2);
            rule__GameDynamic__GamemechanicsAssignment_7_1();

            state._fsp--;


            }

             after(grammarAccess.getGameDynamicAccess().getGamemechanicsAssignment_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group_7__1__Impl"


    // $ANTLR start "rule__GameDynamic__Group_12__0"
    // InternalDSL.g:2755:1: rule__GameDynamic__Group_12__0 : rule__GameDynamic__Group_12__0__Impl rule__GameDynamic__Group_12__1 ;
    public final void rule__GameDynamic__Group_12__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2759:1: ( rule__GameDynamic__Group_12__0__Impl rule__GameDynamic__Group_12__1 )
            // InternalDSL.g:2760:2: rule__GameDynamic__Group_12__0__Impl rule__GameDynamic__Group_12__1
            {
            pushFollow(FOLLOW_21);
            rule__GameDynamic__Group_12__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group_12__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group_12__0"


    // $ANTLR start "rule__GameDynamic__Group_12__0__Impl"
    // InternalDSL.g:2767:1: rule__GameDynamic__Group_12__0__Impl : ( ',' ) ;
    public final void rule__GameDynamic__Group_12__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2771:1: ( ( ',' ) )
            // InternalDSL.g:2772:1: ( ',' )
            {
            // InternalDSL.g:2772:1: ( ',' )
            // InternalDSL.g:2773:2: ','
            {
             before(grammarAccess.getGameDynamicAccess().getCommaKeyword_12_0()); 
            match(input,64,FOLLOW_2); 
             after(grammarAccess.getGameDynamicAccess().getCommaKeyword_12_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group_12__0__Impl"


    // $ANTLR start "rule__GameDynamic__Group_12__1"
    // InternalDSL.g:2782:1: rule__GameDynamic__Group_12__1 : rule__GameDynamic__Group_12__1__Impl ;
    public final void rule__GameDynamic__Group_12__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2786:1: ( rule__GameDynamic__Group_12__1__Impl )
            // InternalDSL.g:2787:2: rule__GameDynamic__Group_12__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group_12__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group_12__1"


    // $ANTLR start "rule__GameDynamic__Group_12__1__Impl"
    // InternalDSL.g:2793:1: rule__GameDynamic__Group_12__1__Impl : ( ( rule__GameDynamic__AchievementsAssignment_12_1 ) ) ;
    public final void rule__GameDynamic__Group_12__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2797:1: ( ( ( rule__GameDynamic__AchievementsAssignment_12_1 ) ) )
            // InternalDSL.g:2798:1: ( ( rule__GameDynamic__AchievementsAssignment_12_1 ) )
            {
            // InternalDSL.g:2798:1: ( ( rule__GameDynamic__AchievementsAssignment_12_1 ) )
            // InternalDSL.g:2799:2: ( rule__GameDynamic__AchievementsAssignment_12_1 )
            {
             before(grammarAccess.getGameDynamicAccess().getAchievementsAssignment_12_1()); 
            // InternalDSL.g:2800:2: ( rule__GameDynamic__AchievementsAssignment_12_1 )
            // InternalDSL.g:2800:3: rule__GameDynamic__AchievementsAssignment_12_1
            {
            pushFollow(FOLLOW_2);
            rule__GameDynamic__AchievementsAssignment_12_1();

            state._fsp--;


            }

             after(grammarAccess.getGameDynamicAccess().getAchievementsAssignment_12_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group_12__1__Impl"


    // $ANTLR start "rule__GameDynamic__Group_14__0"
    // InternalDSL.g:2809:1: rule__GameDynamic__Group_14__0 : rule__GameDynamic__Group_14__0__Impl rule__GameDynamic__Group_14__1 ;
    public final void rule__GameDynamic__Group_14__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2813:1: ( rule__GameDynamic__Group_14__0__Impl rule__GameDynamic__Group_14__1 )
            // InternalDSL.g:2814:2: rule__GameDynamic__Group_14__0__Impl rule__GameDynamic__Group_14__1
            {
            pushFollow(FOLLOW_12);
            rule__GameDynamic__Group_14__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group_14__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group_14__0"


    // $ANTLR start "rule__GameDynamic__Group_14__0__Impl"
    // InternalDSL.g:2821:1: rule__GameDynamic__Group_14__0__Impl : ( 'Information:' ) ;
    public final void rule__GameDynamic__Group_14__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2825:1: ( ( 'Information:' ) )
            // InternalDSL.g:2826:1: ( 'Information:' )
            {
            // InternalDSL.g:2826:1: ( 'Information:' )
            // InternalDSL.g:2827:2: 'Information:'
            {
             before(grammarAccess.getGameDynamicAccess().getInformationKeyword_14_0()); 
            match(input,65,FOLLOW_2); 
             after(grammarAccess.getGameDynamicAccess().getInformationKeyword_14_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group_14__0__Impl"


    // $ANTLR start "rule__GameDynamic__Group_14__1"
    // InternalDSL.g:2836:1: rule__GameDynamic__Group_14__1 : rule__GameDynamic__Group_14__1__Impl ;
    public final void rule__GameDynamic__Group_14__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2840:1: ( rule__GameDynamic__Group_14__1__Impl )
            // InternalDSL.g:2841:2: rule__GameDynamic__Group_14__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GameDynamic__Group_14__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group_14__1"


    // $ANTLR start "rule__GameDynamic__Group_14__1__Impl"
    // InternalDSL.g:2847:1: rule__GameDynamic__Group_14__1__Impl : ( ( rule__GameDynamic__InformationAssignment_14_1 ) ) ;
    public final void rule__GameDynamic__Group_14__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2851:1: ( ( ( rule__GameDynamic__InformationAssignment_14_1 ) ) )
            // InternalDSL.g:2852:1: ( ( rule__GameDynamic__InformationAssignment_14_1 ) )
            {
            // InternalDSL.g:2852:1: ( ( rule__GameDynamic__InformationAssignment_14_1 ) )
            // InternalDSL.g:2853:2: ( rule__GameDynamic__InformationAssignment_14_1 )
            {
             before(grammarAccess.getGameDynamicAccess().getInformationAssignment_14_1()); 
            // InternalDSL.g:2854:2: ( rule__GameDynamic__InformationAssignment_14_1 )
            // InternalDSL.g:2854:3: rule__GameDynamic__InformationAssignment_14_1
            {
            pushFollow(FOLLOW_2);
            rule__GameDynamic__InformationAssignment_14_1();

            state._fsp--;


            }

             after(grammarAccess.getGameDynamicAccess().getInformationAssignment_14_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__Group_14__1__Impl"


    // $ANTLR start "rule__GameMechanic__Group__0"
    // InternalDSL.g:2863:1: rule__GameMechanic__Group__0 : rule__GameMechanic__Group__0__Impl rule__GameMechanic__Group__1 ;
    public final void rule__GameMechanic__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2867:1: ( rule__GameMechanic__Group__0__Impl rule__GameMechanic__Group__1 )
            // InternalDSL.g:2868:2: rule__GameMechanic__Group__0__Impl rule__GameMechanic__Group__1
            {
            pushFollow(FOLLOW_19);
            rule__GameMechanic__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameMechanic__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group__0"


    // $ANTLR start "rule__GameMechanic__Group__0__Impl"
    // InternalDSL.g:2875:1: rule__GameMechanic__Group__0__Impl : ( () ) ;
    public final void rule__GameMechanic__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2879:1: ( ( () ) )
            // InternalDSL.g:2880:1: ( () )
            {
            // InternalDSL.g:2880:1: ( () )
            // InternalDSL.g:2881:2: ()
            {
             before(grammarAccess.getGameMechanicAccess().getGameMechanicAction_0()); 
            // InternalDSL.g:2882:2: ()
            // InternalDSL.g:2882:3: 
            {
            }

             after(grammarAccess.getGameMechanicAccess().getGameMechanicAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group__0__Impl"


    // $ANTLR start "rule__GameMechanic__Group__1"
    // InternalDSL.g:2890:1: rule__GameMechanic__Group__1 : rule__GameMechanic__Group__1__Impl rule__GameMechanic__Group__2 ;
    public final void rule__GameMechanic__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2894:1: ( rule__GameMechanic__Group__1__Impl rule__GameMechanic__Group__2 )
            // InternalDSL.g:2895:2: rule__GameMechanic__Group__1__Impl rule__GameMechanic__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__GameMechanic__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameMechanic__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group__1"


    // $ANTLR start "rule__GameMechanic__Group__1__Impl"
    // InternalDSL.g:2902:1: rule__GameMechanic__Group__1__Impl : ( 'GameMechanic' ) ;
    public final void rule__GameMechanic__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2906:1: ( ( 'GameMechanic' ) )
            // InternalDSL.g:2907:1: ( 'GameMechanic' )
            {
            // InternalDSL.g:2907:1: ( 'GameMechanic' )
            // InternalDSL.g:2908:2: 'GameMechanic'
            {
             before(grammarAccess.getGameMechanicAccess().getGameMechanicKeyword_1()); 
            match(input,74,FOLLOW_2); 
             after(grammarAccess.getGameMechanicAccess().getGameMechanicKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group__1__Impl"


    // $ANTLR start "rule__GameMechanic__Group__2"
    // InternalDSL.g:2917:1: rule__GameMechanic__Group__2 : rule__GameMechanic__Group__2__Impl rule__GameMechanic__Group__3 ;
    public final void rule__GameMechanic__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2921:1: ( rule__GameMechanic__Group__2__Impl rule__GameMechanic__Group__3 )
            // InternalDSL.g:2922:2: rule__GameMechanic__Group__2__Impl rule__GameMechanic__Group__3
            {
            pushFollow(FOLLOW_23);
            rule__GameMechanic__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameMechanic__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group__2"


    // $ANTLR start "rule__GameMechanic__Group__2__Impl"
    // InternalDSL.g:2929:1: rule__GameMechanic__Group__2__Impl : ( '{' ) ;
    public final void rule__GameMechanic__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2933:1: ( ( '{' ) )
            // InternalDSL.g:2934:1: ( '{' )
            {
            // InternalDSL.g:2934:1: ( '{' )
            // InternalDSL.g:2935:2: '{'
            {
             before(grammarAccess.getGameMechanicAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,58,FOLLOW_2); 
             after(grammarAccess.getGameMechanicAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group__2__Impl"


    // $ANTLR start "rule__GameMechanic__Group__3"
    // InternalDSL.g:2944:1: rule__GameMechanic__Group__3 : rule__GameMechanic__Group__3__Impl rule__GameMechanic__Group__4 ;
    public final void rule__GameMechanic__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2948:1: ( rule__GameMechanic__Group__3__Impl rule__GameMechanic__Group__4 )
            // InternalDSL.g:2949:2: rule__GameMechanic__Group__3__Impl rule__GameMechanic__Group__4
            {
            pushFollow(FOLLOW_23);
            rule__GameMechanic__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameMechanic__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group__3"


    // $ANTLR start "rule__GameMechanic__Group__3__Impl"
    // InternalDSL.g:2956:1: rule__GameMechanic__Group__3__Impl : ( ( rule__GameMechanic__Group_3__0 )? ) ;
    public final void rule__GameMechanic__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2960:1: ( ( ( rule__GameMechanic__Group_3__0 )? ) )
            // InternalDSL.g:2961:1: ( ( rule__GameMechanic__Group_3__0 )? )
            {
            // InternalDSL.g:2961:1: ( ( rule__GameMechanic__Group_3__0 )? )
            // InternalDSL.g:2962:2: ( rule__GameMechanic__Group_3__0 )?
            {
             before(grammarAccess.getGameMechanicAccess().getGroup_3()); 
            // InternalDSL.g:2963:2: ( rule__GameMechanic__Group_3__0 )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==62) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalDSL.g:2963:3: rule__GameMechanic__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__GameMechanic__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getGameMechanicAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group__3__Impl"


    // $ANTLR start "rule__GameMechanic__Group__4"
    // InternalDSL.g:2971:1: rule__GameMechanic__Group__4 : rule__GameMechanic__Group__4__Impl rule__GameMechanic__Group__5 ;
    public final void rule__GameMechanic__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2975:1: ( rule__GameMechanic__Group__4__Impl rule__GameMechanic__Group__5 )
            // InternalDSL.g:2976:2: rule__GameMechanic__Group__4__Impl rule__GameMechanic__Group__5
            {
            pushFollow(FOLLOW_23);
            rule__GameMechanic__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameMechanic__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group__4"


    // $ANTLR start "rule__GameMechanic__Group__4__Impl"
    // InternalDSL.g:2983:1: rule__GameMechanic__Group__4__Impl : ( ( rule__GameMechanic__Group_4__0 )? ) ;
    public final void rule__GameMechanic__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:2987:1: ( ( ( rule__GameMechanic__Group_4__0 )? ) )
            // InternalDSL.g:2988:1: ( ( rule__GameMechanic__Group_4__0 )? )
            {
            // InternalDSL.g:2988:1: ( ( rule__GameMechanic__Group_4__0 )? )
            // InternalDSL.g:2989:2: ( rule__GameMechanic__Group_4__0 )?
            {
             before(grammarAccess.getGameMechanicAccess().getGroup_4()); 
            // InternalDSL.g:2990:2: ( rule__GameMechanic__Group_4__0 )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==75) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalDSL.g:2990:3: rule__GameMechanic__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__GameMechanic__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getGameMechanicAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group__4__Impl"


    // $ANTLR start "rule__GameMechanic__Group__5"
    // InternalDSL.g:2998:1: rule__GameMechanic__Group__5 : rule__GameMechanic__Group__5__Impl rule__GameMechanic__Group__6 ;
    public final void rule__GameMechanic__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3002:1: ( rule__GameMechanic__Group__5__Impl rule__GameMechanic__Group__6 )
            // InternalDSL.g:3003:2: rule__GameMechanic__Group__5__Impl rule__GameMechanic__Group__6
            {
            pushFollow(FOLLOW_23);
            rule__GameMechanic__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameMechanic__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group__5"


    // $ANTLR start "rule__GameMechanic__Group__5__Impl"
    // InternalDSL.g:3010:1: rule__GameMechanic__Group__5__Impl : ( ( rule__GameMechanic__Group_5__0 )? ) ;
    public final void rule__GameMechanic__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3014:1: ( ( ( rule__GameMechanic__Group_5__0 )? ) )
            // InternalDSL.g:3015:1: ( ( rule__GameMechanic__Group_5__0 )? )
            {
            // InternalDSL.g:3015:1: ( ( rule__GameMechanic__Group_5__0 )? )
            // InternalDSL.g:3016:2: ( rule__GameMechanic__Group_5__0 )?
            {
             before(grammarAccess.getGameMechanicAccess().getGroup_5()); 
            // InternalDSL.g:3017:2: ( rule__GameMechanic__Group_5__0 )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==76) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalDSL.g:3017:3: rule__GameMechanic__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__GameMechanic__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getGameMechanicAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group__5__Impl"


    // $ANTLR start "rule__GameMechanic__Group__6"
    // InternalDSL.g:3025:1: rule__GameMechanic__Group__6 : rule__GameMechanic__Group__6__Impl rule__GameMechanic__Group__7 ;
    public final void rule__GameMechanic__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3029:1: ( rule__GameMechanic__Group__6__Impl rule__GameMechanic__Group__7 )
            // InternalDSL.g:3030:2: rule__GameMechanic__Group__6__Impl rule__GameMechanic__Group__7
            {
            pushFollow(FOLLOW_23);
            rule__GameMechanic__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameMechanic__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group__6"


    // $ANTLR start "rule__GameMechanic__Group__6__Impl"
    // InternalDSL.g:3037:1: rule__GameMechanic__Group__6__Impl : ( ( rule__GameMechanic__Group_6__0 )? ) ;
    public final void rule__GameMechanic__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3041:1: ( ( ( rule__GameMechanic__Group_6__0 )? ) )
            // InternalDSL.g:3042:1: ( ( rule__GameMechanic__Group_6__0 )? )
            {
            // InternalDSL.g:3042:1: ( ( rule__GameMechanic__Group_6__0 )? )
            // InternalDSL.g:3043:2: ( rule__GameMechanic__Group_6__0 )?
            {
             before(grammarAccess.getGameMechanicAccess().getGroup_6()); 
            // InternalDSL.g:3044:2: ( rule__GameMechanic__Group_6__0 )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==65) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalDSL.g:3044:3: rule__GameMechanic__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__GameMechanic__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getGameMechanicAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group__6__Impl"


    // $ANTLR start "rule__GameMechanic__Group__7"
    // InternalDSL.g:3052:1: rule__GameMechanic__Group__7 : rule__GameMechanic__Group__7__Impl ;
    public final void rule__GameMechanic__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3056:1: ( rule__GameMechanic__Group__7__Impl )
            // InternalDSL.g:3057:2: rule__GameMechanic__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GameMechanic__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group__7"


    // $ANTLR start "rule__GameMechanic__Group__7__Impl"
    // InternalDSL.g:3063:1: rule__GameMechanic__Group__7__Impl : ( '}' ) ;
    public final void rule__GameMechanic__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3067:1: ( ( '}' ) )
            // InternalDSL.g:3068:1: ( '}' )
            {
            // InternalDSL.g:3068:1: ( '}' )
            // InternalDSL.g:3069:2: '}'
            {
             before(grammarAccess.getGameMechanicAccess().getRightCurlyBracketKeyword_7()); 
            match(input,60,FOLLOW_2); 
             after(grammarAccess.getGameMechanicAccess().getRightCurlyBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group__7__Impl"


    // $ANTLR start "rule__GameMechanic__Group_3__0"
    // InternalDSL.g:3079:1: rule__GameMechanic__Group_3__0 : rule__GameMechanic__Group_3__0__Impl rule__GameMechanic__Group_3__1 ;
    public final void rule__GameMechanic__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3083:1: ( rule__GameMechanic__Group_3__0__Impl rule__GameMechanic__Group_3__1 )
            // InternalDSL.g:3084:2: rule__GameMechanic__Group_3__0__Impl rule__GameMechanic__Group_3__1
            {
            pushFollow(FOLLOW_11);
            rule__GameMechanic__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameMechanic__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_3__0"


    // $ANTLR start "rule__GameMechanic__Group_3__0__Impl"
    // InternalDSL.g:3091:1: rule__GameMechanic__Group_3__0__Impl : ( 'Name:' ) ;
    public final void rule__GameMechanic__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3095:1: ( ( 'Name:' ) )
            // InternalDSL.g:3096:1: ( 'Name:' )
            {
            // InternalDSL.g:3096:1: ( 'Name:' )
            // InternalDSL.g:3097:2: 'Name:'
            {
             before(grammarAccess.getGameMechanicAccess().getNameKeyword_3_0()); 
            match(input,62,FOLLOW_2); 
             after(grammarAccess.getGameMechanicAccess().getNameKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_3__0__Impl"


    // $ANTLR start "rule__GameMechanic__Group_3__1"
    // InternalDSL.g:3106:1: rule__GameMechanic__Group_3__1 : rule__GameMechanic__Group_3__1__Impl ;
    public final void rule__GameMechanic__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3110:1: ( rule__GameMechanic__Group_3__1__Impl )
            // InternalDSL.g:3111:2: rule__GameMechanic__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GameMechanic__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_3__1"


    // $ANTLR start "rule__GameMechanic__Group_3__1__Impl"
    // InternalDSL.g:3117:1: rule__GameMechanic__Group_3__1__Impl : ( ( rule__GameMechanic__NameAssignment_3_1 ) ) ;
    public final void rule__GameMechanic__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3121:1: ( ( ( rule__GameMechanic__NameAssignment_3_1 ) ) )
            // InternalDSL.g:3122:1: ( ( rule__GameMechanic__NameAssignment_3_1 ) )
            {
            // InternalDSL.g:3122:1: ( ( rule__GameMechanic__NameAssignment_3_1 ) )
            // InternalDSL.g:3123:2: ( rule__GameMechanic__NameAssignment_3_1 )
            {
             before(grammarAccess.getGameMechanicAccess().getNameAssignment_3_1()); 
            // InternalDSL.g:3124:2: ( rule__GameMechanic__NameAssignment_3_1 )
            // InternalDSL.g:3124:3: rule__GameMechanic__NameAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__GameMechanic__NameAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getGameMechanicAccess().getNameAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_3__1__Impl"


    // $ANTLR start "rule__GameMechanic__Group_4__0"
    // InternalDSL.g:3133:1: rule__GameMechanic__Group_4__0 : rule__GameMechanic__Group_4__0__Impl rule__GameMechanic__Group_4__1 ;
    public final void rule__GameMechanic__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3137:1: ( rule__GameMechanic__Group_4__0__Impl rule__GameMechanic__Group_4__1 )
            // InternalDSL.g:3138:2: rule__GameMechanic__Group_4__0__Impl rule__GameMechanic__Group_4__1
            {
            pushFollow(FOLLOW_24);
            rule__GameMechanic__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameMechanic__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_4__0"


    // $ANTLR start "rule__GameMechanic__Group_4__0__Impl"
    // InternalDSL.g:3145:1: rule__GameMechanic__Group_4__0__Impl : ( 'MechanicType:' ) ;
    public final void rule__GameMechanic__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3149:1: ( ( 'MechanicType:' ) )
            // InternalDSL.g:3150:1: ( 'MechanicType:' )
            {
            // InternalDSL.g:3150:1: ( 'MechanicType:' )
            // InternalDSL.g:3151:2: 'MechanicType:'
            {
             before(grammarAccess.getGameMechanicAccess().getMechanicTypeKeyword_4_0()); 
            match(input,75,FOLLOW_2); 
             after(grammarAccess.getGameMechanicAccess().getMechanicTypeKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_4__0__Impl"


    // $ANTLR start "rule__GameMechanic__Group_4__1"
    // InternalDSL.g:3160:1: rule__GameMechanic__Group_4__1 : rule__GameMechanic__Group_4__1__Impl ;
    public final void rule__GameMechanic__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3164:1: ( rule__GameMechanic__Group_4__1__Impl )
            // InternalDSL.g:3165:2: rule__GameMechanic__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GameMechanic__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_4__1"


    // $ANTLR start "rule__GameMechanic__Group_4__1__Impl"
    // InternalDSL.g:3171:1: rule__GameMechanic__Group_4__1__Impl : ( ( rule__GameMechanic__MechanicTypeAssignment_4_1 ) ) ;
    public final void rule__GameMechanic__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3175:1: ( ( ( rule__GameMechanic__MechanicTypeAssignment_4_1 ) ) )
            // InternalDSL.g:3176:1: ( ( rule__GameMechanic__MechanicTypeAssignment_4_1 ) )
            {
            // InternalDSL.g:3176:1: ( ( rule__GameMechanic__MechanicTypeAssignment_4_1 ) )
            // InternalDSL.g:3177:2: ( rule__GameMechanic__MechanicTypeAssignment_4_1 )
            {
             before(grammarAccess.getGameMechanicAccess().getMechanicTypeAssignment_4_1()); 
            // InternalDSL.g:3178:2: ( rule__GameMechanic__MechanicTypeAssignment_4_1 )
            // InternalDSL.g:3178:3: rule__GameMechanic__MechanicTypeAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__GameMechanic__MechanicTypeAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getGameMechanicAccess().getMechanicTypeAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_4__1__Impl"


    // $ANTLR start "rule__GameMechanic__Group_5__0"
    // InternalDSL.g:3187:1: rule__GameMechanic__Group_5__0 : rule__GameMechanic__Group_5__0__Impl rule__GameMechanic__Group_5__1 ;
    public final void rule__GameMechanic__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3191:1: ( rule__GameMechanic__Group_5__0__Impl rule__GameMechanic__Group_5__1 )
            // InternalDSL.g:3192:2: rule__GameMechanic__Group_5__0__Impl rule__GameMechanic__Group_5__1
            {
            pushFollow(FOLLOW_3);
            rule__GameMechanic__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameMechanic__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_5__0"


    // $ANTLR start "rule__GameMechanic__Group_5__0__Impl"
    // InternalDSL.g:3199:1: rule__GameMechanic__Group_5__0__Impl : ( 'Events' ) ;
    public final void rule__GameMechanic__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3203:1: ( ( 'Events' ) )
            // InternalDSL.g:3204:1: ( 'Events' )
            {
            // InternalDSL.g:3204:1: ( 'Events' )
            // InternalDSL.g:3205:2: 'Events'
            {
             before(grammarAccess.getGameMechanicAccess().getEventsKeyword_5_0()); 
            match(input,76,FOLLOW_2); 
             after(grammarAccess.getGameMechanicAccess().getEventsKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_5__0__Impl"


    // $ANTLR start "rule__GameMechanic__Group_5__1"
    // InternalDSL.g:3214:1: rule__GameMechanic__Group_5__1 : rule__GameMechanic__Group_5__1__Impl rule__GameMechanic__Group_5__2 ;
    public final void rule__GameMechanic__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3218:1: ( rule__GameMechanic__Group_5__1__Impl rule__GameMechanic__Group_5__2 )
            // InternalDSL.g:3219:2: rule__GameMechanic__Group_5__1__Impl rule__GameMechanic__Group_5__2
            {
            pushFollow(FOLLOW_25);
            rule__GameMechanic__Group_5__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameMechanic__Group_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_5__1"


    // $ANTLR start "rule__GameMechanic__Group_5__1__Impl"
    // InternalDSL.g:3226:1: rule__GameMechanic__Group_5__1__Impl : ( '{' ) ;
    public final void rule__GameMechanic__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3230:1: ( ( '{' ) )
            // InternalDSL.g:3231:1: ( '{' )
            {
            // InternalDSL.g:3231:1: ( '{' )
            // InternalDSL.g:3232:2: '{'
            {
             before(grammarAccess.getGameMechanicAccess().getLeftCurlyBracketKeyword_5_1()); 
            match(input,58,FOLLOW_2); 
             after(grammarAccess.getGameMechanicAccess().getLeftCurlyBracketKeyword_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_5__1__Impl"


    // $ANTLR start "rule__GameMechanic__Group_5__2"
    // InternalDSL.g:3241:1: rule__GameMechanic__Group_5__2 : rule__GameMechanic__Group_5__2__Impl rule__GameMechanic__Group_5__3 ;
    public final void rule__GameMechanic__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3245:1: ( rule__GameMechanic__Group_5__2__Impl rule__GameMechanic__Group_5__3 )
            // InternalDSL.g:3246:2: rule__GameMechanic__Group_5__2__Impl rule__GameMechanic__Group_5__3
            {
            pushFollow(FOLLOW_6);
            rule__GameMechanic__Group_5__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameMechanic__Group_5__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_5__2"


    // $ANTLR start "rule__GameMechanic__Group_5__2__Impl"
    // InternalDSL.g:3253:1: rule__GameMechanic__Group_5__2__Impl : ( ( rule__GameMechanic__EventsAssignment_5_2 ) ) ;
    public final void rule__GameMechanic__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3257:1: ( ( ( rule__GameMechanic__EventsAssignment_5_2 ) ) )
            // InternalDSL.g:3258:1: ( ( rule__GameMechanic__EventsAssignment_5_2 ) )
            {
            // InternalDSL.g:3258:1: ( ( rule__GameMechanic__EventsAssignment_5_2 ) )
            // InternalDSL.g:3259:2: ( rule__GameMechanic__EventsAssignment_5_2 )
            {
             before(grammarAccess.getGameMechanicAccess().getEventsAssignment_5_2()); 
            // InternalDSL.g:3260:2: ( rule__GameMechanic__EventsAssignment_5_2 )
            // InternalDSL.g:3260:3: rule__GameMechanic__EventsAssignment_5_2
            {
            pushFollow(FOLLOW_2);
            rule__GameMechanic__EventsAssignment_5_2();

            state._fsp--;


            }

             after(grammarAccess.getGameMechanicAccess().getEventsAssignment_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_5__2__Impl"


    // $ANTLR start "rule__GameMechanic__Group_5__3"
    // InternalDSL.g:3268:1: rule__GameMechanic__Group_5__3 : rule__GameMechanic__Group_5__3__Impl rule__GameMechanic__Group_5__4 ;
    public final void rule__GameMechanic__Group_5__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3272:1: ( rule__GameMechanic__Group_5__3__Impl rule__GameMechanic__Group_5__4 )
            // InternalDSL.g:3273:2: rule__GameMechanic__Group_5__3__Impl rule__GameMechanic__Group_5__4
            {
            pushFollow(FOLLOW_6);
            rule__GameMechanic__Group_5__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameMechanic__Group_5__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_5__3"


    // $ANTLR start "rule__GameMechanic__Group_5__3__Impl"
    // InternalDSL.g:3280:1: rule__GameMechanic__Group_5__3__Impl : ( ( rule__GameMechanic__Group_5_3__0 )* ) ;
    public final void rule__GameMechanic__Group_5__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3284:1: ( ( ( rule__GameMechanic__Group_5_3__0 )* ) )
            // InternalDSL.g:3285:1: ( ( rule__GameMechanic__Group_5_3__0 )* )
            {
            // InternalDSL.g:3285:1: ( ( rule__GameMechanic__Group_5_3__0 )* )
            // InternalDSL.g:3286:2: ( rule__GameMechanic__Group_5_3__0 )*
            {
             before(grammarAccess.getGameMechanicAccess().getGroup_5_3()); 
            // InternalDSL.g:3287:2: ( rule__GameMechanic__Group_5_3__0 )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==64) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // InternalDSL.g:3287:3: rule__GameMechanic__Group_5_3__0
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__GameMechanic__Group_5_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);

             after(grammarAccess.getGameMechanicAccess().getGroup_5_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_5__3__Impl"


    // $ANTLR start "rule__GameMechanic__Group_5__4"
    // InternalDSL.g:3295:1: rule__GameMechanic__Group_5__4 : rule__GameMechanic__Group_5__4__Impl ;
    public final void rule__GameMechanic__Group_5__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3299:1: ( rule__GameMechanic__Group_5__4__Impl )
            // InternalDSL.g:3300:2: rule__GameMechanic__Group_5__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GameMechanic__Group_5__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_5__4"


    // $ANTLR start "rule__GameMechanic__Group_5__4__Impl"
    // InternalDSL.g:3306:1: rule__GameMechanic__Group_5__4__Impl : ( '}' ) ;
    public final void rule__GameMechanic__Group_5__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3310:1: ( ( '}' ) )
            // InternalDSL.g:3311:1: ( '}' )
            {
            // InternalDSL.g:3311:1: ( '}' )
            // InternalDSL.g:3312:2: '}'
            {
             before(grammarAccess.getGameMechanicAccess().getRightCurlyBracketKeyword_5_4()); 
            match(input,60,FOLLOW_2); 
             after(grammarAccess.getGameMechanicAccess().getRightCurlyBracketKeyword_5_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_5__4__Impl"


    // $ANTLR start "rule__GameMechanic__Group_5_3__0"
    // InternalDSL.g:3322:1: rule__GameMechanic__Group_5_3__0 : rule__GameMechanic__Group_5_3__0__Impl rule__GameMechanic__Group_5_3__1 ;
    public final void rule__GameMechanic__Group_5_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3326:1: ( rule__GameMechanic__Group_5_3__0__Impl rule__GameMechanic__Group_5_3__1 )
            // InternalDSL.g:3327:2: rule__GameMechanic__Group_5_3__0__Impl rule__GameMechanic__Group_5_3__1
            {
            pushFollow(FOLLOW_25);
            rule__GameMechanic__Group_5_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameMechanic__Group_5_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_5_3__0"


    // $ANTLR start "rule__GameMechanic__Group_5_3__0__Impl"
    // InternalDSL.g:3334:1: rule__GameMechanic__Group_5_3__0__Impl : ( ',' ) ;
    public final void rule__GameMechanic__Group_5_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3338:1: ( ( ',' ) )
            // InternalDSL.g:3339:1: ( ',' )
            {
            // InternalDSL.g:3339:1: ( ',' )
            // InternalDSL.g:3340:2: ','
            {
             before(grammarAccess.getGameMechanicAccess().getCommaKeyword_5_3_0()); 
            match(input,64,FOLLOW_2); 
             after(grammarAccess.getGameMechanicAccess().getCommaKeyword_5_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_5_3__0__Impl"


    // $ANTLR start "rule__GameMechanic__Group_5_3__1"
    // InternalDSL.g:3349:1: rule__GameMechanic__Group_5_3__1 : rule__GameMechanic__Group_5_3__1__Impl ;
    public final void rule__GameMechanic__Group_5_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3353:1: ( rule__GameMechanic__Group_5_3__1__Impl )
            // InternalDSL.g:3354:2: rule__GameMechanic__Group_5_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GameMechanic__Group_5_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_5_3__1"


    // $ANTLR start "rule__GameMechanic__Group_5_3__1__Impl"
    // InternalDSL.g:3360:1: rule__GameMechanic__Group_5_3__1__Impl : ( ( rule__GameMechanic__EventsAssignment_5_3_1 ) ) ;
    public final void rule__GameMechanic__Group_5_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3364:1: ( ( ( rule__GameMechanic__EventsAssignment_5_3_1 ) ) )
            // InternalDSL.g:3365:1: ( ( rule__GameMechanic__EventsAssignment_5_3_1 ) )
            {
            // InternalDSL.g:3365:1: ( ( rule__GameMechanic__EventsAssignment_5_3_1 ) )
            // InternalDSL.g:3366:2: ( rule__GameMechanic__EventsAssignment_5_3_1 )
            {
             before(grammarAccess.getGameMechanicAccess().getEventsAssignment_5_3_1()); 
            // InternalDSL.g:3367:2: ( rule__GameMechanic__EventsAssignment_5_3_1 )
            // InternalDSL.g:3367:3: rule__GameMechanic__EventsAssignment_5_3_1
            {
            pushFollow(FOLLOW_2);
            rule__GameMechanic__EventsAssignment_5_3_1();

            state._fsp--;


            }

             after(grammarAccess.getGameMechanicAccess().getEventsAssignment_5_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_5_3__1__Impl"


    // $ANTLR start "rule__GameMechanic__Group_6__0"
    // InternalDSL.g:3376:1: rule__GameMechanic__Group_6__0 : rule__GameMechanic__Group_6__0__Impl rule__GameMechanic__Group_6__1 ;
    public final void rule__GameMechanic__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3380:1: ( rule__GameMechanic__Group_6__0__Impl rule__GameMechanic__Group_6__1 )
            // InternalDSL.g:3381:2: rule__GameMechanic__Group_6__0__Impl rule__GameMechanic__Group_6__1
            {
            pushFollow(FOLLOW_12);
            rule__GameMechanic__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameMechanic__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_6__0"


    // $ANTLR start "rule__GameMechanic__Group_6__0__Impl"
    // InternalDSL.g:3388:1: rule__GameMechanic__Group_6__0__Impl : ( 'Information:' ) ;
    public final void rule__GameMechanic__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3392:1: ( ( 'Information:' ) )
            // InternalDSL.g:3393:1: ( 'Information:' )
            {
            // InternalDSL.g:3393:1: ( 'Information:' )
            // InternalDSL.g:3394:2: 'Information:'
            {
             before(grammarAccess.getGameMechanicAccess().getInformationKeyword_6_0()); 
            match(input,65,FOLLOW_2); 
             after(grammarAccess.getGameMechanicAccess().getInformationKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_6__0__Impl"


    // $ANTLR start "rule__GameMechanic__Group_6__1"
    // InternalDSL.g:3403:1: rule__GameMechanic__Group_6__1 : rule__GameMechanic__Group_6__1__Impl ;
    public final void rule__GameMechanic__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3407:1: ( rule__GameMechanic__Group_6__1__Impl )
            // InternalDSL.g:3408:2: rule__GameMechanic__Group_6__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GameMechanic__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_6__1"


    // $ANTLR start "rule__GameMechanic__Group_6__1__Impl"
    // InternalDSL.g:3414:1: rule__GameMechanic__Group_6__1__Impl : ( ( rule__GameMechanic__InformationAssignment_6_1 ) ) ;
    public final void rule__GameMechanic__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3418:1: ( ( ( rule__GameMechanic__InformationAssignment_6_1 ) ) )
            // InternalDSL.g:3419:1: ( ( rule__GameMechanic__InformationAssignment_6_1 ) )
            {
            // InternalDSL.g:3419:1: ( ( rule__GameMechanic__InformationAssignment_6_1 ) )
            // InternalDSL.g:3420:2: ( rule__GameMechanic__InformationAssignment_6_1 )
            {
             before(grammarAccess.getGameMechanicAccess().getInformationAssignment_6_1()); 
            // InternalDSL.g:3421:2: ( rule__GameMechanic__InformationAssignment_6_1 )
            // InternalDSL.g:3421:3: rule__GameMechanic__InformationAssignment_6_1
            {
            pushFollow(FOLLOW_2);
            rule__GameMechanic__InformationAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getGameMechanicAccess().getInformationAssignment_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__Group_6__1__Impl"


    // $ANTLR start "rule__Achievement__Group__0"
    // InternalDSL.g:3430:1: rule__Achievement__Group__0 : rule__Achievement__Group__0__Impl rule__Achievement__Group__1 ;
    public final void rule__Achievement__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3434:1: ( rule__Achievement__Group__0__Impl rule__Achievement__Group__1 )
            // InternalDSL.g:3435:2: rule__Achievement__Group__0__Impl rule__Achievement__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Achievement__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__0"


    // $ANTLR start "rule__Achievement__Group__0__Impl"
    // InternalDSL.g:3442:1: rule__Achievement__Group__0__Impl : ( 'Achievement' ) ;
    public final void rule__Achievement__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3446:1: ( ( 'Achievement' ) )
            // InternalDSL.g:3447:1: ( 'Achievement' )
            {
            // InternalDSL.g:3447:1: ( 'Achievement' )
            // InternalDSL.g:3448:2: 'Achievement'
            {
             before(grammarAccess.getAchievementAccess().getAchievementKeyword_0()); 
            match(input,77,FOLLOW_2); 
             after(grammarAccess.getAchievementAccess().getAchievementKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__0__Impl"


    // $ANTLR start "rule__Achievement__Group__1"
    // InternalDSL.g:3457:1: rule__Achievement__Group__1 : rule__Achievement__Group__1__Impl rule__Achievement__Group__2 ;
    public final void rule__Achievement__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3461:1: ( rule__Achievement__Group__1__Impl rule__Achievement__Group__2 )
            // InternalDSL.g:3462:2: rule__Achievement__Group__1__Impl rule__Achievement__Group__2
            {
            pushFollow(FOLLOW_26);
            rule__Achievement__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__1"


    // $ANTLR start "rule__Achievement__Group__1__Impl"
    // InternalDSL.g:3469:1: rule__Achievement__Group__1__Impl : ( '{' ) ;
    public final void rule__Achievement__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3473:1: ( ( '{' ) )
            // InternalDSL.g:3474:1: ( '{' )
            {
            // InternalDSL.g:3474:1: ( '{' )
            // InternalDSL.g:3475:2: '{'
            {
             before(grammarAccess.getAchievementAccess().getLeftCurlyBracketKeyword_1()); 
            match(input,58,FOLLOW_2); 
             after(grammarAccess.getAchievementAccess().getLeftCurlyBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__1__Impl"


    // $ANTLR start "rule__Achievement__Group__2"
    // InternalDSL.g:3484:1: rule__Achievement__Group__2 : rule__Achievement__Group__2__Impl rule__Achievement__Group__3 ;
    public final void rule__Achievement__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3488:1: ( rule__Achievement__Group__2__Impl rule__Achievement__Group__3 )
            // InternalDSL.g:3489:2: rule__Achievement__Group__2__Impl rule__Achievement__Group__3
            {
            pushFollow(FOLLOW_26);
            rule__Achievement__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__2"


    // $ANTLR start "rule__Achievement__Group__2__Impl"
    // InternalDSL.g:3496:1: rule__Achievement__Group__2__Impl : ( ( rule__Achievement__Group_2__0 )? ) ;
    public final void rule__Achievement__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3500:1: ( ( ( rule__Achievement__Group_2__0 )? ) )
            // InternalDSL.g:3501:1: ( ( rule__Achievement__Group_2__0 )? )
            {
            // InternalDSL.g:3501:1: ( ( rule__Achievement__Group_2__0 )? )
            // InternalDSL.g:3502:2: ( rule__Achievement__Group_2__0 )?
            {
             before(grammarAccess.getAchievementAccess().getGroup_2()); 
            // InternalDSL.g:3503:2: ( rule__Achievement__Group_2__0 )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==62) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // InternalDSL.g:3503:3: rule__Achievement__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Achievement__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAchievementAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__2__Impl"


    // $ANTLR start "rule__Achievement__Group__3"
    // InternalDSL.g:3511:1: rule__Achievement__Group__3 : rule__Achievement__Group__3__Impl rule__Achievement__Group__4 ;
    public final void rule__Achievement__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3515:1: ( rule__Achievement__Group__3__Impl rule__Achievement__Group__4 )
            // InternalDSL.g:3516:2: rule__Achievement__Group__3__Impl rule__Achievement__Group__4
            {
            pushFollow(FOLLOW_26);
            rule__Achievement__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__3"


    // $ANTLR start "rule__Achievement__Group__3__Impl"
    // InternalDSL.g:3523:1: rule__Achievement__Group__3__Impl : ( ( rule__Achievement__Group_3__0 )? ) ;
    public final void rule__Achievement__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3527:1: ( ( ( rule__Achievement__Group_3__0 )? ) )
            // InternalDSL.g:3528:1: ( ( rule__Achievement__Group_3__0 )? )
            {
            // InternalDSL.g:3528:1: ( ( rule__Achievement__Group_3__0 )? )
            // InternalDSL.g:3529:2: ( rule__Achievement__Group_3__0 )?
            {
             before(grammarAccess.getAchievementAccess().getGroup_3()); 
            // InternalDSL.g:3530:2: ( rule__Achievement__Group_3__0 )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==79) ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // InternalDSL.g:3530:3: rule__Achievement__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Achievement__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAchievementAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__3__Impl"


    // $ANTLR start "rule__Achievement__Group__4"
    // InternalDSL.g:3538:1: rule__Achievement__Group__4 : rule__Achievement__Group__4__Impl rule__Achievement__Group__5 ;
    public final void rule__Achievement__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3542:1: ( rule__Achievement__Group__4__Impl rule__Achievement__Group__5 )
            // InternalDSL.g:3543:2: rule__Achievement__Group__4__Impl rule__Achievement__Group__5
            {
            pushFollow(FOLLOW_26);
            rule__Achievement__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__4"


    // $ANTLR start "rule__Achievement__Group__4__Impl"
    // InternalDSL.g:3550:1: rule__Achievement__Group__4__Impl : ( ( rule__Achievement__Group_4__0 )? ) ;
    public final void rule__Achievement__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3554:1: ( ( ( rule__Achievement__Group_4__0 )? ) )
            // InternalDSL.g:3555:1: ( ( rule__Achievement__Group_4__0 )? )
            {
            // InternalDSL.g:3555:1: ( ( rule__Achievement__Group_4__0 )? )
            // InternalDSL.g:3556:2: ( rule__Achievement__Group_4__0 )?
            {
             before(grammarAccess.getAchievementAccess().getGroup_4()); 
            // InternalDSL.g:3557:2: ( rule__Achievement__Group_4__0 )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==80) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalDSL.g:3557:3: rule__Achievement__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Achievement__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAchievementAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__4__Impl"


    // $ANTLR start "rule__Achievement__Group__5"
    // InternalDSL.g:3565:1: rule__Achievement__Group__5 : rule__Achievement__Group__5__Impl rule__Achievement__Group__6 ;
    public final void rule__Achievement__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3569:1: ( rule__Achievement__Group__5__Impl rule__Achievement__Group__6 )
            // InternalDSL.g:3570:2: rule__Achievement__Group__5__Impl rule__Achievement__Group__6
            {
            pushFollow(FOLLOW_26);
            rule__Achievement__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__5"


    // $ANTLR start "rule__Achievement__Group__5__Impl"
    // InternalDSL.g:3577:1: rule__Achievement__Group__5__Impl : ( ( rule__Achievement__Group_5__0 )? ) ;
    public final void rule__Achievement__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3581:1: ( ( ( rule__Achievement__Group_5__0 )? ) )
            // InternalDSL.g:3582:1: ( ( rule__Achievement__Group_5__0 )? )
            {
            // InternalDSL.g:3582:1: ( ( rule__Achievement__Group_5__0 )? )
            // InternalDSL.g:3583:2: ( rule__Achievement__Group_5__0 )?
            {
             before(grammarAccess.getAchievementAccess().getGroup_5()); 
            // InternalDSL.g:3584:2: ( rule__Achievement__Group_5__0 )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==83) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // InternalDSL.g:3584:3: rule__Achievement__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Achievement__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAchievementAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__5__Impl"


    // $ANTLR start "rule__Achievement__Group__6"
    // InternalDSL.g:3592:1: rule__Achievement__Group__6 : rule__Achievement__Group__6__Impl rule__Achievement__Group__7 ;
    public final void rule__Achievement__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3596:1: ( rule__Achievement__Group__6__Impl rule__Achievement__Group__7 )
            // InternalDSL.g:3597:2: rule__Achievement__Group__6__Impl rule__Achievement__Group__7
            {
            pushFollow(FOLLOW_3);
            rule__Achievement__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__6"


    // $ANTLR start "rule__Achievement__Group__6__Impl"
    // InternalDSL.g:3604:1: rule__Achievement__Group__6__Impl : ( 'Conditions' ) ;
    public final void rule__Achievement__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3608:1: ( ( 'Conditions' ) )
            // InternalDSL.g:3609:1: ( 'Conditions' )
            {
            // InternalDSL.g:3609:1: ( 'Conditions' )
            // InternalDSL.g:3610:2: 'Conditions'
            {
             before(grammarAccess.getAchievementAccess().getConditionsKeyword_6()); 
            match(input,78,FOLLOW_2); 
             after(grammarAccess.getAchievementAccess().getConditionsKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__6__Impl"


    // $ANTLR start "rule__Achievement__Group__7"
    // InternalDSL.g:3619:1: rule__Achievement__Group__7 : rule__Achievement__Group__7__Impl rule__Achievement__Group__8 ;
    public final void rule__Achievement__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3623:1: ( rule__Achievement__Group__7__Impl rule__Achievement__Group__8 )
            // InternalDSL.g:3624:2: rule__Achievement__Group__7__Impl rule__Achievement__Group__8
            {
            pushFollow(FOLLOW_27);
            rule__Achievement__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__7"


    // $ANTLR start "rule__Achievement__Group__7__Impl"
    // InternalDSL.g:3631:1: rule__Achievement__Group__7__Impl : ( '{' ) ;
    public final void rule__Achievement__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3635:1: ( ( '{' ) )
            // InternalDSL.g:3636:1: ( '{' )
            {
            // InternalDSL.g:3636:1: ( '{' )
            // InternalDSL.g:3637:2: '{'
            {
             before(grammarAccess.getAchievementAccess().getLeftCurlyBracketKeyword_7()); 
            match(input,58,FOLLOW_2); 
             after(grammarAccess.getAchievementAccess().getLeftCurlyBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__7__Impl"


    // $ANTLR start "rule__Achievement__Group__8"
    // InternalDSL.g:3646:1: rule__Achievement__Group__8 : rule__Achievement__Group__8__Impl rule__Achievement__Group__9 ;
    public final void rule__Achievement__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3650:1: ( rule__Achievement__Group__8__Impl rule__Achievement__Group__9 )
            // InternalDSL.g:3651:2: rule__Achievement__Group__8__Impl rule__Achievement__Group__9
            {
            pushFollow(FOLLOW_6);
            rule__Achievement__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__8"


    // $ANTLR start "rule__Achievement__Group__8__Impl"
    // InternalDSL.g:3658:1: rule__Achievement__Group__8__Impl : ( ( rule__Achievement__ConditionsAssignment_8 ) ) ;
    public final void rule__Achievement__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3662:1: ( ( ( rule__Achievement__ConditionsAssignment_8 ) ) )
            // InternalDSL.g:3663:1: ( ( rule__Achievement__ConditionsAssignment_8 ) )
            {
            // InternalDSL.g:3663:1: ( ( rule__Achievement__ConditionsAssignment_8 ) )
            // InternalDSL.g:3664:2: ( rule__Achievement__ConditionsAssignment_8 )
            {
             before(grammarAccess.getAchievementAccess().getConditionsAssignment_8()); 
            // InternalDSL.g:3665:2: ( rule__Achievement__ConditionsAssignment_8 )
            // InternalDSL.g:3665:3: rule__Achievement__ConditionsAssignment_8
            {
            pushFollow(FOLLOW_2);
            rule__Achievement__ConditionsAssignment_8();

            state._fsp--;


            }

             after(grammarAccess.getAchievementAccess().getConditionsAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__8__Impl"


    // $ANTLR start "rule__Achievement__Group__9"
    // InternalDSL.g:3673:1: rule__Achievement__Group__9 : rule__Achievement__Group__9__Impl rule__Achievement__Group__10 ;
    public final void rule__Achievement__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3677:1: ( rule__Achievement__Group__9__Impl rule__Achievement__Group__10 )
            // InternalDSL.g:3678:2: rule__Achievement__Group__9__Impl rule__Achievement__Group__10
            {
            pushFollow(FOLLOW_6);
            rule__Achievement__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__9"


    // $ANTLR start "rule__Achievement__Group__9__Impl"
    // InternalDSL.g:3685:1: rule__Achievement__Group__9__Impl : ( ( rule__Achievement__Group_9__0 )* ) ;
    public final void rule__Achievement__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3689:1: ( ( ( rule__Achievement__Group_9__0 )* ) )
            // InternalDSL.g:3690:1: ( ( rule__Achievement__Group_9__0 )* )
            {
            // InternalDSL.g:3690:1: ( ( rule__Achievement__Group_9__0 )* )
            // InternalDSL.g:3691:2: ( rule__Achievement__Group_9__0 )*
            {
             before(grammarAccess.getAchievementAccess().getGroup_9()); 
            // InternalDSL.g:3692:2: ( rule__Achievement__Group_9__0 )*
            loop35:
            do {
                int alt35=2;
                int LA35_0 = input.LA(1);

                if ( (LA35_0==64) ) {
                    alt35=1;
                }


                switch (alt35) {
            	case 1 :
            	    // InternalDSL.g:3692:3: rule__Achievement__Group_9__0
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__Achievement__Group_9__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop35;
                }
            } while (true);

             after(grammarAccess.getAchievementAccess().getGroup_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__9__Impl"


    // $ANTLR start "rule__Achievement__Group__10"
    // InternalDSL.g:3700:1: rule__Achievement__Group__10 : rule__Achievement__Group__10__Impl rule__Achievement__Group__11 ;
    public final void rule__Achievement__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3704:1: ( rule__Achievement__Group__10__Impl rule__Achievement__Group__11 )
            // InternalDSL.g:3705:2: rule__Achievement__Group__10__Impl rule__Achievement__Group__11
            {
            pushFollow(FOLLOW_10);
            rule__Achievement__Group__10__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__10"


    // $ANTLR start "rule__Achievement__Group__10__Impl"
    // InternalDSL.g:3712:1: rule__Achievement__Group__10__Impl : ( '}' ) ;
    public final void rule__Achievement__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3716:1: ( ( '}' ) )
            // InternalDSL.g:3717:1: ( '}' )
            {
            // InternalDSL.g:3717:1: ( '}' )
            // InternalDSL.g:3718:2: '}'
            {
             before(grammarAccess.getAchievementAccess().getRightCurlyBracketKeyword_10()); 
            match(input,60,FOLLOW_2); 
             after(grammarAccess.getAchievementAccess().getRightCurlyBracketKeyword_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__10__Impl"


    // $ANTLR start "rule__Achievement__Group__11"
    // InternalDSL.g:3727:1: rule__Achievement__Group__11 : rule__Achievement__Group__11__Impl rule__Achievement__Group__12 ;
    public final void rule__Achievement__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3731:1: ( rule__Achievement__Group__11__Impl rule__Achievement__Group__12 )
            // InternalDSL.g:3732:2: rule__Achievement__Group__11__Impl rule__Achievement__Group__12
            {
            pushFollow(FOLLOW_10);
            rule__Achievement__Group__11__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group__12();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__11"


    // $ANTLR start "rule__Achievement__Group__11__Impl"
    // InternalDSL.g:3739:1: rule__Achievement__Group__11__Impl : ( ( rule__Achievement__Group_11__0 )? ) ;
    public final void rule__Achievement__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3743:1: ( ( ( rule__Achievement__Group_11__0 )? ) )
            // InternalDSL.g:3744:1: ( ( rule__Achievement__Group_11__0 )? )
            {
            // InternalDSL.g:3744:1: ( ( rule__Achievement__Group_11__0 )? )
            // InternalDSL.g:3745:2: ( rule__Achievement__Group_11__0 )?
            {
             before(grammarAccess.getAchievementAccess().getGroup_11()); 
            // InternalDSL.g:3746:2: ( rule__Achievement__Group_11__0 )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==65) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalDSL.g:3746:3: rule__Achievement__Group_11__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Achievement__Group_11__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAchievementAccess().getGroup_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__11__Impl"


    // $ANTLR start "rule__Achievement__Group__12"
    // InternalDSL.g:3754:1: rule__Achievement__Group__12 : rule__Achievement__Group__12__Impl ;
    public final void rule__Achievement__Group__12() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3758:1: ( rule__Achievement__Group__12__Impl )
            // InternalDSL.g:3759:2: rule__Achievement__Group__12__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Achievement__Group__12__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__12"


    // $ANTLR start "rule__Achievement__Group__12__Impl"
    // InternalDSL.g:3765:1: rule__Achievement__Group__12__Impl : ( '}' ) ;
    public final void rule__Achievement__Group__12__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3769:1: ( ( '}' ) )
            // InternalDSL.g:3770:1: ( '}' )
            {
            // InternalDSL.g:3770:1: ( '}' )
            // InternalDSL.g:3771:2: '}'
            {
             before(grammarAccess.getAchievementAccess().getRightCurlyBracketKeyword_12()); 
            match(input,60,FOLLOW_2); 
             after(grammarAccess.getAchievementAccess().getRightCurlyBracketKeyword_12()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group__12__Impl"


    // $ANTLR start "rule__Achievement__Group_2__0"
    // InternalDSL.g:3781:1: rule__Achievement__Group_2__0 : rule__Achievement__Group_2__0__Impl rule__Achievement__Group_2__1 ;
    public final void rule__Achievement__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3785:1: ( rule__Achievement__Group_2__0__Impl rule__Achievement__Group_2__1 )
            // InternalDSL.g:3786:2: rule__Achievement__Group_2__0__Impl rule__Achievement__Group_2__1
            {
            pushFollow(FOLLOW_11);
            rule__Achievement__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_2__0"


    // $ANTLR start "rule__Achievement__Group_2__0__Impl"
    // InternalDSL.g:3793:1: rule__Achievement__Group_2__0__Impl : ( 'Name:' ) ;
    public final void rule__Achievement__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3797:1: ( ( 'Name:' ) )
            // InternalDSL.g:3798:1: ( 'Name:' )
            {
            // InternalDSL.g:3798:1: ( 'Name:' )
            // InternalDSL.g:3799:2: 'Name:'
            {
             before(grammarAccess.getAchievementAccess().getNameKeyword_2_0()); 
            match(input,62,FOLLOW_2); 
             after(grammarAccess.getAchievementAccess().getNameKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_2__0__Impl"


    // $ANTLR start "rule__Achievement__Group_2__1"
    // InternalDSL.g:3808:1: rule__Achievement__Group_2__1 : rule__Achievement__Group_2__1__Impl ;
    public final void rule__Achievement__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3812:1: ( rule__Achievement__Group_2__1__Impl )
            // InternalDSL.g:3813:2: rule__Achievement__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Achievement__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_2__1"


    // $ANTLR start "rule__Achievement__Group_2__1__Impl"
    // InternalDSL.g:3819:1: rule__Achievement__Group_2__1__Impl : ( ( rule__Achievement__NameAssignment_2_1 ) ) ;
    public final void rule__Achievement__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3823:1: ( ( ( rule__Achievement__NameAssignment_2_1 ) ) )
            // InternalDSL.g:3824:1: ( ( rule__Achievement__NameAssignment_2_1 ) )
            {
            // InternalDSL.g:3824:1: ( ( rule__Achievement__NameAssignment_2_1 ) )
            // InternalDSL.g:3825:2: ( rule__Achievement__NameAssignment_2_1 )
            {
             before(grammarAccess.getAchievementAccess().getNameAssignment_2_1()); 
            // InternalDSL.g:3826:2: ( rule__Achievement__NameAssignment_2_1 )
            // InternalDSL.g:3826:3: rule__Achievement__NameAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__Achievement__NameAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getAchievementAccess().getNameAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_2__1__Impl"


    // $ANTLR start "rule__Achievement__Group_3__0"
    // InternalDSL.g:3835:1: rule__Achievement__Group_3__0 : rule__Achievement__Group_3__0__Impl rule__Achievement__Group_3__1 ;
    public final void rule__Achievement__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3839:1: ( rule__Achievement__Group_3__0__Impl rule__Achievement__Group_3__1 )
            // InternalDSL.g:3840:2: rule__Achievement__Group_3__0__Impl rule__Achievement__Group_3__1
            {
            pushFollow(FOLLOW_28);
            rule__Achievement__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_3__0"


    // $ANTLR start "rule__Achievement__Group_3__0__Impl"
    // InternalDSL.g:3847:1: rule__Achievement__Group_3__0__Impl : ( 'Hidden:' ) ;
    public final void rule__Achievement__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3851:1: ( ( 'Hidden:' ) )
            // InternalDSL.g:3852:1: ( 'Hidden:' )
            {
            // InternalDSL.g:3852:1: ( 'Hidden:' )
            // InternalDSL.g:3853:2: 'Hidden:'
            {
             before(grammarAccess.getAchievementAccess().getHiddenKeyword_3_0()); 
            match(input,79,FOLLOW_2); 
             after(grammarAccess.getAchievementAccess().getHiddenKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_3__0__Impl"


    // $ANTLR start "rule__Achievement__Group_3__1"
    // InternalDSL.g:3862:1: rule__Achievement__Group_3__1 : rule__Achievement__Group_3__1__Impl ;
    public final void rule__Achievement__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3866:1: ( rule__Achievement__Group_3__1__Impl )
            // InternalDSL.g:3867:2: rule__Achievement__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Achievement__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_3__1"


    // $ANTLR start "rule__Achievement__Group_3__1__Impl"
    // InternalDSL.g:3873:1: rule__Achievement__Group_3__1__Impl : ( ( rule__Achievement__HiddenAssignment_3_1 ) ) ;
    public final void rule__Achievement__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3877:1: ( ( ( rule__Achievement__HiddenAssignment_3_1 ) ) )
            // InternalDSL.g:3878:1: ( ( rule__Achievement__HiddenAssignment_3_1 ) )
            {
            // InternalDSL.g:3878:1: ( ( rule__Achievement__HiddenAssignment_3_1 ) )
            // InternalDSL.g:3879:2: ( rule__Achievement__HiddenAssignment_3_1 )
            {
             before(grammarAccess.getAchievementAccess().getHiddenAssignment_3_1()); 
            // InternalDSL.g:3880:2: ( rule__Achievement__HiddenAssignment_3_1 )
            // InternalDSL.g:3880:3: rule__Achievement__HiddenAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Achievement__HiddenAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getAchievementAccess().getHiddenAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_3__1__Impl"


    // $ANTLR start "rule__Achievement__Group_4__0"
    // InternalDSL.g:3889:1: rule__Achievement__Group_4__0 : rule__Achievement__Group_4__0__Impl rule__Achievement__Group_4__1 ;
    public final void rule__Achievement__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3893:1: ( rule__Achievement__Group_4__0__Impl rule__Achievement__Group_4__1 )
            // InternalDSL.g:3894:2: rule__Achievement__Group_4__0__Impl rule__Achievement__Group_4__1
            {
            pushFollow(FOLLOW_29);
            rule__Achievement__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_4__0"


    // $ANTLR start "rule__Achievement__Group_4__0__Impl"
    // InternalDSL.g:3901:1: rule__Achievement__Group_4__0__Impl : ( 'RequiredAchievements' ) ;
    public final void rule__Achievement__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3905:1: ( ( 'RequiredAchievements' ) )
            // InternalDSL.g:3906:1: ( 'RequiredAchievements' )
            {
            // InternalDSL.g:3906:1: ( 'RequiredAchievements' )
            // InternalDSL.g:3907:2: 'RequiredAchievements'
            {
             before(grammarAccess.getAchievementAccess().getRequiredAchievementsKeyword_4_0()); 
            match(input,80,FOLLOW_2); 
             after(grammarAccess.getAchievementAccess().getRequiredAchievementsKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_4__0__Impl"


    // $ANTLR start "rule__Achievement__Group_4__1"
    // InternalDSL.g:3916:1: rule__Achievement__Group_4__1 : rule__Achievement__Group_4__1__Impl rule__Achievement__Group_4__2 ;
    public final void rule__Achievement__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3920:1: ( rule__Achievement__Group_4__1__Impl rule__Achievement__Group_4__2 )
            // InternalDSL.g:3921:2: rule__Achievement__Group_4__1__Impl rule__Achievement__Group_4__2
            {
            pushFollow(FOLLOW_11);
            rule__Achievement__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_4__1"


    // $ANTLR start "rule__Achievement__Group_4__1__Impl"
    // InternalDSL.g:3928:1: rule__Achievement__Group_4__1__Impl : ( '(' ) ;
    public final void rule__Achievement__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3932:1: ( ( '(' ) )
            // InternalDSL.g:3933:1: ( '(' )
            {
            // InternalDSL.g:3933:1: ( '(' )
            // InternalDSL.g:3934:2: '('
            {
             before(grammarAccess.getAchievementAccess().getLeftParenthesisKeyword_4_1()); 
            match(input,81,FOLLOW_2); 
             after(grammarAccess.getAchievementAccess().getLeftParenthesisKeyword_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_4__1__Impl"


    // $ANTLR start "rule__Achievement__Group_4__2"
    // InternalDSL.g:3943:1: rule__Achievement__Group_4__2 : rule__Achievement__Group_4__2__Impl rule__Achievement__Group_4__3 ;
    public final void rule__Achievement__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3947:1: ( rule__Achievement__Group_4__2__Impl rule__Achievement__Group_4__3 )
            // InternalDSL.g:3948:2: rule__Achievement__Group_4__2__Impl rule__Achievement__Group_4__3
            {
            pushFollow(FOLLOW_30);
            rule__Achievement__Group_4__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group_4__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_4__2"


    // $ANTLR start "rule__Achievement__Group_4__2__Impl"
    // InternalDSL.g:3955:1: rule__Achievement__Group_4__2__Impl : ( ( rule__Achievement__RequiredAchievementsAssignment_4_2 ) ) ;
    public final void rule__Achievement__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3959:1: ( ( ( rule__Achievement__RequiredAchievementsAssignment_4_2 ) ) )
            // InternalDSL.g:3960:1: ( ( rule__Achievement__RequiredAchievementsAssignment_4_2 ) )
            {
            // InternalDSL.g:3960:1: ( ( rule__Achievement__RequiredAchievementsAssignment_4_2 ) )
            // InternalDSL.g:3961:2: ( rule__Achievement__RequiredAchievementsAssignment_4_2 )
            {
             before(grammarAccess.getAchievementAccess().getRequiredAchievementsAssignment_4_2()); 
            // InternalDSL.g:3962:2: ( rule__Achievement__RequiredAchievementsAssignment_4_2 )
            // InternalDSL.g:3962:3: rule__Achievement__RequiredAchievementsAssignment_4_2
            {
            pushFollow(FOLLOW_2);
            rule__Achievement__RequiredAchievementsAssignment_4_2();

            state._fsp--;


            }

             after(grammarAccess.getAchievementAccess().getRequiredAchievementsAssignment_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_4__2__Impl"


    // $ANTLR start "rule__Achievement__Group_4__3"
    // InternalDSL.g:3970:1: rule__Achievement__Group_4__3 : rule__Achievement__Group_4__3__Impl rule__Achievement__Group_4__4 ;
    public final void rule__Achievement__Group_4__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3974:1: ( rule__Achievement__Group_4__3__Impl rule__Achievement__Group_4__4 )
            // InternalDSL.g:3975:2: rule__Achievement__Group_4__3__Impl rule__Achievement__Group_4__4
            {
            pushFollow(FOLLOW_30);
            rule__Achievement__Group_4__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group_4__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_4__3"


    // $ANTLR start "rule__Achievement__Group_4__3__Impl"
    // InternalDSL.g:3982:1: rule__Achievement__Group_4__3__Impl : ( ( rule__Achievement__Group_4_3__0 )* ) ;
    public final void rule__Achievement__Group_4__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:3986:1: ( ( ( rule__Achievement__Group_4_3__0 )* ) )
            // InternalDSL.g:3987:1: ( ( rule__Achievement__Group_4_3__0 )* )
            {
            // InternalDSL.g:3987:1: ( ( rule__Achievement__Group_4_3__0 )* )
            // InternalDSL.g:3988:2: ( rule__Achievement__Group_4_3__0 )*
            {
             before(grammarAccess.getAchievementAccess().getGroup_4_3()); 
            // InternalDSL.g:3989:2: ( rule__Achievement__Group_4_3__0 )*
            loop37:
            do {
                int alt37=2;
                int LA37_0 = input.LA(1);

                if ( (LA37_0==64) ) {
                    alt37=1;
                }


                switch (alt37) {
            	case 1 :
            	    // InternalDSL.g:3989:3: rule__Achievement__Group_4_3__0
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__Achievement__Group_4_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop37;
                }
            } while (true);

             after(grammarAccess.getAchievementAccess().getGroup_4_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_4__3__Impl"


    // $ANTLR start "rule__Achievement__Group_4__4"
    // InternalDSL.g:3997:1: rule__Achievement__Group_4__4 : rule__Achievement__Group_4__4__Impl ;
    public final void rule__Achievement__Group_4__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4001:1: ( rule__Achievement__Group_4__4__Impl )
            // InternalDSL.g:4002:2: rule__Achievement__Group_4__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Achievement__Group_4__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_4__4"


    // $ANTLR start "rule__Achievement__Group_4__4__Impl"
    // InternalDSL.g:4008:1: rule__Achievement__Group_4__4__Impl : ( ')' ) ;
    public final void rule__Achievement__Group_4__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4012:1: ( ( ')' ) )
            // InternalDSL.g:4013:1: ( ')' )
            {
            // InternalDSL.g:4013:1: ( ')' )
            // InternalDSL.g:4014:2: ')'
            {
             before(grammarAccess.getAchievementAccess().getRightParenthesisKeyword_4_4()); 
            match(input,82,FOLLOW_2); 
             after(grammarAccess.getAchievementAccess().getRightParenthesisKeyword_4_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_4__4__Impl"


    // $ANTLR start "rule__Achievement__Group_4_3__0"
    // InternalDSL.g:4024:1: rule__Achievement__Group_4_3__0 : rule__Achievement__Group_4_3__0__Impl rule__Achievement__Group_4_3__1 ;
    public final void rule__Achievement__Group_4_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4028:1: ( rule__Achievement__Group_4_3__0__Impl rule__Achievement__Group_4_3__1 )
            // InternalDSL.g:4029:2: rule__Achievement__Group_4_3__0__Impl rule__Achievement__Group_4_3__1
            {
            pushFollow(FOLLOW_11);
            rule__Achievement__Group_4_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group_4_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_4_3__0"


    // $ANTLR start "rule__Achievement__Group_4_3__0__Impl"
    // InternalDSL.g:4036:1: rule__Achievement__Group_4_3__0__Impl : ( ',' ) ;
    public final void rule__Achievement__Group_4_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4040:1: ( ( ',' ) )
            // InternalDSL.g:4041:1: ( ',' )
            {
            // InternalDSL.g:4041:1: ( ',' )
            // InternalDSL.g:4042:2: ','
            {
             before(grammarAccess.getAchievementAccess().getCommaKeyword_4_3_0()); 
            match(input,64,FOLLOW_2); 
             after(grammarAccess.getAchievementAccess().getCommaKeyword_4_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_4_3__0__Impl"


    // $ANTLR start "rule__Achievement__Group_4_3__1"
    // InternalDSL.g:4051:1: rule__Achievement__Group_4_3__1 : rule__Achievement__Group_4_3__1__Impl ;
    public final void rule__Achievement__Group_4_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4055:1: ( rule__Achievement__Group_4_3__1__Impl )
            // InternalDSL.g:4056:2: rule__Achievement__Group_4_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Achievement__Group_4_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_4_3__1"


    // $ANTLR start "rule__Achievement__Group_4_3__1__Impl"
    // InternalDSL.g:4062:1: rule__Achievement__Group_4_3__1__Impl : ( ( rule__Achievement__RequiredAchievementsAssignment_4_3_1 ) ) ;
    public final void rule__Achievement__Group_4_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4066:1: ( ( ( rule__Achievement__RequiredAchievementsAssignment_4_3_1 ) ) )
            // InternalDSL.g:4067:1: ( ( rule__Achievement__RequiredAchievementsAssignment_4_3_1 ) )
            {
            // InternalDSL.g:4067:1: ( ( rule__Achievement__RequiredAchievementsAssignment_4_3_1 ) )
            // InternalDSL.g:4068:2: ( rule__Achievement__RequiredAchievementsAssignment_4_3_1 )
            {
             before(grammarAccess.getAchievementAccess().getRequiredAchievementsAssignment_4_3_1()); 
            // InternalDSL.g:4069:2: ( rule__Achievement__RequiredAchievementsAssignment_4_3_1 )
            // InternalDSL.g:4069:3: rule__Achievement__RequiredAchievementsAssignment_4_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Achievement__RequiredAchievementsAssignment_4_3_1();

            state._fsp--;


            }

             after(grammarAccess.getAchievementAccess().getRequiredAchievementsAssignment_4_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_4_3__1__Impl"


    // $ANTLR start "rule__Achievement__Group_5__0"
    // InternalDSL.g:4078:1: rule__Achievement__Group_5__0 : rule__Achievement__Group_5__0__Impl rule__Achievement__Group_5__1 ;
    public final void rule__Achievement__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4082:1: ( rule__Achievement__Group_5__0__Impl rule__Achievement__Group_5__1 )
            // InternalDSL.g:4083:2: rule__Achievement__Group_5__0__Impl rule__Achievement__Group_5__1
            {
            pushFollow(FOLLOW_3);
            rule__Achievement__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_5__0"


    // $ANTLR start "rule__Achievement__Group_5__0__Impl"
    // InternalDSL.g:4090:1: rule__Achievement__Group_5__0__Impl : ( 'Rewards' ) ;
    public final void rule__Achievement__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4094:1: ( ( 'Rewards' ) )
            // InternalDSL.g:4095:1: ( 'Rewards' )
            {
            // InternalDSL.g:4095:1: ( 'Rewards' )
            // InternalDSL.g:4096:2: 'Rewards'
            {
             before(grammarAccess.getAchievementAccess().getRewardsKeyword_5_0()); 
            match(input,83,FOLLOW_2); 
             after(grammarAccess.getAchievementAccess().getRewardsKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_5__0__Impl"


    // $ANTLR start "rule__Achievement__Group_5__1"
    // InternalDSL.g:4105:1: rule__Achievement__Group_5__1 : rule__Achievement__Group_5__1__Impl rule__Achievement__Group_5__2 ;
    public final void rule__Achievement__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4109:1: ( rule__Achievement__Group_5__1__Impl rule__Achievement__Group_5__2 )
            // InternalDSL.g:4110:2: rule__Achievement__Group_5__1__Impl rule__Achievement__Group_5__2
            {
            pushFollow(FOLLOW_31);
            rule__Achievement__Group_5__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_5__1"


    // $ANTLR start "rule__Achievement__Group_5__1__Impl"
    // InternalDSL.g:4117:1: rule__Achievement__Group_5__1__Impl : ( '{' ) ;
    public final void rule__Achievement__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4121:1: ( ( '{' ) )
            // InternalDSL.g:4122:1: ( '{' )
            {
            // InternalDSL.g:4122:1: ( '{' )
            // InternalDSL.g:4123:2: '{'
            {
             before(grammarAccess.getAchievementAccess().getLeftCurlyBracketKeyword_5_1()); 
            match(input,58,FOLLOW_2); 
             after(grammarAccess.getAchievementAccess().getLeftCurlyBracketKeyword_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_5__1__Impl"


    // $ANTLR start "rule__Achievement__Group_5__2"
    // InternalDSL.g:4132:1: rule__Achievement__Group_5__2 : rule__Achievement__Group_5__2__Impl rule__Achievement__Group_5__3 ;
    public final void rule__Achievement__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4136:1: ( rule__Achievement__Group_5__2__Impl rule__Achievement__Group_5__3 )
            // InternalDSL.g:4137:2: rule__Achievement__Group_5__2__Impl rule__Achievement__Group_5__3
            {
            pushFollow(FOLLOW_6);
            rule__Achievement__Group_5__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group_5__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_5__2"


    // $ANTLR start "rule__Achievement__Group_5__2__Impl"
    // InternalDSL.g:4144:1: rule__Achievement__Group_5__2__Impl : ( ( rule__Achievement__RewardsAssignment_5_2 ) ) ;
    public final void rule__Achievement__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4148:1: ( ( ( rule__Achievement__RewardsAssignment_5_2 ) ) )
            // InternalDSL.g:4149:1: ( ( rule__Achievement__RewardsAssignment_5_2 ) )
            {
            // InternalDSL.g:4149:1: ( ( rule__Achievement__RewardsAssignment_5_2 ) )
            // InternalDSL.g:4150:2: ( rule__Achievement__RewardsAssignment_5_2 )
            {
             before(grammarAccess.getAchievementAccess().getRewardsAssignment_5_2()); 
            // InternalDSL.g:4151:2: ( rule__Achievement__RewardsAssignment_5_2 )
            // InternalDSL.g:4151:3: rule__Achievement__RewardsAssignment_5_2
            {
            pushFollow(FOLLOW_2);
            rule__Achievement__RewardsAssignment_5_2();

            state._fsp--;


            }

             after(grammarAccess.getAchievementAccess().getRewardsAssignment_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_5__2__Impl"


    // $ANTLR start "rule__Achievement__Group_5__3"
    // InternalDSL.g:4159:1: rule__Achievement__Group_5__3 : rule__Achievement__Group_5__3__Impl rule__Achievement__Group_5__4 ;
    public final void rule__Achievement__Group_5__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4163:1: ( rule__Achievement__Group_5__3__Impl rule__Achievement__Group_5__4 )
            // InternalDSL.g:4164:2: rule__Achievement__Group_5__3__Impl rule__Achievement__Group_5__4
            {
            pushFollow(FOLLOW_6);
            rule__Achievement__Group_5__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group_5__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_5__3"


    // $ANTLR start "rule__Achievement__Group_5__3__Impl"
    // InternalDSL.g:4171:1: rule__Achievement__Group_5__3__Impl : ( ( rule__Achievement__Group_5_3__0 )* ) ;
    public final void rule__Achievement__Group_5__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4175:1: ( ( ( rule__Achievement__Group_5_3__0 )* ) )
            // InternalDSL.g:4176:1: ( ( rule__Achievement__Group_5_3__0 )* )
            {
            // InternalDSL.g:4176:1: ( ( rule__Achievement__Group_5_3__0 )* )
            // InternalDSL.g:4177:2: ( rule__Achievement__Group_5_3__0 )*
            {
             before(grammarAccess.getAchievementAccess().getGroup_5_3()); 
            // InternalDSL.g:4178:2: ( rule__Achievement__Group_5_3__0 )*
            loop38:
            do {
                int alt38=2;
                int LA38_0 = input.LA(1);

                if ( (LA38_0==64) ) {
                    alt38=1;
                }


                switch (alt38) {
            	case 1 :
            	    // InternalDSL.g:4178:3: rule__Achievement__Group_5_3__0
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__Achievement__Group_5_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop38;
                }
            } while (true);

             after(grammarAccess.getAchievementAccess().getGroup_5_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_5__3__Impl"


    // $ANTLR start "rule__Achievement__Group_5__4"
    // InternalDSL.g:4186:1: rule__Achievement__Group_5__4 : rule__Achievement__Group_5__4__Impl ;
    public final void rule__Achievement__Group_5__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4190:1: ( rule__Achievement__Group_5__4__Impl )
            // InternalDSL.g:4191:2: rule__Achievement__Group_5__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Achievement__Group_5__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_5__4"


    // $ANTLR start "rule__Achievement__Group_5__4__Impl"
    // InternalDSL.g:4197:1: rule__Achievement__Group_5__4__Impl : ( '}' ) ;
    public final void rule__Achievement__Group_5__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4201:1: ( ( '}' ) )
            // InternalDSL.g:4202:1: ( '}' )
            {
            // InternalDSL.g:4202:1: ( '}' )
            // InternalDSL.g:4203:2: '}'
            {
             before(grammarAccess.getAchievementAccess().getRightCurlyBracketKeyword_5_4()); 
            match(input,60,FOLLOW_2); 
             after(grammarAccess.getAchievementAccess().getRightCurlyBracketKeyword_5_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_5__4__Impl"


    // $ANTLR start "rule__Achievement__Group_5_3__0"
    // InternalDSL.g:4213:1: rule__Achievement__Group_5_3__0 : rule__Achievement__Group_5_3__0__Impl rule__Achievement__Group_5_3__1 ;
    public final void rule__Achievement__Group_5_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4217:1: ( rule__Achievement__Group_5_3__0__Impl rule__Achievement__Group_5_3__1 )
            // InternalDSL.g:4218:2: rule__Achievement__Group_5_3__0__Impl rule__Achievement__Group_5_3__1
            {
            pushFollow(FOLLOW_31);
            rule__Achievement__Group_5_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group_5_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_5_3__0"


    // $ANTLR start "rule__Achievement__Group_5_3__0__Impl"
    // InternalDSL.g:4225:1: rule__Achievement__Group_5_3__0__Impl : ( ',' ) ;
    public final void rule__Achievement__Group_5_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4229:1: ( ( ',' ) )
            // InternalDSL.g:4230:1: ( ',' )
            {
            // InternalDSL.g:4230:1: ( ',' )
            // InternalDSL.g:4231:2: ','
            {
             before(grammarAccess.getAchievementAccess().getCommaKeyword_5_3_0()); 
            match(input,64,FOLLOW_2); 
             after(grammarAccess.getAchievementAccess().getCommaKeyword_5_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_5_3__0__Impl"


    // $ANTLR start "rule__Achievement__Group_5_3__1"
    // InternalDSL.g:4240:1: rule__Achievement__Group_5_3__1 : rule__Achievement__Group_5_3__1__Impl ;
    public final void rule__Achievement__Group_5_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4244:1: ( rule__Achievement__Group_5_3__1__Impl )
            // InternalDSL.g:4245:2: rule__Achievement__Group_5_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Achievement__Group_5_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_5_3__1"


    // $ANTLR start "rule__Achievement__Group_5_3__1__Impl"
    // InternalDSL.g:4251:1: rule__Achievement__Group_5_3__1__Impl : ( ( rule__Achievement__RewardsAssignment_5_3_1 ) ) ;
    public final void rule__Achievement__Group_5_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4255:1: ( ( ( rule__Achievement__RewardsAssignment_5_3_1 ) ) )
            // InternalDSL.g:4256:1: ( ( rule__Achievement__RewardsAssignment_5_3_1 ) )
            {
            // InternalDSL.g:4256:1: ( ( rule__Achievement__RewardsAssignment_5_3_1 ) )
            // InternalDSL.g:4257:2: ( rule__Achievement__RewardsAssignment_5_3_1 )
            {
             before(grammarAccess.getAchievementAccess().getRewardsAssignment_5_3_1()); 
            // InternalDSL.g:4258:2: ( rule__Achievement__RewardsAssignment_5_3_1 )
            // InternalDSL.g:4258:3: rule__Achievement__RewardsAssignment_5_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Achievement__RewardsAssignment_5_3_1();

            state._fsp--;


            }

             after(grammarAccess.getAchievementAccess().getRewardsAssignment_5_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_5_3__1__Impl"


    // $ANTLR start "rule__Achievement__Group_9__0"
    // InternalDSL.g:4267:1: rule__Achievement__Group_9__0 : rule__Achievement__Group_9__0__Impl rule__Achievement__Group_9__1 ;
    public final void rule__Achievement__Group_9__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4271:1: ( rule__Achievement__Group_9__0__Impl rule__Achievement__Group_9__1 )
            // InternalDSL.g:4272:2: rule__Achievement__Group_9__0__Impl rule__Achievement__Group_9__1
            {
            pushFollow(FOLLOW_27);
            rule__Achievement__Group_9__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group_9__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_9__0"


    // $ANTLR start "rule__Achievement__Group_9__0__Impl"
    // InternalDSL.g:4279:1: rule__Achievement__Group_9__0__Impl : ( ',' ) ;
    public final void rule__Achievement__Group_9__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4283:1: ( ( ',' ) )
            // InternalDSL.g:4284:1: ( ',' )
            {
            // InternalDSL.g:4284:1: ( ',' )
            // InternalDSL.g:4285:2: ','
            {
             before(grammarAccess.getAchievementAccess().getCommaKeyword_9_0()); 
            match(input,64,FOLLOW_2); 
             after(grammarAccess.getAchievementAccess().getCommaKeyword_9_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_9__0__Impl"


    // $ANTLR start "rule__Achievement__Group_9__1"
    // InternalDSL.g:4294:1: rule__Achievement__Group_9__1 : rule__Achievement__Group_9__1__Impl ;
    public final void rule__Achievement__Group_9__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4298:1: ( rule__Achievement__Group_9__1__Impl )
            // InternalDSL.g:4299:2: rule__Achievement__Group_9__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Achievement__Group_9__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_9__1"


    // $ANTLR start "rule__Achievement__Group_9__1__Impl"
    // InternalDSL.g:4305:1: rule__Achievement__Group_9__1__Impl : ( ( rule__Achievement__ConditionsAssignment_9_1 ) ) ;
    public final void rule__Achievement__Group_9__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4309:1: ( ( ( rule__Achievement__ConditionsAssignment_9_1 ) ) )
            // InternalDSL.g:4310:1: ( ( rule__Achievement__ConditionsAssignment_9_1 ) )
            {
            // InternalDSL.g:4310:1: ( ( rule__Achievement__ConditionsAssignment_9_1 ) )
            // InternalDSL.g:4311:2: ( rule__Achievement__ConditionsAssignment_9_1 )
            {
             before(grammarAccess.getAchievementAccess().getConditionsAssignment_9_1()); 
            // InternalDSL.g:4312:2: ( rule__Achievement__ConditionsAssignment_9_1 )
            // InternalDSL.g:4312:3: rule__Achievement__ConditionsAssignment_9_1
            {
            pushFollow(FOLLOW_2);
            rule__Achievement__ConditionsAssignment_9_1();

            state._fsp--;


            }

             after(grammarAccess.getAchievementAccess().getConditionsAssignment_9_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_9__1__Impl"


    // $ANTLR start "rule__Achievement__Group_11__0"
    // InternalDSL.g:4321:1: rule__Achievement__Group_11__0 : rule__Achievement__Group_11__0__Impl rule__Achievement__Group_11__1 ;
    public final void rule__Achievement__Group_11__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4325:1: ( rule__Achievement__Group_11__0__Impl rule__Achievement__Group_11__1 )
            // InternalDSL.g:4326:2: rule__Achievement__Group_11__0__Impl rule__Achievement__Group_11__1
            {
            pushFollow(FOLLOW_12);
            rule__Achievement__Group_11__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Achievement__Group_11__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_11__0"


    // $ANTLR start "rule__Achievement__Group_11__0__Impl"
    // InternalDSL.g:4333:1: rule__Achievement__Group_11__0__Impl : ( 'Information:' ) ;
    public final void rule__Achievement__Group_11__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4337:1: ( ( 'Information:' ) )
            // InternalDSL.g:4338:1: ( 'Information:' )
            {
            // InternalDSL.g:4338:1: ( 'Information:' )
            // InternalDSL.g:4339:2: 'Information:'
            {
             before(grammarAccess.getAchievementAccess().getInformationKeyword_11_0()); 
            match(input,65,FOLLOW_2); 
             after(grammarAccess.getAchievementAccess().getInformationKeyword_11_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_11__0__Impl"


    // $ANTLR start "rule__Achievement__Group_11__1"
    // InternalDSL.g:4348:1: rule__Achievement__Group_11__1 : rule__Achievement__Group_11__1__Impl ;
    public final void rule__Achievement__Group_11__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4352:1: ( rule__Achievement__Group_11__1__Impl )
            // InternalDSL.g:4353:2: rule__Achievement__Group_11__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Achievement__Group_11__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_11__1"


    // $ANTLR start "rule__Achievement__Group_11__1__Impl"
    // InternalDSL.g:4359:1: rule__Achievement__Group_11__1__Impl : ( ( rule__Achievement__InformationAssignment_11_1 ) ) ;
    public final void rule__Achievement__Group_11__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4363:1: ( ( ( rule__Achievement__InformationAssignment_11_1 ) ) )
            // InternalDSL.g:4364:1: ( ( rule__Achievement__InformationAssignment_11_1 ) )
            {
            // InternalDSL.g:4364:1: ( ( rule__Achievement__InformationAssignment_11_1 ) )
            // InternalDSL.g:4365:2: ( rule__Achievement__InformationAssignment_11_1 )
            {
             before(grammarAccess.getAchievementAccess().getInformationAssignment_11_1()); 
            // InternalDSL.g:4366:2: ( rule__Achievement__InformationAssignment_11_1 )
            // InternalDSL.g:4366:3: rule__Achievement__InformationAssignment_11_1
            {
            pushFollow(FOLLOW_2);
            rule__Achievement__InformationAssignment_11_1();

            state._fsp--;


            }

             after(grammarAccess.getAchievementAccess().getInformationAssignment_11_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__Group_11__1__Impl"


    // $ANTLR start "rule__Event__Group__0"
    // InternalDSL.g:4375:1: rule__Event__Group__0 : rule__Event__Group__0__Impl rule__Event__Group__1 ;
    public final void rule__Event__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4379:1: ( rule__Event__Group__0__Impl rule__Event__Group__1 )
            // InternalDSL.g:4380:2: rule__Event__Group__0__Impl rule__Event__Group__1
            {
            pushFollow(FOLLOW_25);
            rule__Event__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__0"


    // $ANTLR start "rule__Event__Group__0__Impl"
    // InternalDSL.g:4387:1: rule__Event__Group__0__Impl : ( () ) ;
    public final void rule__Event__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4391:1: ( ( () ) )
            // InternalDSL.g:4392:1: ( () )
            {
            // InternalDSL.g:4392:1: ( () )
            // InternalDSL.g:4393:2: ()
            {
             before(grammarAccess.getEventAccess().getEventAction_0()); 
            // InternalDSL.g:4394:2: ()
            // InternalDSL.g:4394:3: 
            {
            }

             after(grammarAccess.getEventAccess().getEventAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__0__Impl"


    // $ANTLR start "rule__Event__Group__1"
    // InternalDSL.g:4402:1: rule__Event__Group__1 : rule__Event__Group__1__Impl rule__Event__Group__2 ;
    public final void rule__Event__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4406:1: ( rule__Event__Group__1__Impl rule__Event__Group__2 )
            // InternalDSL.g:4407:2: rule__Event__Group__1__Impl rule__Event__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__Event__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__1"


    // $ANTLR start "rule__Event__Group__1__Impl"
    // InternalDSL.g:4414:1: rule__Event__Group__1__Impl : ( 'Event' ) ;
    public final void rule__Event__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4418:1: ( ( 'Event' ) )
            // InternalDSL.g:4419:1: ( 'Event' )
            {
            // InternalDSL.g:4419:1: ( 'Event' )
            // InternalDSL.g:4420:2: 'Event'
            {
             before(grammarAccess.getEventAccess().getEventKeyword_1()); 
            match(input,84,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getEventKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__1__Impl"


    // $ANTLR start "rule__Event__Group__2"
    // InternalDSL.g:4429:1: rule__Event__Group__2 : rule__Event__Group__2__Impl rule__Event__Group__3 ;
    public final void rule__Event__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4433:1: ( rule__Event__Group__2__Impl rule__Event__Group__3 )
            // InternalDSL.g:4434:2: rule__Event__Group__2__Impl rule__Event__Group__3
            {
            pushFollow(FOLLOW_32);
            rule__Event__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__2"


    // $ANTLR start "rule__Event__Group__2__Impl"
    // InternalDSL.g:4441:1: rule__Event__Group__2__Impl : ( '{' ) ;
    public final void rule__Event__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4445:1: ( ( '{' ) )
            // InternalDSL.g:4446:1: ( '{' )
            {
            // InternalDSL.g:4446:1: ( '{' )
            // InternalDSL.g:4447:2: '{'
            {
             before(grammarAccess.getEventAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,58,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__2__Impl"


    // $ANTLR start "rule__Event__Group__3"
    // InternalDSL.g:4456:1: rule__Event__Group__3 : rule__Event__Group__3__Impl rule__Event__Group__4 ;
    public final void rule__Event__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4460:1: ( rule__Event__Group__3__Impl rule__Event__Group__4 )
            // InternalDSL.g:4461:2: rule__Event__Group__3__Impl rule__Event__Group__4
            {
            pushFollow(FOLLOW_32);
            rule__Event__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__3"


    // $ANTLR start "rule__Event__Group__3__Impl"
    // InternalDSL.g:4468:1: rule__Event__Group__3__Impl : ( ( rule__Event__Group_3__0 )? ) ;
    public final void rule__Event__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4472:1: ( ( ( rule__Event__Group_3__0 )? ) )
            // InternalDSL.g:4473:1: ( ( rule__Event__Group_3__0 )? )
            {
            // InternalDSL.g:4473:1: ( ( rule__Event__Group_3__0 )? )
            // InternalDSL.g:4474:2: ( rule__Event__Group_3__0 )?
            {
             before(grammarAccess.getEventAccess().getGroup_3()); 
            // InternalDSL.g:4475:2: ( rule__Event__Group_3__0 )?
            int alt39=2;
            int LA39_0 = input.LA(1);

            if ( (LA39_0==62) ) {
                alt39=1;
            }
            switch (alt39) {
                case 1 :
                    // InternalDSL.g:4475:3: rule__Event__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Event__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getEventAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__3__Impl"


    // $ANTLR start "rule__Event__Group__4"
    // InternalDSL.g:4483:1: rule__Event__Group__4 : rule__Event__Group__4__Impl rule__Event__Group__5 ;
    public final void rule__Event__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4487:1: ( rule__Event__Group__4__Impl rule__Event__Group__5 )
            // InternalDSL.g:4488:2: rule__Event__Group__4__Impl rule__Event__Group__5
            {
            pushFollow(FOLLOW_32);
            rule__Event__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__4"


    // $ANTLR start "rule__Event__Group__4__Impl"
    // InternalDSL.g:4495:1: rule__Event__Group__4__Impl : ( ( rule__Event__Group_4__0 )? ) ;
    public final void rule__Event__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4499:1: ( ( ( rule__Event__Group_4__0 )? ) )
            // InternalDSL.g:4500:1: ( ( rule__Event__Group_4__0 )? )
            {
            // InternalDSL.g:4500:1: ( ( rule__Event__Group_4__0 )? )
            // InternalDSL.g:4501:2: ( rule__Event__Group_4__0 )?
            {
             before(grammarAccess.getEventAccess().getGroup_4()); 
            // InternalDSL.g:4502:2: ( rule__Event__Group_4__0 )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==85) ) {
                alt40=1;
            }
            switch (alt40) {
                case 1 :
                    // InternalDSL.g:4502:3: rule__Event__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Event__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getEventAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__4__Impl"


    // $ANTLR start "rule__Event__Group__5"
    // InternalDSL.g:4510:1: rule__Event__Group__5 : rule__Event__Group__5__Impl rule__Event__Group__6 ;
    public final void rule__Event__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4514:1: ( rule__Event__Group__5__Impl rule__Event__Group__6 )
            // InternalDSL.g:4515:2: rule__Event__Group__5__Impl rule__Event__Group__6
            {
            pushFollow(FOLLOW_32);
            rule__Event__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__5"


    // $ANTLR start "rule__Event__Group__5__Impl"
    // InternalDSL.g:4522:1: rule__Event__Group__5__Impl : ( ( rule__Event__Group_5__0 )? ) ;
    public final void rule__Event__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4526:1: ( ( ( rule__Event__Group_5__0 )? ) )
            // InternalDSL.g:4527:1: ( ( rule__Event__Group_5__0 )? )
            {
            // InternalDSL.g:4527:1: ( ( rule__Event__Group_5__0 )? )
            // InternalDSL.g:4528:2: ( rule__Event__Group_5__0 )?
            {
             before(grammarAccess.getEventAccess().getGroup_5()); 
            // InternalDSL.g:4529:2: ( rule__Event__Group_5__0 )?
            int alt41=2;
            int LA41_0 = input.LA(1);

            if ( (LA41_0==86) ) {
                alt41=1;
            }
            switch (alt41) {
                case 1 :
                    // InternalDSL.g:4529:3: rule__Event__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Event__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getEventAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__5__Impl"


    // $ANTLR start "rule__Event__Group__6"
    // InternalDSL.g:4537:1: rule__Event__Group__6 : rule__Event__Group__6__Impl rule__Event__Group__7 ;
    public final void rule__Event__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4541:1: ( rule__Event__Group__6__Impl rule__Event__Group__7 )
            // InternalDSL.g:4542:2: rule__Event__Group__6__Impl rule__Event__Group__7
            {
            pushFollow(FOLLOW_32);
            rule__Event__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__6"


    // $ANTLR start "rule__Event__Group__6__Impl"
    // InternalDSL.g:4549:1: rule__Event__Group__6__Impl : ( ( rule__Event__Group_6__0 )? ) ;
    public final void rule__Event__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4553:1: ( ( ( rule__Event__Group_6__0 )? ) )
            // InternalDSL.g:4554:1: ( ( rule__Event__Group_6__0 )? )
            {
            // InternalDSL.g:4554:1: ( ( rule__Event__Group_6__0 )? )
            // InternalDSL.g:4555:2: ( rule__Event__Group_6__0 )?
            {
             before(grammarAccess.getEventAccess().getGroup_6()); 
            // InternalDSL.g:4556:2: ( rule__Event__Group_6__0 )?
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( (LA42_0==87) ) {
                alt42=1;
            }
            switch (alt42) {
                case 1 :
                    // InternalDSL.g:4556:3: rule__Event__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Event__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getEventAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__6__Impl"


    // $ANTLR start "rule__Event__Group__7"
    // InternalDSL.g:4564:1: rule__Event__Group__7 : rule__Event__Group__7__Impl rule__Event__Group__8 ;
    public final void rule__Event__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4568:1: ( rule__Event__Group__7__Impl rule__Event__Group__8 )
            // InternalDSL.g:4569:2: rule__Event__Group__7__Impl rule__Event__Group__8
            {
            pushFollow(FOLLOW_32);
            rule__Event__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__7"


    // $ANTLR start "rule__Event__Group__7__Impl"
    // InternalDSL.g:4576:1: rule__Event__Group__7__Impl : ( ( rule__Event__Group_7__0 )? ) ;
    public final void rule__Event__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4580:1: ( ( ( rule__Event__Group_7__0 )? ) )
            // InternalDSL.g:4581:1: ( ( rule__Event__Group_7__0 )? )
            {
            // InternalDSL.g:4581:1: ( ( rule__Event__Group_7__0 )? )
            // InternalDSL.g:4582:2: ( rule__Event__Group_7__0 )?
            {
             before(grammarAccess.getEventAccess().getGroup_7()); 
            // InternalDSL.g:4583:2: ( rule__Event__Group_7__0 )?
            int alt43=2;
            int LA43_0 = input.LA(1);

            if ( (LA43_0==88) ) {
                alt43=1;
            }
            switch (alt43) {
                case 1 :
                    // InternalDSL.g:4583:3: rule__Event__Group_7__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Event__Group_7__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getEventAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__7__Impl"


    // $ANTLR start "rule__Event__Group__8"
    // InternalDSL.g:4591:1: rule__Event__Group__8 : rule__Event__Group__8__Impl rule__Event__Group__9 ;
    public final void rule__Event__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4595:1: ( rule__Event__Group__8__Impl rule__Event__Group__9 )
            // InternalDSL.g:4596:2: rule__Event__Group__8__Impl rule__Event__Group__9
            {
            pushFollow(FOLLOW_32);
            rule__Event__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__8"


    // $ANTLR start "rule__Event__Group__8__Impl"
    // InternalDSL.g:4603:1: rule__Event__Group__8__Impl : ( ( rule__Event__Group_8__0 )? ) ;
    public final void rule__Event__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4607:1: ( ( ( rule__Event__Group_8__0 )? ) )
            // InternalDSL.g:4608:1: ( ( rule__Event__Group_8__0 )? )
            {
            // InternalDSL.g:4608:1: ( ( rule__Event__Group_8__0 )? )
            // InternalDSL.g:4609:2: ( rule__Event__Group_8__0 )?
            {
             before(grammarAccess.getEventAccess().getGroup_8()); 
            // InternalDSL.g:4610:2: ( rule__Event__Group_8__0 )?
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( (LA44_0==65) ) {
                alt44=1;
            }
            switch (alt44) {
                case 1 :
                    // InternalDSL.g:4610:3: rule__Event__Group_8__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Event__Group_8__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getEventAccess().getGroup_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__8__Impl"


    // $ANTLR start "rule__Event__Group__9"
    // InternalDSL.g:4618:1: rule__Event__Group__9 : rule__Event__Group__9__Impl ;
    public final void rule__Event__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4622:1: ( rule__Event__Group__9__Impl )
            // InternalDSL.g:4623:2: rule__Event__Group__9__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Event__Group__9__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__9"


    // $ANTLR start "rule__Event__Group__9__Impl"
    // InternalDSL.g:4629:1: rule__Event__Group__9__Impl : ( '}' ) ;
    public final void rule__Event__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4633:1: ( ( '}' ) )
            // InternalDSL.g:4634:1: ( '}' )
            {
            // InternalDSL.g:4634:1: ( '}' )
            // InternalDSL.g:4635:2: '}'
            {
             before(grammarAccess.getEventAccess().getRightCurlyBracketKeyword_9()); 
            match(input,60,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getRightCurlyBracketKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__9__Impl"


    // $ANTLR start "rule__Event__Group_3__0"
    // InternalDSL.g:4645:1: rule__Event__Group_3__0 : rule__Event__Group_3__0__Impl rule__Event__Group_3__1 ;
    public final void rule__Event__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4649:1: ( rule__Event__Group_3__0__Impl rule__Event__Group_3__1 )
            // InternalDSL.g:4650:2: rule__Event__Group_3__0__Impl rule__Event__Group_3__1
            {
            pushFollow(FOLLOW_11);
            rule__Event__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_3__0"


    // $ANTLR start "rule__Event__Group_3__0__Impl"
    // InternalDSL.g:4657:1: rule__Event__Group_3__0__Impl : ( 'Name:' ) ;
    public final void rule__Event__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4661:1: ( ( 'Name:' ) )
            // InternalDSL.g:4662:1: ( 'Name:' )
            {
            // InternalDSL.g:4662:1: ( 'Name:' )
            // InternalDSL.g:4663:2: 'Name:'
            {
             before(grammarAccess.getEventAccess().getNameKeyword_3_0()); 
            match(input,62,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getNameKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_3__0__Impl"


    // $ANTLR start "rule__Event__Group_3__1"
    // InternalDSL.g:4672:1: rule__Event__Group_3__1 : rule__Event__Group_3__1__Impl ;
    public final void rule__Event__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4676:1: ( rule__Event__Group_3__1__Impl )
            // InternalDSL.g:4677:2: rule__Event__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Event__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_3__1"


    // $ANTLR start "rule__Event__Group_3__1__Impl"
    // InternalDSL.g:4683:1: rule__Event__Group_3__1__Impl : ( ( rule__Event__NameAssignment_3_1 ) ) ;
    public final void rule__Event__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4687:1: ( ( ( rule__Event__NameAssignment_3_1 ) ) )
            // InternalDSL.g:4688:1: ( ( rule__Event__NameAssignment_3_1 ) )
            {
            // InternalDSL.g:4688:1: ( ( rule__Event__NameAssignment_3_1 ) )
            // InternalDSL.g:4689:2: ( rule__Event__NameAssignment_3_1 )
            {
             before(grammarAccess.getEventAccess().getNameAssignment_3_1()); 
            // InternalDSL.g:4690:2: ( rule__Event__NameAssignment_3_1 )
            // InternalDSL.g:4690:3: rule__Event__NameAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Event__NameAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getEventAccess().getNameAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_3__1__Impl"


    // $ANTLR start "rule__Event__Group_4__0"
    // InternalDSL.g:4699:1: rule__Event__Group_4__0 : rule__Event__Group_4__0__Impl rule__Event__Group_4__1 ;
    public final void rule__Event__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4703:1: ( rule__Event__Group_4__0__Impl rule__Event__Group_4__1 )
            // InternalDSL.g:4704:2: rule__Event__Group_4__0__Impl rule__Event__Group_4__1
            {
            pushFollow(FOLLOW_33);
            rule__Event__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_4__0"


    // $ANTLR start "rule__Event__Group_4__0__Impl"
    // InternalDSL.g:4711:1: rule__Event__Group_4__0__Impl : ( 'EventType:' ) ;
    public final void rule__Event__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4715:1: ( ( 'EventType:' ) )
            // InternalDSL.g:4716:1: ( 'EventType:' )
            {
            // InternalDSL.g:4716:1: ( 'EventType:' )
            // InternalDSL.g:4717:2: 'EventType:'
            {
             before(grammarAccess.getEventAccess().getEventTypeKeyword_4_0()); 
            match(input,85,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getEventTypeKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_4__0__Impl"


    // $ANTLR start "rule__Event__Group_4__1"
    // InternalDSL.g:4726:1: rule__Event__Group_4__1 : rule__Event__Group_4__1__Impl ;
    public final void rule__Event__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4730:1: ( rule__Event__Group_4__1__Impl )
            // InternalDSL.g:4731:2: rule__Event__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Event__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_4__1"


    // $ANTLR start "rule__Event__Group_4__1__Impl"
    // InternalDSL.g:4737:1: rule__Event__Group_4__1__Impl : ( ( rule__Event__EventTypeAssignment_4_1 ) ) ;
    public final void rule__Event__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4741:1: ( ( ( rule__Event__EventTypeAssignment_4_1 ) ) )
            // InternalDSL.g:4742:1: ( ( rule__Event__EventTypeAssignment_4_1 ) )
            {
            // InternalDSL.g:4742:1: ( ( rule__Event__EventTypeAssignment_4_1 ) )
            // InternalDSL.g:4743:2: ( rule__Event__EventTypeAssignment_4_1 )
            {
             before(grammarAccess.getEventAccess().getEventTypeAssignment_4_1()); 
            // InternalDSL.g:4744:2: ( rule__Event__EventTypeAssignment_4_1 )
            // InternalDSL.g:4744:3: rule__Event__EventTypeAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Event__EventTypeAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getEventAccess().getEventTypeAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_4__1__Impl"


    // $ANTLR start "rule__Event__Group_5__0"
    // InternalDSL.g:4753:1: rule__Event__Group_5__0 : rule__Event__Group_5__0__Impl rule__Event__Group_5__1 ;
    public final void rule__Event__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4757:1: ( rule__Event__Group_5__0__Impl rule__Event__Group_5__1 )
            // InternalDSL.g:4758:2: rule__Event__Group_5__0__Impl rule__Event__Group_5__1
            {
            pushFollow(FOLLOW_34);
            rule__Event__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_5__0"


    // $ANTLR start "rule__Event__Group_5__0__Impl"
    // InternalDSL.g:4765:1: rule__Event__Group_5__0__Impl : ( 'PointGain:' ) ;
    public final void rule__Event__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4769:1: ( ( 'PointGain:' ) )
            // InternalDSL.g:4770:1: ( 'PointGain:' )
            {
            // InternalDSL.g:4770:1: ( 'PointGain:' )
            // InternalDSL.g:4771:2: 'PointGain:'
            {
             before(grammarAccess.getEventAccess().getPointGainKeyword_5_0()); 
            match(input,86,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getPointGainKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_5__0__Impl"


    // $ANTLR start "rule__Event__Group_5__1"
    // InternalDSL.g:4780:1: rule__Event__Group_5__1 : rule__Event__Group_5__1__Impl ;
    public final void rule__Event__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4784:1: ( rule__Event__Group_5__1__Impl )
            // InternalDSL.g:4785:2: rule__Event__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Event__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_5__1"


    // $ANTLR start "rule__Event__Group_5__1__Impl"
    // InternalDSL.g:4791:1: rule__Event__Group_5__1__Impl : ( ( rule__Event__PointGainAssignment_5_1 ) ) ;
    public final void rule__Event__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4795:1: ( ( ( rule__Event__PointGainAssignment_5_1 ) ) )
            // InternalDSL.g:4796:1: ( ( rule__Event__PointGainAssignment_5_1 ) )
            {
            // InternalDSL.g:4796:1: ( ( rule__Event__PointGainAssignment_5_1 ) )
            // InternalDSL.g:4797:2: ( rule__Event__PointGainAssignment_5_1 )
            {
             before(grammarAccess.getEventAccess().getPointGainAssignment_5_1()); 
            // InternalDSL.g:4798:2: ( rule__Event__PointGainAssignment_5_1 )
            // InternalDSL.g:4798:3: rule__Event__PointGainAssignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__Event__PointGainAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getEventAccess().getPointGainAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_5__1__Impl"


    // $ANTLR start "rule__Event__Group_6__0"
    // InternalDSL.g:4807:1: rule__Event__Group_6__0 : rule__Event__Group_6__0__Impl rule__Event__Group_6__1 ;
    public final void rule__Event__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4811:1: ( rule__Event__Group_6__0__Impl rule__Event__Group_6__1 )
            // InternalDSL.g:4812:2: rule__Event__Group_6__0__Impl rule__Event__Group_6__1
            {
            pushFollow(FOLLOW_29);
            rule__Event__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_6__0"


    // $ANTLR start "rule__Event__Group_6__0__Impl"
    // InternalDSL.g:4819:1: rule__Event__Group_6__0__Impl : ( 'TriggerConditions' ) ;
    public final void rule__Event__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4823:1: ( ( 'TriggerConditions' ) )
            // InternalDSL.g:4824:1: ( 'TriggerConditions' )
            {
            // InternalDSL.g:4824:1: ( 'TriggerConditions' )
            // InternalDSL.g:4825:2: 'TriggerConditions'
            {
             before(grammarAccess.getEventAccess().getTriggerConditionsKeyword_6_0()); 
            match(input,87,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getTriggerConditionsKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_6__0__Impl"


    // $ANTLR start "rule__Event__Group_6__1"
    // InternalDSL.g:4834:1: rule__Event__Group_6__1 : rule__Event__Group_6__1__Impl rule__Event__Group_6__2 ;
    public final void rule__Event__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4838:1: ( rule__Event__Group_6__1__Impl rule__Event__Group_6__2 )
            // InternalDSL.g:4839:2: rule__Event__Group_6__1__Impl rule__Event__Group_6__2
            {
            pushFollow(FOLLOW_11);
            rule__Event__Group_6__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group_6__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_6__1"


    // $ANTLR start "rule__Event__Group_6__1__Impl"
    // InternalDSL.g:4846:1: rule__Event__Group_6__1__Impl : ( '(' ) ;
    public final void rule__Event__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4850:1: ( ( '(' ) )
            // InternalDSL.g:4851:1: ( '(' )
            {
            // InternalDSL.g:4851:1: ( '(' )
            // InternalDSL.g:4852:2: '('
            {
             before(grammarAccess.getEventAccess().getLeftParenthesisKeyword_6_1()); 
            match(input,81,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getLeftParenthesisKeyword_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_6__1__Impl"


    // $ANTLR start "rule__Event__Group_6__2"
    // InternalDSL.g:4861:1: rule__Event__Group_6__2 : rule__Event__Group_6__2__Impl rule__Event__Group_6__3 ;
    public final void rule__Event__Group_6__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4865:1: ( rule__Event__Group_6__2__Impl rule__Event__Group_6__3 )
            // InternalDSL.g:4866:2: rule__Event__Group_6__2__Impl rule__Event__Group_6__3
            {
            pushFollow(FOLLOW_30);
            rule__Event__Group_6__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group_6__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_6__2"


    // $ANTLR start "rule__Event__Group_6__2__Impl"
    // InternalDSL.g:4873:1: rule__Event__Group_6__2__Impl : ( ( rule__Event__TriggerConditionsAssignment_6_2 ) ) ;
    public final void rule__Event__Group_6__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4877:1: ( ( ( rule__Event__TriggerConditionsAssignment_6_2 ) ) )
            // InternalDSL.g:4878:1: ( ( rule__Event__TriggerConditionsAssignment_6_2 ) )
            {
            // InternalDSL.g:4878:1: ( ( rule__Event__TriggerConditionsAssignment_6_2 ) )
            // InternalDSL.g:4879:2: ( rule__Event__TriggerConditionsAssignment_6_2 )
            {
             before(grammarAccess.getEventAccess().getTriggerConditionsAssignment_6_2()); 
            // InternalDSL.g:4880:2: ( rule__Event__TriggerConditionsAssignment_6_2 )
            // InternalDSL.g:4880:3: rule__Event__TriggerConditionsAssignment_6_2
            {
            pushFollow(FOLLOW_2);
            rule__Event__TriggerConditionsAssignment_6_2();

            state._fsp--;


            }

             after(grammarAccess.getEventAccess().getTriggerConditionsAssignment_6_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_6__2__Impl"


    // $ANTLR start "rule__Event__Group_6__3"
    // InternalDSL.g:4888:1: rule__Event__Group_6__3 : rule__Event__Group_6__3__Impl rule__Event__Group_6__4 ;
    public final void rule__Event__Group_6__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4892:1: ( rule__Event__Group_6__3__Impl rule__Event__Group_6__4 )
            // InternalDSL.g:4893:2: rule__Event__Group_6__3__Impl rule__Event__Group_6__4
            {
            pushFollow(FOLLOW_30);
            rule__Event__Group_6__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group_6__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_6__3"


    // $ANTLR start "rule__Event__Group_6__3__Impl"
    // InternalDSL.g:4900:1: rule__Event__Group_6__3__Impl : ( ( rule__Event__Group_6_3__0 )* ) ;
    public final void rule__Event__Group_6__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4904:1: ( ( ( rule__Event__Group_6_3__0 )* ) )
            // InternalDSL.g:4905:1: ( ( rule__Event__Group_6_3__0 )* )
            {
            // InternalDSL.g:4905:1: ( ( rule__Event__Group_6_3__0 )* )
            // InternalDSL.g:4906:2: ( rule__Event__Group_6_3__0 )*
            {
             before(grammarAccess.getEventAccess().getGroup_6_3()); 
            // InternalDSL.g:4907:2: ( rule__Event__Group_6_3__0 )*
            loop45:
            do {
                int alt45=2;
                int LA45_0 = input.LA(1);

                if ( (LA45_0==64) ) {
                    alt45=1;
                }


                switch (alt45) {
            	case 1 :
            	    // InternalDSL.g:4907:3: rule__Event__Group_6_3__0
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__Event__Group_6_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop45;
                }
            } while (true);

             after(grammarAccess.getEventAccess().getGroup_6_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_6__3__Impl"


    // $ANTLR start "rule__Event__Group_6__4"
    // InternalDSL.g:4915:1: rule__Event__Group_6__4 : rule__Event__Group_6__4__Impl ;
    public final void rule__Event__Group_6__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4919:1: ( rule__Event__Group_6__4__Impl )
            // InternalDSL.g:4920:2: rule__Event__Group_6__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Event__Group_6__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_6__4"


    // $ANTLR start "rule__Event__Group_6__4__Impl"
    // InternalDSL.g:4926:1: rule__Event__Group_6__4__Impl : ( ')' ) ;
    public final void rule__Event__Group_6__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4930:1: ( ( ')' ) )
            // InternalDSL.g:4931:1: ( ')' )
            {
            // InternalDSL.g:4931:1: ( ')' )
            // InternalDSL.g:4932:2: ')'
            {
             before(grammarAccess.getEventAccess().getRightParenthesisKeyword_6_4()); 
            match(input,82,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getRightParenthesisKeyword_6_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_6__4__Impl"


    // $ANTLR start "rule__Event__Group_6_3__0"
    // InternalDSL.g:4942:1: rule__Event__Group_6_3__0 : rule__Event__Group_6_3__0__Impl rule__Event__Group_6_3__1 ;
    public final void rule__Event__Group_6_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4946:1: ( rule__Event__Group_6_3__0__Impl rule__Event__Group_6_3__1 )
            // InternalDSL.g:4947:2: rule__Event__Group_6_3__0__Impl rule__Event__Group_6_3__1
            {
            pushFollow(FOLLOW_11);
            rule__Event__Group_6_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group_6_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_6_3__0"


    // $ANTLR start "rule__Event__Group_6_3__0__Impl"
    // InternalDSL.g:4954:1: rule__Event__Group_6_3__0__Impl : ( ',' ) ;
    public final void rule__Event__Group_6_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4958:1: ( ( ',' ) )
            // InternalDSL.g:4959:1: ( ',' )
            {
            // InternalDSL.g:4959:1: ( ',' )
            // InternalDSL.g:4960:2: ','
            {
             before(grammarAccess.getEventAccess().getCommaKeyword_6_3_0()); 
            match(input,64,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getCommaKeyword_6_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_6_3__0__Impl"


    // $ANTLR start "rule__Event__Group_6_3__1"
    // InternalDSL.g:4969:1: rule__Event__Group_6_3__1 : rule__Event__Group_6_3__1__Impl ;
    public final void rule__Event__Group_6_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4973:1: ( rule__Event__Group_6_3__1__Impl )
            // InternalDSL.g:4974:2: rule__Event__Group_6_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Event__Group_6_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_6_3__1"


    // $ANTLR start "rule__Event__Group_6_3__1__Impl"
    // InternalDSL.g:4980:1: rule__Event__Group_6_3__1__Impl : ( ( rule__Event__TriggerConditionsAssignment_6_3_1 ) ) ;
    public final void rule__Event__Group_6_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:4984:1: ( ( ( rule__Event__TriggerConditionsAssignment_6_3_1 ) ) )
            // InternalDSL.g:4985:1: ( ( rule__Event__TriggerConditionsAssignment_6_3_1 ) )
            {
            // InternalDSL.g:4985:1: ( ( rule__Event__TriggerConditionsAssignment_6_3_1 ) )
            // InternalDSL.g:4986:2: ( rule__Event__TriggerConditionsAssignment_6_3_1 )
            {
             before(grammarAccess.getEventAccess().getTriggerConditionsAssignment_6_3_1()); 
            // InternalDSL.g:4987:2: ( rule__Event__TriggerConditionsAssignment_6_3_1 )
            // InternalDSL.g:4987:3: rule__Event__TriggerConditionsAssignment_6_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Event__TriggerConditionsAssignment_6_3_1();

            state._fsp--;


            }

             after(grammarAccess.getEventAccess().getTriggerConditionsAssignment_6_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_6_3__1__Impl"


    // $ANTLR start "rule__Event__Group_7__0"
    // InternalDSL.g:4996:1: rule__Event__Group_7__0 : rule__Event__Group_7__0__Impl rule__Event__Group_7__1 ;
    public final void rule__Event__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5000:1: ( rule__Event__Group_7__0__Impl rule__Event__Group_7__1 )
            // InternalDSL.g:5001:2: rule__Event__Group_7__0__Impl rule__Event__Group_7__1
            {
            pushFollow(FOLLOW_29);
            rule__Event__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_7__0"


    // $ANTLR start "rule__Event__Group_7__0__Impl"
    // InternalDSL.g:5008:1: rule__Event__Group_7__0__Impl : ( 'Restrictions' ) ;
    public final void rule__Event__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5012:1: ( ( 'Restrictions' ) )
            // InternalDSL.g:5013:1: ( 'Restrictions' )
            {
            // InternalDSL.g:5013:1: ( 'Restrictions' )
            // InternalDSL.g:5014:2: 'Restrictions'
            {
             before(grammarAccess.getEventAccess().getRestrictionsKeyword_7_0()); 
            match(input,88,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getRestrictionsKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_7__0__Impl"


    // $ANTLR start "rule__Event__Group_7__1"
    // InternalDSL.g:5023:1: rule__Event__Group_7__1 : rule__Event__Group_7__1__Impl rule__Event__Group_7__2 ;
    public final void rule__Event__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5027:1: ( rule__Event__Group_7__1__Impl rule__Event__Group_7__2 )
            // InternalDSL.g:5028:2: rule__Event__Group_7__1__Impl rule__Event__Group_7__2
            {
            pushFollow(FOLLOW_35);
            rule__Event__Group_7__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group_7__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_7__1"


    // $ANTLR start "rule__Event__Group_7__1__Impl"
    // InternalDSL.g:5035:1: rule__Event__Group_7__1__Impl : ( '(' ) ;
    public final void rule__Event__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5039:1: ( ( '(' ) )
            // InternalDSL.g:5040:1: ( '(' )
            {
            // InternalDSL.g:5040:1: ( '(' )
            // InternalDSL.g:5041:2: '('
            {
             before(grammarAccess.getEventAccess().getLeftParenthesisKeyword_7_1()); 
            match(input,81,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getLeftParenthesisKeyword_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_7__1__Impl"


    // $ANTLR start "rule__Event__Group_7__2"
    // InternalDSL.g:5050:1: rule__Event__Group_7__2 : rule__Event__Group_7__2__Impl rule__Event__Group_7__3 ;
    public final void rule__Event__Group_7__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5054:1: ( rule__Event__Group_7__2__Impl rule__Event__Group_7__3 )
            // InternalDSL.g:5055:2: rule__Event__Group_7__2__Impl rule__Event__Group_7__3
            {
            pushFollow(FOLLOW_30);
            rule__Event__Group_7__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group_7__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_7__2"


    // $ANTLR start "rule__Event__Group_7__2__Impl"
    // InternalDSL.g:5062:1: rule__Event__Group_7__2__Impl : ( ( rule__Event__RestrictionAssignment_7_2 ) ) ;
    public final void rule__Event__Group_7__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5066:1: ( ( ( rule__Event__RestrictionAssignment_7_2 ) ) )
            // InternalDSL.g:5067:1: ( ( rule__Event__RestrictionAssignment_7_2 ) )
            {
            // InternalDSL.g:5067:1: ( ( rule__Event__RestrictionAssignment_7_2 ) )
            // InternalDSL.g:5068:2: ( rule__Event__RestrictionAssignment_7_2 )
            {
             before(grammarAccess.getEventAccess().getRestrictionAssignment_7_2()); 
            // InternalDSL.g:5069:2: ( rule__Event__RestrictionAssignment_7_2 )
            // InternalDSL.g:5069:3: rule__Event__RestrictionAssignment_7_2
            {
            pushFollow(FOLLOW_2);
            rule__Event__RestrictionAssignment_7_2();

            state._fsp--;


            }

             after(grammarAccess.getEventAccess().getRestrictionAssignment_7_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_7__2__Impl"


    // $ANTLR start "rule__Event__Group_7__3"
    // InternalDSL.g:5077:1: rule__Event__Group_7__3 : rule__Event__Group_7__3__Impl rule__Event__Group_7__4 ;
    public final void rule__Event__Group_7__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5081:1: ( rule__Event__Group_7__3__Impl rule__Event__Group_7__4 )
            // InternalDSL.g:5082:2: rule__Event__Group_7__3__Impl rule__Event__Group_7__4
            {
            pushFollow(FOLLOW_30);
            rule__Event__Group_7__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group_7__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_7__3"


    // $ANTLR start "rule__Event__Group_7__3__Impl"
    // InternalDSL.g:5089:1: rule__Event__Group_7__3__Impl : ( ( rule__Event__Group_7_3__0 )* ) ;
    public final void rule__Event__Group_7__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5093:1: ( ( ( rule__Event__Group_7_3__0 )* ) )
            // InternalDSL.g:5094:1: ( ( rule__Event__Group_7_3__0 )* )
            {
            // InternalDSL.g:5094:1: ( ( rule__Event__Group_7_3__0 )* )
            // InternalDSL.g:5095:2: ( rule__Event__Group_7_3__0 )*
            {
             before(grammarAccess.getEventAccess().getGroup_7_3()); 
            // InternalDSL.g:5096:2: ( rule__Event__Group_7_3__0 )*
            loop46:
            do {
                int alt46=2;
                int LA46_0 = input.LA(1);

                if ( (LA46_0==64) ) {
                    alt46=1;
                }


                switch (alt46) {
            	case 1 :
            	    // InternalDSL.g:5096:3: rule__Event__Group_7_3__0
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__Event__Group_7_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop46;
                }
            } while (true);

             after(grammarAccess.getEventAccess().getGroup_7_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_7__3__Impl"


    // $ANTLR start "rule__Event__Group_7__4"
    // InternalDSL.g:5104:1: rule__Event__Group_7__4 : rule__Event__Group_7__4__Impl ;
    public final void rule__Event__Group_7__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5108:1: ( rule__Event__Group_7__4__Impl )
            // InternalDSL.g:5109:2: rule__Event__Group_7__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Event__Group_7__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_7__4"


    // $ANTLR start "rule__Event__Group_7__4__Impl"
    // InternalDSL.g:5115:1: rule__Event__Group_7__4__Impl : ( ')' ) ;
    public final void rule__Event__Group_7__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5119:1: ( ( ')' ) )
            // InternalDSL.g:5120:1: ( ')' )
            {
            // InternalDSL.g:5120:1: ( ')' )
            // InternalDSL.g:5121:2: ')'
            {
             before(grammarAccess.getEventAccess().getRightParenthesisKeyword_7_4()); 
            match(input,82,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getRightParenthesisKeyword_7_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_7__4__Impl"


    // $ANTLR start "rule__Event__Group_7_3__0"
    // InternalDSL.g:5131:1: rule__Event__Group_7_3__0 : rule__Event__Group_7_3__0__Impl rule__Event__Group_7_3__1 ;
    public final void rule__Event__Group_7_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5135:1: ( rule__Event__Group_7_3__0__Impl rule__Event__Group_7_3__1 )
            // InternalDSL.g:5136:2: rule__Event__Group_7_3__0__Impl rule__Event__Group_7_3__1
            {
            pushFollow(FOLLOW_35);
            rule__Event__Group_7_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group_7_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_7_3__0"


    // $ANTLR start "rule__Event__Group_7_3__0__Impl"
    // InternalDSL.g:5143:1: rule__Event__Group_7_3__0__Impl : ( ',' ) ;
    public final void rule__Event__Group_7_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5147:1: ( ( ',' ) )
            // InternalDSL.g:5148:1: ( ',' )
            {
            // InternalDSL.g:5148:1: ( ',' )
            // InternalDSL.g:5149:2: ','
            {
             before(grammarAccess.getEventAccess().getCommaKeyword_7_3_0()); 
            match(input,64,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getCommaKeyword_7_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_7_3__0__Impl"


    // $ANTLR start "rule__Event__Group_7_3__1"
    // InternalDSL.g:5158:1: rule__Event__Group_7_3__1 : rule__Event__Group_7_3__1__Impl ;
    public final void rule__Event__Group_7_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5162:1: ( rule__Event__Group_7_3__1__Impl )
            // InternalDSL.g:5163:2: rule__Event__Group_7_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Event__Group_7_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_7_3__1"


    // $ANTLR start "rule__Event__Group_7_3__1__Impl"
    // InternalDSL.g:5169:1: rule__Event__Group_7_3__1__Impl : ( ( rule__Event__RestrictionAssignment_7_3_1 ) ) ;
    public final void rule__Event__Group_7_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5173:1: ( ( ( rule__Event__RestrictionAssignment_7_3_1 ) ) )
            // InternalDSL.g:5174:1: ( ( rule__Event__RestrictionAssignment_7_3_1 ) )
            {
            // InternalDSL.g:5174:1: ( ( rule__Event__RestrictionAssignment_7_3_1 ) )
            // InternalDSL.g:5175:2: ( rule__Event__RestrictionAssignment_7_3_1 )
            {
             before(grammarAccess.getEventAccess().getRestrictionAssignment_7_3_1()); 
            // InternalDSL.g:5176:2: ( rule__Event__RestrictionAssignment_7_3_1 )
            // InternalDSL.g:5176:3: rule__Event__RestrictionAssignment_7_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Event__RestrictionAssignment_7_3_1();

            state._fsp--;


            }

             after(grammarAccess.getEventAccess().getRestrictionAssignment_7_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_7_3__1__Impl"


    // $ANTLR start "rule__Event__Group_8__0"
    // InternalDSL.g:5185:1: rule__Event__Group_8__0 : rule__Event__Group_8__0__Impl rule__Event__Group_8__1 ;
    public final void rule__Event__Group_8__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5189:1: ( rule__Event__Group_8__0__Impl rule__Event__Group_8__1 )
            // InternalDSL.g:5190:2: rule__Event__Group_8__0__Impl rule__Event__Group_8__1
            {
            pushFollow(FOLLOW_12);
            rule__Event__Group_8__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group_8__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_8__0"


    // $ANTLR start "rule__Event__Group_8__0__Impl"
    // InternalDSL.g:5197:1: rule__Event__Group_8__0__Impl : ( 'Information:' ) ;
    public final void rule__Event__Group_8__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5201:1: ( ( 'Information:' ) )
            // InternalDSL.g:5202:1: ( 'Information:' )
            {
            // InternalDSL.g:5202:1: ( 'Information:' )
            // InternalDSL.g:5203:2: 'Information:'
            {
             before(grammarAccess.getEventAccess().getInformationKeyword_8_0()); 
            match(input,65,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getInformationKeyword_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_8__0__Impl"


    // $ANTLR start "rule__Event__Group_8__1"
    // InternalDSL.g:5212:1: rule__Event__Group_8__1 : rule__Event__Group_8__1__Impl ;
    public final void rule__Event__Group_8__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5216:1: ( rule__Event__Group_8__1__Impl )
            // InternalDSL.g:5217:2: rule__Event__Group_8__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Event__Group_8__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_8__1"


    // $ANTLR start "rule__Event__Group_8__1__Impl"
    // InternalDSL.g:5223:1: rule__Event__Group_8__1__Impl : ( ( rule__Event__InformationAssignment_8_1 ) ) ;
    public final void rule__Event__Group_8__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5227:1: ( ( ( rule__Event__InformationAssignment_8_1 ) ) )
            // InternalDSL.g:5228:1: ( ( rule__Event__InformationAssignment_8_1 ) )
            {
            // InternalDSL.g:5228:1: ( ( rule__Event__InformationAssignment_8_1 ) )
            // InternalDSL.g:5229:2: ( rule__Event__InformationAssignment_8_1 )
            {
             before(grammarAccess.getEventAccess().getInformationAssignment_8_1()); 
            // InternalDSL.g:5230:2: ( rule__Event__InformationAssignment_8_1 )
            // InternalDSL.g:5230:3: rule__Event__InformationAssignment_8_1
            {
            pushFollow(FOLLOW_2);
            rule__Event__InformationAssignment_8_1();

            state._fsp--;


            }

             after(grammarAccess.getEventAccess().getInformationAssignment_8_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_8__1__Impl"


    // $ANTLR start "rule__Condition__Group__0"
    // InternalDSL.g:5239:1: rule__Condition__Group__0 : rule__Condition__Group__0__Impl rule__Condition__Group__1 ;
    public final void rule__Condition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5243:1: ( rule__Condition__Group__0__Impl rule__Condition__Group__1 )
            // InternalDSL.g:5244:2: rule__Condition__Group__0__Impl rule__Condition__Group__1
            {
            pushFollow(FOLLOW_27);
            rule__Condition__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Condition__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__0"


    // $ANTLR start "rule__Condition__Group__0__Impl"
    // InternalDSL.g:5251:1: rule__Condition__Group__0__Impl : ( () ) ;
    public final void rule__Condition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5255:1: ( ( () ) )
            // InternalDSL.g:5256:1: ( () )
            {
            // InternalDSL.g:5256:1: ( () )
            // InternalDSL.g:5257:2: ()
            {
             before(grammarAccess.getConditionAccess().getConditionAction_0()); 
            // InternalDSL.g:5258:2: ()
            // InternalDSL.g:5258:3: 
            {
            }

             after(grammarAccess.getConditionAccess().getConditionAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__0__Impl"


    // $ANTLR start "rule__Condition__Group__1"
    // InternalDSL.g:5266:1: rule__Condition__Group__1 : rule__Condition__Group__1__Impl rule__Condition__Group__2 ;
    public final void rule__Condition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5270:1: ( rule__Condition__Group__1__Impl rule__Condition__Group__2 )
            // InternalDSL.g:5271:2: rule__Condition__Group__1__Impl rule__Condition__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__Condition__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Condition__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__1"


    // $ANTLR start "rule__Condition__Group__1__Impl"
    // InternalDSL.g:5278:1: rule__Condition__Group__1__Impl : ( 'Condition' ) ;
    public final void rule__Condition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5282:1: ( ( 'Condition' ) )
            // InternalDSL.g:5283:1: ( 'Condition' )
            {
            // InternalDSL.g:5283:1: ( 'Condition' )
            // InternalDSL.g:5284:2: 'Condition'
            {
             before(grammarAccess.getConditionAccess().getConditionKeyword_1()); 
            match(input,89,FOLLOW_2); 
             after(grammarAccess.getConditionAccess().getConditionKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__1__Impl"


    // $ANTLR start "rule__Condition__Group__2"
    // InternalDSL.g:5293:1: rule__Condition__Group__2 : rule__Condition__Group__2__Impl rule__Condition__Group__3 ;
    public final void rule__Condition__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5297:1: ( rule__Condition__Group__2__Impl rule__Condition__Group__3 )
            // InternalDSL.g:5298:2: rule__Condition__Group__2__Impl rule__Condition__Group__3
            {
            pushFollow(FOLLOW_36);
            rule__Condition__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Condition__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__2"


    // $ANTLR start "rule__Condition__Group__2__Impl"
    // InternalDSL.g:5305:1: rule__Condition__Group__2__Impl : ( '{' ) ;
    public final void rule__Condition__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5309:1: ( ( '{' ) )
            // InternalDSL.g:5310:1: ( '{' )
            {
            // InternalDSL.g:5310:1: ( '{' )
            // InternalDSL.g:5311:2: '{'
            {
             before(grammarAccess.getConditionAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,58,FOLLOW_2); 
             after(grammarAccess.getConditionAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__2__Impl"


    // $ANTLR start "rule__Condition__Group__3"
    // InternalDSL.g:5320:1: rule__Condition__Group__3 : rule__Condition__Group__3__Impl rule__Condition__Group__4 ;
    public final void rule__Condition__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5324:1: ( rule__Condition__Group__3__Impl rule__Condition__Group__4 )
            // InternalDSL.g:5325:2: rule__Condition__Group__3__Impl rule__Condition__Group__4
            {
            pushFollow(FOLLOW_36);
            rule__Condition__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Condition__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__3"


    // $ANTLR start "rule__Condition__Group__3__Impl"
    // InternalDSL.g:5332:1: rule__Condition__Group__3__Impl : ( ( rule__Condition__Group_3__0 )? ) ;
    public final void rule__Condition__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5336:1: ( ( ( rule__Condition__Group_3__0 )? ) )
            // InternalDSL.g:5337:1: ( ( rule__Condition__Group_3__0 )? )
            {
            // InternalDSL.g:5337:1: ( ( rule__Condition__Group_3__0 )? )
            // InternalDSL.g:5338:2: ( rule__Condition__Group_3__0 )?
            {
             before(grammarAccess.getConditionAccess().getGroup_3()); 
            // InternalDSL.g:5339:2: ( rule__Condition__Group_3__0 )?
            int alt47=2;
            int LA47_0 = input.LA(1);

            if ( (LA47_0==62) ) {
                alt47=1;
            }
            switch (alt47) {
                case 1 :
                    // InternalDSL.g:5339:3: rule__Condition__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Condition__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getConditionAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__3__Impl"


    // $ANTLR start "rule__Condition__Group__4"
    // InternalDSL.g:5347:1: rule__Condition__Group__4 : rule__Condition__Group__4__Impl rule__Condition__Group__5 ;
    public final void rule__Condition__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5351:1: ( rule__Condition__Group__4__Impl rule__Condition__Group__5 )
            // InternalDSL.g:5352:2: rule__Condition__Group__4__Impl rule__Condition__Group__5
            {
            pushFollow(FOLLOW_36);
            rule__Condition__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Condition__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__4"


    // $ANTLR start "rule__Condition__Group__4__Impl"
    // InternalDSL.g:5359:1: rule__Condition__Group__4__Impl : ( ( rule__Condition__Group_4__0 )? ) ;
    public final void rule__Condition__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5363:1: ( ( ( rule__Condition__Group_4__0 )? ) )
            // InternalDSL.g:5364:1: ( ( rule__Condition__Group_4__0 )? )
            {
            // InternalDSL.g:5364:1: ( ( rule__Condition__Group_4__0 )? )
            // InternalDSL.g:5365:2: ( rule__Condition__Group_4__0 )?
            {
             before(grammarAccess.getConditionAccess().getGroup_4()); 
            // InternalDSL.g:5366:2: ( rule__Condition__Group_4__0 )?
            int alt48=2;
            int LA48_0 = input.LA(1);

            if ( (LA48_0==90) ) {
                alt48=1;
            }
            switch (alt48) {
                case 1 :
                    // InternalDSL.g:5366:3: rule__Condition__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Condition__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getConditionAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__4__Impl"


    // $ANTLR start "rule__Condition__Group__5"
    // InternalDSL.g:5374:1: rule__Condition__Group__5 : rule__Condition__Group__5__Impl rule__Condition__Group__6 ;
    public final void rule__Condition__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5378:1: ( rule__Condition__Group__5__Impl rule__Condition__Group__6 )
            // InternalDSL.g:5379:2: rule__Condition__Group__5__Impl rule__Condition__Group__6
            {
            pushFollow(FOLLOW_36);
            rule__Condition__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Condition__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__5"


    // $ANTLR start "rule__Condition__Group__5__Impl"
    // InternalDSL.g:5386:1: rule__Condition__Group__5__Impl : ( ( rule__Condition__Group_5__0 )? ) ;
    public final void rule__Condition__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5390:1: ( ( ( rule__Condition__Group_5__0 )? ) )
            // InternalDSL.g:5391:1: ( ( rule__Condition__Group_5__0 )? )
            {
            // InternalDSL.g:5391:1: ( ( rule__Condition__Group_5__0 )? )
            // InternalDSL.g:5392:2: ( rule__Condition__Group_5__0 )?
            {
             before(grammarAccess.getConditionAccess().getGroup_5()); 
            // InternalDSL.g:5393:2: ( rule__Condition__Group_5__0 )?
            int alt49=2;
            int LA49_0 = input.LA(1);

            if ( (LA49_0==91) ) {
                alt49=1;
            }
            switch (alt49) {
                case 1 :
                    // InternalDSL.g:5393:3: rule__Condition__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Condition__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getConditionAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__5__Impl"


    // $ANTLR start "rule__Condition__Group__6"
    // InternalDSL.g:5401:1: rule__Condition__Group__6 : rule__Condition__Group__6__Impl rule__Condition__Group__7 ;
    public final void rule__Condition__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5405:1: ( rule__Condition__Group__6__Impl rule__Condition__Group__7 )
            // InternalDSL.g:5406:2: rule__Condition__Group__6__Impl rule__Condition__Group__7
            {
            pushFollow(FOLLOW_36);
            rule__Condition__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Condition__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__6"


    // $ANTLR start "rule__Condition__Group__6__Impl"
    // InternalDSL.g:5413:1: rule__Condition__Group__6__Impl : ( ( rule__Condition__Group_6__0 )? ) ;
    public final void rule__Condition__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5417:1: ( ( ( rule__Condition__Group_6__0 )? ) )
            // InternalDSL.g:5418:1: ( ( rule__Condition__Group_6__0 )? )
            {
            // InternalDSL.g:5418:1: ( ( rule__Condition__Group_6__0 )? )
            // InternalDSL.g:5419:2: ( rule__Condition__Group_6__0 )?
            {
             before(grammarAccess.getConditionAccess().getGroup_6()); 
            // InternalDSL.g:5420:2: ( rule__Condition__Group_6__0 )?
            int alt50=2;
            int LA50_0 = input.LA(1);

            if ( (LA50_0==92) ) {
                alt50=1;
            }
            switch (alt50) {
                case 1 :
                    // InternalDSL.g:5420:3: rule__Condition__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Condition__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getConditionAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__6__Impl"


    // $ANTLR start "rule__Condition__Group__7"
    // InternalDSL.g:5428:1: rule__Condition__Group__7 : rule__Condition__Group__7__Impl rule__Condition__Group__8 ;
    public final void rule__Condition__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5432:1: ( rule__Condition__Group__7__Impl rule__Condition__Group__8 )
            // InternalDSL.g:5433:2: rule__Condition__Group__7__Impl rule__Condition__Group__8
            {
            pushFollow(FOLLOW_36);
            rule__Condition__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Condition__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__7"


    // $ANTLR start "rule__Condition__Group__7__Impl"
    // InternalDSL.g:5440:1: rule__Condition__Group__7__Impl : ( ( rule__Condition__Group_7__0 )? ) ;
    public final void rule__Condition__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5444:1: ( ( ( rule__Condition__Group_7__0 )? ) )
            // InternalDSL.g:5445:1: ( ( rule__Condition__Group_7__0 )? )
            {
            // InternalDSL.g:5445:1: ( ( rule__Condition__Group_7__0 )? )
            // InternalDSL.g:5446:2: ( rule__Condition__Group_7__0 )?
            {
             before(grammarAccess.getConditionAccess().getGroup_7()); 
            // InternalDSL.g:5447:2: ( rule__Condition__Group_7__0 )?
            int alt51=2;
            int LA51_0 = input.LA(1);

            if ( (LA51_0==65) ) {
                alt51=1;
            }
            switch (alt51) {
                case 1 :
                    // InternalDSL.g:5447:3: rule__Condition__Group_7__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Condition__Group_7__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getConditionAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__7__Impl"


    // $ANTLR start "rule__Condition__Group__8"
    // InternalDSL.g:5455:1: rule__Condition__Group__8 : rule__Condition__Group__8__Impl ;
    public final void rule__Condition__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5459:1: ( rule__Condition__Group__8__Impl )
            // InternalDSL.g:5460:2: rule__Condition__Group__8__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Condition__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__8"


    // $ANTLR start "rule__Condition__Group__8__Impl"
    // InternalDSL.g:5466:1: rule__Condition__Group__8__Impl : ( '}' ) ;
    public final void rule__Condition__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5470:1: ( ( '}' ) )
            // InternalDSL.g:5471:1: ( '}' )
            {
            // InternalDSL.g:5471:1: ( '}' )
            // InternalDSL.g:5472:2: '}'
            {
             before(grammarAccess.getConditionAccess().getRightCurlyBracketKeyword_8()); 
            match(input,60,FOLLOW_2); 
             after(grammarAccess.getConditionAccess().getRightCurlyBracketKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group__8__Impl"


    // $ANTLR start "rule__Condition__Group_3__0"
    // InternalDSL.g:5482:1: rule__Condition__Group_3__0 : rule__Condition__Group_3__0__Impl rule__Condition__Group_3__1 ;
    public final void rule__Condition__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5486:1: ( rule__Condition__Group_3__0__Impl rule__Condition__Group_3__1 )
            // InternalDSL.g:5487:2: rule__Condition__Group_3__0__Impl rule__Condition__Group_3__1
            {
            pushFollow(FOLLOW_11);
            rule__Condition__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Condition__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_3__0"


    // $ANTLR start "rule__Condition__Group_3__0__Impl"
    // InternalDSL.g:5494:1: rule__Condition__Group_3__0__Impl : ( 'Name:' ) ;
    public final void rule__Condition__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5498:1: ( ( 'Name:' ) )
            // InternalDSL.g:5499:1: ( 'Name:' )
            {
            // InternalDSL.g:5499:1: ( 'Name:' )
            // InternalDSL.g:5500:2: 'Name:'
            {
             before(grammarAccess.getConditionAccess().getNameKeyword_3_0()); 
            match(input,62,FOLLOW_2); 
             after(grammarAccess.getConditionAccess().getNameKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_3__0__Impl"


    // $ANTLR start "rule__Condition__Group_3__1"
    // InternalDSL.g:5509:1: rule__Condition__Group_3__1 : rule__Condition__Group_3__1__Impl ;
    public final void rule__Condition__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5513:1: ( rule__Condition__Group_3__1__Impl )
            // InternalDSL.g:5514:2: rule__Condition__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Condition__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_3__1"


    // $ANTLR start "rule__Condition__Group_3__1__Impl"
    // InternalDSL.g:5520:1: rule__Condition__Group_3__1__Impl : ( ( rule__Condition__NameAssignment_3_1 ) ) ;
    public final void rule__Condition__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5524:1: ( ( ( rule__Condition__NameAssignment_3_1 ) ) )
            // InternalDSL.g:5525:1: ( ( rule__Condition__NameAssignment_3_1 ) )
            {
            // InternalDSL.g:5525:1: ( ( rule__Condition__NameAssignment_3_1 ) )
            // InternalDSL.g:5526:2: ( rule__Condition__NameAssignment_3_1 )
            {
             before(grammarAccess.getConditionAccess().getNameAssignment_3_1()); 
            // InternalDSL.g:5527:2: ( rule__Condition__NameAssignment_3_1 )
            // InternalDSL.g:5527:3: rule__Condition__NameAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Condition__NameAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getConditionAccess().getNameAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_3__1__Impl"


    // $ANTLR start "rule__Condition__Group_4__0"
    // InternalDSL.g:5536:1: rule__Condition__Group_4__0 : rule__Condition__Group_4__0__Impl rule__Condition__Group_4__1 ;
    public final void rule__Condition__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5540:1: ( rule__Condition__Group_4__0__Impl rule__Condition__Group_4__1 )
            // InternalDSL.g:5541:2: rule__Condition__Group_4__0__Impl rule__Condition__Group_4__1
            {
            pushFollow(FOLLOW_34);
            rule__Condition__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Condition__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_4__0"


    // $ANTLR start "rule__Condition__Group_4__0__Impl"
    // InternalDSL.g:5548:1: rule__Condition__Group_4__0__Impl : ( 'AmountRequired:' ) ;
    public final void rule__Condition__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5552:1: ( ( 'AmountRequired:' ) )
            // InternalDSL.g:5553:1: ( 'AmountRequired:' )
            {
            // InternalDSL.g:5553:1: ( 'AmountRequired:' )
            // InternalDSL.g:5554:2: 'AmountRequired:'
            {
             before(grammarAccess.getConditionAccess().getAmountRequiredKeyword_4_0()); 
            match(input,90,FOLLOW_2); 
             after(grammarAccess.getConditionAccess().getAmountRequiredKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_4__0__Impl"


    // $ANTLR start "rule__Condition__Group_4__1"
    // InternalDSL.g:5563:1: rule__Condition__Group_4__1 : rule__Condition__Group_4__1__Impl ;
    public final void rule__Condition__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5567:1: ( rule__Condition__Group_4__1__Impl )
            // InternalDSL.g:5568:2: rule__Condition__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Condition__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_4__1"


    // $ANTLR start "rule__Condition__Group_4__1__Impl"
    // InternalDSL.g:5574:1: rule__Condition__Group_4__1__Impl : ( ( rule__Condition__AmountRequiredAssignment_4_1 ) ) ;
    public final void rule__Condition__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5578:1: ( ( ( rule__Condition__AmountRequiredAssignment_4_1 ) ) )
            // InternalDSL.g:5579:1: ( ( rule__Condition__AmountRequiredAssignment_4_1 ) )
            {
            // InternalDSL.g:5579:1: ( ( rule__Condition__AmountRequiredAssignment_4_1 ) )
            // InternalDSL.g:5580:2: ( rule__Condition__AmountRequiredAssignment_4_1 )
            {
             before(grammarAccess.getConditionAccess().getAmountRequiredAssignment_4_1()); 
            // InternalDSL.g:5581:2: ( rule__Condition__AmountRequiredAssignment_4_1 )
            // InternalDSL.g:5581:3: rule__Condition__AmountRequiredAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Condition__AmountRequiredAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getConditionAccess().getAmountRequiredAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_4__1__Impl"


    // $ANTLR start "rule__Condition__Group_5__0"
    // InternalDSL.g:5590:1: rule__Condition__Group_5__0 : rule__Condition__Group_5__0__Impl rule__Condition__Group_5__1 ;
    public final void rule__Condition__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5594:1: ( rule__Condition__Group_5__0__Impl rule__Condition__Group_5__1 )
            // InternalDSL.g:5595:2: rule__Condition__Group_5__0__Impl rule__Condition__Group_5__1
            {
            pushFollow(FOLLOW_37);
            rule__Condition__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Condition__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_5__0"


    // $ANTLR start "rule__Condition__Group_5__0__Impl"
    // InternalDSL.g:5602:1: rule__Condition__Group_5__0__Impl : ( 'ConditionType:' ) ;
    public final void rule__Condition__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5606:1: ( ( 'ConditionType:' ) )
            // InternalDSL.g:5607:1: ( 'ConditionType:' )
            {
            // InternalDSL.g:5607:1: ( 'ConditionType:' )
            // InternalDSL.g:5608:2: 'ConditionType:'
            {
             before(grammarAccess.getConditionAccess().getConditionTypeKeyword_5_0()); 
            match(input,91,FOLLOW_2); 
             after(grammarAccess.getConditionAccess().getConditionTypeKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_5__0__Impl"


    // $ANTLR start "rule__Condition__Group_5__1"
    // InternalDSL.g:5617:1: rule__Condition__Group_5__1 : rule__Condition__Group_5__1__Impl ;
    public final void rule__Condition__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5621:1: ( rule__Condition__Group_5__1__Impl )
            // InternalDSL.g:5622:2: rule__Condition__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Condition__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_5__1"


    // $ANTLR start "rule__Condition__Group_5__1__Impl"
    // InternalDSL.g:5628:1: rule__Condition__Group_5__1__Impl : ( ( rule__Condition__ConditionTypeAssignment_5_1 ) ) ;
    public final void rule__Condition__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5632:1: ( ( ( rule__Condition__ConditionTypeAssignment_5_1 ) ) )
            // InternalDSL.g:5633:1: ( ( rule__Condition__ConditionTypeAssignment_5_1 ) )
            {
            // InternalDSL.g:5633:1: ( ( rule__Condition__ConditionTypeAssignment_5_1 ) )
            // InternalDSL.g:5634:2: ( rule__Condition__ConditionTypeAssignment_5_1 )
            {
             before(grammarAccess.getConditionAccess().getConditionTypeAssignment_5_1()); 
            // InternalDSL.g:5635:2: ( rule__Condition__ConditionTypeAssignment_5_1 )
            // InternalDSL.g:5635:3: rule__Condition__ConditionTypeAssignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__Condition__ConditionTypeAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getConditionAccess().getConditionTypeAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_5__1__Impl"


    // $ANTLR start "rule__Condition__Group_6__0"
    // InternalDSL.g:5644:1: rule__Condition__Group_6__0 : rule__Condition__Group_6__0__Impl rule__Condition__Group_6__1 ;
    public final void rule__Condition__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5648:1: ( rule__Condition__Group_6__0__Impl rule__Condition__Group_6__1 )
            // InternalDSL.g:5649:2: rule__Condition__Group_6__0__Impl rule__Condition__Group_6__1
            {
            pushFollow(FOLLOW_29);
            rule__Condition__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Condition__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_6__0"


    // $ANTLR start "rule__Condition__Group_6__0__Impl"
    // InternalDSL.g:5656:1: rule__Condition__Group_6__0__Impl : ( 'PreRequirements' ) ;
    public final void rule__Condition__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5660:1: ( ( 'PreRequirements' ) )
            // InternalDSL.g:5661:1: ( 'PreRequirements' )
            {
            // InternalDSL.g:5661:1: ( 'PreRequirements' )
            // InternalDSL.g:5662:2: 'PreRequirements'
            {
             before(grammarAccess.getConditionAccess().getPreRequirementsKeyword_6_0()); 
            match(input,92,FOLLOW_2); 
             after(grammarAccess.getConditionAccess().getPreRequirementsKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_6__0__Impl"


    // $ANTLR start "rule__Condition__Group_6__1"
    // InternalDSL.g:5671:1: rule__Condition__Group_6__1 : rule__Condition__Group_6__1__Impl rule__Condition__Group_6__2 ;
    public final void rule__Condition__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5675:1: ( rule__Condition__Group_6__1__Impl rule__Condition__Group_6__2 )
            // InternalDSL.g:5676:2: rule__Condition__Group_6__1__Impl rule__Condition__Group_6__2
            {
            pushFollow(FOLLOW_11);
            rule__Condition__Group_6__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Condition__Group_6__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_6__1"


    // $ANTLR start "rule__Condition__Group_6__1__Impl"
    // InternalDSL.g:5683:1: rule__Condition__Group_6__1__Impl : ( '(' ) ;
    public final void rule__Condition__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5687:1: ( ( '(' ) )
            // InternalDSL.g:5688:1: ( '(' )
            {
            // InternalDSL.g:5688:1: ( '(' )
            // InternalDSL.g:5689:2: '('
            {
             before(grammarAccess.getConditionAccess().getLeftParenthesisKeyword_6_1()); 
            match(input,81,FOLLOW_2); 
             after(grammarAccess.getConditionAccess().getLeftParenthesisKeyword_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_6__1__Impl"


    // $ANTLR start "rule__Condition__Group_6__2"
    // InternalDSL.g:5698:1: rule__Condition__Group_6__2 : rule__Condition__Group_6__2__Impl rule__Condition__Group_6__3 ;
    public final void rule__Condition__Group_6__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5702:1: ( rule__Condition__Group_6__2__Impl rule__Condition__Group_6__3 )
            // InternalDSL.g:5703:2: rule__Condition__Group_6__2__Impl rule__Condition__Group_6__3
            {
            pushFollow(FOLLOW_30);
            rule__Condition__Group_6__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Condition__Group_6__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_6__2"


    // $ANTLR start "rule__Condition__Group_6__2__Impl"
    // InternalDSL.g:5710:1: rule__Condition__Group_6__2__Impl : ( ( rule__Condition__PreRequirementsAssignment_6_2 ) ) ;
    public final void rule__Condition__Group_6__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5714:1: ( ( ( rule__Condition__PreRequirementsAssignment_6_2 ) ) )
            // InternalDSL.g:5715:1: ( ( rule__Condition__PreRequirementsAssignment_6_2 ) )
            {
            // InternalDSL.g:5715:1: ( ( rule__Condition__PreRequirementsAssignment_6_2 ) )
            // InternalDSL.g:5716:2: ( rule__Condition__PreRequirementsAssignment_6_2 )
            {
             before(grammarAccess.getConditionAccess().getPreRequirementsAssignment_6_2()); 
            // InternalDSL.g:5717:2: ( rule__Condition__PreRequirementsAssignment_6_2 )
            // InternalDSL.g:5717:3: rule__Condition__PreRequirementsAssignment_6_2
            {
            pushFollow(FOLLOW_2);
            rule__Condition__PreRequirementsAssignment_6_2();

            state._fsp--;


            }

             after(grammarAccess.getConditionAccess().getPreRequirementsAssignment_6_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_6__2__Impl"


    // $ANTLR start "rule__Condition__Group_6__3"
    // InternalDSL.g:5725:1: rule__Condition__Group_6__3 : rule__Condition__Group_6__3__Impl rule__Condition__Group_6__4 ;
    public final void rule__Condition__Group_6__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5729:1: ( rule__Condition__Group_6__3__Impl rule__Condition__Group_6__4 )
            // InternalDSL.g:5730:2: rule__Condition__Group_6__3__Impl rule__Condition__Group_6__4
            {
            pushFollow(FOLLOW_30);
            rule__Condition__Group_6__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Condition__Group_6__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_6__3"


    // $ANTLR start "rule__Condition__Group_6__3__Impl"
    // InternalDSL.g:5737:1: rule__Condition__Group_6__3__Impl : ( ( rule__Condition__Group_6_3__0 )* ) ;
    public final void rule__Condition__Group_6__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5741:1: ( ( ( rule__Condition__Group_6_3__0 )* ) )
            // InternalDSL.g:5742:1: ( ( rule__Condition__Group_6_3__0 )* )
            {
            // InternalDSL.g:5742:1: ( ( rule__Condition__Group_6_3__0 )* )
            // InternalDSL.g:5743:2: ( rule__Condition__Group_6_3__0 )*
            {
             before(grammarAccess.getConditionAccess().getGroup_6_3()); 
            // InternalDSL.g:5744:2: ( rule__Condition__Group_6_3__0 )*
            loop52:
            do {
                int alt52=2;
                int LA52_0 = input.LA(1);

                if ( (LA52_0==64) ) {
                    alt52=1;
                }


                switch (alt52) {
            	case 1 :
            	    // InternalDSL.g:5744:3: rule__Condition__Group_6_3__0
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__Condition__Group_6_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop52;
                }
            } while (true);

             after(grammarAccess.getConditionAccess().getGroup_6_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_6__3__Impl"


    // $ANTLR start "rule__Condition__Group_6__4"
    // InternalDSL.g:5752:1: rule__Condition__Group_6__4 : rule__Condition__Group_6__4__Impl ;
    public final void rule__Condition__Group_6__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5756:1: ( rule__Condition__Group_6__4__Impl )
            // InternalDSL.g:5757:2: rule__Condition__Group_6__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Condition__Group_6__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_6__4"


    // $ANTLR start "rule__Condition__Group_6__4__Impl"
    // InternalDSL.g:5763:1: rule__Condition__Group_6__4__Impl : ( ')' ) ;
    public final void rule__Condition__Group_6__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5767:1: ( ( ')' ) )
            // InternalDSL.g:5768:1: ( ')' )
            {
            // InternalDSL.g:5768:1: ( ')' )
            // InternalDSL.g:5769:2: ')'
            {
             before(grammarAccess.getConditionAccess().getRightParenthesisKeyword_6_4()); 
            match(input,82,FOLLOW_2); 
             after(grammarAccess.getConditionAccess().getRightParenthesisKeyword_6_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_6__4__Impl"


    // $ANTLR start "rule__Condition__Group_6_3__0"
    // InternalDSL.g:5779:1: rule__Condition__Group_6_3__0 : rule__Condition__Group_6_3__0__Impl rule__Condition__Group_6_3__1 ;
    public final void rule__Condition__Group_6_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5783:1: ( rule__Condition__Group_6_3__0__Impl rule__Condition__Group_6_3__1 )
            // InternalDSL.g:5784:2: rule__Condition__Group_6_3__0__Impl rule__Condition__Group_6_3__1
            {
            pushFollow(FOLLOW_11);
            rule__Condition__Group_6_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Condition__Group_6_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_6_3__0"


    // $ANTLR start "rule__Condition__Group_6_3__0__Impl"
    // InternalDSL.g:5791:1: rule__Condition__Group_6_3__0__Impl : ( ',' ) ;
    public final void rule__Condition__Group_6_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5795:1: ( ( ',' ) )
            // InternalDSL.g:5796:1: ( ',' )
            {
            // InternalDSL.g:5796:1: ( ',' )
            // InternalDSL.g:5797:2: ','
            {
             before(grammarAccess.getConditionAccess().getCommaKeyword_6_3_0()); 
            match(input,64,FOLLOW_2); 
             after(grammarAccess.getConditionAccess().getCommaKeyword_6_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_6_3__0__Impl"


    // $ANTLR start "rule__Condition__Group_6_3__1"
    // InternalDSL.g:5806:1: rule__Condition__Group_6_3__1 : rule__Condition__Group_6_3__1__Impl ;
    public final void rule__Condition__Group_6_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5810:1: ( rule__Condition__Group_6_3__1__Impl )
            // InternalDSL.g:5811:2: rule__Condition__Group_6_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Condition__Group_6_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_6_3__1"


    // $ANTLR start "rule__Condition__Group_6_3__1__Impl"
    // InternalDSL.g:5817:1: rule__Condition__Group_6_3__1__Impl : ( ( rule__Condition__PreRequirementsAssignment_6_3_1 ) ) ;
    public final void rule__Condition__Group_6_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5821:1: ( ( ( rule__Condition__PreRequirementsAssignment_6_3_1 ) ) )
            // InternalDSL.g:5822:1: ( ( rule__Condition__PreRequirementsAssignment_6_3_1 ) )
            {
            // InternalDSL.g:5822:1: ( ( rule__Condition__PreRequirementsAssignment_6_3_1 ) )
            // InternalDSL.g:5823:2: ( rule__Condition__PreRequirementsAssignment_6_3_1 )
            {
             before(grammarAccess.getConditionAccess().getPreRequirementsAssignment_6_3_1()); 
            // InternalDSL.g:5824:2: ( rule__Condition__PreRequirementsAssignment_6_3_1 )
            // InternalDSL.g:5824:3: rule__Condition__PreRequirementsAssignment_6_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Condition__PreRequirementsAssignment_6_3_1();

            state._fsp--;


            }

             after(grammarAccess.getConditionAccess().getPreRequirementsAssignment_6_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_6_3__1__Impl"


    // $ANTLR start "rule__Condition__Group_7__0"
    // InternalDSL.g:5833:1: rule__Condition__Group_7__0 : rule__Condition__Group_7__0__Impl rule__Condition__Group_7__1 ;
    public final void rule__Condition__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5837:1: ( rule__Condition__Group_7__0__Impl rule__Condition__Group_7__1 )
            // InternalDSL.g:5838:2: rule__Condition__Group_7__0__Impl rule__Condition__Group_7__1
            {
            pushFollow(FOLLOW_12);
            rule__Condition__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Condition__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_7__0"


    // $ANTLR start "rule__Condition__Group_7__0__Impl"
    // InternalDSL.g:5845:1: rule__Condition__Group_7__0__Impl : ( 'Information:' ) ;
    public final void rule__Condition__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5849:1: ( ( 'Information:' ) )
            // InternalDSL.g:5850:1: ( 'Information:' )
            {
            // InternalDSL.g:5850:1: ( 'Information:' )
            // InternalDSL.g:5851:2: 'Information:'
            {
             before(grammarAccess.getConditionAccess().getInformationKeyword_7_0()); 
            match(input,65,FOLLOW_2); 
             after(grammarAccess.getConditionAccess().getInformationKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_7__0__Impl"


    // $ANTLR start "rule__Condition__Group_7__1"
    // InternalDSL.g:5860:1: rule__Condition__Group_7__1 : rule__Condition__Group_7__1__Impl ;
    public final void rule__Condition__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5864:1: ( rule__Condition__Group_7__1__Impl )
            // InternalDSL.g:5865:2: rule__Condition__Group_7__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Condition__Group_7__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_7__1"


    // $ANTLR start "rule__Condition__Group_7__1__Impl"
    // InternalDSL.g:5871:1: rule__Condition__Group_7__1__Impl : ( ( rule__Condition__InformationAssignment_7_1 ) ) ;
    public final void rule__Condition__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5875:1: ( ( ( rule__Condition__InformationAssignment_7_1 ) ) )
            // InternalDSL.g:5876:1: ( ( rule__Condition__InformationAssignment_7_1 ) )
            {
            // InternalDSL.g:5876:1: ( ( rule__Condition__InformationAssignment_7_1 ) )
            // InternalDSL.g:5877:2: ( rule__Condition__InformationAssignment_7_1 )
            {
             before(grammarAccess.getConditionAccess().getInformationAssignment_7_1()); 
            // InternalDSL.g:5878:2: ( rule__Condition__InformationAssignment_7_1 )
            // InternalDSL.g:5878:3: rule__Condition__InformationAssignment_7_1
            {
            pushFollow(FOLLOW_2);
            rule__Condition__InformationAssignment_7_1();

            state._fsp--;


            }

             after(grammarAccess.getConditionAccess().getInformationAssignment_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__Group_7__1__Impl"


    // $ANTLR start "rule__Restriction__Group__0"
    // InternalDSL.g:5887:1: rule__Restriction__Group__0 : rule__Restriction__Group__0__Impl rule__Restriction__Group__1 ;
    public final void rule__Restriction__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5891:1: ( rule__Restriction__Group__0__Impl rule__Restriction__Group__1 )
            // InternalDSL.g:5892:2: rule__Restriction__Group__0__Impl rule__Restriction__Group__1
            {
            pushFollow(FOLLOW_35);
            rule__Restriction__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Restriction__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group__0"


    // $ANTLR start "rule__Restriction__Group__0__Impl"
    // InternalDSL.g:5899:1: rule__Restriction__Group__0__Impl : ( () ) ;
    public final void rule__Restriction__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5903:1: ( ( () ) )
            // InternalDSL.g:5904:1: ( () )
            {
            // InternalDSL.g:5904:1: ( () )
            // InternalDSL.g:5905:2: ()
            {
             before(grammarAccess.getRestrictionAccess().getRestrictionAction_0()); 
            // InternalDSL.g:5906:2: ()
            // InternalDSL.g:5906:3: 
            {
            }

             after(grammarAccess.getRestrictionAccess().getRestrictionAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group__0__Impl"


    // $ANTLR start "rule__Restriction__Group__1"
    // InternalDSL.g:5914:1: rule__Restriction__Group__1 : rule__Restriction__Group__1__Impl rule__Restriction__Group__2 ;
    public final void rule__Restriction__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5918:1: ( rule__Restriction__Group__1__Impl rule__Restriction__Group__2 )
            // InternalDSL.g:5919:2: rule__Restriction__Group__1__Impl rule__Restriction__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__Restriction__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Restriction__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group__1"


    // $ANTLR start "rule__Restriction__Group__1__Impl"
    // InternalDSL.g:5926:1: rule__Restriction__Group__1__Impl : ( 'Restriction' ) ;
    public final void rule__Restriction__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5930:1: ( ( 'Restriction' ) )
            // InternalDSL.g:5931:1: ( 'Restriction' )
            {
            // InternalDSL.g:5931:1: ( 'Restriction' )
            // InternalDSL.g:5932:2: 'Restriction'
            {
             before(grammarAccess.getRestrictionAccess().getRestrictionKeyword_1()); 
            match(input,93,FOLLOW_2); 
             after(grammarAccess.getRestrictionAccess().getRestrictionKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group__1__Impl"


    // $ANTLR start "rule__Restriction__Group__2"
    // InternalDSL.g:5941:1: rule__Restriction__Group__2 : rule__Restriction__Group__2__Impl rule__Restriction__Group__3 ;
    public final void rule__Restriction__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5945:1: ( rule__Restriction__Group__2__Impl rule__Restriction__Group__3 )
            // InternalDSL.g:5946:2: rule__Restriction__Group__2__Impl rule__Restriction__Group__3
            {
            pushFollow(FOLLOW_38);
            rule__Restriction__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Restriction__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group__2"


    // $ANTLR start "rule__Restriction__Group__2__Impl"
    // InternalDSL.g:5953:1: rule__Restriction__Group__2__Impl : ( '{' ) ;
    public final void rule__Restriction__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5957:1: ( ( '{' ) )
            // InternalDSL.g:5958:1: ( '{' )
            {
            // InternalDSL.g:5958:1: ( '{' )
            // InternalDSL.g:5959:2: '{'
            {
             before(grammarAccess.getRestrictionAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,58,FOLLOW_2); 
             after(grammarAccess.getRestrictionAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group__2__Impl"


    // $ANTLR start "rule__Restriction__Group__3"
    // InternalDSL.g:5968:1: rule__Restriction__Group__3 : rule__Restriction__Group__3__Impl rule__Restriction__Group__4 ;
    public final void rule__Restriction__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5972:1: ( rule__Restriction__Group__3__Impl rule__Restriction__Group__4 )
            // InternalDSL.g:5973:2: rule__Restriction__Group__3__Impl rule__Restriction__Group__4
            {
            pushFollow(FOLLOW_38);
            rule__Restriction__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Restriction__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group__3"


    // $ANTLR start "rule__Restriction__Group__3__Impl"
    // InternalDSL.g:5980:1: rule__Restriction__Group__3__Impl : ( ( rule__Restriction__Group_3__0 )? ) ;
    public final void rule__Restriction__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5984:1: ( ( ( rule__Restriction__Group_3__0 )? ) )
            // InternalDSL.g:5985:1: ( ( rule__Restriction__Group_3__0 )? )
            {
            // InternalDSL.g:5985:1: ( ( rule__Restriction__Group_3__0 )? )
            // InternalDSL.g:5986:2: ( rule__Restriction__Group_3__0 )?
            {
             before(grammarAccess.getRestrictionAccess().getGroup_3()); 
            // InternalDSL.g:5987:2: ( rule__Restriction__Group_3__0 )?
            int alt53=2;
            int LA53_0 = input.LA(1);

            if ( (LA53_0==62) ) {
                alt53=1;
            }
            switch (alt53) {
                case 1 :
                    // InternalDSL.g:5987:3: rule__Restriction__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Restriction__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRestrictionAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group__3__Impl"


    // $ANTLR start "rule__Restriction__Group__4"
    // InternalDSL.g:5995:1: rule__Restriction__Group__4 : rule__Restriction__Group__4__Impl rule__Restriction__Group__5 ;
    public final void rule__Restriction__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:5999:1: ( rule__Restriction__Group__4__Impl rule__Restriction__Group__5 )
            // InternalDSL.g:6000:2: rule__Restriction__Group__4__Impl rule__Restriction__Group__5
            {
            pushFollow(FOLLOW_38);
            rule__Restriction__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Restriction__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group__4"


    // $ANTLR start "rule__Restriction__Group__4__Impl"
    // InternalDSL.g:6007:1: rule__Restriction__Group__4__Impl : ( ( rule__Restriction__Group_4__0 )? ) ;
    public final void rule__Restriction__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6011:1: ( ( ( rule__Restriction__Group_4__0 )? ) )
            // InternalDSL.g:6012:1: ( ( rule__Restriction__Group_4__0 )? )
            {
            // InternalDSL.g:6012:1: ( ( rule__Restriction__Group_4__0 )? )
            // InternalDSL.g:6013:2: ( rule__Restriction__Group_4__0 )?
            {
             before(grammarAccess.getRestrictionAccess().getGroup_4()); 
            // InternalDSL.g:6014:2: ( rule__Restriction__Group_4__0 )?
            int alt54=2;
            int LA54_0 = input.LA(1);

            if ( (LA54_0==94) ) {
                alt54=1;
            }
            switch (alt54) {
                case 1 :
                    // InternalDSL.g:6014:3: rule__Restriction__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Restriction__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRestrictionAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group__4__Impl"


    // $ANTLR start "rule__Restriction__Group__5"
    // InternalDSL.g:6022:1: rule__Restriction__Group__5 : rule__Restriction__Group__5__Impl rule__Restriction__Group__6 ;
    public final void rule__Restriction__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6026:1: ( rule__Restriction__Group__5__Impl rule__Restriction__Group__6 )
            // InternalDSL.g:6027:2: rule__Restriction__Group__5__Impl rule__Restriction__Group__6
            {
            pushFollow(FOLLOW_38);
            rule__Restriction__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Restriction__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group__5"


    // $ANTLR start "rule__Restriction__Group__5__Impl"
    // InternalDSL.g:6034:1: rule__Restriction__Group__5__Impl : ( ( rule__Restriction__Group_5__0 )? ) ;
    public final void rule__Restriction__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6038:1: ( ( ( rule__Restriction__Group_5__0 )? ) )
            // InternalDSL.g:6039:1: ( ( rule__Restriction__Group_5__0 )? )
            {
            // InternalDSL.g:6039:1: ( ( rule__Restriction__Group_5__0 )? )
            // InternalDSL.g:6040:2: ( rule__Restriction__Group_5__0 )?
            {
             before(grammarAccess.getRestrictionAccess().getGroup_5()); 
            // InternalDSL.g:6041:2: ( rule__Restriction__Group_5__0 )?
            int alt55=2;
            int LA55_0 = input.LA(1);

            if ( (LA55_0==95) ) {
                alt55=1;
            }
            switch (alt55) {
                case 1 :
                    // InternalDSL.g:6041:3: rule__Restriction__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Restriction__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRestrictionAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group__5__Impl"


    // $ANTLR start "rule__Restriction__Group__6"
    // InternalDSL.g:6049:1: rule__Restriction__Group__6 : rule__Restriction__Group__6__Impl rule__Restriction__Group__7 ;
    public final void rule__Restriction__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6053:1: ( rule__Restriction__Group__6__Impl rule__Restriction__Group__7 )
            // InternalDSL.g:6054:2: rule__Restriction__Group__6__Impl rule__Restriction__Group__7
            {
            pushFollow(FOLLOW_38);
            rule__Restriction__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Restriction__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group__6"


    // $ANTLR start "rule__Restriction__Group__6__Impl"
    // InternalDSL.g:6061:1: rule__Restriction__Group__6__Impl : ( ( rule__Restriction__Group_6__0 )? ) ;
    public final void rule__Restriction__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6065:1: ( ( ( rule__Restriction__Group_6__0 )? ) )
            // InternalDSL.g:6066:1: ( ( rule__Restriction__Group_6__0 )? )
            {
            // InternalDSL.g:6066:1: ( ( rule__Restriction__Group_6__0 )? )
            // InternalDSL.g:6067:2: ( rule__Restriction__Group_6__0 )?
            {
             before(grammarAccess.getRestrictionAccess().getGroup_6()); 
            // InternalDSL.g:6068:2: ( rule__Restriction__Group_6__0 )?
            int alt56=2;
            int LA56_0 = input.LA(1);

            if ( (LA56_0==96) ) {
                alt56=1;
            }
            switch (alt56) {
                case 1 :
                    // InternalDSL.g:6068:3: rule__Restriction__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Restriction__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRestrictionAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group__6__Impl"


    // $ANTLR start "rule__Restriction__Group__7"
    // InternalDSL.g:6076:1: rule__Restriction__Group__7 : rule__Restriction__Group__7__Impl rule__Restriction__Group__8 ;
    public final void rule__Restriction__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6080:1: ( rule__Restriction__Group__7__Impl rule__Restriction__Group__8 )
            // InternalDSL.g:6081:2: rule__Restriction__Group__7__Impl rule__Restriction__Group__8
            {
            pushFollow(FOLLOW_38);
            rule__Restriction__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Restriction__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group__7"


    // $ANTLR start "rule__Restriction__Group__7__Impl"
    // InternalDSL.g:6088:1: rule__Restriction__Group__7__Impl : ( ( rule__Restriction__Group_7__0 )? ) ;
    public final void rule__Restriction__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6092:1: ( ( ( rule__Restriction__Group_7__0 )? ) )
            // InternalDSL.g:6093:1: ( ( rule__Restriction__Group_7__0 )? )
            {
            // InternalDSL.g:6093:1: ( ( rule__Restriction__Group_7__0 )? )
            // InternalDSL.g:6094:2: ( rule__Restriction__Group_7__0 )?
            {
             before(grammarAccess.getRestrictionAccess().getGroup_7()); 
            // InternalDSL.g:6095:2: ( rule__Restriction__Group_7__0 )?
            int alt57=2;
            int LA57_0 = input.LA(1);

            if ( (LA57_0==65) ) {
                alt57=1;
            }
            switch (alt57) {
                case 1 :
                    // InternalDSL.g:6095:3: rule__Restriction__Group_7__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Restriction__Group_7__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRestrictionAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group__7__Impl"


    // $ANTLR start "rule__Restriction__Group__8"
    // InternalDSL.g:6103:1: rule__Restriction__Group__8 : rule__Restriction__Group__8__Impl ;
    public final void rule__Restriction__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6107:1: ( rule__Restriction__Group__8__Impl )
            // InternalDSL.g:6108:2: rule__Restriction__Group__8__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Restriction__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group__8"


    // $ANTLR start "rule__Restriction__Group__8__Impl"
    // InternalDSL.g:6114:1: rule__Restriction__Group__8__Impl : ( '}' ) ;
    public final void rule__Restriction__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6118:1: ( ( '}' ) )
            // InternalDSL.g:6119:1: ( '}' )
            {
            // InternalDSL.g:6119:1: ( '}' )
            // InternalDSL.g:6120:2: '}'
            {
             before(grammarAccess.getRestrictionAccess().getRightCurlyBracketKeyword_8()); 
            match(input,60,FOLLOW_2); 
             after(grammarAccess.getRestrictionAccess().getRightCurlyBracketKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group__8__Impl"


    // $ANTLR start "rule__Restriction__Group_3__0"
    // InternalDSL.g:6130:1: rule__Restriction__Group_3__0 : rule__Restriction__Group_3__0__Impl rule__Restriction__Group_3__1 ;
    public final void rule__Restriction__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6134:1: ( rule__Restriction__Group_3__0__Impl rule__Restriction__Group_3__1 )
            // InternalDSL.g:6135:2: rule__Restriction__Group_3__0__Impl rule__Restriction__Group_3__1
            {
            pushFollow(FOLLOW_11);
            rule__Restriction__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Restriction__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group_3__0"


    // $ANTLR start "rule__Restriction__Group_3__0__Impl"
    // InternalDSL.g:6142:1: rule__Restriction__Group_3__0__Impl : ( 'Name:' ) ;
    public final void rule__Restriction__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6146:1: ( ( 'Name:' ) )
            // InternalDSL.g:6147:1: ( 'Name:' )
            {
            // InternalDSL.g:6147:1: ( 'Name:' )
            // InternalDSL.g:6148:2: 'Name:'
            {
             before(grammarAccess.getRestrictionAccess().getNameKeyword_3_0()); 
            match(input,62,FOLLOW_2); 
             after(grammarAccess.getRestrictionAccess().getNameKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group_3__0__Impl"


    // $ANTLR start "rule__Restriction__Group_3__1"
    // InternalDSL.g:6157:1: rule__Restriction__Group_3__1 : rule__Restriction__Group_3__1__Impl ;
    public final void rule__Restriction__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6161:1: ( rule__Restriction__Group_3__1__Impl )
            // InternalDSL.g:6162:2: rule__Restriction__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Restriction__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group_3__1"


    // $ANTLR start "rule__Restriction__Group_3__1__Impl"
    // InternalDSL.g:6168:1: rule__Restriction__Group_3__1__Impl : ( ( rule__Restriction__NameAssignment_3_1 ) ) ;
    public final void rule__Restriction__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6172:1: ( ( ( rule__Restriction__NameAssignment_3_1 ) ) )
            // InternalDSL.g:6173:1: ( ( rule__Restriction__NameAssignment_3_1 ) )
            {
            // InternalDSL.g:6173:1: ( ( rule__Restriction__NameAssignment_3_1 ) )
            // InternalDSL.g:6174:2: ( rule__Restriction__NameAssignment_3_1 )
            {
             before(grammarAccess.getRestrictionAccess().getNameAssignment_3_1()); 
            // InternalDSL.g:6175:2: ( rule__Restriction__NameAssignment_3_1 )
            // InternalDSL.g:6175:3: rule__Restriction__NameAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Restriction__NameAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getRestrictionAccess().getNameAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group_3__1__Impl"


    // $ANTLR start "rule__Restriction__Group_4__0"
    // InternalDSL.g:6184:1: rule__Restriction__Group_4__0 : rule__Restriction__Group_4__0__Impl rule__Restriction__Group_4__1 ;
    public final void rule__Restriction__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6188:1: ( rule__Restriction__Group_4__0__Impl rule__Restriction__Group_4__1 )
            // InternalDSL.g:6189:2: rule__Restriction__Group_4__0__Impl rule__Restriction__Group_4__1
            {
            pushFollow(FOLLOW_12);
            rule__Restriction__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Restriction__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group_4__0"


    // $ANTLR start "rule__Restriction__Group_4__0__Impl"
    // InternalDSL.g:6196:1: rule__Restriction__Group_4__0__Impl : ( 'RuleDescription:' ) ;
    public final void rule__Restriction__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6200:1: ( ( 'RuleDescription:' ) )
            // InternalDSL.g:6201:1: ( 'RuleDescription:' )
            {
            // InternalDSL.g:6201:1: ( 'RuleDescription:' )
            // InternalDSL.g:6202:2: 'RuleDescription:'
            {
             before(grammarAccess.getRestrictionAccess().getRuleDescriptionKeyword_4_0()); 
            match(input,94,FOLLOW_2); 
             after(grammarAccess.getRestrictionAccess().getRuleDescriptionKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group_4__0__Impl"


    // $ANTLR start "rule__Restriction__Group_4__1"
    // InternalDSL.g:6211:1: rule__Restriction__Group_4__1 : rule__Restriction__Group_4__1__Impl ;
    public final void rule__Restriction__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6215:1: ( rule__Restriction__Group_4__1__Impl )
            // InternalDSL.g:6216:2: rule__Restriction__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Restriction__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group_4__1"


    // $ANTLR start "rule__Restriction__Group_4__1__Impl"
    // InternalDSL.g:6222:1: rule__Restriction__Group_4__1__Impl : ( ( rule__Restriction__RuleDescriptionAssignment_4_1 ) ) ;
    public final void rule__Restriction__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6226:1: ( ( ( rule__Restriction__RuleDescriptionAssignment_4_1 ) ) )
            // InternalDSL.g:6227:1: ( ( rule__Restriction__RuleDescriptionAssignment_4_1 ) )
            {
            // InternalDSL.g:6227:1: ( ( rule__Restriction__RuleDescriptionAssignment_4_1 ) )
            // InternalDSL.g:6228:2: ( rule__Restriction__RuleDescriptionAssignment_4_1 )
            {
             before(grammarAccess.getRestrictionAccess().getRuleDescriptionAssignment_4_1()); 
            // InternalDSL.g:6229:2: ( rule__Restriction__RuleDescriptionAssignment_4_1 )
            // InternalDSL.g:6229:3: rule__Restriction__RuleDescriptionAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Restriction__RuleDescriptionAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getRestrictionAccess().getRuleDescriptionAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group_4__1__Impl"


    // $ANTLR start "rule__Restriction__Group_5__0"
    // InternalDSL.g:6238:1: rule__Restriction__Group_5__0 : rule__Restriction__Group_5__0__Impl rule__Restriction__Group_5__1 ;
    public final void rule__Restriction__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6242:1: ( rule__Restriction__Group_5__0__Impl rule__Restriction__Group_5__1 )
            // InternalDSL.g:6243:2: rule__Restriction__Group_5__0__Impl rule__Restriction__Group_5__1
            {
            pushFollow(FOLLOW_39);
            rule__Restriction__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Restriction__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group_5__0"


    // $ANTLR start "rule__Restriction__Group_5__0__Impl"
    // InternalDSL.g:6250:1: rule__Restriction__Group_5__0__Impl : ( 'RestrictionType:' ) ;
    public final void rule__Restriction__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6254:1: ( ( 'RestrictionType:' ) )
            // InternalDSL.g:6255:1: ( 'RestrictionType:' )
            {
            // InternalDSL.g:6255:1: ( 'RestrictionType:' )
            // InternalDSL.g:6256:2: 'RestrictionType:'
            {
             before(grammarAccess.getRestrictionAccess().getRestrictionTypeKeyword_5_0()); 
            match(input,95,FOLLOW_2); 
             after(grammarAccess.getRestrictionAccess().getRestrictionTypeKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group_5__0__Impl"


    // $ANTLR start "rule__Restriction__Group_5__1"
    // InternalDSL.g:6265:1: rule__Restriction__Group_5__1 : rule__Restriction__Group_5__1__Impl ;
    public final void rule__Restriction__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6269:1: ( rule__Restriction__Group_5__1__Impl )
            // InternalDSL.g:6270:2: rule__Restriction__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Restriction__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group_5__1"


    // $ANTLR start "rule__Restriction__Group_5__1__Impl"
    // InternalDSL.g:6276:1: rule__Restriction__Group_5__1__Impl : ( ( rule__Restriction__RestrictionTypeAssignment_5_1 ) ) ;
    public final void rule__Restriction__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6280:1: ( ( ( rule__Restriction__RestrictionTypeAssignment_5_1 ) ) )
            // InternalDSL.g:6281:1: ( ( rule__Restriction__RestrictionTypeAssignment_5_1 ) )
            {
            // InternalDSL.g:6281:1: ( ( rule__Restriction__RestrictionTypeAssignment_5_1 ) )
            // InternalDSL.g:6282:2: ( rule__Restriction__RestrictionTypeAssignment_5_1 )
            {
             before(grammarAccess.getRestrictionAccess().getRestrictionTypeAssignment_5_1()); 
            // InternalDSL.g:6283:2: ( rule__Restriction__RestrictionTypeAssignment_5_1 )
            // InternalDSL.g:6283:3: rule__Restriction__RestrictionTypeAssignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__Restriction__RestrictionTypeAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getRestrictionAccess().getRestrictionTypeAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group_5__1__Impl"


    // $ANTLR start "rule__Restriction__Group_6__0"
    // InternalDSL.g:6292:1: rule__Restriction__Group_6__0 : rule__Restriction__Group_6__0__Impl rule__Restriction__Group_6__1 ;
    public final void rule__Restriction__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6296:1: ( rule__Restriction__Group_6__0__Impl rule__Restriction__Group_6__1 )
            // InternalDSL.g:6297:2: rule__Restriction__Group_6__0__Impl rule__Restriction__Group_6__1
            {
            pushFollow(FOLLOW_34);
            rule__Restriction__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Restriction__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group_6__0"


    // $ANTLR start "rule__Restriction__Group_6__0__Impl"
    // InternalDSL.g:6304:1: rule__Restriction__Group_6__0__Impl : ( 'Amount:' ) ;
    public final void rule__Restriction__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6308:1: ( ( 'Amount:' ) )
            // InternalDSL.g:6309:1: ( 'Amount:' )
            {
            // InternalDSL.g:6309:1: ( 'Amount:' )
            // InternalDSL.g:6310:2: 'Amount:'
            {
             before(grammarAccess.getRestrictionAccess().getAmountKeyword_6_0()); 
            match(input,96,FOLLOW_2); 
             after(grammarAccess.getRestrictionAccess().getAmountKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group_6__0__Impl"


    // $ANTLR start "rule__Restriction__Group_6__1"
    // InternalDSL.g:6319:1: rule__Restriction__Group_6__1 : rule__Restriction__Group_6__1__Impl ;
    public final void rule__Restriction__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6323:1: ( rule__Restriction__Group_6__1__Impl )
            // InternalDSL.g:6324:2: rule__Restriction__Group_6__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Restriction__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group_6__1"


    // $ANTLR start "rule__Restriction__Group_6__1__Impl"
    // InternalDSL.g:6330:1: rule__Restriction__Group_6__1__Impl : ( ( rule__Restriction__AmountAssignment_6_1 ) ) ;
    public final void rule__Restriction__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6334:1: ( ( ( rule__Restriction__AmountAssignment_6_1 ) ) )
            // InternalDSL.g:6335:1: ( ( rule__Restriction__AmountAssignment_6_1 ) )
            {
            // InternalDSL.g:6335:1: ( ( rule__Restriction__AmountAssignment_6_1 ) )
            // InternalDSL.g:6336:2: ( rule__Restriction__AmountAssignment_6_1 )
            {
             before(grammarAccess.getRestrictionAccess().getAmountAssignment_6_1()); 
            // InternalDSL.g:6337:2: ( rule__Restriction__AmountAssignment_6_1 )
            // InternalDSL.g:6337:3: rule__Restriction__AmountAssignment_6_1
            {
            pushFollow(FOLLOW_2);
            rule__Restriction__AmountAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getRestrictionAccess().getAmountAssignment_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group_6__1__Impl"


    // $ANTLR start "rule__Restriction__Group_7__0"
    // InternalDSL.g:6346:1: rule__Restriction__Group_7__0 : rule__Restriction__Group_7__0__Impl rule__Restriction__Group_7__1 ;
    public final void rule__Restriction__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6350:1: ( rule__Restriction__Group_7__0__Impl rule__Restriction__Group_7__1 )
            // InternalDSL.g:6351:2: rule__Restriction__Group_7__0__Impl rule__Restriction__Group_7__1
            {
            pushFollow(FOLLOW_12);
            rule__Restriction__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Restriction__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group_7__0"


    // $ANTLR start "rule__Restriction__Group_7__0__Impl"
    // InternalDSL.g:6358:1: rule__Restriction__Group_7__0__Impl : ( 'Information:' ) ;
    public final void rule__Restriction__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6362:1: ( ( 'Information:' ) )
            // InternalDSL.g:6363:1: ( 'Information:' )
            {
            // InternalDSL.g:6363:1: ( 'Information:' )
            // InternalDSL.g:6364:2: 'Information:'
            {
             before(grammarAccess.getRestrictionAccess().getInformationKeyword_7_0()); 
            match(input,65,FOLLOW_2); 
             after(grammarAccess.getRestrictionAccess().getInformationKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group_7__0__Impl"


    // $ANTLR start "rule__Restriction__Group_7__1"
    // InternalDSL.g:6373:1: rule__Restriction__Group_7__1 : rule__Restriction__Group_7__1__Impl ;
    public final void rule__Restriction__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6377:1: ( rule__Restriction__Group_7__1__Impl )
            // InternalDSL.g:6378:2: rule__Restriction__Group_7__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Restriction__Group_7__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group_7__1"


    // $ANTLR start "rule__Restriction__Group_7__1__Impl"
    // InternalDSL.g:6384:1: rule__Restriction__Group_7__1__Impl : ( ( rule__Restriction__InformationAssignment_7_1 ) ) ;
    public final void rule__Restriction__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6388:1: ( ( ( rule__Restriction__InformationAssignment_7_1 ) ) )
            // InternalDSL.g:6389:1: ( ( rule__Restriction__InformationAssignment_7_1 ) )
            {
            // InternalDSL.g:6389:1: ( ( rule__Restriction__InformationAssignment_7_1 ) )
            // InternalDSL.g:6390:2: ( rule__Restriction__InformationAssignment_7_1 )
            {
             before(grammarAccess.getRestrictionAccess().getInformationAssignment_7_1()); 
            // InternalDSL.g:6391:2: ( rule__Restriction__InformationAssignment_7_1 )
            // InternalDSL.g:6391:3: rule__Restriction__InformationAssignment_7_1
            {
            pushFollow(FOLLOW_2);
            rule__Restriction__InformationAssignment_7_1();

            state._fsp--;


            }

             after(grammarAccess.getRestrictionAccess().getInformationAssignment_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__Group_7__1__Impl"


    // $ANTLR start "rule__RandomReward__Group__0"
    // InternalDSL.g:6400:1: rule__RandomReward__Group__0 : rule__RandomReward__Group__0__Impl rule__RandomReward__Group__1 ;
    public final void rule__RandomReward__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6404:1: ( rule__RandomReward__Group__0__Impl rule__RandomReward__Group__1 )
            // InternalDSL.g:6405:2: rule__RandomReward__Group__0__Impl rule__RandomReward__Group__1
            {
            pushFollow(FOLLOW_40);
            rule__RandomReward__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RandomReward__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group__0"


    // $ANTLR start "rule__RandomReward__Group__0__Impl"
    // InternalDSL.g:6412:1: rule__RandomReward__Group__0__Impl : ( () ) ;
    public final void rule__RandomReward__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6416:1: ( ( () ) )
            // InternalDSL.g:6417:1: ( () )
            {
            // InternalDSL.g:6417:1: ( () )
            // InternalDSL.g:6418:2: ()
            {
             before(grammarAccess.getRandomRewardAccess().getRandomRewardAction_0()); 
            // InternalDSL.g:6419:2: ()
            // InternalDSL.g:6419:3: 
            {
            }

             after(grammarAccess.getRandomRewardAccess().getRandomRewardAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group__0__Impl"


    // $ANTLR start "rule__RandomReward__Group__1"
    // InternalDSL.g:6427:1: rule__RandomReward__Group__1 : rule__RandomReward__Group__1__Impl rule__RandomReward__Group__2 ;
    public final void rule__RandomReward__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6431:1: ( rule__RandomReward__Group__1__Impl rule__RandomReward__Group__2 )
            // InternalDSL.g:6432:2: rule__RandomReward__Group__1__Impl rule__RandomReward__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__RandomReward__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RandomReward__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group__1"


    // $ANTLR start "rule__RandomReward__Group__1__Impl"
    // InternalDSL.g:6439:1: rule__RandomReward__Group__1__Impl : ( 'RandomReward' ) ;
    public final void rule__RandomReward__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6443:1: ( ( 'RandomReward' ) )
            // InternalDSL.g:6444:1: ( 'RandomReward' )
            {
            // InternalDSL.g:6444:1: ( 'RandomReward' )
            // InternalDSL.g:6445:2: 'RandomReward'
            {
             before(grammarAccess.getRandomRewardAccess().getRandomRewardKeyword_1()); 
            match(input,97,FOLLOW_2); 
             after(grammarAccess.getRandomRewardAccess().getRandomRewardKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group__1__Impl"


    // $ANTLR start "rule__RandomReward__Group__2"
    // InternalDSL.g:6454:1: rule__RandomReward__Group__2 : rule__RandomReward__Group__2__Impl rule__RandomReward__Group__3 ;
    public final void rule__RandomReward__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6458:1: ( rule__RandomReward__Group__2__Impl rule__RandomReward__Group__3 )
            // InternalDSL.g:6459:2: rule__RandomReward__Group__2__Impl rule__RandomReward__Group__3
            {
            pushFollow(FOLLOW_41);
            rule__RandomReward__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RandomReward__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group__2"


    // $ANTLR start "rule__RandomReward__Group__2__Impl"
    // InternalDSL.g:6466:1: rule__RandomReward__Group__2__Impl : ( '{' ) ;
    public final void rule__RandomReward__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6470:1: ( ( '{' ) )
            // InternalDSL.g:6471:1: ( '{' )
            {
            // InternalDSL.g:6471:1: ( '{' )
            // InternalDSL.g:6472:2: '{'
            {
             before(grammarAccess.getRandomRewardAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,58,FOLLOW_2); 
             after(grammarAccess.getRandomRewardAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group__2__Impl"


    // $ANTLR start "rule__RandomReward__Group__3"
    // InternalDSL.g:6481:1: rule__RandomReward__Group__3 : rule__RandomReward__Group__3__Impl rule__RandomReward__Group__4 ;
    public final void rule__RandomReward__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6485:1: ( rule__RandomReward__Group__3__Impl rule__RandomReward__Group__4 )
            // InternalDSL.g:6486:2: rule__RandomReward__Group__3__Impl rule__RandomReward__Group__4
            {
            pushFollow(FOLLOW_41);
            rule__RandomReward__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RandomReward__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group__3"


    // $ANTLR start "rule__RandomReward__Group__3__Impl"
    // InternalDSL.g:6493:1: rule__RandomReward__Group__3__Impl : ( ( rule__RandomReward__Group_3__0 )? ) ;
    public final void rule__RandomReward__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6497:1: ( ( ( rule__RandomReward__Group_3__0 )? ) )
            // InternalDSL.g:6498:1: ( ( rule__RandomReward__Group_3__0 )? )
            {
            // InternalDSL.g:6498:1: ( ( rule__RandomReward__Group_3__0 )? )
            // InternalDSL.g:6499:2: ( rule__RandomReward__Group_3__0 )?
            {
             before(grammarAccess.getRandomRewardAccess().getGroup_3()); 
            // InternalDSL.g:6500:2: ( rule__RandomReward__Group_3__0 )?
            int alt58=2;
            int LA58_0 = input.LA(1);

            if ( (LA58_0==98) ) {
                alt58=1;
            }
            switch (alt58) {
                case 1 :
                    // InternalDSL.g:6500:3: rule__RandomReward__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__RandomReward__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRandomRewardAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group__3__Impl"


    // $ANTLR start "rule__RandomReward__Group__4"
    // InternalDSL.g:6508:1: rule__RandomReward__Group__4 : rule__RandomReward__Group__4__Impl rule__RandomReward__Group__5 ;
    public final void rule__RandomReward__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6512:1: ( rule__RandomReward__Group__4__Impl rule__RandomReward__Group__5 )
            // InternalDSL.g:6513:2: rule__RandomReward__Group__4__Impl rule__RandomReward__Group__5
            {
            pushFollow(FOLLOW_41);
            rule__RandomReward__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RandomReward__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group__4"


    // $ANTLR start "rule__RandomReward__Group__4__Impl"
    // InternalDSL.g:6520:1: rule__RandomReward__Group__4__Impl : ( ( rule__RandomReward__Group_4__0 )? ) ;
    public final void rule__RandomReward__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6524:1: ( ( ( rule__RandomReward__Group_4__0 )? ) )
            // InternalDSL.g:6525:1: ( ( rule__RandomReward__Group_4__0 )? )
            {
            // InternalDSL.g:6525:1: ( ( rule__RandomReward__Group_4__0 )? )
            // InternalDSL.g:6526:2: ( rule__RandomReward__Group_4__0 )?
            {
             before(grammarAccess.getRandomRewardAccess().getGroup_4()); 
            // InternalDSL.g:6527:2: ( rule__RandomReward__Group_4__0 )?
            int alt59=2;
            int LA59_0 = input.LA(1);

            if ( (LA59_0==99) ) {
                alt59=1;
            }
            switch (alt59) {
                case 1 :
                    // InternalDSL.g:6527:3: rule__RandomReward__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__RandomReward__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRandomRewardAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group__4__Impl"


    // $ANTLR start "rule__RandomReward__Group__5"
    // InternalDSL.g:6535:1: rule__RandomReward__Group__5 : rule__RandomReward__Group__5__Impl rule__RandomReward__Group__6 ;
    public final void rule__RandomReward__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6539:1: ( rule__RandomReward__Group__5__Impl rule__RandomReward__Group__6 )
            // InternalDSL.g:6540:2: rule__RandomReward__Group__5__Impl rule__RandomReward__Group__6
            {
            pushFollow(FOLLOW_41);
            rule__RandomReward__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RandomReward__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group__5"


    // $ANTLR start "rule__RandomReward__Group__5__Impl"
    // InternalDSL.g:6547:1: rule__RandomReward__Group__5__Impl : ( ( rule__RandomReward__Group_5__0 )? ) ;
    public final void rule__RandomReward__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6551:1: ( ( ( rule__RandomReward__Group_5__0 )? ) )
            // InternalDSL.g:6552:1: ( ( rule__RandomReward__Group_5__0 )? )
            {
            // InternalDSL.g:6552:1: ( ( rule__RandomReward__Group_5__0 )? )
            // InternalDSL.g:6553:2: ( rule__RandomReward__Group_5__0 )?
            {
             before(grammarAccess.getRandomRewardAccess().getGroup_5()); 
            // InternalDSL.g:6554:2: ( rule__RandomReward__Group_5__0 )?
            int alt60=2;
            int LA60_0 = input.LA(1);

            if ( (LA60_0==100) ) {
                alt60=1;
            }
            switch (alt60) {
                case 1 :
                    // InternalDSL.g:6554:3: rule__RandomReward__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__RandomReward__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRandomRewardAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group__5__Impl"


    // $ANTLR start "rule__RandomReward__Group__6"
    // InternalDSL.g:6562:1: rule__RandomReward__Group__6 : rule__RandomReward__Group__6__Impl ;
    public final void rule__RandomReward__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6566:1: ( rule__RandomReward__Group__6__Impl )
            // InternalDSL.g:6567:2: rule__RandomReward__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__RandomReward__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group__6"


    // $ANTLR start "rule__RandomReward__Group__6__Impl"
    // InternalDSL.g:6573:1: rule__RandomReward__Group__6__Impl : ( '}' ) ;
    public final void rule__RandomReward__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6577:1: ( ( '}' ) )
            // InternalDSL.g:6578:1: ( '}' )
            {
            // InternalDSL.g:6578:1: ( '}' )
            // InternalDSL.g:6579:2: '}'
            {
             before(grammarAccess.getRandomRewardAccess().getRightCurlyBracketKeyword_6()); 
            match(input,60,FOLLOW_2); 
             after(grammarAccess.getRandomRewardAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group__6__Impl"


    // $ANTLR start "rule__RandomReward__Group_3__0"
    // InternalDSL.g:6589:1: rule__RandomReward__Group_3__0 : rule__RandomReward__Group_3__0__Impl rule__RandomReward__Group_3__1 ;
    public final void rule__RandomReward__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6593:1: ( rule__RandomReward__Group_3__0__Impl rule__RandomReward__Group_3__1 )
            // InternalDSL.g:6594:2: rule__RandomReward__Group_3__0__Impl rule__RandomReward__Group_3__1
            {
            pushFollow(FOLLOW_42);
            rule__RandomReward__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RandomReward__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group_3__0"


    // $ANTLR start "rule__RandomReward__Group_3__0__Impl"
    // InternalDSL.g:6601:1: rule__RandomReward__Group_3__0__Impl : ( 'DropRate:' ) ;
    public final void rule__RandomReward__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6605:1: ( ( 'DropRate:' ) )
            // InternalDSL.g:6606:1: ( 'DropRate:' )
            {
            // InternalDSL.g:6606:1: ( 'DropRate:' )
            // InternalDSL.g:6607:2: 'DropRate:'
            {
             before(grammarAccess.getRandomRewardAccess().getDropRateKeyword_3_0()); 
            match(input,98,FOLLOW_2); 
             after(grammarAccess.getRandomRewardAccess().getDropRateKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group_3__0__Impl"


    // $ANTLR start "rule__RandomReward__Group_3__1"
    // InternalDSL.g:6616:1: rule__RandomReward__Group_3__1 : rule__RandomReward__Group_3__1__Impl ;
    public final void rule__RandomReward__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6620:1: ( rule__RandomReward__Group_3__1__Impl )
            // InternalDSL.g:6621:2: rule__RandomReward__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__RandomReward__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group_3__1"


    // $ANTLR start "rule__RandomReward__Group_3__1__Impl"
    // InternalDSL.g:6627:1: rule__RandomReward__Group_3__1__Impl : ( ( rule__RandomReward__DropRateAssignment_3_1 ) ) ;
    public final void rule__RandomReward__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6631:1: ( ( ( rule__RandomReward__DropRateAssignment_3_1 ) ) )
            // InternalDSL.g:6632:1: ( ( rule__RandomReward__DropRateAssignment_3_1 ) )
            {
            // InternalDSL.g:6632:1: ( ( rule__RandomReward__DropRateAssignment_3_1 ) )
            // InternalDSL.g:6633:2: ( rule__RandomReward__DropRateAssignment_3_1 )
            {
             before(grammarAccess.getRandomRewardAccess().getDropRateAssignment_3_1()); 
            // InternalDSL.g:6634:2: ( rule__RandomReward__DropRateAssignment_3_1 )
            // InternalDSL.g:6634:3: rule__RandomReward__DropRateAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__RandomReward__DropRateAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getRandomRewardAccess().getDropRateAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group_3__1__Impl"


    // $ANTLR start "rule__RandomReward__Group_4__0"
    // InternalDSL.g:6643:1: rule__RandomReward__Group_4__0 : rule__RandomReward__Group_4__0__Impl rule__RandomReward__Group_4__1 ;
    public final void rule__RandomReward__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6647:1: ( rule__RandomReward__Group_4__0__Impl rule__RandomReward__Group_4__1 )
            // InternalDSL.g:6648:2: rule__RandomReward__Group_4__0__Impl rule__RandomReward__Group_4__1
            {
            pushFollow(FOLLOW_28);
            rule__RandomReward__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RandomReward__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group_4__0"


    // $ANTLR start "rule__RandomReward__Group_4__0__Impl"
    // InternalDSL.g:6655:1: rule__RandomReward__Group_4__0__Impl : ( 'IsVisible:' ) ;
    public final void rule__RandomReward__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6659:1: ( ( 'IsVisible:' ) )
            // InternalDSL.g:6660:1: ( 'IsVisible:' )
            {
            // InternalDSL.g:6660:1: ( 'IsVisible:' )
            // InternalDSL.g:6661:2: 'IsVisible:'
            {
             before(grammarAccess.getRandomRewardAccess().getIsVisibleKeyword_4_0()); 
            match(input,99,FOLLOW_2); 
             after(grammarAccess.getRandomRewardAccess().getIsVisibleKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group_4__0__Impl"


    // $ANTLR start "rule__RandomReward__Group_4__1"
    // InternalDSL.g:6670:1: rule__RandomReward__Group_4__1 : rule__RandomReward__Group_4__1__Impl ;
    public final void rule__RandomReward__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6674:1: ( rule__RandomReward__Group_4__1__Impl )
            // InternalDSL.g:6675:2: rule__RandomReward__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__RandomReward__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group_4__1"


    // $ANTLR start "rule__RandomReward__Group_4__1__Impl"
    // InternalDSL.g:6681:1: rule__RandomReward__Group_4__1__Impl : ( ( rule__RandomReward__IsVisibleAssignment_4_1 ) ) ;
    public final void rule__RandomReward__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6685:1: ( ( ( rule__RandomReward__IsVisibleAssignment_4_1 ) ) )
            // InternalDSL.g:6686:1: ( ( rule__RandomReward__IsVisibleAssignment_4_1 ) )
            {
            // InternalDSL.g:6686:1: ( ( rule__RandomReward__IsVisibleAssignment_4_1 ) )
            // InternalDSL.g:6687:2: ( rule__RandomReward__IsVisibleAssignment_4_1 )
            {
             before(grammarAccess.getRandomRewardAccess().getIsVisibleAssignment_4_1()); 
            // InternalDSL.g:6688:2: ( rule__RandomReward__IsVisibleAssignment_4_1 )
            // InternalDSL.g:6688:3: rule__RandomReward__IsVisibleAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__RandomReward__IsVisibleAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getRandomRewardAccess().getIsVisibleAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group_4__1__Impl"


    // $ANTLR start "rule__RandomReward__Group_5__0"
    // InternalDSL.g:6697:1: rule__RandomReward__Group_5__0 : rule__RandomReward__Group_5__0__Impl rule__RandomReward__Group_5__1 ;
    public final void rule__RandomReward__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6701:1: ( rule__RandomReward__Group_5__0__Impl rule__RandomReward__Group_5__1 )
            // InternalDSL.g:6702:2: rule__RandomReward__Group_5__0__Impl rule__RandomReward__Group_5__1
            {
            pushFollow(FOLLOW_11);
            rule__RandomReward__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RandomReward__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group_5__0"


    // $ANTLR start "rule__RandomReward__Group_5__0__Impl"
    // InternalDSL.g:6709:1: rule__RandomReward__Group_5__0__Impl : ( 'Item:' ) ;
    public final void rule__RandomReward__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6713:1: ( ( 'Item:' ) )
            // InternalDSL.g:6714:1: ( 'Item:' )
            {
            // InternalDSL.g:6714:1: ( 'Item:' )
            // InternalDSL.g:6715:2: 'Item:'
            {
             before(grammarAccess.getRandomRewardAccess().getItemKeyword_5_0()); 
            match(input,100,FOLLOW_2); 
             after(grammarAccess.getRandomRewardAccess().getItemKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group_5__0__Impl"


    // $ANTLR start "rule__RandomReward__Group_5__1"
    // InternalDSL.g:6724:1: rule__RandomReward__Group_5__1 : rule__RandomReward__Group_5__1__Impl ;
    public final void rule__RandomReward__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6728:1: ( rule__RandomReward__Group_5__1__Impl )
            // InternalDSL.g:6729:2: rule__RandomReward__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__RandomReward__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group_5__1"


    // $ANTLR start "rule__RandomReward__Group_5__1__Impl"
    // InternalDSL.g:6735:1: rule__RandomReward__Group_5__1__Impl : ( ( rule__RandomReward__ItemAssignment_5_1 ) ) ;
    public final void rule__RandomReward__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6739:1: ( ( ( rule__RandomReward__ItemAssignment_5_1 ) ) )
            // InternalDSL.g:6740:1: ( ( rule__RandomReward__ItemAssignment_5_1 ) )
            {
            // InternalDSL.g:6740:1: ( ( rule__RandomReward__ItemAssignment_5_1 ) )
            // InternalDSL.g:6741:2: ( rule__RandomReward__ItemAssignment_5_1 )
            {
             before(grammarAccess.getRandomRewardAccess().getItemAssignment_5_1()); 
            // InternalDSL.g:6742:2: ( rule__RandomReward__ItemAssignment_5_1 )
            // InternalDSL.g:6742:3: rule__RandomReward__ItemAssignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__RandomReward__ItemAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getRandomRewardAccess().getItemAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__Group_5__1__Impl"


    // $ANTLR start "rule__FixedReward__Group__0"
    // InternalDSL.g:6751:1: rule__FixedReward__Group__0 : rule__FixedReward__Group__0__Impl rule__FixedReward__Group__1 ;
    public final void rule__FixedReward__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6755:1: ( rule__FixedReward__Group__0__Impl rule__FixedReward__Group__1 )
            // InternalDSL.g:6756:2: rule__FixedReward__Group__0__Impl rule__FixedReward__Group__1
            {
            pushFollow(FOLLOW_31);
            rule__FixedReward__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FixedReward__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FixedReward__Group__0"


    // $ANTLR start "rule__FixedReward__Group__0__Impl"
    // InternalDSL.g:6763:1: rule__FixedReward__Group__0__Impl : ( () ) ;
    public final void rule__FixedReward__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6767:1: ( ( () ) )
            // InternalDSL.g:6768:1: ( () )
            {
            // InternalDSL.g:6768:1: ( () )
            // InternalDSL.g:6769:2: ()
            {
             before(grammarAccess.getFixedRewardAccess().getFixedRewardAction_0()); 
            // InternalDSL.g:6770:2: ()
            // InternalDSL.g:6770:3: 
            {
            }

             after(grammarAccess.getFixedRewardAccess().getFixedRewardAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FixedReward__Group__0__Impl"


    // $ANTLR start "rule__FixedReward__Group__1"
    // InternalDSL.g:6778:1: rule__FixedReward__Group__1 : rule__FixedReward__Group__1__Impl rule__FixedReward__Group__2 ;
    public final void rule__FixedReward__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6782:1: ( rule__FixedReward__Group__1__Impl rule__FixedReward__Group__2 )
            // InternalDSL.g:6783:2: rule__FixedReward__Group__1__Impl rule__FixedReward__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__FixedReward__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FixedReward__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FixedReward__Group__1"


    // $ANTLR start "rule__FixedReward__Group__1__Impl"
    // InternalDSL.g:6790:1: rule__FixedReward__Group__1__Impl : ( 'FixedReward' ) ;
    public final void rule__FixedReward__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6794:1: ( ( 'FixedReward' ) )
            // InternalDSL.g:6795:1: ( 'FixedReward' )
            {
            // InternalDSL.g:6795:1: ( 'FixedReward' )
            // InternalDSL.g:6796:2: 'FixedReward'
            {
             before(grammarAccess.getFixedRewardAccess().getFixedRewardKeyword_1()); 
            match(input,101,FOLLOW_2); 
             after(grammarAccess.getFixedRewardAccess().getFixedRewardKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FixedReward__Group__1__Impl"


    // $ANTLR start "rule__FixedReward__Group__2"
    // InternalDSL.g:6805:1: rule__FixedReward__Group__2 : rule__FixedReward__Group__2__Impl rule__FixedReward__Group__3 ;
    public final void rule__FixedReward__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6809:1: ( rule__FixedReward__Group__2__Impl rule__FixedReward__Group__3 )
            // InternalDSL.g:6810:2: rule__FixedReward__Group__2__Impl rule__FixedReward__Group__3
            {
            pushFollow(FOLLOW_43);
            rule__FixedReward__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FixedReward__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FixedReward__Group__2"


    // $ANTLR start "rule__FixedReward__Group__2__Impl"
    // InternalDSL.g:6817:1: rule__FixedReward__Group__2__Impl : ( '{' ) ;
    public final void rule__FixedReward__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6821:1: ( ( '{' ) )
            // InternalDSL.g:6822:1: ( '{' )
            {
            // InternalDSL.g:6822:1: ( '{' )
            // InternalDSL.g:6823:2: '{'
            {
             before(grammarAccess.getFixedRewardAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,58,FOLLOW_2); 
             after(grammarAccess.getFixedRewardAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FixedReward__Group__2__Impl"


    // $ANTLR start "rule__FixedReward__Group__3"
    // InternalDSL.g:6832:1: rule__FixedReward__Group__3 : rule__FixedReward__Group__3__Impl rule__FixedReward__Group__4 ;
    public final void rule__FixedReward__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6836:1: ( rule__FixedReward__Group__3__Impl rule__FixedReward__Group__4 )
            // InternalDSL.g:6837:2: rule__FixedReward__Group__3__Impl rule__FixedReward__Group__4
            {
            pushFollow(FOLLOW_43);
            rule__FixedReward__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FixedReward__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FixedReward__Group__3"


    // $ANTLR start "rule__FixedReward__Group__3__Impl"
    // InternalDSL.g:6844:1: rule__FixedReward__Group__3__Impl : ( ( rule__FixedReward__Group_3__0 )? ) ;
    public final void rule__FixedReward__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6848:1: ( ( ( rule__FixedReward__Group_3__0 )? ) )
            // InternalDSL.g:6849:1: ( ( rule__FixedReward__Group_3__0 )? )
            {
            // InternalDSL.g:6849:1: ( ( rule__FixedReward__Group_3__0 )? )
            // InternalDSL.g:6850:2: ( rule__FixedReward__Group_3__0 )?
            {
             before(grammarAccess.getFixedRewardAccess().getGroup_3()); 
            // InternalDSL.g:6851:2: ( rule__FixedReward__Group_3__0 )?
            int alt61=2;
            int LA61_0 = input.LA(1);

            if ( (LA61_0==100) ) {
                alt61=1;
            }
            switch (alt61) {
                case 1 :
                    // InternalDSL.g:6851:3: rule__FixedReward__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__FixedReward__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFixedRewardAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FixedReward__Group__3__Impl"


    // $ANTLR start "rule__FixedReward__Group__4"
    // InternalDSL.g:6859:1: rule__FixedReward__Group__4 : rule__FixedReward__Group__4__Impl ;
    public final void rule__FixedReward__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6863:1: ( rule__FixedReward__Group__4__Impl )
            // InternalDSL.g:6864:2: rule__FixedReward__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FixedReward__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FixedReward__Group__4"


    // $ANTLR start "rule__FixedReward__Group__4__Impl"
    // InternalDSL.g:6870:1: rule__FixedReward__Group__4__Impl : ( '}' ) ;
    public final void rule__FixedReward__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6874:1: ( ( '}' ) )
            // InternalDSL.g:6875:1: ( '}' )
            {
            // InternalDSL.g:6875:1: ( '}' )
            // InternalDSL.g:6876:2: '}'
            {
             before(grammarAccess.getFixedRewardAccess().getRightCurlyBracketKeyword_4()); 
            match(input,60,FOLLOW_2); 
             after(grammarAccess.getFixedRewardAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FixedReward__Group__4__Impl"


    // $ANTLR start "rule__FixedReward__Group_3__0"
    // InternalDSL.g:6886:1: rule__FixedReward__Group_3__0 : rule__FixedReward__Group_3__0__Impl rule__FixedReward__Group_3__1 ;
    public final void rule__FixedReward__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6890:1: ( rule__FixedReward__Group_3__0__Impl rule__FixedReward__Group_3__1 )
            // InternalDSL.g:6891:2: rule__FixedReward__Group_3__0__Impl rule__FixedReward__Group_3__1
            {
            pushFollow(FOLLOW_11);
            rule__FixedReward__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FixedReward__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FixedReward__Group_3__0"


    // $ANTLR start "rule__FixedReward__Group_3__0__Impl"
    // InternalDSL.g:6898:1: rule__FixedReward__Group_3__0__Impl : ( 'Item:' ) ;
    public final void rule__FixedReward__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6902:1: ( ( 'Item:' ) )
            // InternalDSL.g:6903:1: ( 'Item:' )
            {
            // InternalDSL.g:6903:1: ( 'Item:' )
            // InternalDSL.g:6904:2: 'Item:'
            {
             before(grammarAccess.getFixedRewardAccess().getItemKeyword_3_0()); 
            match(input,100,FOLLOW_2); 
             after(grammarAccess.getFixedRewardAccess().getItemKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FixedReward__Group_3__0__Impl"


    // $ANTLR start "rule__FixedReward__Group_3__1"
    // InternalDSL.g:6913:1: rule__FixedReward__Group_3__1 : rule__FixedReward__Group_3__1__Impl ;
    public final void rule__FixedReward__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6917:1: ( rule__FixedReward__Group_3__1__Impl )
            // InternalDSL.g:6918:2: rule__FixedReward__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FixedReward__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FixedReward__Group_3__1"


    // $ANTLR start "rule__FixedReward__Group_3__1__Impl"
    // InternalDSL.g:6924:1: rule__FixedReward__Group_3__1__Impl : ( ( rule__FixedReward__ItemAssignment_3_1 ) ) ;
    public final void rule__FixedReward__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6928:1: ( ( ( rule__FixedReward__ItemAssignment_3_1 ) ) )
            // InternalDSL.g:6929:1: ( ( rule__FixedReward__ItemAssignment_3_1 ) )
            {
            // InternalDSL.g:6929:1: ( ( rule__FixedReward__ItemAssignment_3_1 ) )
            // InternalDSL.g:6930:2: ( rule__FixedReward__ItemAssignment_3_1 )
            {
             before(grammarAccess.getFixedRewardAccess().getItemAssignment_3_1()); 
            // InternalDSL.g:6931:2: ( rule__FixedReward__ItemAssignment_3_1 )
            // InternalDSL.g:6931:3: rule__FixedReward__ItemAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__FixedReward__ItemAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getFixedRewardAccess().getItemAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FixedReward__Group_3__1__Impl"


    // $ANTLR start "rule__Badge__Group__0"
    // InternalDSL.g:6940:1: rule__Badge__Group__0 : rule__Badge__Group__0__Impl rule__Badge__Group__1 ;
    public final void rule__Badge__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6944:1: ( rule__Badge__Group__0__Impl rule__Badge__Group__1 )
            // InternalDSL.g:6945:2: rule__Badge__Group__0__Impl rule__Badge__Group__1
            {
            pushFollow(FOLLOW_44);
            rule__Badge__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badge__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group__0"


    // $ANTLR start "rule__Badge__Group__0__Impl"
    // InternalDSL.g:6952:1: rule__Badge__Group__0__Impl : ( () ) ;
    public final void rule__Badge__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6956:1: ( ( () ) )
            // InternalDSL.g:6957:1: ( () )
            {
            // InternalDSL.g:6957:1: ( () )
            // InternalDSL.g:6958:2: ()
            {
             before(grammarAccess.getBadgeAccess().getBadgeAction_0()); 
            // InternalDSL.g:6959:2: ()
            // InternalDSL.g:6959:3: 
            {
            }

             after(grammarAccess.getBadgeAccess().getBadgeAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group__0__Impl"


    // $ANTLR start "rule__Badge__Group__1"
    // InternalDSL.g:6967:1: rule__Badge__Group__1 : rule__Badge__Group__1__Impl rule__Badge__Group__2 ;
    public final void rule__Badge__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6971:1: ( rule__Badge__Group__1__Impl rule__Badge__Group__2 )
            // InternalDSL.g:6972:2: rule__Badge__Group__1__Impl rule__Badge__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__Badge__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badge__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group__1"


    // $ANTLR start "rule__Badge__Group__1__Impl"
    // InternalDSL.g:6979:1: rule__Badge__Group__1__Impl : ( 'Badge' ) ;
    public final void rule__Badge__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6983:1: ( ( 'Badge' ) )
            // InternalDSL.g:6984:1: ( 'Badge' )
            {
            // InternalDSL.g:6984:1: ( 'Badge' )
            // InternalDSL.g:6985:2: 'Badge'
            {
             before(grammarAccess.getBadgeAccess().getBadgeKeyword_1()); 
            match(input,102,FOLLOW_2); 
             after(grammarAccess.getBadgeAccess().getBadgeKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group__1__Impl"


    // $ANTLR start "rule__Badge__Group__2"
    // InternalDSL.g:6994:1: rule__Badge__Group__2 : rule__Badge__Group__2__Impl rule__Badge__Group__3 ;
    public final void rule__Badge__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:6998:1: ( rule__Badge__Group__2__Impl rule__Badge__Group__3 )
            // InternalDSL.g:6999:2: rule__Badge__Group__2__Impl rule__Badge__Group__3
            {
            pushFollow(FOLLOW_45);
            rule__Badge__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badge__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group__2"


    // $ANTLR start "rule__Badge__Group__2__Impl"
    // InternalDSL.g:7006:1: rule__Badge__Group__2__Impl : ( '{' ) ;
    public final void rule__Badge__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7010:1: ( ( '{' ) )
            // InternalDSL.g:7011:1: ( '{' )
            {
            // InternalDSL.g:7011:1: ( '{' )
            // InternalDSL.g:7012:2: '{'
            {
             before(grammarAccess.getBadgeAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,58,FOLLOW_2); 
             after(grammarAccess.getBadgeAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group__2__Impl"


    // $ANTLR start "rule__Badge__Group__3"
    // InternalDSL.g:7021:1: rule__Badge__Group__3 : rule__Badge__Group__3__Impl rule__Badge__Group__4 ;
    public final void rule__Badge__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7025:1: ( rule__Badge__Group__3__Impl rule__Badge__Group__4 )
            // InternalDSL.g:7026:2: rule__Badge__Group__3__Impl rule__Badge__Group__4
            {
            pushFollow(FOLLOW_45);
            rule__Badge__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badge__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group__3"


    // $ANTLR start "rule__Badge__Group__3__Impl"
    // InternalDSL.g:7033:1: rule__Badge__Group__3__Impl : ( ( rule__Badge__Group_3__0 )? ) ;
    public final void rule__Badge__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7037:1: ( ( ( rule__Badge__Group_3__0 )? ) )
            // InternalDSL.g:7038:1: ( ( rule__Badge__Group_3__0 )? )
            {
            // InternalDSL.g:7038:1: ( ( rule__Badge__Group_3__0 )? )
            // InternalDSL.g:7039:2: ( rule__Badge__Group_3__0 )?
            {
             before(grammarAccess.getBadgeAccess().getGroup_3()); 
            // InternalDSL.g:7040:2: ( rule__Badge__Group_3__0 )?
            int alt62=2;
            int LA62_0 = input.LA(1);

            if ( (LA62_0==62) ) {
                alt62=1;
            }
            switch (alt62) {
                case 1 :
                    // InternalDSL.g:7040:3: rule__Badge__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Badge__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBadgeAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group__3__Impl"


    // $ANTLR start "rule__Badge__Group__4"
    // InternalDSL.g:7048:1: rule__Badge__Group__4 : rule__Badge__Group__4__Impl rule__Badge__Group__5 ;
    public final void rule__Badge__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7052:1: ( rule__Badge__Group__4__Impl rule__Badge__Group__5 )
            // InternalDSL.g:7053:2: rule__Badge__Group__4__Impl rule__Badge__Group__5
            {
            pushFollow(FOLLOW_45);
            rule__Badge__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badge__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group__4"


    // $ANTLR start "rule__Badge__Group__4__Impl"
    // InternalDSL.g:7060:1: rule__Badge__Group__4__Impl : ( ( rule__Badge__Group_4__0 )? ) ;
    public final void rule__Badge__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7064:1: ( ( ( rule__Badge__Group_4__0 )? ) )
            // InternalDSL.g:7065:1: ( ( rule__Badge__Group_4__0 )? )
            {
            // InternalDSL.g:7065:1: ( ( rule__Badge__Group_4__0 )? )
            // InternalDSL.g:7066:2: ( rule__Badge__Group_4__0 )?
            {
             before(grammarAccess.getBadgeAccess().getGroup_4()); 
            // InternalDSL.g:7067:2: ( rule__Badge__Group_4__0 )?
            int alt63=2;
            int LA63_0 = input.LA(1);

            if ( (LA63_0==103) ) {
                alt63=1;
            }
            switch (alt63) {
                case 1 :
                    // InternalDSL.g:7067:3: rule__Badge__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Badge__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBadgeAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group__4__Impl"


    // $ANTLR start "rule__Badge__Group__5"
    // InternalDSL.g:7075:1: rule__Badge__Group__5 : rule__Badge__Group__5__Impl rule__Badge__Group__6 ;
    public final void rule__Badge__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7079:1: ( rule__Badge__Group__5__Impl rule__Badge__Group__6 )
            // InternalDSL.g:7080:2: rule__Badge__Group__5__Impl rule__Badge__Group__6
            {
            pushFollow(FOLLOW_45);
            rule__Badge__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badge__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group__5"


    // $ANTLR start "rule__Badge__Group__5__Impl"
    // InternalDSL.g:7087:1: rule__Badge__Group__5__Impl : ( ( rule__Badge__Group_5__0 )? ) ;
    public final void rule__Badge__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7091:1: ( ( ( rule__Badge__Group_5__0 )? ) )
            // InternalDSL.g:7092:1: ( ( rule__Badge__Group_5__0 )? )
            {
            // InternalDSL.g:7092:1: ( ( rule__Badge__Group_5__0 )? )
            // InternalDSL.g:7093:2: ( rule__Badge__Group_5__0 )?
            {
             before(grammarAccess.getBadgeAccess().getGroup_5()); 
            // InternalDSL.g:7094:2: ( rule__Badge__Group_5__0 )?
            int alt64=2;
            int LA64_0 = input.LA(1);

            if ( (LA64_0==104) ) {
                alt64=1;
            }
            switch (alt64) {
                case 1 :
                    // InternalDSL.g:7094:3: rule__Badge__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Badge__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBadgeAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group__5__Impl"


    // $ANTLR start "rule__Badge__Group__6"
    // InternalDSL.g:7102:1: rule__Badge__Group__6 : rule__Badge__Group__6__Impl rule__Badge__Group__7 ;
    public final void rule__Badge__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7106:1: ( rule__Badge__Group__6__Impl rule__Badge__Group__7 )
            // InternalDSL.g:7107:2: rule__Badge__Group__6__Impl rule__Badge__Group__7
            {
            pushFollow(FOLLOW_45);
            rule__Badge__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badge__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group__6"


    // $ANTLR start "rule__Badge__Group__6__Impl"
    // InternalDSL.g:7114:1: rule__Badge__Group__6__Impl : ( ( rule__Badge__Group_6__0 )? ) ;
    public final void rule__Badge__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7118:1: ( ( ( rule__Badge__Group_6__0 )? ) )
            // InternalDSL.g:7119:1: ( ( rule__Badge__Group_6__0 )? )
            {
            // InternalDSL.g:7119:1: ( ( rule__Badge__Group_6__0 )? )
            // InternalDSL.g:7120:2: ( rule__Badge__Group_6__0 )?
            {
             before(grammarAccess.getBadgeAccess().getGroup_6()); 
            // InternalDSL.g:7121:2: ( rule__Badge__Group_6__0 )?
            int alt65=2;
            int LA65_0 = input.LA(1);

            if ( (LA65_0==105) ) {
                alt65=1;
            }
            switch (alt65) {
                case 1 :
                    // InternalDSL.g:7121:3: rule__Badge__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Badge__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBadgeAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group__6__Impl"


    // $ANTLR start "rule__Badge__Group__7"
    // InternalDSL.g:7129:1: rule__Badge__Group__7 : rule__Badge__Group__7__Impl rule__Badge__Group__8 ;
    public final void rule__Badge__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7133:1: ( rule__Badge__Group__7__Impl rule__Badge__Group__8 )
            // InternalDSL.g:7134:2: rule__Badge__Group__7__Impl rule__Badge__Group__8
            {
            pushFollow(FOLLOW_45);
            rule__Badge__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badge__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group__7"


    // $ANTLR start "rule__Badge__Group__7__Impl"
    // InternalDSL.g:7141:1: rule__Badge__Group__7__Impl : ( ( rule__Badge__Group_7__0 )? ) ;
    public final void rule__Badge__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7145:1: ( ( ( rule__Badge__Group_7__0 )? ) )
            // InternalDSL.g:7146:1: ( ( rule__Badge__Group_7__0 )? )
            {
            // InternalDSL.g:7146:1: ( ( rule__Badge__Group_7__0 )? )
            // InternalDSL.g:7147:2: ( rule__Badge__Group_7__0 )?
            {
             before(grammarAccess.getBadgeAccess().getGroup_7()); 
            // InternalDSL.g:7148:2: ( rule__Badge__Group_7__0 )?
            int alt66=2;
            int LA66_0 = input.LA(1);

            if ( (LA66_0==65) ) {
                alt66=1;
            }
            switch (alt66) {
                case 1 :
                    // InternalDSL.g:7148:3: rule__Badge__Group_7__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Badge__Group_7__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBadgeAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group__7__Impl"


    // $ANTLR start "rule__Badge__Group__8"
    // InternalDSL.g:7156:1: rule__Badge__Group__8 : rule__Badge__Group__8__Impl ;
    public final void rule__Badge__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7160:1: ( rule__Badge__Group__8__Impl )
            // InternalDSL.g:7161:2: rule__Badge__Group__8__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Badge__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group__8"


    // $ANTLR start "rule__Badge__Group__8__Impl"
    // InternalDSL.g:7167:1: rule__Badge__Group__8__Impl : ( '}' ) ;
    public final void rule__Badge__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7171:1: ( ( '}' ) )
            // InternalDSL.g:7172:1: ( '}' )
            {
            // InternalDSL.g:7172:1: ( '}' )
            // InternalDSL.g:7173:2: '}'
            {
             before(grammarAccess.getBadgeAccess().getRightCurlyBracketKeyword_8()); 
            match(input,60,FOLLOW_2); 
             after(grammarAccess.getBadgeAccess().getRightCurlyBracketKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group__8__Impl"


    // $ANTLR start "rule__Badge__Group_3__0"
    // InternalDSL.g:7183:1: rule__Badge__Group_3__0 : rule__Badge__Group_3__0__Impl rule__Badge__Group_3__1 ;
    public final void rule__Badge__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7187:1: ( rule__Badge__Group_3__0__Impl rule__Badge__Group_3__1 )
            // InternalDSL.g:7188:2: rule__Badge__Group_3__0__Impl rule__Badge__Group_3__1
            {
            pushFollow(FOLLOW_11);
            rule__Badge__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badge__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group_3__0"


    // $ANTLR start "rule__Badge__Group_3__0__Impl"
    // InternalDSL.g:7195:1: rule__Badge__Group_3__0__Impl : ( 'Name:' ) ;
    public final void rule__Badge__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7199:1: ( ( 'Name:' ) )
            // InternalDSL.g:7200:1: ( 'Name:' )
            {
            // InternalDSL.g:7200:1: ( 'Name:' )
            // InternalDSL.g:7201:2: 'Name:'
            {
             before(grammarAccess.getBadgeAccess().getNameKeyword_3_0()); 
            match(input,62,FOLLOW_2); 
             after(grammarAccess.getBadgeAccess().getNameKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group_3__0__Impl"


    // $ANTLR start "rule__Badge__Group_3__1"
    // InternalDSL.g:7210:1: rule__Badge__Group_3__1 : rule__Badge__Group_3__1__Impl ;
    public final void rule__Badge__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7214:1: ( rule__Badge__Group_3__1__Impl )
            // InternalDSL.g:7215:2: rule__Badge__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Badge__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group_3__1"


    // $ANTLR start "rule__Badge__Group_3__1__Impl"
    // InternalDSL.g:7221:1: rule__Badge__Group_3__1__Impl : ( ( rule__Badge__NameAssignment_3_1 ) ) ;
    public final void rule__Badge__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7225:1: ( ( ( rule__Badge__NameAssignment_3_1 ) ) )
            // InternalDSL.g:7226:1: ( ( rule__Badge__NameAssignment_3_1 ) )
            {
            // InternalDSL.g:7226:1: ( ( rule__Badge__NameAssignment_3_1 ) )
            // InternalDSL.g:7227:2: ( rule__Badge__NameAssignment_3_1 )
            {
             before(grammarAccess.getBadgeAccess().getNameAssignment_3_1()); 
            // InternalDSL.g:7228:2: ( rule__Badge__NameAssignment_3_1 )
            // InternalDSL.g:7228:3: rule__Badge__NameAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Badge__NameAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getBadgeAccess().getNameAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group_3__1__Impl"


    // $ANTLR start "rule__Badge__Group_4__0"
    // InternalDSL.g:7237:1: rule__Badge__Group_4__0 : rule__Badge__Group_4__0__Impl rule__Badge__Group_4__1 ;
    public final void rule__Badge__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7241:1: ( rule__Badge__Group_4__0__Impl rule__Badge__Group_4__1 )
            // InternalDSL.g:7242:2: rule__Badge__Group_4__0__Impl rule__Badge__Group_4__1
            {
            pushFollow(FOLLOW_12);
            rule__Badge__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badge__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group_4__0"


    // $ANTLR start "rule__Badge__Group_4__0__Impl"
    // InternalDSL.g:7249:1: rule__Badge__Group_4__0__Impl : ( 'RewardDescription:' ) ;
    public final void rule__Badge__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7253:1: ( ( 'RewardDescription:' ) )
            // InternalDSL.g:7254:1: ( 'RewardDescription:' )
            {
            // InternalDSL.g:7254:1: ( 'RewardDescription:' )
            // InternalDSL.g:7255:2: 'RewardDescription:'
            {
             before(grammarAccess.getBadgeAccess().getRewardDescriptionKeyword_4_0()); 
            match(input,103,FOLLOW_2); 
             after(grammarAccess.getBadgeAccess().getRewardDescriptionKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group_4__0__Impl"


    // $ANTLR start "rule__Badge__Group_4__1"
    // InternalDSL.g:7264:1: rule__Badge__Group_4__1 : rule__Badge__Group_4__1__Impl ;
    public final void rule__Badge__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7268:1: ( rule__Badge__Group_4__1__Impl )
            // InternalDSL.g:7269:2: rule__Badge__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Badge__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group_4__1"


    // $ANTLR start "rule__Badge__Group_4__1__Impl"
    // InternalDSL.g:7275:1: rule__Badge__Group_4__1__Impl : ( ( rule__Badge__RewardDescriptionAssignment_4_1 ) ) ;
    public final void rule__Badge__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7279:1: ( ( ( rule__Badge__RewardDescriptionAssignment_4_1 ) ) )
            // InternalDSL.g:7280:1: ( ( rule__Badge__RewardDescriptionAssignment_4_1 ) )
            {
            // InternalDSL.g:7280:1: ( ( rule__Badge__RewardDescriptionAssignment_4_1 ) )
            // InternalDSL.g:7281:2: ( rule__Badge__RewardDescriptionAssignment_4_1 )
            {
             before(grammarAccess.getBadgeAccess().getRewardDescriptionAssignment_4_1()); 
            // InternalDSL.g:7282:2: ( rule__Badge__RewardDescriptionAssignment_4_1 )
            // InternalDSL.g:7282:3: rule__Badge__RewardDescriptionAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Badge__RewardDescriptionAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getBadgeAccess().getRewardDescriptionAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group_4__1__Impl"


    // $ANTLR start "rule__Badge__Group_5__0"
    // InternalDSL.g:7291:1: rule__Badge__Group_5__0 : rule__Badge__Group_5__0__Impl rule__Badge__Group_5__1 ;
    public final void rule__Badge__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7295:1: ( rule__Badge__Group_5__0__Impl rule__Badge__Group_5__1 )
            // InternalDSL.g:7296:2: rule__Badge__Group_5__0__Impl rule__Badge__Group_5__1
            {
            pushFollow(FOLLOW_34);
            rule__Badge__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badge__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group_5__0"


    // $ANTLR start "rule__Badge__Group_5__0__Impl"
    // InternalDSL.g:7303:1: rule__Badge__Group_5__0__Impl : ( 'BadgeLevel:' ) ;
    public final void rule__Badge__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7307:1: ( ( 'BadgeLevel:' ) )
            // InternalDSL.g:7308:1: ( 'BadgeLevel:' )
            {
            // InternalDSL.g:7308:1: ( 'BadgeLevel:' )
            // InternalDSL.g:7309:2: 'BadgeLevel:'
            {
             before(grammarAccess.getBadgeAccess().getBadgeLevelKeyword_5_0()); 
            match(input,104,FOLLOW_2); 
             after(grammarAccess.getBadgeAccess().getBadgeLevelKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group_5__0__Impl"


    // $ANTLR start "rule__Badge__Group_5__1"
    // InternalDSL.g:7318:1: rule__Badge__Group_5__1 : rule__Badge__Group_5__1__Impl ;
    public final void rule__Badge__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7322:1: ( rule__Badge__Group_5__1__Impl )
            // InternalDSL.g:7323:2: rule__Badge__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Badge__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group_5__1"


    // $ANTLR start "rule__Badge__Group_5__1__Impl"
    // InternalDSL.g:7329:1: rule__Badge__Group_5__1__Impl : ( ( rule__Badge__BadgeLevelAssignment_5_1 ) ) ;
    public final void rule__Badge__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7333:1: ( ( ( rule__Badge__BadgeLevelAssignment_5_1 ) ) )
            // InternalDSL.g:7334:1: ( ( rule__Badge__BadgeLevelAssignment_5_1 ) )
            {
            // InternalDSL.g:7334:1: ( ( rule__Badge__BadgeLevelAssignment_5_1 ) )
            // InternalDSL.g:7335:2: ( rule__Badge__BadgeLevelAssignment_5_1 )
            {
             before(grammarAccess.getBadgeAccess().getBadgeLevelAssignment_5_1()); 
            // InternalDSL.g:7336:2: ( rule__Badge__BadgeLevelAssignment_5_1 )
            // InternalDSL.g:7336:3: rule__Badge__BadgeLevelAssignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__Badge__BadgeLevelAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getBadgeAccess().getBadgeLevelAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group_5__1__Impl"


    // $ANTLR start "rule__Badge__Group_6__0"
    // InternalDSL.g:7345:1: rule__Badge__Group_6__0 : rule__Badge__Group_6__0__Impl rule__Badge__Group_6__1 ;
    public final void rule__Badge__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7349:1: ( rule__Badge__Group_6__0__Impl rule__Badge__Group_6__1 )
            // InternalDSL.g:7350:2: rule__Badge__Group_6__0__Impl rule__Badge__Group_6__1
            {
            pushFollow(FOLLOW_12);
            rule__Badge__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badge__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group_6__0"


    // $ANTLR start "rule__Badge__Group_6__0__Impl"
    // InternalDSL.g:7357:1: rule__Badge__Group_6__0__Impl : ( 'BadgeImage:' ) ;
    public final void rule__Badge__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7361:1: ( ( 'BadgeImage:' ) )
            // InternalDSL.g:7362:1: ( 'BadgeImage:' )
            {
            // InternalDSL.g:7362:1: ( 'BadgeImage:' )
            // InternalDSL.g:7363:2: 'BadgeImage:'
            {
             before(grammarAccess.getBadgeAccess().getBadgeImageKeyword_6_0()); 
            match(input,105,FOLLOW_2); 
             after(grammarAccess.getBadgeAccess().getBadgeImageKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group_6__0__Impl"


    // $ANTLR start "rule__Badge__Group_6__1"
    // InternalDSL.g:7372:1: rule__Badge__Group_6__1 : rule__Badge__Group_6__1__Impl ;
    public final void rule__Badge__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7376:1: ( rule__Badge__Group_6__1__Impl )
            // InternalDSL.g:7377:2: rule__Badge__Group_6__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Badge__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group_6__1"


    // $ANTLR start "rule__Badge__Group_6__1__Impl"
    // InternalDSL.g:7383:1: rule__Badge__Group_6__1__Impl : ( ( rule__Badge__BadgeImageAssignment_6_1 ) ) ;
    public final void rule__Badge__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7387:1: ( ( ( rule__Badge__BadgeImageAssignment_6_1 ) ) )
            // InternalDSL.g:7388:1: ( ( rule__Badge__BadgeImageAssignment_6_1 ) )
            {
            // InternalDSL.g:7388:1: ( ( rule__Badge__BadgeImageAssignment_6_1 ) )
            // InternalDSL.g:7389:2: ( rule__Badge__BadgeImageAssignment_6_1 )
            {
             before(grammarAccess.getBadgeAccess().getBadgeImageAssignment_6_1()); 
            // InternalDSL.g:7390:2: ( rule__Badge__BadgeImageAssignment_6_1 )
            // InternalDSL.g:7390:3: rule__Badge__BadgeImageAssignment_6_1
            {
            pushFollow(FOLLOW_2);
            rule__Badge__BadgeImageAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getBadgeAccess().getBadgeImageAssignment_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group_6__1__Impl"


    // $ANTLR start "rule__Badge__Group_7__0"
    // InternalDSL.g:7399:1: rule__Badge__Group_7__0 : rule__Badge__Group_7__0__Impl rule__Badge__Group_7__1 ;
    public final void rule__Badge__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7403:1: ( rule__Badge__Group_7__0__Impl rule__Badge__Group_7__1 )
            // InternalDSL.g:7404:2: rule__Badge__Group_7__0__Impl rule__Badge__Group_7__1
            {
            pushFollow(FOLLOW_12);
            rule__Badge__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Badge__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group_7__0"


    // $ANTLR start "rule__Badge__Group_7__0__Impl"
    // InternalDSL.g:7411:1: rule__Badge__Group_7__0__Impl : ( 'Information:' ) ;
    public final void rule__Badge__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7415:1: ( ( 'Information:' ) )
            // InternalDSL.g:7416:1: ( 'Information:' )
            {
            // InternalDSL.g:7416:1: ( 'Information:' )
            // InternalDSL.g:7417:2: 'Information:'
            {
             before(grammarAccess.getBadgeAccess().getInformationKeyword_7_0()); 
            match(input,65,FOLLOW_2); 
             after(grammarAccess.getBadgeAccess().getInformationKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group_7__0__Impl"


    // $ANTLR start "rule__Badge__Group_7__1"
    // InternalDSL.g:7426:1: rule__Badge__Group_7__1 : rule__Badge__Group_7__1__Impl ;
    public final void rule__Badge__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7430:1: ( rule__Badge__Group_7__1__Impl )
            // InternalDSL.g:7431:2: rule__Badge__Group_7__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Badge__Group_7__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group_7__1"


    // $ANTLR start "rule__Badge__Group_7__1__Impl"
    // InternalDSL.g:7437:1: rule__Badge__Group_7__1__Impl : ( ( rule__Badge__InformationAssignment_7_1 ) ) ;
    public final void rule__Badge__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7441:1: ( ( ( rule__Badge__InformationAssignment_7_1 ) ) )
            // InternalDSL.g:7442:1: ( ( rule__Badge__InformationAssignment_7_1 ) )
            {
            // InternalDSL.g:7442:1: ( ( rule__Badge__InformationAssignment_7_1 ) )
            // InternalDSL.g:7443:2: ( rule__Badge__InformationAssignment_7_1 )
            {
             before(grammarAccess.getBadgeAccess().getInformationAssignment_7_1()); 
            // InternalDSL.g:7444:2: ( rule__Badge__InformationAssignment_7_1 )
            // InternalDSL.g:7444:3: rule__Badge__InformationAssignment_7_1
            {
            pushFollow(FOLLOW_2);
            rule__Badge__InformationAssignment_7_1();

            state._fsp--;


            }

             after(grammarAccess.getBadgeAccess().getInformationAssignment_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__Group_7__1__Impl"


    // $ANTLR start "rule__Points__Group__0"
    // InternalDSL.g:7453:1: rule__Points__Group__0 : rule__Points__Group__0__Impl rule__Points__Group__1 ;
    public final void rule__Points__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7457:1: ( rule__Points__Group__0__Impl rule__Points__Group__1 )
            // InternalDSL.g:7458:2: rule__Points__Group__0__Impl rule__Points__Group__1
            {
            pushFollow(FOLLOW_46);
            rule__Points__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Points__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group__0"


    // $ANTLR start "rule__Points__Group__0__Impl"
    // InternalDSL.g:7465:1: rule__Points__Group__0__Impl : ( () ) ;
    public final void rule__Points__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7469:1: ( ( () ) )
            // InternalDSL.g:7470:1: ( () )
            {
            // InternalDSL.g:7470:1: ( () )
            // InternalDSL.g:7471:2: ()
            {
             before(grammarAccess.getPointsAccess().getPointsAction_0()); 
            // InternalDSL.g:7472:2: ()
            // InternalDSL.g:7472:3: 
            {
            }

             after(grammarAccess.getPointsAccess().getPointsAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group__0__Impl"


    // $ANTLR start "rule__Points__Group__1"
    // InternalDSL.g:7480:1: rule__Points__Group__1 : rule__Points__Group__1__Impl rule__Points__Group__2 ;
    public final void rule__Points__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7484:1: ( rule__Points__Group__1__Impl rule__Points__Group__2 )
            // InternalDSL.g:7485:2: rule__Points__Group__1__Impl rule__Points__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__Points__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Points__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group__1"


    // $ANTLR start "rule__Points__Group__1__Impl"
    // InternalDSL.g:7492:1: rule__Points__Group__1__Impl : ( 'Points' ) ;
    public final void rule__Points__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7496:1: ( ( 'Points' ) )
            // InternalDSL.g:7497:1: ( 'Points' )
            {
            // InternalDSL.g:7497:1: ( 'Points' )
            // InternalDSL.g:7498:2: 'Points'
            {
             before(grammarAccess.getPointsAccess().getPointsKeyword_1()); 
            match(input,106,FOLLOW_2); 
             after(grammarAccess.getPointsAccess().getPointsKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group__1__Impl"


    // $ANTLR start "rule__Points__Group__2"
    // InternalDSL.g:7507:1: rule__Points__Group__2 : rule__Points__Group__2__Impl rule__Points__Group__3 ;
    public final void rule__Points__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7511:1: ( rule__Points__Group__2__Impl rule__Points__Group__3 )
            // InternalDSL.g:7512:2: rule__Points__Group__2__Impl rule__Points__Group__3
            {
            pushFollow(FOLLOW_47);
            rule__Points__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Points__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group__2"


    // $ANTLR start "rule__Points__Group__2__Impl"
    // InternalDSL.g:7519:1: rule__Points__Group__2__Impl : ( '{' ) ;
    public final void rule__Points__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7523:1: ( ( '{' ) )
            // InternalDSL.g:7524:1: ( '{' )
            {
            // InternalDSL.g:7524:1: ( '{' )
            // InternalDSL.g:7525:2: '{'
            {
             before(grammarAccess.getPointsAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,58,FOLLOW_2); 
             after(grammarAccess.getPointsAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group__2__Impl"


    // $ANTLR start "rule__Points__Group__3"
    // InternalDSL.g:7534:1: rule__Points__Group__3 : rule__Points__Group__3__Impl rule__Points__Group__4 ;
    public final void rule__Points__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7538:1: ( rule__Points__Group__3__Impl rule__Points__Group__4 )
            // InternalDSL.g:7539:2: rule__Points__Group__3__Impl rule__Points__Group__4
            {
            pushFollow(FOLLOW_47);
            rule__Points__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Points__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group__3"


    // $ANTLR start "rule__Points__Group__3__Impl"
    // InternalDSL.g:7546:1: rule__Points__Group__3__Impl : ( ( rule__Points__Group_3__0 )? ) ;
    public final void rule__Points__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7550:1: ( ( ( rule__Points__Group_3__0 )? ) )
            // InternalDSL.g:7551:1: ( ( rule__Points__Group_3__0 )? )
            {
            // InternalDSL.g:7551:1: ( ( rule__Points__Group_3__0 )? )
            // InternalDSL.g:7552:2: ( rule__Points__Group_3__0 )?
            {
             before(grammarAccess.getPointsAccess().getGroup_3()); 
            // InternalDSL.g:7553:2: ( rule__Points__Group_3__0 )?
            int alt67=2;
            int LA67_0 = input.LA(1);

            if ( (LA67_0==62) ) {
                alt67=1;
            }
            switch (alt67) {
                case 1 :
                    // InternalDSL.g:7553:3: rule__Points__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Points__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPointsAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group__3__Impl"


    // $ANTLR start "rule__Points__Group__4"
    // InternalDSL.g:7561:1: rule__Points__Group__4 : rule__Points__Group__4__Impl rule__Points__Group__5 ;
    public final void rule__Points__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7565:1: ( rule__Points__Group__4__Impl rule__Points__Group__5 )
            // InternalDSL.g:7566:2: rule__Points__Group__4__Impl rule__Points__Group__5
            {
            pushFollow(FOLLOW_47);
            rule__Points__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Points__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group__4"


    // $ANTLR start "rule__Points__Group__4__Impl"
    // InternalDSL.g:7573:1: rule__Points__Group__4__Impl : ( ( rule__Points__Group_4__0 )? ) ;
    public final void rule__Points__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7577:1: ( ( ( rule__Points__Group_4__0 )? ) )
            // InternalDSL.g:7578:1: ( ( rule__Points__Group_4__0 )? )
            {
            // InternalDSL.g:7578:1: ( ( rule__Points__Group_4__0 )? )
            // InternalDSL.g:7579:2: ( rule__Points__Group_4__0 )?
            {
             before(grammarAccess.getPointsAccess().getGroup_4()); 
            // InternalDSL.g:7580:2: ( rule__Points__Group_4__0 )?
            int alt68=2;
            int LA68_0 = input.LA(1);

            if ( (LA68_0==103) ) {
                alt68=1;
            }
            switch (alt68) {
                case 1 :
                    // InternalDSL.g:7580:3: rule__Points__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Points__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPointsAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group__4__Impl"


    // $ANTLR start "rule__Points__Group__5"
    // InternalDSL.g:7588:1: rule__Points__Group__5 : rule__Points__Group__5__Impl rule__Points__Group__6 ;
    public final void rule__Points__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7592:1: ( rule__Points__Group__5__Impl rule__Points__Group__6 )
            // InternalDSL.g:7593:2: rule__Points__Group__5__Impl rule__Points__Group__6
            {
            pushFollow(FOLLOW_47);
            rule__Points__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Points__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group__5"


    // $ANTLR start "rule__Points__Group__5__Impl"
    // InternalDSL.g:7600:1: rule__Points__Group__5__Impl : ( ( rule__Points__Group_5__0 )? ) ;
    public final void rule__Points__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7604:1: ( ( ( rule__Points__Group_5__0 )? ) )
            // InternalDSL.g:7605:1: ( ( rule__Points__Group_5__0 )? )
            {
            // InternalDSL.g:7605:1: ( ( rule__Points__Group_5__0 )? )
            // InternalDSL.g:7606:2: ( rule__Points__Group_5__0 )?
            {
             before(grammarAccess.getPointsAccess().getGroup_5()); 
            // InternalDSL.g:7607:2: ( rule__Points__Group_5__0 )?
            int alt69=2;
            int LA69_0 = input.LA(1);

            if ( (LA69_0==96) ) {
                alt69=1;
            }
            switch (alt69) {
                case 1 :
                    // InternalDSL.g:7607:3: rule__Points__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Points__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPointsAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group__5__Impl"


    // $ANTLR start "rule__Points__Group__6"
    // InternalDSL.g:7615:1: rule__Points__Group__6 : rule__Points__Group__6__Impl rule__Points__Group__7 ;
    public final void rule__Points__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7619:1: ( rule__Points__Group__6__Impl rule__Points__Group__7 )
            // InternalDSL.g:7620:2: rule__Points__Group__6__Impl rule__Points__Group__7
            {
            pushFollow(FOLLOW_47);
            rule__Points__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Points__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group__6"


    // $ANTLR start "rule__Points__Group__6__Impl"
    // InternalDSL.g:7627:1: rule__Points__Group__6__Impl : ( ( rule__Points__Group_6__0 )? ) ;
    public final void rule__Points__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7631:1: ( ( ( rule__Points__Group_6__0 )? ) )
            // InternalDSL.g:7632:1: ( ( rule__Points__Group_6__0 )? )
            {
            // InternalDSL.g:7632:1: ( ( rule__Points__Group_6__0 )? )
            // InternalDSL.g:7633:2: ( rule__Points__Group_6__0 )?
            {
             before(grammarAccess.getPointsAccess().getGroup_6()); 
            // InternalDSL.g:7634:2: ( rule__Points__Group_6__0 )?
            int alt70=2;
            int LA70_0 = input.LA(1);

            if ( (LA70_0==107) ) {
                alt70=1;
            }
            switch (alt70) {
                case 1 :
                    // InternalDSL.g:7634:3: rule__Points__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Points__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPointsAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group__6__Impl"


    // $ANTLR start "rule__Points__Group__7"
    // InternalDSL.g:7642:1: rule__Points__Group__7 : rule__Points__Group__7__Impl rule__Points__Group__8 ;
    public final void rule__Points__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7646:1: ( rule__Points__Group__7__Impl rule__Points__Group__8 )
            // InternalDSL.g:7647:2: rule__Points__Group__7__Impl rule__Points__Group__8
            {
            pushFollow(FOLLOW_47);
            rule__Points__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Points__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group__7"


    // $ANTLR start "rule__Points__Group__7__Impl"
    // InternalDSL.g:7654:1: rule__Points__Group__7__Impl : ( ( rule__Points__Group_7__0 )? ) ;
    public final void rule__Points__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7658:1: ( ( ( rule__Points__Group_7__0 )? ) )
            // InternalDSL.g:7659:1: ( ( rule__Points__Group_7__0 )? )
            {
            // InternalDSL.g:7659:1: ( ( rule__Points__Group_7__0 )? )
            // InternalDSL.g:7660:2: ( rule__Points__Group_7__0 )?
            {
             before(grammarAccess.getPointsAccess().getGroup_7()); 
            // InternalDSL.g:7661:2: ( rule__Points__Group_7__0 )?
            int alt71=2;
            int LA71_0 = input.LA(1);

            if ( (LA71_0==78) ) {
                alt71=1;
            }
            switch (alt71) {
                case 1 :
                    // InternalDSL.g:7661:3: rule__Points__Group_7__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Points__Group_7__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPointsAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group__7__Impl"


    // $ANTLR start "rule__Points__Group__8"
    // InternalDSL.g:7669:1: rule__Points__Group__8 : rule__Points__Group__8__Impl rule__Points__Group__9 ;
    public final void rule__Points__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7673:1: ( rule__Points__Group__8__Impl rule__Points__Group__9 )
            // InternalDSL.g:7674:2: rule__Points__Group__8__Impl rule__Points__Group__9
            {
            pushFollow(FOLLOW_47);
            rule__Points__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Points__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group__8"


    // $ANTLR start "rule__Points__Group__8__Impl"
    // InternalDSL.g:7681:1: rule__Points__Group__8__Impl : ( ( rule__Points__Group_8__0 )? ) ;
    public final void rule__Points__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7685:1: ( ( ( rule__Points__Group_8__0 )? ) )
            // InternalDSL.g:7686:1: ( ( rule__Points__Group_8__0 )? )
            {
            // InternalDSL.g:7686:1: ( ( rule__Points__Group_8__0 )? )
            // InternalDSL.g:7687:2: ( rule__Points__Group_8__0 )?
            {
             before(grammarAccess.getPointsAccess().getGroup_8()); 
            // InternalDSL.g:7688:2: ( rule__Points__Group_8__0 )?
            int alt72=2;
            int LA72_0 = input.LA(1);

            if ( (LA72_0==65) ) {
                alt72=1;
            }
            switch (alt72) {
                case 1 :
                    // InternalDSL.g:7688:3: rule__Points__Group_8__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Points__Group_8__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPointsAccess().getGroup_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group__8__Impl"


    // $ANTLR start "rule__Points__Group__9"
    // InternalDSL.g:7696:1: rule__Points__Group__9 : rule__Points__Group__9__Impl ;
    public final void rule__Points__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7700:1: ( rule__Points__Group__9__Impl )
            // InternalDSL.g:7701:2: rule__Points__Group__9__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Points__Group__9__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group__9"


    // $ANTLR start "rule__Points__Group__9__Impl"
    // InternalDSL.g:7707:1: rule__Points__Group__9__Impl : ( '}' ) ;
    public final void rule__Points__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7711:1: ( ( '}' ) )
            // InternalDSL.g:7712:1: ( '}' )
            {
            // InternalDSL.g:7712:1: ( '}' )
            // InternalDSL.g:7713:2: '}'
            {
             before(grammarAccess.getPointsAccess().getRightCurlyBracketKeyword_9()); 
            match(input,60,FOLLOW_2); 
             after(grammarAccess.getPointsAccess().getRightCurlyBracketKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group__9__Impl"


    // $ANTLR start "rule__Points__Group_3__0"
    // InternalDSL.g:7723:1: rule__Points__Group_3__0 : rule__Points__Group_3__0__Impl rule__Points__Group_3__1 ;
    public final void rule__Points__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7727:1: ( rule__Points__Group_3__0__Impl rule__Points__Group_3__1 )
            // InternalDSL.g:7728:2: rule__Points__Group_3__0__Impl rule__Points__Group_3__1
            {
            pushFollow(FOLLOW_11);
            rule__Points__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Points__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_3__0"


    // $ANTLR start "rule__Points__Group_3__0__Impl"
    // InternalDSL.g:7735:1: rule__Points__Group_3__0__Impl : ( 'Name:' ) ;
    public final void rule__Points__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7739:1: ( ( 'Name:' ) )
            // InternalDSL.g:7740:1: ( 'Name:' )
            {
            // InternalDSL.g:7740:1: ( 'Name:' )
            // InternalDSL.g:7741:2: 'Name:'
            {
             before(grammarAccess.getPointsAccess().getNameKeyword_3_0()); 
            match(input,62,FOLLOW_2); 
             after(grammarAccess.getPointsAccess().getNameKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_3__0__Impl"


    // $ANTLR start "rule__Points__Group_3__1"
    // InternalDSL.g:7750:1: rule__Points__Group_3__1 : rule__Points__Group_3__1__Impl ;
    public final void rule__Points__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7754:1: ( rule__Points__Group_3__1__Impl )
            // InternalDSL.g:7755:2: rule__Points__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Points__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_3__1"


    // $ANTLR start "rule__Points__Group_3__1__Impl"
    // InternalDSL.g:7761:1: rule__Points__Group_3__1__Impl : ( ( rule__Points__NameAssignment_3_1 ) ) ;
    public final void rule__Points__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7765:1: ( ( ( rule__Points__NameAssignment_3_1 ) ) )
            // InternalDSL.g:7766:1: ( ( rule__Points__NameAssignment_3_1 ) )
            {
            // InternalDSL.g:7766:1: ( ( rule__Points__NameAssignment_3_1 ) )
            // InternalDSL.g:7767:2: ( rule__Points__NameAssignment_3_1 )
            {
             before(grammarAccess.getPointsAccess().getNameAssignment_3_1()); 
            // InternalDSL.g:7768:2: ( rule__Points__NameAssignment_3_1 )
            // InternalDSL.g:7768:3: rule__Points__NameAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Points__NameAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getPointsAccess().getNameAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_3__1__Impl"


    // $ANTLR start "rule__Points__Group_4__0"
    // InternalDSL.g:7777:1: rule__Points__Group_4__0 : rule__Points__Group_4__0__Impl rule__Points__Group_4__1 ;
    public final void rule__Points__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7781:1: ( rule__Points__Group_4__0__Impl rule__Points__Group_4__1 )
            // InternalDSL.g:7782:2: rule__Points__Group_4__0__Impl rule__Points__Group_4__1
            {
            pushFollow(FOLLOW_12);
            rule__Points__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Points__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_4__0"


    // $ANTLR start "rule__Points__Group_4__0__Impl"
    // InternalDSL.g:7789:1: rule__Points__Group_4__0__Impl : ( 'RewardDescription:' ) ;
    public final void rule__Points__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7793:1: ( ( 'RewardDescription:' ) )
            // InternalDSL.g:7794:1: ( 'RewardDescription:' )
            {
            // InternalDSL.g:7794:1: ( 'RewardDescription:' )
            // InternalDSL.g:7795:2: 'RewardDescription:'
            {
             before(grammarAccess.getPointsAccess().getRewardDescriptionKeyword_4_0()); 
            match(input,103,FOLLOW_2); 
             after(grammarAccess.getPointsAccess().getRewardDescriptionKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_4__0__Impl"


    // $ANTLR start "rule__Points__Group_4__1"
    // InternalDSL.g:7804:1: rule__Points__Group_4__1 : rule__Points__Group_4__1__Impl ;
    public final void rule__Points__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7808:1: ( rule__Points__Group_4__1__Impl )
            // InternalDSL.g:7809:2: rule__Points__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Points__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_4__1"


    // $ANTLR start "rule__Points__Group_4__1__Impl"
    // InternalDSL.g:7815:1: rule__Points__Group_4__1__Impl : ( ( rule__Points__RewardDescriptionAssignment_4_1 ) ) ;
    public final void rule__Points__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7819:1: ( ( ( rule__Points__RewardDescriptionAssignment_4_1 ) ) )
            // InternalDSL.g:7820:1: ( ( rule__Points__RewardDescriptionAssignment_4_1 ) )
            {
            // InternalDSL.g:7820:1: ( ( rule__Points__RewardDescriptionAssignment_4_1 ) )
            // InternalDSL.g:7821:2: ( rule__Points__RewardDescriptionAssignment_4_1 )
            {
             before(grammarAccess.getPointsAccess().getRewardDescriptionAssignment_4_1()); 
            // InternalDSL.g:7822:2: ( rule__Points__RewardDescriptionAssignment_4_1 )
            // InternalDSL.g:7822:3: rule__Points__RewardDescriptionAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Points__RewardDescriptionAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getPointsAccess().getRewardDescriptionAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_4__1__Impl"


    // $ANTLR start "rule__Points__Group_5__0"
    // InternalDSL.g:7831:1: rule__Points__Group_5__0 : rule__Points__Group_5__0__Impl rule__Points__Group_5__1 ;
    public final void rule__Points__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7835:1: ( rule__Points__Group_5__0__Impl rule__Points__Group_5__1 )
            // InternalDSL.g:7836:2: rule__Points__Group_5__0__Impl rule__Points__Group_5__1
            {
            pushFollow(FOLLOW_34);
            rule__Points__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Points__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_5__0"


    // $ANTLR start "rule__Points__Group_5__0__Impl"
    // InternalDSL.g:7843:1: rule__Points__Group_5__0__Impl : ( 'Amount:' ) ;
    public final void rule__Points__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7847:1: ( ( 'Amount:' ) )
            // InternalDSL.g:7848:1: ( 'Amount:' )
            {
            // InternalDSL.g:7848:1: ( 'Amount:' )
            // InternalDSL.g:7849:2: 'Amount:'
            {
             before(grammarAccess.getPointsAccess().getAmountKeyword_5_0()); 
            match(input,96,FOLLOW_2); 
             after(grammarAccess.getPointsAccess().getAmountKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_5__0__Impl"


    // $ANTLR start "rule__Points__Group_5__1"
    // InternalDSL.g:7858:1: rule__Points__Group_5__1 : rule__Points__Group_5__1__Impl ;
    public final void rule__Points__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7862:1: ( rule__Points__Group_5__1__Impl )
            // InternalDSL.g:7863:2: rule__Points__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Points__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_5__1"


    // $ANTLR start "rule__Points__Group_5__1__Impl"
    // InternalDSL.g:7869:1: rule__Points__Group_5__1__Impl : ( ( rule__Points__AmountAssignment_5_1 ) ) ;
    public final void rule__Points__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7873:1: ( ( ( rule__Points__AmountAssignment_5_1 ) ) )
            // InternalDSL.g:7874:1: ( ( rule__Points__AmountAssignment_5_1 ) )
            {
            // InternalDSL.g:7874:1: ( ( rule__Points__AmountAssignment_5_1 ) )
            // InternalDSL.g:7875:2: ( rule__Points__AmountAssignment_5_1 )
            {
             before(grammarAccess.getPointsAccess().getAmountAssignment_5_1()); 
            // InternalDSL.g:7876:2: ( rule__Points__AmountAssignment_5_1 )
            // InternalDSL.g:7876:3: rule__Points__AmountAssignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__Points__AmountAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getPointsAccess().getAmountAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_5__1__Impl"


    // $ANTLR start "rule__Points__Group_6__0"
    // InternalDSL.g:7885:1: rule__Points__Group_6__0 : rule__Points__Group_6__0__Impl rule__Points__Group_6__1 ;
    public final void rule__Points__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7889:1: ( rule__Points__Group_6__0__Impl rule__Points__Group_6__1 )
            // InternalDSL.g:7890:2: rule__Points__Group_6__0__Impl rule__Points__Group_6__1
            {
            pushFollow(FOLLOW_28);
            rule__Points__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Points__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_6__0"


    // $ANTLR start "rule__Points__Group_6__0__Impl"
    // InternalDSL.g:7897:1: rule__Points__Group_6__0__Impl : ( 'IsCollectible:' ) ;
    public final void rule__Points__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7901:1: ( ( 'IsCollectible:' ) )
            // InternalDSL.g:7902:1: ( 'IsCollectible:' )
            {
            // InternalDSL.g:7902:1: ( 'IsCollectible:' )
            // InternalDSL.g:7903:2: 'IsCollectible:'
            {
             before(grammarAccess.getPointsAccess().getIsCollectibleKeyword_6_0()); 
            match(input,107,FOLLOW_2); 
             after(grammarAccess.getPointsAccess().getIsCollectibleKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_6__0__Impl"


    // $ANTLR start "rule__Points__Group_6__1"
    // InternalDSL.g:7912:1: rule__Points__Group_6__1 : rule__Points__Group_6__1__Impl ;
    public final void rule__Points__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7916:1: ( rule__Points__Group_6__1__Impl )
            // InternalDSL.g:7917:2: rule__Points__Group_6__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Points__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_6__1"


    // $ANTLR start "rule__Points__Group_6__1__Impl"
    // InternalDSL.g:7923:1: rule__Points__Group_6__1__Impl : ( ( rule__Points__IsCollectibleAssignment_6_1 ) ) ;
    public final void rule__Points__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7927:1: ( ( ( rule__Points__IsCollectibleAssignment_6_1 ) ) )
            // InternalDSL.g:7928:1: ( ( rule__Points__IsCollectibleAssignment_6_1 ) )
            {
            // InternalDSL.g:7928:1: ( ( rule__Points__IsCollectibleAssignment_6_1 ) )
            // InternalDSL.g:7929:2: ( rule__Points__IsCollectibleAssignment_6_1 )
            {
             before(grammarAccess.getPointsAccess().getIsCollectibleAssignment_6_1()); 
            // InternalDSL.g:7930:2: ( rule__Points__IsCollectibleAssignment_6_1 )
            // InternalDSL.g:7930:3: rule__Points__IsCollectibleAssignment_6_1
            {
            pushFollow(FOLLOW_2);
            rule__Points__IsCollectibleAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getPointsAccess().getIsCollectibleAssignment_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_6__1__Impl"


    // $ANTLR start "rule__Points__Group_7__0"
    // InternalDSL.g:7939:1: rule__Points__Group_7__0 : rule__Points__Group_7__0__Impl rule__Points__Group_7__1 ;
    public final void rule__Points__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7943:1: ( rule__Points__Group_7__0__Impl rule__Points__Group_7__1 )
            // InternalDSL.g:7944:2: rule__Points__Group_7__0__Impl rule__Points__Group_7__1
            {
            pushFollow(FOLLOW_29);
            rule__Points__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Points__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_7__0"


    // $ANTLR start "rule__Points__Group_7__0__Impl"
    // InternalDSL.g:7951:1: rule__Points__Group_7__0__Impl : ( 'Conditions' ) ;
    public final void rule__Points__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7955:1: ( ( 'Conditions' ) )
            // InternalDSL.g:7956:1: ( 'Conditions' )
            {
            // InternalDSL.g:7956:1: ( 'Conditions' )
            // InternalDSL.g:7957:2: 'Conditions'
            {
             before(grammarAccess.getPointsAccess().getConditionsKeyword_7_0()); 
            match(input,78,FOLLOW_2); 
             after(grammarAccess.getPointsAccess().getConditionsKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_7__0__Impl"


    // $ANTLR start "rule__Points__Group_7__1"
    // InternalDSL.g:7966:1: rule__Points__Group_7__1 : rule__Points__Group_7__1__Impl rule__Points__Group_7__2 ;
    public final void rule__Points__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7970:1: ( rule__Points__Group_7__1__Impl rule__Points__Group_7__2 )
            // InternalDSL.g:7971:2: rule__Points__Group_7__1__Impl rule__Points__Group_7__2
            {
            pushFollow(FOLLOW_11);
            rule__Points__Group_7__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Points__Group_7__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_7__1"


    // $ANTLR start "rule__Points__Group_7__1__Impl"
    // InternalDSL.g:7978:1: rule__Points__Group_7__1__Impl : ( '(' ) ;
    public final void rule__Points__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7982:1: ( ( '(' ) )
            // InternalDSL.g:7983:1: ( '(' )
            {
            // InternalDSL.g:7983:1: ( '(' )
            // InternalDSL.g:7984:2: '('
            {
             before(grammarAccess.getPointsAccess().getLeftParenthesisKeyword_7_1()); 
            match(input,81,FOLLOW_2); 
             after(grammarAccess.getPointsAccess().getLeftParenthesisKeyword_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_7__1__Impl"


    // $ANTLR start "rule__Points__Group_7__2"
    // InternalDSL.g:7993:1: rule__Points__Group_7__2 : rule__Points__Group_7__2__Impl rule__Points__Group_7__3 ;
    public final void rule__Points__Group_7__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:7997:1: ( rule__Points__Group_7__2__Impl rule__Points__Group_7__3 )
            // InternalDSL.g:7998:2: rule__Points__Group_7__2__Impl rule__Points__Group_7__3
            {
            pushFollow(FOLLOW_30);
            rule__Points__Group_7__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Points__Group_7__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_7__2"


    // $ANTLR start "rule__Points__Group_7__2__Impl"
    // InternalDSL.g:8005:1: rule__Points__Group_7__2__Impl : ( ( rule__Points__ConditionsAssignment_7_2 ) ) ;
    public final void rule__Points__Group_7__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8009:1: ( ( ( rule__Points__ConditionsAssignment_7_2 ) ) )
            // InternalDSL.g:8010:1: ( ( rule__Points__ConditionsAssignment_7_2 ) )
            {
            // InternalDSL.g:8010:1: ( ( rule__Points__ConditionsAssignment_7_2 ) )
            // InternalDSL.g:8011:2: ( rule__Points__ConditionsAssignment_7_2 )
            {
             before(grammarAccess.getPointsAccess().getConditionsAssignment_7_2()); 
            // InternalDSL.g:8012:2: ( rule__Points__ConditionsAssignment_7_2 )
            // InternalDSL.g:8012:3: rule__Points__ConditionsAssignment_7_2
            {
            pushFollow(FOLLOW_2);
            rule__Points__ConditionsAssignment_7_2();

            state._fsp--;


            }

             after(grammarAccess.getPointsAccess().getConditionsAssignment_7_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_7__2__Impl"


    // $ANTLR start "rule__Points__Group_7__3"
    // InternalDSL.g:8020:1: rule__Points__Group_7__3 : rule__Points__Group_7__3__Impl rule__Points__Group_7__4 ;
    public final void rule__Points__Group_7__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8024:1: ( rule__Points__Group_7__3__Impl rule__Points__Group_7__4 )
            // InternalDSL.g:8025:2: rule__Points__Group_7__3__Impl rule__Points__Group_7__4
            {
            pushFollow(FOLLOW_30);
            rule__Points__Group_7__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Points__Group_7__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_7__3"


    // $ANTLR start "rule__Points__Group_7__3__Impl"
    // InternalDSL.g:8032:1: rule__Points__Group_7__3__Impl : ( ( rule__Points__Group_7_3__0 )* ) ;
    public final void rule__Points__Group_7__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8036:1: ( ( ( rule__Points__Group_7_3__0 )* ) )
            // InternalDSL.g:8037:1: ( ( rule__Points__Group_7_3__0 )* )
            {
            // InternalDSL.g:8037:1: ( ( rule__Points__Group_7_3__0 )* )
            // InternalDSL.g:8038:2: ( rule__Points__Group_7_3__0 )*
            {
             before(grammarAccess.getPointsAccess().getGroup_7_3()); 
            // InternalDSL.g:8039:2: ( rule__Points__Group_7_3__0 )*
            loop73:
            do {
                int alt73=2;
                int LA73_0 = input.LA(1);

                if ( (LA73_0==64) ) {
                    alt73=1;
                }


                switch (alt73) {
            	case 1 :
            	    // InternalDSL.g:8039:3: rule__Points__Group_7_3__0
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__Points__Group_7_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop73;
                }
            } while (true);

             after(grammarAccess.getPointsAccess().getGroup_7_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_7__3__Impl"


    // $ANTLR start "rule__Points__Group_7__4"
    // InternalDSL.g:8047:1: rule__Points__Group_7__4 : rule__Points__Group_7__4__Impl ;
    public final void rule__Points__Group_7__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8051:1: ( rule__Points__Group_7__4__Impl )
            // InternalDSL.g:8052:2: rule__Points__Group_7__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Points__Group_7__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_7__4"


    // $ANTLR start "rule__Points__Group_7__4__Impl"
    // InternalDSL.g:8058:1: rule__Points__Group_7__4__Impl : ( ')' ) ;
    public final void rule__Points__Group_7__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8062:1: ( ( ')' ) )
            // InternalDSL.g:8063:1: ( ')' )
            {
            // InternalDSL.g:8063:1: ( ')' )
            // InternalDSL.g:8064:2: ')'
            {
             before(grammarAccess.getPointsAccess().getRightParenthesisKeyword_7_4()); 
            match(input,82,FOLLOW_2); 
             after(grammarAccess.getPointsAccess().getRightParenthesisKeyword_7_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_7__4__Impl"


    // $ANTLR start "rule__Points__Group_7_3__0"
    // InternalDSL.g:8074:1: rule__Points__Group_7_3__0 : rule__Points__Group_7_3__0__Impl rule__Points__Group_7_3__1 ;
    public final void rule__Points__Group_7_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8078:1: ( rule__Points__Group_7_3__0__Impl rule__Points__Group_7_3__1 )
            // InternalDSL.g:8079:2: rule__Points__Group_7_3__0__Impl rule__Points__Group_7_3__1
            {
            pushFollow(FOLLOW_11);
            rule__Points__Group_7_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Points__Group_7_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_7_3__0"


    // $ANTLR start "rule__Points__Group_7_3__0__Impl"
    // InternalDSL.g:8086:1: rule__Points__Group_7_3__0__Impl : ( ',' ) ;
    public final void rule__Points__Group_7_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8090:1: ( ( ',' ) )
            // InternalDSL.g:8091:1: ( ',' )
            {
            // InternalDSL.g:8091:1: ( ',' )
            // InternalDSL.g:8092:2: ','
            {
             before(grammarAccess.getPointsAccess().getCommaKeyword_7_3_0()); 
            match(input,64,FOLLOW_2); 
             after(grammarAccess.getPointsAccess().getCommaKeyword_7_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_7_3__0__Impl"


    // $ANTLR start "rule__Points__Group_7_3__1"
    // InternalDSL.g:8101:1: rule__Points__Group_7_3__1 : rule__Points__Group_7_3__1__Impl ;
    public final void rule__Points__Group_7_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8105:1: ( rule__Points__Group_7_3__1__Impl )
            // InternalDSL.g:8106:2: rule__Points__Group_7_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Points__Group_7_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_7_3__1"


    // $ANTLR start "rule__Points__Group_7_3__1__Impl"
    // InternalDSL.g:8112:1: rule__Points__Group_7_3__1__Impl : ( ( rule__Points__ConditionsAssignment_7_3_1 ) ) ;
    public final void rule__Points__Group_7_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8116:1: ( ( ( rule__Points__ConditionsAssignment_7_3_1 ) ) )
            // InternalDSL.g:8117:1: ( ( rule__Points__ConditionsAssignment_7_3_1 ) )
            {
            // InternalDSL.g:8117:1: ( ( rule__Points__ConditionsAssignment_7_3_1 ) )
            // InternalDSL.g:8118:2: ( rule__Points__ConditionsAssignment_7_3_1 )
            {
             before(grammarAccess.getPointsAccess().getConditionsAssignment_7_3_1()); 
            // InternalDSL.g:8119:2: ( rule__Points__ConditionsAssignment_7_3_1 )
            // InternalDSL.g:8119:3: rule__Points__ConditionsAssignment_7_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Points__ConditionsAssignment_7_3_1();

            state._fsp--;


            }

             after(grammarAccess.getPointsAccess().getConditionsAssignment_7_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_7_3__1__Impl"


    // $ANTLR start "rule__Points__Group_8__0"
    // InternalDSL.g:8128:1: rule__Points__Group_8__0 : rule__Points__Group_8__0__Impl rule__Points__Group_8__1 ;
    public final void rule__Points__Group_8__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8132:1: ( rule__Points__Group_8__0__Impl rule__Points__Group_8__1 )
            // InternalDSL.g:8133:2: rule__Points__Group_8__0__Impl rule__Points__Group_8__1
            {
            pushFollow(FOLLOW_12);
            rule__Points__Group_8__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Points__Group_8__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_8__0"


    // $ANTLR start "rule__Points__Group_8__0__Impl"
    // InternalDSL.g:8140:1: rule__Points__Group_8__0__Impl : ( 'Information:' ) ;
    public final void rule__Points__Group_8__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8144:1: ( ( 'Information:' ) )
            // InternalDSL.g:8145:1: ( 'Information:' )
            {
            // InternalDSL.g:8145:1: ( 'Information:' )
            // InternalDSL.g:8146:2: 'Information:'
            {
             before(grammarAccess.getPointsAccess().getInformationKeyword_8_0()); 
            match(input,65,FOLLOW_2); 
             after(grammarAccess.getPointsAccess().getInformationKeyword_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_8__0__Impl"


    // $ANTLR start "rule__Points__Group_8__1"
    // InternalDSL.g:8155:1: rule__Points__Group_8__1 : rule__Points__Group_8__1__Impl ;
    public final void rule__Points__Group_8__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8159:1: ( rule__Points__Group_8__1__Impl )
            // InternalDSL.g:8160:2: rule__Points__Group_8__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Points__Group_8__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_8__1"


    // $ANTLR start "rule__Points__Group_8__1__Impl"
    // InternalDSL.g:8166:1: rule__Points__Group_8__1__Impl : ( ( rule__Points__InformationAssignment_8_1 ) ) ;
    public final void rule__Points__Group_8__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8170:1: ( ( ( rule__Points__InformationAssignment_8_1 ) ) )
            // InternalDSL.g:8171:1: ( ( rule__Points__InformationAssignment_8_1 ) )
            {
            // InternalDSL.g:8171:1: ( ( rule__Points__InformationAssignment_8_1 ) )
            // InternalDSL.g:8172:2: ( rule__Points__InformationAssignment_8_1 )
            {
             before(grammarAccess.getPointsAccess().getInformationAssignment_8_1()); 
            // InternalDSL.g:8173:2: ( rule__Points__InformationAssignment_8_1 )
            // InternalDSL.g:8173:3: rule__Points__InformationAssignment_8_1
            {
            pushFollow(FOLLOW_2);
            rule__Points__InformationAssignment_8_1();

            state._fsp--;


            }

             after(grammarAccess.getPointsAccess().getInformationAssignment_8_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__Group_8__1__Impl"


    // $ANTLR start "rule__Prize__Group__0"
    // InternalDSL.g:8182:1: rule__Prize__Group__0 : rule__Prize__Group__0__Impl rule__Prize__Group__1 ;
    public final void rule__Prize__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8186:1: ( rule__Prize__Group__0__Impl rule__Prize__Group__1 )
            // InternalDSL.g:8187:2: rule__Prize__Group__0__Impl rule__Prize__Group__1
            {
            pushFollow(FOLLOW_9);
            rule__Prize__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Prize__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group__0"


    // $ANTLR start "rule__Prize__Group__0__Impl"
    // InternalDSL.g:8194:1: rule__Prize__Group__0__Impl : ( () ) ;
    public final void rule__Prize__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8198:1: ( ( () ) )
            // InternalDSL.g:8199:1: ( () )
            {
            // InternalDSL.g:8199:1: ( () )
            // InternalDSL.g:8200:2: ()
            {
             before(grammarAccess.getPrizeAccess().getPrizeAction_0()); 
            // InternalDSL.g:8201:2: ()
            // InternalDSL.g:8201:3: 
            {
            }

             after(grammarAccess.getPrizeAccess().getPrizeAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group__0__Impl"


    // $ANTLR start "rule__Prize__Group__1"
    // InternalDSL.g:8209:1: rule__Prize__Group__1 : rule__Prize__Group__1__Impl rule__Prize__Group__2 ;
    public final void rule__Prize__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8213:1: ( rule__Prize__Group__1__Impl rule__Prize__Group__2 )
            // InternalDSL.g:8214:2: rule__Prize__Group__1__Impl rule__Prize__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__Prize__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Prize__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group__1"


    // $ANTLR start "rule__Prize__Group__1__Impl"
    // InternalDSL.g:8221:1: rule__Prize__Group__1__Impl : ( 'Prize' ) ;
    public final void rule__Prize__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8225:1: ( ( 'Prize' ) )
            // InternalDSL.g:8226:1: ( 'Prize' )
            {
            // InternalDSL.g:8226:1: ( 'Prize' )
            // InternalDSL.g:8227:2: 'Prize'
            {
             before(grammarAccess.getPrizeAccess().getPrizeKeyword_1()); 
            match(input,108,FOLLOW_2); 
             after(grammarAccess.getPrizeAccess().getPrizeKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group__1__Impl"


    // $ANTLR start "rule__Prize__Group__2"
    // InternalDSL.g:8236:1: rule__Prize__Group__2 : rule__Prize__Group__2__Impl rule__Prize__Group__3 ;
    public final void rule__Prize__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8240:1: ( rule__Prize__Group__2__Impl rule__Prize__Group__3 )
            // InternalDSL.g:8241:2: rule__Prize__Group__2__Impl rule__Prize__Group__3
            {
            pushFollow(FOLLOW_48);
            rule__Prize__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Prize__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group__2"


    // $ANTLR start "rule__Prize__Group__2__Impl"
    // InternalDSL.g:8248:1: rule__Prize__Group__2__Impl : ( '{' ) ;
    public final void rule__Prize__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8252:1: ( ( '{' ) )
            // InternalDSL.g:8253:1: ( '{' )
            {
            // InternalDSL.g:8253:1: ( '{' )
            // InternalDSL.g:8254:2: '{'
            {
             before(grammarAccess.getPrizeAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,58,FOLLOW_2); 
             after(grammarAccess.getPrizeAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group__2__Impl"


    // $ANTLR start "rule__Prize__Group__3"
    // InternalDSL.g:8263:1: rule__Prize__Group__3 : rule__Prize__Group__3__Impl rule__Prize__Group__4 ;
    public final void rule__Prize__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8267:1: ( rule__Prize__Group__3__Impl rule__Prize__Group__4 )
            // InternalDSL.g:8268:2: rule__Prize__Group__3__Impl rule__Prize__Group__4
            {
            pushFollow(FOLLOW_48);
            rule__Prize__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Prize__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group__3"


    // $ANTLR start "rule__Prize__Group__3__Impl"
    // InternalDSL.g:8275:1: rule__Prize__Group__3__Impl : ( ( rule__Prize__Group_3__0 )? ) ;
    public final void rule__Prize__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8279:1: ( ( ( rule__Prize__Group_3__0 )? ) )
            // InternalDSL.g:8280:1: ( ( rule__Prize__Group_3__0 )? )
            {
            // InternalDSL.g:8280:1: ( ( rule__Prize__Group_3__0 )? )
            // InternalDSL.g:8281:2: ( rule__Prize__Group_3__0 )?
            {
             before(grammarAccess.getPrizeAccess().getGroup_3()); 
            // InternalDSL.g:8282:2: ( rule__Prize__Group_3__0 )?
            int alt74=2;
            int LA74_0 = input.LA(1);

            if ( (LA74_0==62) ) {
                alt74=1;
            }
            switch (alt74) {
                case 1 :
                    // InternalDSL.g:8282:3: rule__Prize__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Prize__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPrizeAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group__3__Impl"


    // $ANTLR start "rule__Prize__Group__4"
    // InternalDSL.g:8290:1: rule__Prize__Group__4 : rule__Prize__Group__4__Impl rule__Prize__Group__5 ;
    public final void rule__Prize__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8294:1: ( rule__Prize__Group__4__Impl rule__Prize__Group__5 )
            // InternalDSL.g:8295:2: rule__Prize__Group__4__Impl rule__Prize__Group__5
            {
            pushFollow(FOLLOW_48);
            rule__Prize__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Prize__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group__4"


    // $ANTLR start "rule__Prize__Group__4__Impl"
    // InternalDSL.g:8302:1: rule__Prize__Group__4__Impl : ( ( rule__Prize__Group_4__0 )? ) ;
    public final void rule__Prize__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8306:1: ( ( ( rule__Prize__Group_4__0 )? ) )
            // InternalDSL.g:8307:1: ( ( rule__Prize__Group_4__0 )? )
            {
            // InternalDSL.g:8307:1: ( ( rule__Prize__Group_4__0 )? )
            // InternalDSL.g:8308:2: ( rule__Prize__Group_4__0 )?
            {
             before(grammarAccess.getPrizeAccess().getGroup_4()); 
            // InternalDSL.g:8309:2: ( rule__Prize__Group_4__0 )?
            int alt75=2;
            int LA75_0 = input.LA(1);

            if ( (LA75_0==103) ) {
                alt75=1;
            }
            switch (alt75) {
                case 1 :
                    // InternalDSL.g:8309:3: rule__Prize__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Prize__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPrizeAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group__4__Impl"


    // $ANTLR start "rule__Prize__Group__5"
    // InternalDSL.g:8317:1: rule__Prize__Group__5 : rule__Prize__Group__5__Impl rule__Prize__Group__6 ;
    public final void rule__Prize__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8321:1: ( rule__Prize__Group__5__Impl rule__Prize__Group__6 )
            // InternalDSL.g:8322:2: rule__Prize__Group__5__Impl rule__Prize__Group__6
            {
            pushFollow(FOLLOW_48);
            rule__Prize__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Prize__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group__5"


    // $ANTLR start "rule__Prize__Group__5__Impl"
    // InternalDSL.g:8329:1: rule__Prize__Group__5__Impl : ( ( rule__Prize__Group_5__0 )? ) ;
    public final void rule__Prize__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8333:1: ( ( ( rule__Prize__Group_5__0 )? ) )
            // InternalDSL.g:8334:1: ( ( rule__Prize__Group_5__0 )? )
            {
            // InternalDSL.g:8334:1: ( ( rule__Prize__Group_5__0 )? )
            // InternalDSL.g:8335:2: ( rule__Prize__Group_5__0 )?
            {
             before(grammarAccess.getPrizeAccess().getGroup_5()); 
            // InternalDSL.g:8336:2: ( rule__Prize__Group_5__0 )?
            int alt76=2;
            int LA76_0 = input.LA(1);

            if ( (LA76_0==109) ) {
                alt76=1;
            }
            switch (alt76) {
                case 1 :
                    // InternalDSL.g:8336:3: rule__Prize__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Prize__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPrizeAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group__5__Impl"


    // $ANTLR start "rule__Prize__Group__6"
    // InternalDSL.g:8344:1: rule__Prize__Group__6 : rule__Prize__Group__6__Impl rule__Prize__Group__7 ;
    public final void rule__Prize__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8348:1: ( rule__Prize__Group__6__Impl rule__Prize__Group__7 )
            // InternalDSL.g:8349:2: rule__Prize__Group__6__Impl rule__Prize__Group__7
            {
            pushFollow(FOLLOW_48);
            rule__Prize__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Prize__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group__6"


    // $ANTLR start "rule__Prize__Group__6__Impl"
    // InternalDSL.g:8356:1: rule__Prize__Group__6__Impl : ( ( rule__Prize__Group_6__0 )? ) ;
    public final void rule__Prize__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8360:1: ( ( ( rule__Prize__Group_6__0 )? ) )
            // InternalDSL.g:8361:1: ( ( rule__Prize__Group_6__0 )? )
            {
            // InternalDSL.g:8361:1: ( ( rule__Prize__Group_6__0 )? )
            // InternalDSL.g:8362:2: ( rule__Prize__Group_6__0 )?
            {
             before(grammarAccess.getPrizeAccess().getGroup_6()); 
            // InternalDSL.g:8363:2: ( rule__Prize__Group_6__0 )?
            int alt77=2;
            int LA77_0 = input.LA(1);

            if ( (LA77_0==65) ) {
                alt77=1;
            }
            switch (alt77) {
                case 1 :
                    // InternalDSL.g:8363:3: rule__Prize__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Prize__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPrizeAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group__6__Impl"


    // $ANTLR start "rule__Prize__Group__7"
    // InternalDSL.g:8371:1: rule__Prize__Group__7 : rule__Prize__Group__7__Impl ;
    public final void rule__Prize__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8375:1: ( rule__Prize__Group__7__Impl )
            // InternalDSL.g:8376:2: rule__Prize__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Prize__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group__7"


    // $ANTLR start "rule__Prize__Group__7__Impl"
    // InternalDSL.g:8382:1: rule__Prize__Group__7__Impl : ( '}' ) ;
    public final void rule__Prize__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8386:1: ( ( '}' ) )
            // InternalDSL.g:8387:1: ( '}' )
            {
            // InternalDSL.g:8387:1: ( '}' )
            // InternalDSL.g:8388:2: '}'
            {
             before(grammarAccess.getPrizeAccess().getRightCurlyBracketKeyword_7()); 
            match(input,60,FOLLOW_2); 
             after(grammarAccess.getPrizeAccess().getRightCurlyBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group__7__Impl"


    // $ANTLR start "rule__Prize__Group_3__0"
    // InternalDSL.g:8398:1: rule__Prize__Group_3__0 : rule__Prize__Group_3__0__Impl rule__Prize__Group_3__1 ;
    public final void rule__Prize__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8402:1: ( rule__Prize__Group_3__0__Impl rule__Prize__Group_3__1 )
            // InternalDSL.g:8403:2: rule__Prize__Group_3__0__Impl rule__Prize__Group_3__1
            {
            pushFollow(FOLLOW_11);
            rule__Prize__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Prize__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group_3__0"


    // $ANTLR start "rule__Prize__Group_3__0__Impl"
    // InternalDSL.g:8410:1: rule__Prize__Group_3__0__Impl : ( 'Name:' ) ;
    public final void rule__Prize__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8414:1: ( ( 'Name:' ) )
            // InternalDSL.g:8415:1: ( 'Name:' )
            {
            // InternalDSL.g:8415:1: ( 'Name:' )
            // InternalDSL.g:8416:2: 'Name:'
            {
             before(grammarAccess.getPrizeAccess().getNameKeyword_3_0()); 
            match(input,62,FOLLOW_2); 
             after(grammarAccess.getPrizeAccess().getNameKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group_3__0__Impl"


    // $ANTLR start "rule__Prize__Group_3__1"
    // InternalDSL.g:8425:1: rule__Prize__Group_3__1 : rule__Prize__Group_3__1__Impl ;
    public final void rule__Prize__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8429:1: ( rule__Prize__Group_3__1__Impl )
            // InternalDSL.g:8430:2: rule__Prize__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Prize__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group_3__1"


    // $ANTLR start "rule__Prize__Group_3__1__Impl"
    // InternalDSL.g:8436:1: rule__Prize__Group_3__1__Impl : ( ( rule__Prize__NameAssignment_3_1 ) ) ;
    public final void rule__Prize__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8440:1: ( ( ( rule__Prize__NameAssignment_3_1 ) ) )
            // InternalDSL.g:8441:1: ( ( rule__Prize__NameAssignment_3_1 ) )
            {
            // InternalDSL.g:8441:1: ( ( rule__Prize__NameAssignment_3_1 ) )
            // InternalDSL.g:8442:2: ( rule__Prize__NameAssignment_3_1 )
            {
             before(grammarAccess.getPrizeAccess().getNameAssignment_3_1()); 
            // InternalDSL.g:8443:2: ( rule__Prize__NameAssignment_3_1 )
            // InternalDSL.g:8443:3: rule__Prize__NameAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Prize__NameAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getPrizeAccess().getNameAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group_3__1__Impl"


    // $ANTLR start "rule__Prize__Group_4__0"
    // InternalDSL.g:8452:1: rule__Prize__Group_4__0 : rule__Prize__Group_4__0__Impl rule__Prize__Group_4__1 ;
    public final void rule__Prize__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8456:1: ( rule__Prize__Group_4__0__Impl rule__Prize__Group_4__1 )
            // InternalDSL.g:8457:2: rule__Prize__Group_4__0__Impl rule__Prize__Group_4__1
            {
            pushFollow(FOLLOW_12);
            rule__Prize__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Prize__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group_4__0"


    // $ANTLR start "rule__Prize__Group_4__0__Impl"
    // InternalDSL.g:8464:1: rule__Prize__Group_4__0__Impl : ( 'RewardDescription:' ) ;
    public final void rule__Prize__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8468:1: ( ( 'RewardDescription:' ) )
            // InternalDSL.g:8469:1: ( 'RewardDescription:' )
            {
            // InternalDSL.g:8469:1: ( 'RewardDescription:' )
            // InternalDSL.g:8470:2: 'RewardDescription:'
            {
             before(grammarAccess.getPrizeAccess().getRewardDescriptionKeyword_4_0()); 
            match(input,103,FOLLOW_2); 
             after(grammarAccess.getPrizeAccess().getRewardDescriptionKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group_4__0__Impl"


    // $ANTLR start "rule__Prize__Group_4__1"
    // InternalDSL.g:8479:1: rule__Prize__Group_4__1 : rule__Prize__Group_4__1__Impl ;
    public final void rule__Prize__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8483:1: ( rule__Prize__Group_4__1__Impl )
            // InternalDSL.g:8484:2: rule__Prize__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Prize__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group_4__1"


    // $ANTLR start "rule__Prize__Group_4__1__Impl"
    // InternalDSL.g:8490:1: rule__Prize__Group_4__1__Impl : ( ( rule__Prize__RewardDescriptionAssignment_4_1 ) ) ;
    public final void rule__Prize__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8494:1: ( ( ( rule__Prize__RewardDescriptionAssignment_4_1 ) ) )
            // InternalDSL.g:8495:1: ( ( rule__Prize__RewardDescriptionAssignment_4_1 ) )
            {
            // InternalDSL.g:8495:1: ( ( rule__Prize__RewardDescriptionAssignment_4_1 ) )
            // InternalDSL.g:8496:2: ( rule__Prize__RewardDescriptionAssignment_4_1 )
            {
             before(grammarAccess.getPrizeAccess().getRewardDescriptionAssignment_4_1()); 
            // InternalDSL.g:8497:2: ( rule__Prize__RewardDescriptionAssignment_4_1 )
            // InternalDSL.g:8497:3: rule__Prize__RewardDescriptionAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__Prize__RewardDescriptionAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getPrizeAccess().getRewardDescriptionAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group_4__1__Impl"


    // $ANTLR start "rule__Prize__Group_5__0"
    // InternalDSL.g:8506:1: rule__Prize__Group_5__0 : rule__Prize__Group_5__0__Impl rule__Prize__Group_5__1 ;
    public final void rule__Prize__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8510:1: ( rule__Prize__Group_5__0__Impl rule__Prize__Group_5__1 )
            // InternalDSL.g:8511:2: rule__Prize__Group_5__0__Impl rule__Prize__Group_5__1
            {
            pushFollow(FOLLOW_49);
            rule__Prize__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Prize__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group_5__0"


    // $ANTLR start "rule__Prize__Group_5__0__Impl"
    // InternalDSL.g:8518:1: rule__Prize__Group_5__0__Impl : ( 'PrizeType:' ) ;
    public final void rule__Prize__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8522:1: ( ( 'PrizeType:' ) )
            // InternalDSL.g:8523:1: ( 'PrizeType:' )
            {
            // InternalDSL.g:8523:1: ( 'PrizeType:' )
            // InternalDSL.g:8524:2: 'PrizeType:'
            {
             before(grammarAccess.getPrizeAccess().getPrizeTypeKeyword_5_0()); 
            match(input,109,FOLLOW_2); 
             after(grammarAccess.getPrizeAccess().getPrizeTypeKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group_5__0__Impl"


    // $ANTLR start "rule__Prize__Group_5__1"
    // InternalDSL.g:8533:1: rule__Prize__Group_5__1 : rule__Prize__Group_5__1__Impl ;
    public final void rule__Prize__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8537:1: ( rule__Prize__Group_5__1__Impl )
            // InternalDSL.g:8538:2: rule__Prize__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Prize__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group_5__1"


    // $ANTLR start "rule__Prize__Group_5__1__Impl"
    // InternalDSL.g:8544:1: rule__Prize__Group_5__1__Impl : ( ( rule__Prize__PrizeTypeAssignment_5_1 ) ) ;
    public final void rule__Prize__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8548:1: ( ( ( rule__Prize__PrizeTypeAssignment_5_1 ) ) )
            // InternalDSL.g:8549:1: ( ( rule__Prize__PrizeTypeAssignment_5_1 ) )
            {
            // InternalDSL.g:8549:1: ( ( rule__Prize__PrizeTypeAssignment_5_1 ) )
            // InternalDSL.g:8550:2: ( rule__Prize__PrizeTypeAssignment_5_1 )
            {
             before(grammarAccess.getPrizeAccess().getPrizeTypeAssignment_5_1()); 
            // InternalDSL.g:8551:2: ( rule__Prize__PrizeTypeAssignment_5_1 )
            // InternalDSL.g:8551:3: rule__Prize__PrizeTypeAssignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__Prize__PrizeTypeAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getPrizeAccess().getPrizeTypeAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group_5__1__Impl"


    // $ANTLR start "rule__Prize__Group_6__0"
    // InternalDSL.g:8560:1: rule__Prize__Group_6__0 : rule__Prize__Group_6__0__Impl rule__Prize__Group_6__1 ;
    public final void rule__Prize__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8564:1: ( rule__Prize__Group_6__0__Impl rule__Prize__Group_6__1 )
            // InternalDSL.g:8565:2: rule__Prize__Group_6__0__Impl rule__Prize__Group_6__1
            {
            pushFollow(FOLLOW_12);
            rule__Prize__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Prize__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group_6__0"


    // $ANTLR start "rule__Prize__Group_6__0__Impl"
    // InternalDSL.g:8572:1: rule__Prize__Group_6__0__Impl : ( 'Information:' ) ;
    public final void rule__Prize__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8576:1: ( ( 'Information:' ) )
            // InternalDSL.g:8577:1: ( 'Information:' )
            {
            // InternalDSL.g:8577:1: ( 'Information:' )
            // InternalDSL.g:8578:2: 'Information:'
            {
             before(grammarAccess.getPrizeAccess().getInformationKeyword_6_0()); 
            match(input,65,FOLLOW_2); 
             after(grammarAccess.getPrizeAccess().getInformationKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group_6__0__Impl"


    // $ANTLR start "rule__Prize__Group_6__1"
    // InternalDSL.g:8587:1: rule__Prize__Group_6__1 : rule__Prize__Group_6__1__Impl ;
    public final void rule__Prize__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8591:1: ( rule__Prize__Group_6__1__Impl )
            // InternalDSL.g:8592:2: rule__Prize__Group_6__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Prize__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group_6__1"


    // $ANTLR start "rule__Prize__Group_6__1__Impl"
    // InternalDSL.g:8598:1: rule__Prize__Group_6__1__Impl : ( ( rule__Prize__InformationAssignment_6_1 ) ) ;
    public final void rule__Prize__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8602:1: ( ( ( rule__Prize__InformationAssignment_6_1 ) ) )
            // InternalDSL.g:8603:1: ( ( rule__Prize__InformationAssignment_6_1 ) )
            {
            // InternalDSL.g:8603:1: ( ( rule__Prize__InformationAssignment_6_1 ) )
            // InternalDSL.g:8604:2: ( rule__Prize__InformationAssignment_6_1 )
            {
             before(grammarAccess.getPrizeAccess().getInformationAssignment_6_1()); 
            // InternalDSL.g:8605:2: ( rule__Prize__InformationAssignment_6_1 )
            // InternalDSL.g:8605:3: rule__Prize__InformationAssignment_6_1
            {
            pushFollow(FOLLOW_2);
            rule__Prize__InformationAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getPrizeAccess().getInformationAssignment_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__Group_6__1__Impl"


    // $ANTLR start "rule__Gamify__NameAssignment_2_1"
    // InternalDSL.g:8614:1: rule__Gamify__NameAssignment_2_1 : ( ruleEString ) ;
    public final void rule__Gamify__NameAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8618:1: ( ( ruleEString ) )
            // InternalDSL.g:8619:2: ( ruleEString )
            {
            // InternalDSL.g:8619:2: ( ruleEString )
            // InternalDSL.g:8620:3: ruleEString
            {
             before(grammarAccess.getGamifyAccess().getNameEStringParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getGamifyAccess().getNameEStringParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__NameAssignment_2_1"


    // $ANTLR start "rule__Gamify__SystemAssignment_3_1"
    // InternalDSL.g:8629:1: rule__Gamify__SystemAssignment_3_1 : ( ruleSystem ) ;
    public final void rule__Gamify__SystemAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8633:1: ( ( ruleSystem ) )
            // InternalDSL.g:8634:2: ( ruleSystem )
            {
            // InternalDSL.g:8634:2: ( ruleSystem )
            // InternalDSL.g:8635:3: ruleSystem
            {
             before(grammarAccess.getGamifyAccess().getSystemSystemParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSystem();

            state._fsp--;

             after(grammarAccess.getGamifyAccess().getSystemSystemParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__SystemAssignment_3_1"


    // $ANTLR start "rule__Gamify__GamedynamicsAssignment_6"
    // InternalDSL.g:8644:1: rule__Gamify__GamedynamicsAssignment_6 : ( ruleGameDynamic ) ;
    public final void rule__Gamify__GamedynamicsAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8648:1: ( ( ruleGameDynamic ) )
            // InternalDSL.g:8649:2: ( ruleGameDynamic )
            {
            // InternalDSL.g:8649:2: ( ruleGameDynamic )
            // InternalDSL.g:8650:3: ruleGameDynamic
            {
             before(grammarAccess.getGamifyAccess().getGamedynamicsGameDynamicParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleGameDynamic();

            state._fsp--;

             after(grammarAccess.getGamifyAccess().getGamedynamicsGameDynamicParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__GamedynamicsAssignment_6"


    // $ANTLR start "rule__Gamify__GamedynamicsAssignment_7_1"
    // InternalDSL.g:8659:1: rule__Gamify__GamedynamicsAssignment_7_1 : ( ruleGameDynamic ) ;
    public final void rule__Gamify__GamedynamicsAssignment_7_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8663:1: ( ( ruleGameDynamic ) )
            // InternalDSL.g:8664:2: ( ruleGameDynamic )
            {
            // InternalDSL.g:8664:2: ( ruleGameDynamic )
            // InternalDSL.g:8665:3: ruleGameDynamic
            {
             before(grammarAccess.getGamifyAccess().getGamedynamicsGameDynamicParserRuleCall_7_1_0()); 
            pushFollow(FOLLOW_2);
            ruleGameDynamic();

            state._fsp--;

             after(grammarAccess.getGamifyAccess().getGamedynamicsGameDynamicParserRuleCall_7_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__GamedynamicsAssignment_7_1"


    // $ANTLR start "rule__Gamify__ItemsAssignment_11"
    // InternalDSL.g:8674:1: rule__Gamify__ItemsAssignment_11 : ( ruleItem ) ;
    public final void rule__Gamify__ItemsAssignment_11() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8678:1: ( ( ruleItem ) )
            // InternalDSL.g:8679:2: ( ruleItem )
            {
            // InternalDSL.g:8679:2: ( ruleItem )
            // InternalDSL.g:8680:3: ruleItem
            {
             before(grammarAccess.getGamifyAccess().getItemsItemParserRuleCall_11_0()); 
            pushFollow(FOLLOW_2);
            ruleItem();

            state._fsp--;

             after(grammarAccess.getGamifyAccess().getItemsItemParserRuleCall_11_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__ItemsAssignment_11"


    // $ANTLR start "rule__Gamify__ItemsAssignment_12_1"
    // InternalDSL.g:8689:1: rule__Gamify__ItemsAssignment_12_1 : ( ruleItem ) ;
    public final void rule__Gamify__ItemsAssignment_12_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8693:1: ( ( ruleItem ) )
            // InternalDSL.g:8694:2: ( ruleItem )
            {
            // InternalDSL.g:8694:2: ( ruleItem )
            // InternalDSL.g:8695:3: ruleItem
            {
             before(grammarAccess.getGamifyAccess().getItemsItemParserRuleCall_12_1_0()); 
            pushFollow(FOLLOW_2);
            ruleItem();

            state._fsp--;

             after(grammarAccess.getGamifyAccess().getItemsItemParserRuleCall_12_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__ItemsAssignment_12_1"


    // $ANTLR start "rule__Gamify__InformationAssignment_14_1"
    // InternalDSL.g:8704:1: rule__Gamify__InformationAssignment_14_1 : ( RULE_STRING ) ;
    public final void rule__Gamify__InformationAssignment_14_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8708:1: ( ( RULE_STRING ) )
            // InternalDSL.g:8709:2: ( RULE_STRING )
            {
            // InternalDSL.g:8709:2: ( RULE_STRING )
            // InternalDSL.g:8710:3: RULE_STRING
            {
             before(grammarAccess.getGamifyAccess().getInformationSTRINGTerminalRuleCall_14_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getGamifyAccess().getInformationSTRINGTerminalRuleCall_14_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gamify__InformationAssignment_14_1"


    // $ANTLR start "rule__System__AttributesAssignment_2_1"
    // InternalDSL.g:8719:1: rule__System__AttributesAssignment_2_1 : ( ruleSystemAttributes ) ;
    public final void rule__System__AttributesAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8723:1: ( ( ruleSystemAttributes ) )
            // InternalDSL.g:8724:2: ( ruleSystemAttributes )
            {
            // InternalDSL.g:8724:2: ( ruleSystemAttributes )
            // InternalDSL.g:8725:3: ruleSystemAttributes
            {
             before(grammarAccess.getSystemAccess().getAttributesSystemAttributesEnumRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSystemAttributes();

            state._fsp--;

             after(grammarAccess.getSystemAccess().getAttributesSystemAttributesEnumRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__AttributesAssignment_2_1"


    // $ANTLR start "rule__System__UserTypeAssignment_3_1"
    // InternalDSL.g:8734:1: rule__System__UserTypeAssignment_3_1 : ( ruleUserTypes ) ;
    public final void rule__System__UserTypeAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8738:1: ( ( ruleUserTypes ) )
            // InternalDSL.g:8739:2: ( ruleUserTypes )
            {
            // InternalDSL.g:8739:2: ( ruleUserTypes )
            // InternalDSL.g:8740:3: ruleUserTypes
            {
             before(grammarAccess.getSystemAccess().getUserTypeUserTypesEnumRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleUserTypes();

            state._fsp--;

             after(grammarAccess.getSystemAccess().getUserTypeUserTypesEnumRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__UserTypeAssignment_3_1"


    // $ANTLR start "rule__System__UserAttributesAssignment_4_1"
    // InternalDSL.g:8749:1: rule__System__UserAttributesAssignment_4_1 : ( ruleUserBaseAttributes ) ;
    public final void rule__System__UserAttributesAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8753:1: ( ( ruleUserBaseAttributes ) )
            // InternalDSL.g:8754:2: ( ruleUserBaseAttributes )
            {
            // InternalDSL.g:8754:2: ( ruleUserBaseAttributes )
            // InternalDSL.g:8755:3: ruleUserBaseAttributes
            {
             before(grammarAccess.getSystemAccess().getUserAttributesUserBaseAttributesEnumRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleUserBaseAttributes();

            state._fsp--;

             after(grammarAccess.getSystemAccess().getUserAttributesUserBaseAttributesEnumRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__System__UserAttributesAssignment_4_1"


    // $ANTLR start "rule__GameDynamic__NameAssignment_2_1"
    // InternalDSL.g:8764:1: rule__GameDynamic__NameAssignment_2_1 : ( ruleEString ) ;
    public final void rule__GameDynamic__NameAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8768:1: ( ( ruleEString ) )
            // InternalDSL.g:8769:2: ( ruleEString )
            {
            // InternalDSL.g:8769:2: ( ruleEString )
            // InternalDSL.g:8770:3: ruleEString
            {
             before(grammarAccess.getGameDynamicAccess().getNameEStringParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getGameDynamicAccess().getNameEStringParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__NameAssignment_2_1"


    // $ANTLR start "rule__GameDynamic__DynamicTypeAssignment_3_1"
    // InternalDSL.g:8779:1: rule__GameDynamic__DynamicTypeAssignment_3_1 : ( ruleDynamicTypes ) ;
    public final void rule__GameDynamic__DynamicTypeAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8783:1: ( ( ruleDynamicTypes ) )
            // InternalDSL.g:8784:2: ( ruleDynamicTypes )
            {
            // InternalDSL.g:8784:2: ( ruleDynamicTypes )
            // InternalDSL.g:8785:3: ruleDynamicTypes
            {
             before(grammarAccess.getGameDynamicAccess().getDynamicTypeDynamicTypesEnumRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleDynamicTypes();

            state._fsp--;

             after(grammarAccess.getGameDynamicAccess().getDynamicTypeDynamicTypesEnumRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__DynamicTypeAssignment_3_1"


    // $ANTLR start "rule__GameDynamic__GamemechanicsAssignment_6"
    // InternalDSL.g:8794:1: rule__GameDynamic__GamemechanicsAssignment_6 : ( ruleGameMechanic ) ;
    public final void rule__GameDynamic__GamemechanicsAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8798:1: ( ( ruleGameMechanic ) )
            // InternalDSL.g:8799:2: ( ruleGameMechanic )
            {
            // InternalDSL.g:8799:2: ( ruleGameMechanic )
            // InternalDSL.g:8800:3: ruleGameMechanic
            {
             before(grammarAccess.getGameDynamicAccess().getGamemechanicsGameMechanicParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleGameMechanic();

            state._fsp--;

             after(grammarAccess.getGameDynamicAccess().getGamemechanicsGameMechanicParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__GamemechanicsAssignment_6"


    // $ANTLR start "rule__GameDynamic__GamemechanicsAssignment_7_1"
    // InternalDSL.g:8809:1: rule__GameDynamic__GamemechanicsAssignment_7_1 : ( ruleGameMechanic ) ;
    public final void rule__GameDynamic__GamemechanicsAssignment_7_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8813:1: ( ( ruleGameMechanic ) )
            // InternalDSL.g:8814:2: ( ruleGameMechanic )
            {
            // InternalDSL.g:8814:2: ( ruleGameMechanic )
            // InternalDSL.g:8815:3: ruleGameMechanic
            {
             before(grammarAccess.getGameDynamicAccess().getGamemechanicsGameMechanicParserRuleCall_7_1_0()); 
            pushFollow(FOLLOW_2);
            ruleGameMechanic();

            state._fsp--;

             after(grammarAccess.getGameDynamicAccess().getGamemechanicsGameMechanicParserRuleCall_7_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__GamemechanicsAssignment_7_1"


    // $ANTLR start "rule__GameDynamic__AchievementsAssignment_11"
    // InternalDSL.g:8824:1: rule__GameDynamic__AchievementsAssignment_11 : ( ruleAchievement ) ;
    public final void rule__GameDynamic__AchievementsAssignment_11() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8828:1: ( ( ruleAchievement ) )
            // InternalDSL.g:8829:2: ( ruleAchievement )
            {
            // InternalDSL.g:8829:2: ( ruleAchievement )
            // InternalDSL.g:8830:3: ruleAchievement
            {
             before(grammarAccess.getGameDynamicAccess().getAchievementsAchievementParserRuleCall_11_0()); 
            pushFollow(FOLLOW_2);
            ruleAchievement();

            state._fsp--;

             after(grammarAccess.getGameDynamicAccess().getAchievementsAchievementParserRuleCall_11_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__AchievementsAssignment_11"


    // $ANTLR start "rule__GameDynamic__AchievementsAssignment_12_1"
    // InternalDSL.g:8839:1: rule__GameDynamic__AchievementsAssignment_12_1 : ( ruleAchievement ) ;
    public final void rule__GameDynamic__AchievementsAssignment_12_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8843:1: ( ( ruleAchievement ) )
            // InternalDSL.g:8844:2: ( ruleAchievement )
            {
            // InternalDSL.g:8844:2: ( ruleAchievement )
            // InternalDSL.g:8845:3: ruleAchievement
            {
             before(grammarAccess.getGameDynamicAccess().getAchievementsAchievementParserRuleCall_12_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAchievement();

            state._fsp--;

             after(grammarAccess.getGameDynamicAccess().getAchievementsAchievementParserRuleCall_12_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__AchievementsAssignment_12_1"


    // $ANTLR start "rule__GameDynamic__InformationAssignment_14_1"
    // InternalDSL.g:8854:1: rule__GameDynamic__InformationAssignment_14_1 : ( RULE_STRING ) ;
    public final void rule__GameDynamic__InformationAssignment_14_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8858:1: ( ( RULE_STRING ) )
            // InternalDSL.g:8859:2: ( RULE_STRING )
            {
            // InternalDSL.g:8859:2: ( RULE_STRING )
            // InternalDSL.g:8860:3: RULE_STRING
            {
             before(grammarAccess.getGameDynamicAccess().getInformationSTRINGTerminalRuleCall_14_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getGameDynamicAccess().getInformationSTRINGTerminalRuleCall_14_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameDynamic__InformationAssignment_14_1"


    // $ANTLR start "rule__GameMechanic__NameAssignment_3_1"
    // InternalDSL.g:8869:1: rule__GameMechanic__NameAssignment_3_1 : ( ruleEString ) ;
    public final void rule__GameMechanic__NameAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8873:1: ( ( ruleEString ) )
            // InternalDSL.g:8874:2: ( ruleEString )
            {
            // InternalDSL.g:8874:2: ( ruleEString )
            // InternalDSL.g:8875:3: ruleEString
            {
             before(grammarAccess.getGameMechanicAccess().getNameEStringParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getGameMechanicAccess().getNameEStringParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__NameAssignment_3_1"


    // $ANTLR start "rule__GameMechanic__MechanicTypeAssignment_4_1"
    // InternalDSL.g:8884:1: rule__GameMechanic__MechanicTypeAssignment_4_1 : ( ruleGameMechanicTypes ) ;
    public final void rule__GameMechanic__MechanicTypeAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8888:1: ( ( ruleGameMechanicTypes ) )
            // InternalDSL.g:8889:2: ( ruleGameMechanicTypes )
            {
            // InternalDSL.g:8889:2: ( ruleGameMechanicTypes )
            // InternalDSL.g:8890:3: ruleGameMechanicTypes
            {
             before(grammarAccess.getGameMechanicAccess().getMechanicTypeGameMechanicTypesEnumRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleGameMechanicTypes();

            state._fsp--;

             after(grammarAccess.getGameMechanicAccess().getMechanicTypeGameMechanicTypesEnumRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__MechanicTypeAssignment_4_1"


    // $ANTLR start "rule__GameMechanic__EventsAssignment_5_2"
    // InternalDSL.g:8899:1: rule__GameMechanic__EventsAssignment_5_2 : ( ruleEvent ) ;
    public final void rule__GameMechanic__EventsAssignment_5_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8903:1: ( ( ruleEvent ) )
            // InternalDSL.g:8904:2: ( ruleEvent )
            {
            // InternalDSL.g:8904:2: ( ruleEvent )
            // InternalDSL.g:8905:3: ruleEvent
            {
             before(grammarAccess.getGameMechanicAccess().getEventsEventParserRuleCall_5_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEvent();

            state._fsp--;

             after(grammarAccess.getGameMechanicAccess().getEventsEventParserRuleCall_5_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__EventsAssignment_5_2"


    // $ANTLR start "rule__GameMechanic__EventsAssignment_5_3_1"
    // InternalDSL.g:8914:1: rule__GameMechanic__EventsAssignment_5_3_1 : ( ruleEvent ) ;
    public final void rule__GameMechanic__EventsAssignment_5_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8918:1: ( ( ruleEvent ) )
            // InternalDSL.g:8919:2: ( ruleEvent )
            {
            // InternalDSL.g:8919:2: ( ruleEvent )
            // InternalDSL.g:8920:3: ruleEvent
            {
             before(grammarAccess.getGameMechanicAccess().getEventsEventParserRuleCall_5_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEvent();

            state._fsp--;

             after(grammarAccess.getGameMechanicAccess().getEventsEventParserRuleCall_5_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__EventsAssignment_5_3_1"


    // $ANTLR start "rule__GameMechanic__InformationAssignment_6_1"
    // InternalDSL.g:8929:1: rule__GameMechanic__InformationAssignment_6_1 : ( RULE_STRING ) ;
    public final void rule__GameMechanic__InformationAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8933:1: ( ( RULE_STRING ) )
            // InternalDSL.g:8934:2: ( RULE_STRING )
            {
            // InternalDSL.g:8934:2: ( RULE_STRING )
            // InternalDSL.g:8935:3: RULE_STRING
            {
             before(grammarAccess.getGameMechanicAccess().getInformationSTRINGTerminalRuleCall_6_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getGameMechanicAccess().getInformationSTRINGTerminalRuleCall_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameMechanic__InformationAssignment_6_1"


    // $ANTLR start "rule__Achievement__NameAssignment_2_1"
    // InternalDSL.g:8944:1: rule__Achievement__NameAssignment_2_1 : ( ruleEString ) ;
    public final void rule__Achievement__NameAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8948:1: ( ( ruleEString ) )
            // InternalDSL.g:8949:2: ( ruleEString )
            {
            // InternalDSL.g:8949:2: ( ruleEString )
            // InternalDSL.g:8950:3: ruleEString
            {
             before(grammarAccess.getAchievementAccess().getNameEStringParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getAchievementAccess().getNameEStringParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__NameAssignment_2_1"


    // $ANTLR start "rule__Achievement__HiddenAssignment_3_1"
    // InternalDSL.g:8959:1: rule__Achievement__HiddenAssignment_3_1 : ( ruleBoolean ) ;
    public final void rule__Achievement__HiddenAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8963:1: ( ( ruleBoolean ) )
            // InternalDSL.g:8964:2: ( ruleBoolean )
            {
            // InternalDSL.g:8964:2: ( ruleBoolean )
            // InternalDSL.g:8965:3: ruleBoolean
            {
             before(grammarAccess.getAchievementAccess().getHiddenBooleanParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleBoolean();

            state._fsp--;

             after(grammarAccess.getAchievementAccess().getHiddenBooleanParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__HiddenAssignment_3_1"


    // $ANTLR start "rule__Achievement__RequiredAchievementsAssignment_4_2"
    // InternalDSL.g:8974:1: rule__Achievement__RequiredAchievementsAssignment_4_2 : ( ( ruleEString ) ) ;
    public final void rule__Achievement__RequiredAchievementsAssignment_4_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8978:1: ( ( ( ruleEString ) ) )
            // InternalDSL.g:8979:2: ( ( ruleEString ) )
            {
            // InternalDSL.g:8979:2: ( ( ruleEString ) )
            // InternalDSL.g:8980:3: ( ruleEString )
            {
             before(grammarAccess.getAchievementAccess().getRequiredAchievementsAchievementCrossReference_4_2_0()); 
            // InternalDSL.g:8981:3: ( ruleEString )
            // InternalDSL.g:8982:4: ruleEString
            {
             before(grammarAccess.getAchievementAccess().getRequiredAchievementsAchievementEStringParserRuleCall_4_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getAchievementAccess().getRequiredAchievementsAchievementEStringParserRuleCall_4_2_0_1()); 

            }

             after(grammarAccess.getAchievementAccess().getRequiredAchievementsAchievementCrossReference_4_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__RequiredAchievementsAssignment_4_2"


    // $ANTLR start "rule__Achievement__RequiredAchievementsAssignment_4_3_1"
    // InternalDSL.g:8993:1: rule__Achievement__RequiredAchievementsAssignment_4_3_1 : ( ( ruleEString ) ) ;
    public final void rule__Achievement__RequiredAchievementsAssignment_4_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:8997:1: ( ( ( ruleEString ) ) )
            // InternalDSL.g:8998:2: ( ( ruleEString ) )
            {
            // InternalDSL.g:8998:2: ( ( ruleEString ) )
            // InternalDSL.g:8999:3: ( ruleEString )
            {
             before(grammarAccess.getAchievementAccess().getRequiredAchievementsAchievementCrossReference_4_3_1_0()); 
            // InternalDSL.g:9000:3: ( ruleEString )
            // InternalDSL.g:9001:4: ruleEString
            {
             before(grammarAccess.getAchievementAccess().getRequiredAchievementsAchievementEStringParserRuleCall_4_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getAchievementAccess().getRequiredAchievementsAchievementEStringParserRuleCall_4_3_1_0_1()); 

            }

             after(grammarAccess.getAchievementAccess().getRequiredAchievementsAchievementCrossReference_4_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__RequiredAchievementsAssignment_4_3_1"


    // $ANTLR start "rule__Achievement__RewardsAssignment_5_2"
    // InternalDSL.g:9012:1: rule__Achievement__RewardsAssignment_5_2 : ( ruleReward ) ;
    public final void rule__Achievement__RewardsAssignment_5_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9016:1: ( ( ruleReward ) )
            // InternalDSL.g:9017:2: ( ruleReward )
            {
            // InternalDSL.g:9017:2: ( ruleReward )
            // InternalDSL.g:9018:3: ruleReward
            {
             before(grammarAccess.getAchievementAccess().getRewardsRewardParserRuleCall_5_2_0()); 
            pushFollow(FOLLOW_2);
            ruleReward();

            state._fsp--;

             after(grammarAccess.getAchievementAccess().getRewardsRewardParserRuleCall_5_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__RewardsAssignment_5_2"


    // $ANTLR start "rule__Achievement__RewardsAssignment_5_3_1"
    // InternalDSL.g:9027:1: rule__Achievement__RewardsAssignment_5_3_1 : ( ruleReward ) ;
    public final void rule__Achievement__RewardsAssignment_5_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9031:1: ( ( ruleReward ) )
            // InternalDSL.g:9032:2: ( ruleReward )
            {
            // InternalDSL.g:9032:2: ( ruleReward )
            // InternalDSL.g:9033:3: ruleReward
            {
             before(grammarAccess.getAchievementAccess().getRewardsRewardParserRuleCall_5_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleReward();

            state._fsp--;

             after(grammarAccess.getAchievementAccess().getRewardsRewardParserRuleCall_5_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__RewardsAssignment_5_3_1"


    // $ANTLR start "rule__Achievement__ConditionsAssignment_8"
    // InternalDSL.g:9042:1: rule__Achievement__ConditionsAssignment_8 : ( ruleCondition ) ;
    public final void rule__Achievement__ConditionsAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9046:1: ( ( ruleCondition ) )
            // InternalDSL.g:9047:2: ( ruleCondition )
            {
            // InternalDSL.g:9047:2: ( ruleCondition )
            // InternalDSL.g:9048:3: ruleCondition
            {
             before(grammarAccess.getAchievementAccess().getConditionsConditionParserRuleCall_8_0()); 
            pushFollow(FOLLOW_2);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getAchievementAccess().getConditionsConditionParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__ConditionsAssignment_8"


    // $ANTLR start "rule__Achievement__ConditionsAssignment_9_1"
    // InternalDSL.g:9057:1: rule__Achievement__ConditionsAssignment_9_1 : ( ruleCondition ) ;
    public final void rule__Achievement__ConditionsAssignment_9_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9061:1: ( ( ruleCondition ) )
            // InternalDSL.g:9062:2: ( ruleCondition )
            {
            // InternalDSL.g:9062:2: ( ruleCondition )
            // InternalDSL.g:9063:3: ruleCondition
            {
             before(grammarAccess.getAchievementAccess().getConditionsConditionParserRuleCall_9_1_0()); 
            pushFollow(FOLLOW_2);
            ruleCondition();

            state._fsp--;

             after(grammarAccess.getAchievementAccess().getConditionsConditionParserRuleCall_9_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__ConditionsAssignment_9_1"


    // $ANTLR start "rule__Achievement__InformationAssignment_11_1"
    // InternalDSL.g:9072:1: rule__Achievement__InformationAssignment_11_1 : ( RULE_STRING ) ;
    public final void rule__Achievement__InformationAssignment_11_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9076:1: ( ( RULE_STRING ) )
            // InternalDSL.g:9077:2: ( RULE_STRING )
            {
            // InternalDSL.g:9077:2: ( RULE_STRING )
            // InternalDSL.g:9078:3: RULE_STRING
            {
             before(grammarAccess.getAchievementAccess().getInformationSTRINGTerminalRuleCall_11_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getAchievementAccess().getInformationSTRINGTerminalRuleCall_11_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Achievement__InformationAssignment_11_1"


    // $ANTLR start "rule__Event__NameAssignment_3_1"
    // InternalDSL.g:9087:1: rule__Event__NameAssignment_3_1 : ( ruleEString ) ;
    public final void rule__Event__NameAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9091:1: ( ( ruleEString ) )
            // InternalDSL.g:9092:2: ( ruleEString )
            {
            // InternalDSL.g:9092:2: ( ruleEString )
            // InternalDSL.g:9093:3: ruleEString
            {
             before(grammarAccess.getEventAccess().getNameEStringParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEventAccess().getNameEStringParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__NameAssignment_3_1"


    // $ANTLR start "rule__Event__EventTypeAssignment_4_1"
    // InternalDSL.g:9102:1: rule__Event__EventTypeAssignment_4_1 : ( ruleEventTypes ) ;
    public final void rule__Event__EventTypeAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9106:1: ( ( ruleEventTypes ) )
            // InternalDSL.g:9107:2: ( ruleEventTypes )
            {
            // InternalDSL.g:9107:2: ( ruleEventTypes )
            // InternalDSL.g:9108:3: ruleEventTypes
            {
             before(grammarAccess.getEventAccess().getEventTypeEventTypesEnumRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEventTypes();

            state._fsp--;

             after(grammarAccess.getEventAccess().getEventTypeEventTypesEnumRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__EventTypeAssignment_4_1"


    // $ANTLR start "rule__Event__PointGainAssignment_5_1"
    // InternalDSL.g:9117:1: rule__Event__PointGainAssignment_5_1 : ( RULE_INT ) ;
    public final void rule__Event__PointGainAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9121:1: ( ( RULE_INT ) )
            // InternalDSL.g:9122:2: ( RULE_INT )
            {
            // InternalDSL.g:9122:2: ( RULE_INT )
            // InternalDSL.g:9123:3: RULE_INT
            {
             before(grammarAccess.getEventAccess().getPointGainINTTerminalRuleCall_5_1_0()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getPointGainINTTerminalRuleCall_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__PointGainAssignment_5_1"


    // $ANTLR start "rule__Event__TriggerConditionsAssignment_6_2"
    // InternalDSL.g:9132:1: rule__Event__TriggerConditionsAssignment_6_2 : ( ( ruleFQNString ) ) ;
    public final void rule__Event__TriggerConditionsAssignment_6_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9136:1: ( ( ( ruleFQNString ) ) )
            // InternalDSL.g:9137:2: ( ( ruleFQNString ) )
            {
            // InternalDSL.g:9137:2: ( ( ruleFQNString ) )
            // InternalDSL.g:9138:3: ( ruleFQNString )
            {
             before(grammarAccess.getEventAccess().getTriggerConditionsConditionCrossReference_6_2_0()); 
            // InternalDSL.g:9139:3: ( ruleFQNString )
            // InternalDSL.g:9140:4: ruleFQNString
            {
             before(grammarAccess.getEventAccess().getTriggerConditionsConditionFQNStringParserRuleCall_6_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleFQNString();

            state._fsp--;

             after(grammarAccess.getEventAccess().getTriggerConditionsConditionFQNStringParserRuleCall_6_2_0_1()); 

            }

             after(grammarAccess.getEventAccess().getTriggerConditionsConditionCrossReference_6_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__TriggerConditionsAssignment_6_2"


    // $ANTLR start "rule__Event__TriggerConditionsAssignment_6_3_1"
    // InternalDSL.g:9151:1: rule__Event__TriggerConditionsAssignment_6_3_1 : ( ( ruleFQNString ) ) ;
    public final void rule__Event__TriggerConditionsAssignment_6_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9155:1: ( ( ( ruleFQNString ) ) )
            // InternalDSL.g:9156:2: ( ( ruleFQNString ) )
            {
            // InternalDSL.g:9156:2: ( ( ruleFQNString ) )
            // InternalDSL.g:9157:3: ( ruleFQNString )
            {
             before(grammarAccess.getEventAccess().getTriggerConditionsConditionCrossReference_6_3_1_0()); 
            // InternalDSL.g:9158:3: ( ruleFQNString )
            // InternalDSL.g:9159:4: ruleFQNString
            {
             before(grammarAccess.getEventAccess().getTriggerConditionsConditionFQNStringParserRuleCall_6_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleFQNString();

            state._fsp--;

             after(grammarAccess.getEventAccess().getTriggerConditionsConditionFQNStringParserRuleCall_6_3_1_0_1()); 

            }

             after(grammarAccess.getEventAccess().getTriggerConditionsConditionCrossReference_6_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__TriggerConditionsAssignment_6_3_1"


    // $ANTLR start "rule__Event__RestrictionAssignment_7_2"
    // InternalDSL.g:9170:1: rule__Event__RestrictionAssignment_7_2 : ( ruleRestriction ) ;
    public final void rule__Event__RestrictionAssignment_7_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9174:1: ( ( ruleRestriction ) )
            // InternalDSL.g:9175:2: ( ruleRestriction )
            {
            // InternalDSL.g:9175:2: ( ruleRestriction )
            // InternalDSL.g:9176:3: ruleRestriction
            {
             before(grammarAccess.getEventAccess().getRestrictionRestrictionParserRuleCall_7_2_0()); 
            pushFollow(FOLLOW_2);
            ruleRestriction();

            state._fsp--;

             after(grammarAccess.getEventAccess().getRestrictionRestrictionParserRuleCall_7_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__RestrictionAssignment_7_2"


    // $ANTLR start "rule__Event__RestrictionAssignment_7_3_1"
    // InternalDSL.g:9185:1: rule__Event__RestrictionAssignment_7_3_1 : ( ruleRestriction ) ;
    public final void rule__Event__RestrictionAssignment_7_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9189:1: ( ( ruleRestriction ) )
            // InternalDSL.g:9190:2: ( ruleRestriction )
            {
            // InternalDSL.g:9190:2: ( ruleRestriction )
            // InternalDSL.g:9191:3: ruleRestriction
            {
             before(grammarAccess.getEventAccess().getRestrictionRestrictionParserRuleCall_7_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleRestriction();

            state._fsp--;

             after(grammarAccess.getEventAccess().getRestrictionRestrictionParserRuleCall_7_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__RestrictionAssignment_7_3_1"


    // $ANTLR start "rule__Event__InformationAssignment_8_1"
    // InternalDSL.g:9200:1: rule__Event__InformationAssignment_8_1 : ( RULE_STRING ) ;
    public final void rule__Event__InformationAssignment_8_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9204:1: ( ( RULE_STRING ) )
            // InternalDSL.g:9205:2: ( RULE_STRING )
            {
            // InternalDSL.g:9205:2: ( RULE_STRING )
            // InternalDSL.g:9206:3: RULE_STRING
            {
             before(grammarAccess.getEventAccess().getInformationSTRINGTerminalRuleCall_8_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getInformationSTRINGTerminalRuleCall_8_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__InformationAssignment_8_1"


    // $ANTLR start "rule__Condition__NameAssignment_3_1"
    // InternalDSL.g:9215:1: rule__Condition__NameAssignment_3_1 : ( ruleEString ) ;
    public final void rule__Condition__NameAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9219:1: ( ( ruleEString ) )
            // InternalDSL.g:9220:2: ( ruleEString )
            {
            // InternalDSL.g:9220:2: ( ruleEString )
            // InternalDSL.g:9221:3: ruleEString
            {
             before(grammarAccess.getConditionAccess().getNameEStringParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getConditionAccess().getNameEStringParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__NameAssignment_3_1"


    // $ANTLR start "rule__Condition__AmountRequiredAssignment_4_1"
    // InternalDSL.g:9230:1: rule__Condition__AmountRequiredAssignment_4_1 : ( RULE_INT ) ;
    public final void rule__Condition__AmountRequiredAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9234:1: ( ( RULE_INT ) )
            // InternalDSL.g:9235:2: ( RULE_INT )
            {
            // InternalDSL.g:9235:2: ( RULE_INT )
            // InternalDSL.g:9236:3: RULE_INT
            {
             before(grammarAccess.getConditionAccess().getAmountRequiredINTTerminalRuleCall_4_1_0()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getConditionAccess().getAmountRequiredINTTerminalRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__AmountRequiredAssignment_4_1"


    // $ANTLR start "rule__Condition__ConditionTypeAssignment_5_1"
    // InternalDSL.g:9245:1: rule__Condition__ConditionTypeAssignment_5_1 : ( ruleConditionTypes ) ;
    public final void rule__Condition__ConditionTypeAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9249:1: ( ( ruleConditionTypes ) )
            // InternalDSL.g:9250:2: ( ruleConditionTypes )
            {
            // InternalDSL.g:9250:2: ( ruleConditionTypes )
            // InternalDSL.g:9251:3: ruleConditionTypes
            {
             before(grammarAccess.getConditionAccess().getConditionTypeConditionTypesEnumRuleCall_5_1_0()); 
            pushFollow(FOLLOW_2);
            ruleConditionTypes();

            state._fsp--;

             after(grammarAccess.getConditionAccess().getConditionTypeConditionTypesEnumRuleCall_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__ConditionTypeAssignment_5_1"


    // $ANTLR start "rule__Condition__PreRequirementsAssignment_6_2"
    // InternalDSL.g:9260:1: rule__Condition__PreRequirementsAssignment_6_2 : ( ( ruleEString ) ) ;
    public final void rule__Condition__PreRequirementsAssignment_6_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9264:1: ( ( ( ruleEString ) ) )
            // InternalDSL.g:9265:2: ( ( ruleEString ) )
            {
            // InternalDSL.g:9265:2: ( ( ruleEString ) )
            // InternalDSL.g:9266:3: ( ruleEString )
            {
             before(grammarAccess.getConditionAccess().getPreRequirementsConditionCrossReference_6_2_0()); 
            // InternalDSL.g:9267:3: ( ruleEString )
            // InternalDSL.g:9268:4: ruleEString
            {
             before(grammarAccess.getConditionAccess().getPreRequirementsConditionEStringParserRuleCall_6_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getConditionAccess().getPreRequirementsConditionEStringParserRuleCall_6_2_0_1()); 

            }

             after(grammarAccess.getConditionAccess().getPreRequirementsConditionCrossReference_6_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__PreRequirementsAssignment_6_2"


    // $ANTLR start "rule__Condition__PreRequirementsAssignment_6_3_1"
    // InternalDSL.g:9279:1: rule__Condition__PreRequirementsAssignment_6_3_1 : ( ( ruleEString ) ) ;
    public final void rule__Condition__PreRequirementsAssignment_6_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9283:1: ( ( ( ruleEString ) ) )
            // InternalDSL.g:9284:2: ( ( ruleEString ) )
            {
            // InternalDSL.g:9284:2: ( ( ruleEString ) )
            // InternalDSL.g:9285:3: ( ruleEString )
            {
             before(grammarAccess.getConditionAccess().getPreRequirementsConditionCrossReference_6_3_1_0()); 
            // InternalDSL.g:9286:3: ( ruleEString )
            // InternalDSL.g:9287:4: ruleEString
            {
             before(grammarAccess.getConditionAccess().getPreRequirementsConditionEStringParserRuleCall_6_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getConditionAccess().getPreRequirementsConditionEStringParserRuleCall_6_3_1_0_1()); 

            }

             after(grammarAccess.getConditionAccess().getPreRequirementsConditionCrossReference_6_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__PreRequirementsAssignment_6_3_1"


    // $ANTLR start "rule__Condition__InformationAssignment_7_1"
    // InternalDSL.g:9298:1: rule__Condition__InformationAssignment_7_1 : ( RULE_STRING ) ;
    public final void rule__Condition__InformationAssignment_7_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9302:1: ( ( RULE_STRING ) )
            // InternalDSL.g:9303:2: ( RULE_STRING )
            {
            // InternalDSL.g:9303:2: ( RULE_STRING )
            // InternalDSL.g:9304:3: RULE_STRING
            {
             before(grammarAccess.getConditionAccess().getInformationSTRINGTerminalRuleCall_7_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getConditionAccess().getInformationSTRINGTerminalRuleCall_7_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Condition__InformationAssignment_7_1"


    // $ANTLR start "rule__Restriction__NameAssignment_3_1"
    // InternalDSL.g:9313:1: rule__Restriction__NameAssignment_3_1 : ( ruleEString ) ;
    public final void rule__Restriction__NameAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9317:1: ( ( ruleEString ) )
            // InternalDSL.g:9318:2: ( ruleEString )
            {
            // InternalDSL.g:9318:2: ( ruleEString )
            // InternalDSL.g:9319:3: ruleEString
            {
             before(grammarAccess.getRestrictionAccess().getNameEStringParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getRestrictionAccess().getNameEStringParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__NameAssignment_3_1"


    // $ANTLR start "rule__Restriction__RuleDescriptionAssignment_4_1"
    // InternalDSL.g:9328:1: rule__Restriction__RuleDescriptionAssignment_4_1 : ( RULE_STRING ) ;
    public final void rule__Restriction__RuleDescriptionAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9332:1: ( ( RULE_STRING ) )
            // InternalDSL.g:9333:2: ( RULE_STRING )
            {
            // InternalDSL.g:9333:2: ( RULE_STRING )
            // InternalDSL.g:9334:3: RULE_STRING
            {
             before(grammarAccess.getRestrictionAccess().getRuleDescriptionSTRINGTerminalRuleCall_4_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getRestrictionAccess().getRuleDescriptionSTRINGTerminalRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__RuleDescriptionAssignment_4_1"


    // $ANTLR start "rule__Restriction__RestrictionTypeAssignment_5_1"
    // InternalDSL.g:9343:1: rule__Restriction__RestrictionTypeAssignment_5_1 : ( ruleRestrictionTypes ) ;
    public final void rule__Restriction__RestrictionTypeAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9347:1: ( ( ruleRestrictionTypes ) )
            // InternalDSL.g:9348:2: ( ruleRestrictionTypes )
            {
            // InternalDSL.g:9348:2: ( ruleRestrictionTypes )
            // InternalDSL.g:9349:3: ruleRestrictionTypes
            {
             before(grammarAccess.getRestrictionAccess().getRestrictionTypeRestrictionTypesEnumRuleCall_5_1_0()); 
            pushFollow(FOLLOW_2);
            ruleRestrictionTypes();

            state._fsp--;

             after(grammarAccess.getRestrictionAccess().getRestrictionTypeRestrictionTypesEnumRuleCall_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__RestrictionTypeAssignment_5_1"


    // $ANTLR start "rule__Restriction__AmountAssignment_6_1"
    // InternalDSL.g:9358:1: rule__Restriction__AmountAssignment_6_1 : ( RULE_INT ) ;
    public final void rule__Restriction__AmountAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9362:1: ( ( RULE_INT ) )
            // InternalDSL.g:9363:2: ( RULE_INT )
            {
            // InternalDSL.g:9363:2: ( RULE_INT )
            // InternalDSL.g:9364:3: RULE_INT
            {
             before(grammarAccess.getRestrictionAccess().getAmountINTTerminalRuleCall_6_1_0()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getRestrictionAccess().getAmountINTTerminalRuleCall_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__AmountAssignment_6_1"


    // $ANTLR start "rule__Restriction__InformationAssignment_7_1"
    // InternalDSL.g:9373:1: rule__Restriction__InformationAssignment_7_1 : ( RULE_STRING ) ;
    public final void rule__Restriction__InformationAssignment_7_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9377:1: ( ( RULE_STRING ) )
            // InternalDSL.g:9378:2: ( RULE_STRING )
            {
            // InternalDSL.g:9378:2: ( RULE_STRING )
            // InternalDSL.g:9379:3: RULE_STRING
            {
             before(grammarAccess.getRestrictionAccess().getInformationSTRINGTerminalRuleCall_7_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getRestrictionAccess().getInformationSTRINGTerminalRuleCall_7_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Restriction__InformationAssignment_7_1"


    // $ANTLR start "rule__RandomReward__DropRateAssignment_3_1"
    // InternalDSL.g:9388:1: rule__RandomReward__DropRateAssignment_3_1 : ( RULE_DOUBLE ) ;
    public final void rule__RandomReward__DropRateAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9392:1: ( ( RULE_DOUBLE ) )
            // InternalDSL.g:9393:2: ( RULE_DOUBLE )
            {
            // InternalDSL.g:9393:2: ( RULE_DOUBLE )
            // InternalDSL.g:9394:3: RULE_DOUBLE
            {
             before(grammarAccess.getRandomRewardAccess().getDropRateDOUBLETerminalRuleCall_3_1_0()); 
            match(input,RULE_DOUBLE,FOLLOW_2); 
             after(grammarAccess.getRandomRewardAccess().getDropRateDOUBLETerminalRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__DropRateAssignment_3_1"


    // $ANTLR start "rule__RandomReward__IsVisibleAssignment_4_1"
    // InternalDSL.g:9403:1: rule__RandomReward__IsVisibleAssignment_4_1 : ( ruleBoolean ) ;
    public final void rule__RandomReward__IsVisibleAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9407:1: ( ( ruleBoolean ) )
            // InternalDSL.g:9408:2: ( ruleBoolean )
            {
            // InternalDSL.g:9408:2: ( ruleBoolean )
            // InternalDSL.g:9409:3: ruleBoolean
            {
             before(grammarAccess.getRandomRewardAccess().getIsVisibleBooleanParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleBoolean();

            state._fsp--;

             after(grammarAccess.getRandomRewardAccess().getIsVisibleBooleanParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__IsVisibleAssignment_4_1"


    // $ANTLR start "rule__RandomReward__ItemAssignment_5_1"
    // InternalDSL.g:9418:1: rule__RandomReward__ItemAssignment_5_1 : ( ( ruleEString ) ) ;
    public final void rule__RandomReward__ItemAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9422:1: ( ( ( ruleEString ) ) )
            // InternalDSL.g:9423:2: ( ( ruleEString ) )
            {
            // InternalDSL.g:9423:2: ( ( ruleEString ) )
            // InternalDSL.g:9424:3: ( ruleEString )
            {
             before(grammarAccess.getRandomRewardAccess().getItemItemCrossReference_5_1_0()); 
            // InternalDSL.g:9425:3: ( ruleEString )
            // InternalDSL.g:9426:4: ruleEString
            {
             before(grammarAccess.getRandomRewardAccess().getItemItemEStringParserRuleCall_5_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getRandomRewardAccess().getItemItemEStringParserRuleCall_5_1_0_1()); 

            }

             after(grammarAccess.getRandomRewardAccess().getItemItemCrossReference_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RandomReward__ItemAssignment_5_1"


    // $ANTLR start "rule__FixedReward__ItemAssignment_3_1"
    // InternalDSL.g:9437:1: rule__FixedReward__ItemAssignment_3_1 : ( ( ruleEString ) ) ;
    public final void rule__FixedReward__ItemAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9441:1: ( ( ( ruleEString ) ) )
            // InternalDSL.g:9442:2: ( ( ruleEString ) )
            {
            // InternalDSL.g:9442:2: ( ( ruleEString ) )
            // InternalDSL.g:9443:3: ( ruleEString )
            {
             before(grammarAccess.getFixedRewardAccess().getItemItemCrossReference_3_1_0()); 
            // InternalDSL.g:9444:3: ( ruleEString )
            // InternalDSL.g:9445:4: ruleEString
            {
             before(grammarAccess.getFixedRewardAccess().getItemItemEStringParserRuleCall_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getFixedRewardAccess().getItemItemEStringParserRuleCall_3_1_0_1()); 

            }

             after(grammarAccess.getFixedRewardAccess().getItemItemCrossReference_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FixedReward__ItemAssignment_3_1"


    // $ANTLR start "rule__Badge__NameAssignment_3_1"
    // InternalDSL.g:9456:1: rule__Badge__NameAssignment_3_1 : ( ruleEString ) ;
    public final void rule__Badge__NameAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9460:1: ( ( ruleEString ) )
            // InternalDSL.g:9461:2: ( ruleEString )
            {
            // InternalDSL.g:9461:2: ( ruleEString )
            // InternalDSL.g:9462:3: ruleEString
            {
             before(grammarAccess.getBadgeAccess().getNameEStringParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getBadgeAccess().getNameEStringParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__NameAssignment_3_1"


    // $ANTLR start "rule__Badge__RewardDescriptionAssignment_4_1"
    // InternalDSL.g:9471:1: rule__Badge__RewardDescriptionAssignment_4_1 : ( RULE_STRING ) ;
    public final void rule__Badge__RewardDescriptionAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9475:1: ( ( RULE_STRING ) )
            // InternalDSL.g:9476:2: ( RULE_STRING )
            {
            // InternalDSL.g:9476:2: ( RULE_STRING )
            // InternalDSL.g:9477:3: RULE_STRING
            {
             before(grammarAccess.getBadgeAccess().getRewardDescriptionSTRINGTerminalRuleCall_4_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getBadgeAccess().getRewardDescriptionSTRINGTerminalRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__RewardDescriptionAssignment_4_1"


    // $ANTLR start "rule__Badge__BadgeLevelAssignment_5_1"
    // InternalDSL.g:9486:1: rule__Badge__BadgeLevelAssignment_5_1 : ( RULE_INT ) ;
    public final void rule__Badge__BadgeLevelAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9490:1: ( ( RULE_INT ) )
            // InternalDSL.g:9491:2: ( RULE_INT )
            {
            // InternalDSL.g:9491:2: ( RULE_INT )
            // InternalDSL.g:9492:3: RULE_INT
            {
             before(grammarAccess.getBadgeAccess().getBadgeLevelINTTerminalRuleCall_5_1_0()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getBadgeAccess().getBadgeLevelINTTerminalRuleCall_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__BadgeLevelAssignment_5_1"


    // $ANTLR start "rule__Badge__BadgeImageAssignment_6_1"
    // InternalDSL.g:9501:1: rule__Badge__BadgeImageAssignment_6_1 : ( RULE_STRING ) ;
    public final void rule__Badge__BadgeImageAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9505:1: ( ( RULE_STRING ) )
            // InternalDSL.g:9506:2: ( RULE_STRING )
            {
            // InternalDSL.g:9506:2: ( RULE_STRING )
            // InternalDSL.g:9507:3: RULE_STRING
            {
             before(grammarAccess.getBadgeAccess().getBadgeImageSTRINGTerminalRuleCall_6_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getBadgeAccess().getBadgeImageSTRINGTerminalRuleCall_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__BadgeImageAssignment_6_1"


    // $ANTLR start "rule__Badge__InformationAssignment_7_1"
    // InternalDSL.g:9516:1: rule__Badge__InformationAssignment_7_1 : ( RULE_STRING ) ;
    public final void rule__Badge__InformationAssignment_7_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9520:1: ( ( RULE_STRING ) )
            // InternalDSL.g:9521:2: ( RULE_STRING )
            {
            // InternalDSL.g:9521:2: ( RULE_STRING )
            // InternalDSL.g:9522:3: RULE_STRING
            {
             before(grammarAccess.getBadgeAccess().getInformationSTRINGTerminalRuleCall_7_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getBadgeAccess().getInformationSTRINGTerminalRuleCall_7_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Badge__InformationAssignment_7_1"


    // $ANTLR start "rule__Points__NameAssignment_3_1"
    // InternalDSL.g:9531:1: rule__Points__NameAssignment_3_1 : ( ruleEString ) ;
    public final void rule__Points__NameAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9535:1: ( ( ruleEString ) )
            // InternalDSL.g:9536:2: ( ruleEString )
            {
            // InternalDSL.g:9536:2: ( ruleEString )
            // InternalDSL.g:9537:3: ruleEString
            {
             before(grammarAccess.getPointsAccess().getNameEStringParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getPointsAccess().getNameEStringParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__NameAssignment_3_1"


    // $ANTLR start "rule__Points__RewardDescriptionAssignment_4_1"
    // InternalDSL.g:9546:1: rule__Points__RewardDescriptionAssignment_4_1 : ( RULE_STRING ) ;
    public final void rule__Points__RewardDescriptionAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9550:1: ( ( RULE_STRING ) )
            // InternalDSL.g:9551:2: ( RULE_STRING )
            {
            // InternalDSL.g:9551:2: ( RULE_STRING )
            // InternalDSL.g:9552:3: RULE_STRING
            {
             before(grammarAccess.getPointsAccess().getRewardDescriptionSTRINGTerminalRuleCall_4_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getPointsAccess().getRewardDescriptionSTRINGTerminalRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__RewardDescriptionAssignment_4_1"


    // $ANTLR start "rule__Points__AmountAssignment_5_1"
    // InternalDSL.g:9561:1: rule__Points__AmountAssignment_5_1 : ( RULE_INT ) ;
    public final void rule__Points__AmountAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9565:1: ( ( RULE_INT ) )
            // InternalDSL.g:9566:2: ( RULE_INT )
            {
            // InternalDSL.g:9566:2: ( RULE_INT )
            // InternalDSL.g:9567:3: RULE_INT
            {
             before(grammarAccess.getPointsAccess().getAmountINTTerminalRuleCall_5_1_0()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getPointsAccess().getAmountINTTerminalRuleCall_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__AmountAssignment_5_1"


    // $ANTLR start "rule__Points__IsCollectibleAssignment_6_1"
    // InternalDSL.g:9576:1: rule__Points__IsCollectibleAssignment_6_1 : ( ruleBoolean ) ;
    public final void rule__Points__IsCollectibleAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9580:1: ( ( ruleBoolean ) )
            // InternalDSL.g:9581:2: ( ruleBoolean )
            {
            // InternalDSL.g:9581:2: ( ruleBoolean )
            // InternalDSL.g:9582:3: ruleBoolean
            {
             before(grammarAccess.getPointsAccess().getIsCollectibleBooleanParserRuleCall_6_1_0()); 
            pushFollow(FOLLOW_2);
            ruleBoolean();

            state._fsp--;

             after(grammarAccess.getPointsAccess().getIsCollectibleBooleanParserRuleCall_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__IsCollectibleAssignment_6_1"


    // $ANTLR start "rule__Points__ConditionsAssignment_7_2"
    // InternalDSL.g:9591:1: rule__Points__ConditionsAssignment_7_2 : ( ( ruleEString ) ) ;
    public final void rule__Points__ConditionsAssignment_7_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9595:1: ( ( ( ruleEString ) ) )
            // InternalDSL.g:9596:2: ( ( ruleEString ) )
            {
            // InternalDSL.g:9596:2: ( ( ruleEString ) )
            // InternalDSL.g:9597:3: ( ruleEString )
            {
             before(grammarAccess.getPointsAccess().getConditionsConditionCrossReference_7_2_0()); 
            // InternalDSL.g:9598:3: ( ruleEString )
            // InternalDSL.g:9599:4: ruleEString
            {
             before(grammarAccess.getPointsAccess().getConditionsConditionEStringParserRuleCall_7_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getPointsAccess().getConditionsConditionEStringParserRuleCall_7_2_0_1()); 

            }

             after(grammarAccess.getPointsAccess().getConditionsConditionCrossReference_7_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__ConditionsAssignment_7_2"


    // $ANTLR start "rule__Points__ConditionsAssignment_7_3_1"
    // InternalDSL.g:9610:1: rule__Points__ConditionsAssignment_7_3_1 : ( ( ruleEString ) ) ;
    public final void rule__Points__ConditionsAssignment_7_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9614:1: ( ( ( ruleEString ) ) )
            // InternalDSL.g:9615:2: ( ( ruleEString ) )
            {
            // InternalDSL.g:9615:2: ( ( ruleEString ) )
            // InternalDSL.g:9616:3: ( ruleEString )
            {
             before(grammarAccess.getPointsAccess().getConditionsConditionCrossReference_7_3_1_0()); 
            // InternalDSL.g:9617:3: ( ruleEString )
            // InternalDSL.g:9618:4: ruleEString
            {
             before(grammarAccess.getPointsAccess().getConditionsConditionEStringParserRuleCall_7_3_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getPointsAccess().getConditionsConditionEStringParserRuleCall_7_3_1_0_1()); 

            }

             after(grammarAccess.getPointsAccess().getConditionsConditionCrossReference_7_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__ConditionsAssignment_7_3_1"


    // $ANTLR start "rule__Points__InformationAssignment_8_1"
    // InternalDSL.g:9629:1: rule__Points__InformationAssignment_8_1 : ( RULE_STRING ) ;
    public final void rule__Points__InformationAssignment_8_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9633:1: ( ( RULE_STRING ) )
            // InternalDSL.g:9634:2: ( RULE_STRING )
            {
            // InternalDSL.g:9634:2: ( RULE_STRING )
            // InternalDSL.g:9635:3: RULE_STRING
            {
             before(grammarAccess.getPointsAccess().getInformationSTRINGTerminalRuleCall_8_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getPointsAccess().getInformationSTRINGTerminalRuleCall_8_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Points__InformationAssignment_8_1"


    // $ANTLR start "rule__Prize__NameAssignment_3_1"
    // InternalDSL.g:9644:1: rule__Prize__NameAssignment_3_1 : ( ruleEString ) ;
    public final void rule__Prize__NameAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9648:1: ( ( ruleEString ) )
            // InternalDSL.g:9649:2: ( ruleEString )
            {
            // InternalDSL.g:9649:2: ( ruleEString )
            // InternalDSL.g:9650:3: ruleEString
            {
             before(grammarAccess.getPrizeAccess().getNameEStringParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getPrizeAccess().getNameEStringParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__NameAssignment_3_1"


    // $ANTLR start "rule__Prize__RewardDescriptionAssignment_4_1"
    // InternalDSL.g:9659:1: rule__Prize__RewardDescriptionAssignment_4_1 : ( RULE_STRING ) ;
    public final void rule__Prize__RewardDescriptionAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9663:1: ( ( RULE_STRING ) )
            // InternalDSL.g:9664:2: ( RULE_STRING )
            {
            // InternalDSL.g:9664:2: ( RULE_STRING )
            // InternalDSL.g:9665:3: RULE_STRING
            {
             before(grammarAccess.getPrizeAccess().getRewardDescriptionSTRINGTerminalRuleCall_4_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getPrizeAccess().getRewardDescriptionSTRINGTerminalRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__RewardDescriptionAssignment_4_1"


    // $ANTLR start "rule__Prize__PrizeTypeAssignment_5_1"
    // InternalDSL.g:9674:1: rule__Prize__PrizeTypeAssignment_5_1 : ( rulePrizeTypes ) ;
    public final void rule__Prize__PrizeTypeAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9678:1: ( ( rulePrizeTypes ) )
            // InternalDSL.g:9679:2: ( rulePrizeTypes )
            {
            // InternalDSL.g:9679:2: ( rulePrizeTypes )
            // InternalDSL.g:9680:3: rulePrizeTypes
            {
             before(grammarAccess.getPrizeAccess().getPrizeTypePrizeTypesEnumRuleCall_5_1_0()); 
            pushFollow(FOLLOW_2);
            rulePrizeTypes();

            state._fsp--;

             after(grammarAccess.getPrizeAccess().getPrizeTypePrizeTypesEnumRuleCall_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__PrizeTypeAssignment_5_1"


    // $ANTLR start "rule__Prize__InformationAssignment_6_1"
    // InternalDSL.g:9689:1: rule__Prize__InformationAssignment_6_1 : ( RULE_STRING ) ;
    public final void rule__Prize__InformationAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDSL.g:9693:1: ( ( RULE_STRING ) )
            // InternalDSL.g:9694:2: ( RULE_STRING )
            {
            // InternalDSL.g:9694:2: ( RULE_STRING )
            // InternalDSL.g:9695:3: RULE_STRING
            {
             before(grammarAccess.getPrizeAccess().getInformationSTRINGTerminalRuleCall_6_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getPrizeAccess().getInformationSTRINGTerminalRuleCall_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prize__InformationAssignment_6_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0400000000000000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0xC800000000000000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x1000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000001L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x2000000000000000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000000L,0x0000144000000000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x1000000000000000L,0x0000000000000002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x1000000000000000L,0x0000000000000038L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x000000000001C000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000060000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000780000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x4000000000000000L,0x0000000000000280L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000400L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000100L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000000000L,0x0000000000002000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x000000007F800000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x5000000000000000L,0x0000000000001802L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x000000FF80000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000000000000L,0x0000000000100000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x4000000000000000L,0x000000000009C000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000000000000L,0x0000000002000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000000003000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000000000000L,0x0000000000040001L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000000000000L,0x0000002200000000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x5000000000000000L,0x0000000001E00002L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0001FF0000000000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000000000000L,0x0000000020000000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x5000000000000000L,0x000000001C000002L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x000E000000000000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x5000000000000000L,0x00000001C0000002L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0070000000000000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000000000000000L,0x0000000200000000L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x1000000000000000L,0x0000001C00000000L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x1000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0000000000000000L,0x0000004000000000L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x5000000000000000L,0x0000038000000002L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0000000000000000L,0x0000040000000000L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x5000000000000000L,0x0000088100004002L});
    public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x5000000000000000L,0x0000208000000002L});
    public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0180000000000000L});

}