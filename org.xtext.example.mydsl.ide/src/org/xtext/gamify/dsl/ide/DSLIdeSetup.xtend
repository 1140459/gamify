/*
 * generated by Xtext 2.16.0
 */
package org.xtext.gamify.dsl.ide

import com.google.inject.Guice
import org.eclipse.xtext.util.Modules2
import org.xtext.gamify.dsl.DSLRuntimeModule
import org.xtext.gamify.dsl.DSLStandaloneSetup

/**
 * Initialization support for running Xtext languages as language servers.
 */
class DSLIdeSetup extends DSLStandaloneSetup {

	override createInjector() {
		Guice.createInjector(Modules2.mixin(new DSLRuntimeModule, new DSLIdeModule))
	}
	
}
