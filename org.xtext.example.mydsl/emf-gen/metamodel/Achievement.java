/**
 */
package metamodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Achievement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link metamodel.Achievement#getCondition <em>Condition</em>}</li>
 *   <li>{@link metamodel.Achievement#getReward <em>Reward</em>}</li>
 *   <li>{@link metamodel.Achievement#getName <em>Name</em>}</li>
 *   <li>{@link metamodel.Achievement#getAchievementId <em>Achievement Id</em>}</li>
 *   <li>{@link metamodel.Achievement#isIsMilestone <em>Is Milestone</em>}</li>
 * </ul>
 *
 * @see metamodel.MetamodelPackage#getAchievement()
 * @model
 * @generated
 */
public interface Achievement extends EObject {
	/**
	 * Returns the value of the '<em><b>Condition</b></em>' containment reference list.
	 * The list contents are of type {@link metamodel.Condition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' containment reference list.
	 * @see metamodel.MetamodelPackage#getAchievement_Condition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Condition> getCondition();

	/**
	 * Returns the value of the '<em><b>Reward</b></em>' containment reference list.
	 * The list contents are of type {@link metamodel.Reward}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reward</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reward</em>' containment reference list.
	 * @see metamodel.MetamodelPackage#getAchievement_Reward()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Reward> getReward();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see metamodel.MetamodelPackage#getAchievement_Name()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link metamodel.Achievement#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Achievement Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Achievement Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Achievement Id</em>' attribute.
	 * @see #setAchievementId(int)
	 * @see metamodel.MetamodelPackage#getAchievement_AchievementId()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Int"
	 * @generated
	 */
	int getAchievementId();

	/**
	 * Sets the value of the '{@link metamodel.Achievement#getAchievementId <em>Achievement Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Achievement Id</em>' attribute.
	 * @see #getAchievementId()
	 * @generated
	 */
	void setAchievementId(int value);

	/**
	 * Returns the value of the '<em><b>Is Milestone</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Milestone</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Milestone</em>' attribute.
	 * @see #setIsMilestone(boolean)
	 * @see metamodel.MetamodelPackage#getAchievement_IsMilestone()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 * @generated
	 */
	boolean isIsMilestone();

	/**
	 * Sets the value of the '{@link metamodel.Achievement#isIsMilestone <em>Is Milestone</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Milestone</em>' attribute.
	 * @see #isIsMilestone()
	 * @generated
	 */
	void setIsMilestone(boolean value);

} // Achievement
