/**
 */
package metamodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Milestone</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link metamodel.Milestone#getAchievementSet <em>Achievement Set</em>}</li>
 * </ul>
 *
 * @see metamodel.MetamodelPackage#getMilestone()
 * @model
 * @generated
 */
public interface Milestone extends Condition {
	/**
	 * Returns the value of the '<em><b>Achievement Set</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Integer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Achievement Set</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Achievement Set</em>' attribute list.
	 * @see metamodel.MetamodelPackage#getMilestone_AchievementSet()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Int"
	 * @generated
	 */
	EList<Integer> getAchievementSet();

} // Milestone
