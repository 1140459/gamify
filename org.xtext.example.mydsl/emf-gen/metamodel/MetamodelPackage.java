/**
 */
package metamodel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see metamodel.MetamodelFactory
 * @model kind="package"
 * @generated
 */
public interface MetamodelPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "metamodel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://org/eclipse/dsl/metamodel";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "org.eclipse.dsl.metamodel";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MetamodelPackage eINSTANCE = metamodel.impl.MetamodelPackageImpl.init();

	/**
	 * The meta object id for the '{@link metamodel.impl.RewardImpl <em>Reward</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodel.impl.RewardImpl
	 * @see metamodel.impl.MetamodelPackageImpl#getReward()
	 * @generated
	 */
	int REWARD = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REWARD__NAME = 0;

	/**
	 * The number of structural features of the '<em>Reward</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REWARD_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Reward</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REWARD_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodel.impl.ConditionImpl <em>Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodel.impl.ConditionImpl
	 * @see metamodel.impl.MetamodelPackageImpl#getCondition()
	 * @generated
	 */
	int CONDITION = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__NAME = 0;

	/**
	 * The number of structural features of the '<em>Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodel.impl.SocialMechanicImpl <em>Social Mechanic</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodel.impl.SocialMechanicImpl
	 * @see metamodel.impl.MetamodelPackageImpl#getSocialMechanic()
	 * @generated
	 */
	int SOCIAL_MECHANIC = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOCIAL_MECHANIC__NAME = 0;

	/**
	 * The number of structural features of the '<em>Social Mechanic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOCIAL_MECHANIC_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Social Mechanic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOCIAL_MECHANIC_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodel.impl.SystemImpl <em>System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodel.impl.SystemImpl
	 * @see metamodel.impl.MetamodelPackageImpl#getSystem()
	 * @generated
	 */
	int SYSTEM = 3;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM__ATTRIBUTES = 0;

	/**
	 * The number of structural features of the '<em>System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodel.impl.GameMechanicImpl <em>Game Mechanic</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodel.impl.GameMechanicImpl
	 * @see metamodel.impl.MetamodelPackageImpl#getGameMechanic()
	 * @generated
	 */
	int GAME_MECHANIC = 4;

	/**
	 * The feature id for the '<em><b>Rule</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAME_MECHANIC__RULE = 0;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAME_MECHANIC__ACTION = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAME_MECHANIC__NAME = 2;

	/**
	 * The number of structural features of the '<em>Game Mechanic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAME_MECHANIC_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Game Mechanic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAME_MECHANIC_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodel.impl.GamifyImpl <em>Gamify</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodel.impl.GamifyImpl
	 * @see metamodel.impl.MetamodelPackageImpl#getGamify()
	 * @generated
	 */
	int GAMIFY = 5;

	/**
	 * The feature id for the '<em><b>Achievement</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAMIFY__ACHIEVEMENT = 0;

	/**
	 * The feature id for the '<em><b>Socialmechanic</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAMIFY__SOCIALMECHANIC = 1;

	/**
	 * The feature id for the '<em><b>Gamemechanic</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAMIFY__GAMEMECHANIC = 2;

	/**
	 * The feature id for the '<em><b>System</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAMIFY__SYSTEM = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAMIFY__NAME = 4;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAMIFY__TRIGGER = 5;

	/**
	 * The number of structural features of the '<em>Gamify</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAMIFY_FEATURE_COUNT = 6;

	/**
	 * The operation id for the '<em>Get All Conditions</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAMIFY___GET_ALL_CONDITIONS = 0;

	/**
	 * The operation id for the '<em>Get All Actions</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAMIFY___GET_ALL_ACTIONS = 1;

	/**
	 * The number of operations of the '<em>Gamify</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAMIFY_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link metamodel.impl.TriggerImpl <em>Trigger</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodel.impl.TriggerImpl
	 * @see metamodel.impl.MetamodelPackageImpl#getTrigger()
	 * @generated
	 */
	int TRIGGER = 6;

	/**
	 * The feature id for the '<em><b>Progress</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIGGER__PROGRESS = 0;

	/**
	 * The feature id for the '<em><b>Conditions</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIGGER__CONDITIONS = 1;

	/**
	 * The feature id for the '<em><b>Actions</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIGGER__ACTIONS = 2;

	/**
	 * The number of structural features of the '<em>Trigger</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIGGER_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Trigger</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIGGER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodel.impl.ActionImpl <em>Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodel.impl.ActionImpl
	 * @see metamodel.impl.MetamodelPackageImpl#getAction()
	 * @generated
	 */
	int ACTION = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__NAME = 0;

	/**
	 * The feature id for the '<em><b>Action Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__ACTION_TYPE = 1;

	/**
	 * The number of structural features of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodel.impl.AchievementImpl <em>Achievement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodel.impl.AchievementImpl
	 * @see metamodel.impl.MetamodelPackageImpl#getAchievement()
	 * @generated
	 */
	int ACHIEVEMENT = 8;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACHIEVEMENT__CONDITION = 0;

	/**
	 * The feature id for the '<em><b>Reward</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACHIEVEMENT__REWARD = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACHIEVEMENT__NAME = 2;

	/**
	 * The feature id for the '<em><b>Achievement Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACHIEVEMENT__ACHIEVEMENT_ID = 3;

	/**
	 * The feature id for the '<em><b>Is Milestone</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACHIEVEMENT__IS_MILESTONE = 4;

	/**
	 * The number of structural features of the '<em>Achievement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACHIEVEMENT_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Achievement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACHIEVEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodel.impl.RuleImpl <em>Rule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodel.impl.RuleImpl
	 * @see metamodel.impl.MetamodelPackageImpl#getRule()
	 * @generated
	 */
	int RULE = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE__NAME = 0;

	/**
	 * The number of structural features of the '<em>Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodel.impl.RestrictionImpl <em>Restriction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodel.impl.RestrictionImpl
	 * @see metamodel.impl.MetamodelPackageImpl#getRestriction()
	 * @generated
	 */
	int RESTRICTION = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESTRICTION__NAME = RULE__NAME;

	/**
	 * The number of structural features of the '<em>Restriction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESTRICTION_FEATURE_COUNT = RULE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Restriction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESTRICTION_OPERATION_COUNT = RULE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodel.impl.ObjectiveImpl <em>Objective</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodel.impl.ObjectiveImpl
	 * @see metamodel.impl.MetamodelPackageImpl#getObjective()
	 * @generated
	 */
	int OBJECTIVE = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECTIVE__NAME = RULE__NAME;

	/**
	 * The number of structural features of the '<em>Objective</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECTIVE_FEATURE_COUNT = RULE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Objective</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECTIVE_OPERATION_COUNT = RULE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodel.impl.RankImpl <em>Rank</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodel.impl.RankImpl
	 * @see metamodel.impl.MetamodelPackageImpl#getRank()
	 * @generated
	 */
	int RANK = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANK__NAME = REWARD__NAME;

	/**
	 * The number of structural features of the '<em>Rank</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANK_FEATURE_COUNT = REWARD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Rank</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANK_OPERATION_COUNT = REWARD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodel.impl.BadgeImpl <em>Badge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodel.impl.BadgeImpl
	 * @see metamodel.impl.MetamodelPackageImpl#getBadge()
	 * @generated
	 */
	int BADGE = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BADGE__NAME = REWARD__NAME;

	/**
	 * The number of structural features of the '<em>Badge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BADGE_FEATURE_COUNT = REWARD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Badge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BADGE_OPERATION_COUNT = REWARD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodel.impl.PointsImpl <em>Points</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodel.impl.PointsImpl
	 * @see metamodel.impl.MetamodelPackageImpl#getPoints()
	 * @generated
	 */
	int POINTS = 14;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINTS__NAME = REWARD__NAME;

	/**
	 * The number of structural features of the '<em>Points</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINTS_FEATURE_COUNT = REWARD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Points</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINTS_OPERATION_COUNT = REWARD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodel.impl.PrizeImpl <em>Prize</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodel.impl.PrizeImpl
	 * @see metamodel.impl.MetamodelPackageImpl#getPrize()
	 * @generated
	 */
	int PRIZE = 15;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIZE__NAME = REWARD__NAME;

	/**
	 * The number of structural features of the '<em>Prize</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIZE_FEATURE_COUNT = REWARD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Prize</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIZE_OPERATION_COUNT = REWARD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodel.impl.LeaderboardImpl <em>Leaderboard</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodel.impl.LeaderboardImpl
	 * @see metamodel.impl.MetamodelPackageImpl#getLeaderboard()
	 * @generated
	 */
	int LEADERBOARD = 16;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEADERBOARD__NAME = SOCIAL_MECHANIC__NAME;

	/**
	 * The feature id for the '<em><b>Board Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEADERBOARD__BOARD_TYPE = SOCIAL_MECHANIC_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Leaderboard</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEADERBOARD_FEATURE_COUNT = SOCIAL_MECHANIC_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Leaderboard</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEADERBOARD_OPERATION_COUNT = SOCIAL_MECHANIC_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodel.impl.PointSumImpl <em>Point Sum</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodel.impl.PointSumImpl
	 * @see metamodel.impl.MetamodelPackageImpl#getPointSum()
	 * @generated
	 */
	int POINT_SUM = 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_SUM__NAME = CONDITION__NAME;

	/**
	 * The feature id for the '<em><b>Amount To Satisfy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_SUM__AMOUNT_TO_SATISFY = CONDITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Point Sum</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_SUM_FEATURE_COUNT = CONDITION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Point Sum</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_SUM_OPERATION_COUNT = CONDITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodel.impl.MilestoneImpl <em>Milestone</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodel.impl.MilestoneImpl
	 * @see metamodel.impl.MetamodelPackageImpl#getMilestone()
	 * @generated
	 */
	int MILESTONE = 18;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MILESTONE__NAME = CONDITION__NAME;

	/**
	 * The feature id for the '<em><b>Achievement Set</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MILESTONE__ACHIEVEMENT_SET = CONDITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Milestone</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MILESTONE_FEATURE_COUNT = CONDITION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Milestone</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MILESTONE_OPERATION_COUNT = CONDITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodel.ActionTypes <em>Action Types</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodel.ActionTypes
	 * @see metamodel.impl.MetamodelPackageImpl#getActionTypes()
	 * @generated
	 */
	int ACTION_TYPES = 19;

	/**
	 * The meta object id for the '{@link metamodel.ConditionTypes <em>Condition Types</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodel.ConditionTypes
	 * @see metamodel.impl.MetamodelPackageImpl#getConditionTypes()
	 * @generated
	 */
	int CONDITION_TYPES = 20;

	/**
	 * The meta object id for the '{@link metamodel.RewardTypes <em>Reward Types</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodel.RewardTypes
	 * @see metamodel.impl.MetamodelPackageImpl#getRewardTypes()
	 * @generated
	 */
	int REWARD_TYPES = 21;

	/**
	 * The meta object id for the '{@link metamodel.LeaderboardTypes <em>Leaderboard Types</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodel.LeaderboardTypes
	 * @see metamodel.impl.MetamodelPackageImpl#getLeaderboardTypes()
	 * @generated
	 */
	int LEADERBOARD_TYPES = 22;

	/**
	 * The meta object id for the '{@link metamodel.SystemAttributes <em>System Attributes</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodel.SystemAttributes
	 * @see metamodel.impl.MetamodelPackageImpl#getSystemAttributes()
	 * @generated
	 */
	int SYSTEM_ATTRIBUTES = 23;


	/**
	 * Returns the meta object for class '{@link metamodel.Reward <em>Reward</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reward</em>'.
	 * @see metamodel.Reward
	 * @generated
	 */
	EClass getReward();

	/**
	 * Returns the meta object for the attribute '{@link metamodel.Reward#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see metamodel.Reward#getName()
	 * @see #getReward()
	 * @generated
	 */
	EAttribute getReward_Name();

	/**
	 * Returns the meta object for class '{@link metamodel.Condition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Condition</em>'.
	 * @see metamodel.Condition
	 * @generated
	 */
	EClass getCondition();

	/**
	 * Returns the meta object for the attribute '{@link metamodel.Condition#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see metamodel.Condition#getName()
	 * @see #getCondition()
	 * @generated
	 */
	EAttribute getCondition_Name();

	/**
	 * Returns the meta object for class '{@link metamodel.SocialMechanic <em>Social Mechanic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Social Mechanic</em>'.
	 * @see metamodel.SocialMechanic
	 * @generated
	 */
	EClass getSocialMechanic();

	/**
	 * Returns the meta object for the attribute '{@link metamodel.SocialMechanic#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see metamodel.SocialMechanic#getName()
	 * @see #getSocialMechanic()
	 * @generated
	 */
	EAttribute getSocialMechanic_Name();

	/**
	 * Returns the meta object for class '{@link metamodel.System <em>System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>System</em>'.
	 * @see metamodel.System
	 * @generated
	 */
	EClass getSystem();

	/**
	 * Returns the meta object for the attribute list '{@link metamodel.System#getAttributes <em>Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Attributes</em>'.
	 * @see metamodel.System#getAttributes()
	 * @see #getSystem()
	 * @generated
	 */
	EAttribute getSystem_Attributes();

	/**
	 * Returns the meta object for class '{@link metamodel.GameMechanic <em>Game Mechanic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Game Mechanic</em>'.
	 * @see metamodel.GameMechanic
	 * @generated
	 */
	EClass getGameMechanic();

	/**
	 * Returns the meta object for the containment reference list '{@link metamodel.GameMechanic#getRule <em>Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rule</em>'.
	 * @see metamodel.GameMechanic#getRule()
	 * @see #getGameMechanic()
	 * @generated
	 */
	EReference getGameMechanic_Rule();

	/**
	 * Returns the meta object for the containment reference list '{@link metamodel.GameMechanic#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Action</em>'.
	 * @see metamodel.GameMechanic#getAction()
	 * @see #getGameMechanic()
	 * @generated
	 */
	EReference getGameMechanic_Action();

	/**
	 * Returns the meta object for the attribute '{@link metamodel.GameMechanic#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see metamodel.GameMechanic#getName()
	 * @see #getGameMechanic()
	 * @generated
	 */
	EAttribute getGameMechanic_Name();

	/**
	 * Returns the meta object for class '{@link metamodel.Gamify <em>Gamify</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Gamify</em>'.
	 * @see metamodel.Gamify
	 * @generated
	 */
	EClass getGamify();

	/**
	 * Returns the meta object for the containment reference list '{@link metamodel.Gamify#getAchievement <em>Achievement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Achievement</em>'.
	 * @see metamodel.Gamify#getAchievement()
	 * @see #getGamify()
	 * @generated
	 */
	EReference getGamify_Achievement();

	/**
	 * Returns the meta object for the containment reference list '{@link metamodel.Gamify#getSocialmechanic <em>Socialmechanic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Socialmechanic</em>'.
	 * @see metamodel.Gamify#getSocialmechanic()
	 * @see #getGamify()
	 * @generated
	 */
	EReference getGamify_Socialmechanic();

	/**
	 * Returns the meta object for the containment reference list '{@link metamodel.Gamify#getGamemechanic <em>Gamemechanic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Gamemechanic</em>'.
	 * @see metamodel.Gamify#getGamemechanic()
	 * @see #getGamify()
	 * @generated
	 */
	EReference getGamify_Gamemechanic();

	/**
	 * Returns the meta object for the containment reference '{@link metamodel.Gamify#getSystem <em>System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>System</em>'.
	 * @see metamodel.Gamify#getSystem()
	 * @see #getGamify()
	 * @generated
	 */
	EReference getGamify_System();

	/**
	 * Returns the meta object for the attribute '{@link metamodel.Gamify#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see metamodel.Gamify#getName()
	 * @see #getGamify()
	 * @generated
	 */
	EAttribute getGamify_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link metamodel.Gamify#getTrigger <em>Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Trigger</em>'.
	 * @see metamodel.Gamify#getTrigger()
	 * @see #getGamify()
	 * @generated
	 */
	EReference getGamify_Trigger();

	/**
	 * Returns the meta object for the '{@link metamodel.Gamify#getAllConditions() <em>Get All Conditions</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get All Conditions</em>' operation.
	 * @see metamodel.Gamify#getAllConditions()
	 * @generated
	 */
	EOperation getGamify__GetAllConditions();

	/**
	 * Returns the meta object for the '{@link metamodel.Gamify#getAllActions() <em>Get All Actions</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get All Actions</em>' operation.
	 * @see metamodel.Gamify#getAllActions()
	 * @generated
	 */
	EOperation getGamify__GetAllActions();

	/**
	 * Returns the meta object for class '{@link metamodel.Trigger <em>Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Trigger</em>'.
	 * @see metamodel.Trigger
	 * @generated
	 */
	EClass getTrigger();

	/**
	 * Returns the meta object for the attribute '{@link metamodel.Trigger#getProgress <em>Progress</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Progress</em>'.
	 * @see metamodel.Trigger#getProgress()
	 * @see #getTrigger()
	 * @generated
	 */
	EAttribute getTrigger_Progress();

	/**
	 * Returns the meta object for the reference '{@link metamodel.Trigger#getConditions <em>Conditions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conditions</em>'.
	 * @see metamodel.Trigger#getConditions()
	 * @see #getTrigger()
	 * @generated
	 */
	EReference getTrigger_Conditions();

	/**
	 * Returns the meta object for the reference '{@link metamodel.Trigger#getActions <em>Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Actions</em>'.
	 * @see metamodel.Trigger#getActions()
	 * @see #getTrigger()
	 * @generated
	 */
	EReference getTrigger_Actions();

	/**
	 * Returns the meta object for class '{@link metamodel.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action</em>'.
	 * @see metamodel.Action
	 * @generated
	 */
	EClass getAction();

	/**
	 * Returns the meta object for the attribute '{@link metamodel.Action#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see metamodel.Action#getName()
	 * @see #getAction()
	 * @generated
	 */
	EAttribute getAction_Name();

	/**
	 * Returns the meta object for the attribute '{@link metamodel.Action#getActionType <em>Action Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Action Type</em>'.
	 * @see metamodel.Action#getActionType()
	 * @see #getAction()
	 * @generated
	 */
	EAttribute getAction_ActionType();

	/**
	 * Returns the meta object for class '{@link metamodel.Achievement <em>Achievement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Achievement</em>'.
	 * @see metamodel.Achievement
	 * @generated
	 */
	EClass getAchievement();

	/**
	 * Returns the meta object for the containment reference list '{@link metamodel.Achievement#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Condition</em>'.
	 * @see metamodel.Achievement#getCondition()
	 * @see #getAchievement()
	 * @generated
	 */
	EReference getAchievement_Condition();

	/**
	 * Returns the meta object for the containment reference list '{@link metamodel.Achievement#getReward <em>Reward</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reward</em>'.
	 * @see metamodel.Achievement#getReward()
	 * @see #getAchievement()
	 * @generated
	 */
	EReference getAchievement_Reward();

	/**
	 * Returns the meta object for the attribute '{@link metamodel.Achievement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see metamodel.Achievement#getName()
	 * @see #getAchievement()
	 * @generated
	 */
	EAttribute getAchievement_Name();

	/**
	 * Returns the meta object for the attribute '{@link metamodel.Achievement#getAchievementId <em>Achievement Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Achievement Id</em>'.
	 * @see metamodel.Achievement#getAchievementId()
	 * @see #getAchievement()
	 * @generated
	 */
	EAttribute getAchievement_AchievementId();

	/**
	 * Returns the meta object for the attribute '{@link metamodel.Achievement#isIsMilestone <em>Is Milestone</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Milestone</em>'.
	 * @see metamodel.Achievement#isIsMilestone()
	 * @see #getAchievement()
	 * @generated
	 */
	EAttribute getAchievement_IsMilestone();

	/**
	 * Returns the meta object for class '{@link metamodel.Rule <em>Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule</em>'.
	 * @see metamodel.Rule
	 * @generated
	 */
	EClass getRule();

	/**
	 * Returns the meta object for the attribute '{@link metamodel.Rule#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see metamodel.Rule#getName()
	 * @see #getRule()
	 * @generated
	 */
	EAttribute getRule_Name();

	/**
	 * Returns the meta object for class '{@link metamodel.Restriction <em>Restriction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Restriction</em>'.
	 * @see metamodel.Restriction
	 * @generated
	 */
	EClass getRestriction();

	/**
	 * Returns the meta object for class '{@link metamodel.Objective <em>Objective</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Objective</em>'.
	 * @see metamodel.Objective
	 * @generated
	 */
	EClass getObjective();

	/**
	 * Returns the meta object for class '{@link metamodel.Rank <em>Rank</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rank</em>'.
	 * @see metamodel.Rank
	 * @generated
	 */
	EClass getRank();

	/**
	 * Returns the meta object for class '{@link metamodel.Badge <em>Badge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Badge</em>'.
	 * @see metamodel.Badge
	 * @generated
	 */
	EClass getBadge();

	/**
	 * Returns the meta object for class '{@link metamodel.Points <em>Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Points</em>'.
	 * @see metamodel.Points
	 * @generated
	 */
	EClass getPoints();

	/**
	 * Returns the meta object for class '{@link metamodel.Prize <em>Prize</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Prize</em>'.
	 * @see metamodel.Prize
	 * @generated
	 */
	EClass getPrize();

	/**
	 * Returns the meta object for class '{@link metamodel.Leaderboard <em>Leaderboard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Leaderboard</em>'.
	 * @see metamodel.Leaderboard
	 * @generated
	 */
	EClass getLeaderboard();

	/**
	 * Returns the meta object for the attribute '{@link metamodel.Leaderboard#getBoardType <em>Board Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Board Type</em>'.
	 * @see metamodel.Leaderboard#getBoardType()
	 * @see #getLeaderboard()
	 * @generated
	 */
	EAttribute getLeaderboard_BoardType();

	/**
	 * Returns the meta object for class '{@link metamodel.PointSum <em>Point Sum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Point Sum</em>'.
	 * @see metamodel.PointSum
	 * @generated
	 */
	EClass getPointSum();

	/**
	 * Returns the meta object for the attribute '{@link metamodel.PointSum#getAmountToSatisfy <em>Amount To Satisfy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Amount To Satisfy</em>'.
	 * @see metamodel.PointSum#getAmountToSatisfy()
	 * @see #getPointSum()
	 * @generated
	 */
	EAttribute getPointSum_AmountToSatisfy();

	/**
	 * Returns the meta object for class '{@link metamodel.Milestone <em>Milestone</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Milestone</em>'.
	 * @see metamodel.Milestone
	 * @generated
	 */
	EClass getMilestone();

	/**
	 * Returns the meta object for the attribute list '{@link metamodel.Milestone#getAchievementSet <em>Achievement Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Achievement Set</em>'.
	 * @see metamodel.Milestone#getAchievementSet()
	 * @see #getMilestone()
	 * @generated
	 */
	EAttribute getMilestone_AchievementSet();

	/**
	 * Returns the meta object for enum '{@link metamodel.ActionTypes <em>Action Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Action Types</em>'.
	 * @see metamodel.ActionTypes
	 * @generated
	 */
	EEnum getActionTypes();

	/**
	 * Returns the meta object for enum '{@link metamodel.ConditionTypes <em>Condition Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Condition Types</em>'.
	 * @see metamodel.ConditionTypes
	 * @generated
	 */
	EEnum getConditionTypes();

	/**
	 * Returns the meta object for enum '{@link metamodel.RewardTypes <em>Reward Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Reward Types</em>'.
	 * @see metamodel.RewardTypes
	 * @generated
	 */
	EEnum getRewardTypes();

	/**
	 * Returns the meta object for enum '{@link metamodel.LeaderboardTypes <em>Leaderboard Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Leaderboard Types</em>'.
	 * @see metamodel.LeaderboardTypes
	 * @generated
	 */
	EEnum getLeaderboardTypes();

	/**
	 * Returns the meta object for enum '{@link metamodel.SystemAttributes <em>System Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>System Attributes</em>'.
	 * @see metamodel.SystemAttributes
	 * @generated
	 */
	EEnum getSystemAttributes();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MetamodelFactory getMetamodelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link metamodel.impl.RewardImpl <em>Reward</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodel.impl.RewardImpl
		 * @see metamodel.impl.MetamodelPackageImpl#getReward()
		 * @generated
		 */
		EClass REWARD = eINSTANCE.getReward();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REWARD__NAME = eINSTANCE.getReward_Name();

		/**
		 * The meta object literal for the '{@link metamodel.impl.ConditionImpl <em>Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodel.impl.ConditionImpl
		 * @see metamodel.impl.MetamodelPackageImpl#getCondition()
		 * @generated
		 */
		EClass CONDITION = eINSTANCE.getCondition();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONDITION__NAME = eINSTANCE.getCondition_Name();

		/**
		 * The meta object literal for the '{@link metamodel.impl.SocialMechanicImpl <em>Social Mechanic</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodel.impl.SocialMechanicImpl
		 * @see metamodel.impl.MetamodelPackageImpl#getSocialMechanic()
		 * @generated
		 */
		EClass SOCIAL_MECHANIC = eINSTANCE.getSocialMechanic();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOCIAL_MECHANIC__NAME = eINSTANCE.getSocialMechanic_Name();

		/**
		 * The meta object literal for the '{@link metamodel.impl.SystemImpl <em>System</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodel.impl.SystemImpl
		 * @see metamodel.impl.MetamodelPackageImpl#getSystem()
		 * @generated
		 */
		EClass SYSTEM = eINSTANCE.getSystem();

		/**
		 * The meta object literal for the '<em><b>Attributes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SYSTEM__ATTRIBUTES = eINSTANCE.getSystem_Attributes();

		/**
		 * The meta object literal for the '{@link metamodel.impl.GameMechanicImpl <em>Game Mechanic</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodel.impl.GameMechanicImpl
		 * @see metamodel.impl.MetamodelPackageImpl#getGameMechanic()
		 * @generated
		 */
		EClass GAME_MECHANIC = eINSTANCE.getGameMechanic();

		/**
		 * The meta object literal for the '<em><b>Rule</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GAME_MECHANIC__RULE = eINSTANCE.getGameMechanic_Rule();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GAME_MECHANIC__ACTION = eINSTANCE.getGameMechanic_Action();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GAME_MECHANIC__NAME = eINSTANCE.getGameMechanic_Name();

		/**
		 * The meta object literal for the '{@link metamodel.impl.GamifyImpl <em>Gamify</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodel.impl.GamifyImpl
		 * @see metamodel.impl.MetamodelPackageImpl#getGamify()
		 * @generated
		 */
		EClass GAMIFY = eINSTANCE.getGamify();

		/**
		 * The meta object literal for the '<em><b>Achievement</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GAMIFY__ACHIEVEMENT = eINSTANCE.getGamify_Achievement();

		/**
		 * The meta object literal for the '<em><b>Socialmechanic</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GAMIFY__SOCIALMECHANIC = eINSTANCE.getGamify_Socialmechanic();

		/**
		 * The meta object literal for the '<em><b>Gamemechanic</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GAMIFY__GAMEMECHANIC = eINSTANCE.getGamify_Gamemechanic();

		/**
		 * The meta object literal for the '<em><b>System</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GAMIFY__SYSTEM = eINSTANCE.getGamify_System();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GAMIFY__NAME = eINSTANCE.getGamify_Name();

		/**
		 * The meta object literal for the '<em><b>Trigger</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GAMIFY__TRIGGER = eINSTANCE.getGamify_Trigger();

		/**
		 * The meta object literal for the '<em><b>Get All Conditions</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation GAMIFY___GET_ALL_CONDITIONS = eINSTANCE.getGamify__GetAllConditions();

		/**
		 * The meta object literal for the '<em><b>Get All Actions</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation GAMIFY___GET_ALL_ACTIONS = eINSTANCE.getGamify__GetAllActions();

		/**
		 * The meta object literal for the '{@link metamodel.impl.TriggerImpl <em>Trigger</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodel.impl.TriggerImpl
		 * @see metamodel.impl.MetamodelPackageImpl#getTrigger()
		 * @generated
		 */
		EClass TRIGGER = eINSTANCE.getTrigger();

		/**
		 * The meta object literal for the '<em><b>Progress</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRIGGER__PROGRESS = eINSTANCE.getTrigger_Progress();

		/**
		 * The meta object literal for the '<em><b>Conditions</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRIGGER__CONDITIONS = eINSTANCE.getTrigger_Conditions();

		/**
		 * The meta object literal for the '<em><b>Actions</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRIGGER__ACTIONS = eINSTANCE.getTrigger_Actions();

		/**
		 * The meta object literal for the '{@link metamodel.impl.ActionImpl <em>Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodel.impl.ActionImpl
		 * @see metamodel.impl.MetamodelPackageImpl#getAction()
		 * @generated
		 */
		EClass ACTION = eINSTANCE.getAction();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION__NAME = eINSTANCE.getAction_Name();

		/**
		 * The meta object literal for the '<em><b>Action Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION__ACTION_TYPE = eINSTANCE.getAction_ActionType();

		/**
		 * The meta object literal for the '{@link metamodel.impl.AchievementImpl <em>Achievement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodel.impl.AchievementImpl
		 * @see metamodel.impl.MetamodelPackageImpl#getAchievement()
		 * @generated
		 */
		EClass ACHIEVEMENT = eINSTANCE.getAchievement();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACHIEVEMENT__CONDITION = eINSTANCE.getAchievement_Condition();

		/**
		 * The meta object literal for the '<em><b>Reward</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACHIEVEMENT__REWARD = eINSTANCE.getAchievement_Reward();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACHIEVEMENT__NAME = eINSTANCE.getAchievement_Name();

		/**
		 * The meta object literal for the '<em><b>Achievement Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACHIEVEMENT__ACHIEVEMENT_ID = eINSTANCE.getAchievement_AchievementId();

		/**
		 * The meta object literal for the '<em><b>Is Milestone</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACHIEVEMENT__IS_MILESTONE = eINSTANCE.getAchievement_IsMilestone();

		/**
		 * The meta object literal for the '{@link metamodel.impl.RuleImpl <em>Rule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodel.impl.RuleImpl
		 * @see metamodel.impl.MetamodelPackageImpl#getRule()
		 * @generated
		 */
		EClass RULE = eINSTANCE.getRule();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RULE__NAME = eINSTANCE.getRule_Name();

		/**
		 * The meta object literal for the '{@link metamodel.impl.RestrictionImpl <em>Restriction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodel.impl.RestrictionImpl
		 * @see metamodel.impl.MetamodelPackageImpl#getRestriction()
		 * @generated
		 */
		EClass RESTRICTION = eINSTANCE.getRestriction();

		/**
		 * The meta object literal for the '{@link metamodel.impl.ObjectiveImpl <em>Objective</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodel.impl.ObjectiveImpl
		 * @see metamodel.impl.MetamodelPackageImpl#getObjective()
		 * @generated
		 */
		EClass OBJECTIVE = eINSTANCE.getObjective();

		/**
		 * The meta object literal for the '{@link metamodel.impl.RankImpl <em>Rank</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodel.impl.RankImpl
		 * @see metamodel.impl.MetamodelPackageImpl#getRank()
		 * @generated
		 */
		EClass RANK = eINSTANCE.getRank();

		/**
		 * The meta object literal for the '{@link metamodel.impl.BadgeImpl <em>Badge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodel.impl.BadgeImpl
		 * @see metamodel.impl.MetamodelPackageImpl#getBadge()
		 * @generated
		 */
		EClass BADGE = eINSTANCE.getBadge();

		/**
		 * The meta object literal for the '{@link metamodel.impl.PointsImpl <em>Points</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodel.impl.PointsImpl
		 * @see metamodel.impl.MetamodelPackageImpl#getPoints()
		 * @generated
		 */
		EClass POINTS = eINSTANCE.getPoints();

		/**
		 * The meta object literal for the '{@link metamodel.impl.PrizeImpl <em>Prize</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodel.impl.PrizeImpl
		 * @see metamodel.impl.MetamodelPackageImpl#getPrize()
		 * @generated
		 */
		EClass PRIZE = eINSTANCE.getPrize();

		/**
		 * The meta object literal for the '{@link metamodel.impl.LeaderboardImpl <em>Leaderboard</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodel.impl.LeaderboardImpl
		 * @see metamodel.impl.MetamodelPackageImpl#getLeaderboard()
		 * @generated
		 */
		EClass LEADERBOARD = eINSTANCE.getLeaderboard();

		/**
		 * The meta object literal for the '<em><b>Board Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LEADERBOARD__BOARD_TYPE = eINSTANCE.getLeaderboard_BoardType();

		/**
		 * The meta object literal for the '{@link metamodel.impl.PointSumImpl <em>Point Sum</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodel.impl.PointSumImpl
		 * @see metamodel.impl.MetamodelPackageImpl#getPointSum()
		 * @generated
		 */
		EClass POINT_SUM = eINSTANCE.getPointSum();

		/**
		 * The meta object literal for the '<em><b>Amount To Satisfy</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POINT_SUM__AMOUNT_TO_SATISFY = eINSTANCE.getPointSum_AmountToSatisfy();

		/**
		 * The meta object literal for the '{@link metamodel.impl.MilestoneImpl <em>Milestone</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodel.impl.MilestoneImpl
		 * @see metamodel.impl.MetamodelPackageImpl#getMilestone()
		 * @generated
		 */
		EClass MILESTONE = eINSTANCE.getMilestone();

		/**
		 * The meta object literal for the '<em><b>Achievement Set</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MILESTONE__ACHIEVEMENT_SET = eINSTANCE.getMilestone_AchievementSet();

		/**
		 * The meta object literal for the '{@link metamodel.ActionTypes <em>Action Types</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodel.ActionTypes
		 * @see metamodel.impl.MetamodelPackageImpl#getActionTypes()
		 * @generated
		 */
		EEnum ACTION_TYPES = eINSTANCE.getActionTypes();

		/**
		 * The meta object literal for the '{@link metamodel.ConditionTypes <em>Condition Types</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodel.ConditionTypes
		 * @see metamodel.impl.MetamodelPackageImpl#getConditionTypes()
		 * @generated
		 */
		EEnum CONDITION_TYPES = eINSTANCE.getConditionTypes();

		/**
		 * The meta object literal for the '{@link metamodel.RewardTypes <em>Reward Types</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodel.RewardTypes
		 * @see metamodel.impl.MetamodelPackageImpl#getRewardTypes()
		 * @generated
		 */
		EEnum REWARD_TYPES = eINSTANCE.getRewardTypes();

		/**
		 * The meta object literal for the '{@link metamodel.LeaderboardTypes <em>Leaderboard Types</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodel.LeaderboardTypes
		 * @see metamodel.impl.MetamodelPackageImpl#getLeaderboardTypes()
		 * @generated
		 */
		EEnum LEADERBOARD_TYPES = eINSTANCE.getLeaderboardTypes();

		/**
		 * The meta object literal for the '{@link metamodel.SystemAttributes <em>System Attributes</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodel.SystemAttributes
		 * @see metamodel.impl.MetamodelPackageImpl#getSystemAttributes()
		 * @generated
		 */
		EEnum SYSTEM_ATTRIBUTES = eINSTANCE.getSystemAttributes();

	}

} //MetamodelPackage
