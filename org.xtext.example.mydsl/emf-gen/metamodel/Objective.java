/**
 */
package metamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Objective</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see metamodel.MetamodelPackage#getObjective()
 * @model
 * @generated
 */
public interface Objective extends Rule {
} // Objective
