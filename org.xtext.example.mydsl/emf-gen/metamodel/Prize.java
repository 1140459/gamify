/**
 */
package metamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Prize</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see metamodel.MetamodelPackage#getPrize()
 * @model
 * @generated
 */
public interface Prize extends Reward {
} // Prize
