/**
 */
package metamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Points</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see metamodel.MetamodelPackage#getPoints()
 * @model
 * @generated
 */
public interface Points extends Reward {
} // Points
