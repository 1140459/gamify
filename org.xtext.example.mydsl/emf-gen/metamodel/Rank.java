/**
 */
package metamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rank</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see metamodel.MetamodelPackage#getRank()
 * @model
 * @generated
 */
public interface Rank extends Reward {
} // Rank
