/**
 */
package metamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Leaderboard</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link metamodel.Leaderboard#getBoardType <em>Board Type</em>}</li>
 * </ul>
 *
 * @see metamodel.MetamodelPackage#getLeaderboard()
 * @model
 * @generated
 */
public interface Leaderboard extends SocialMechanic {
	/**
	 * Returns the value of the '<em><b>Board Type</b></em>' attribute.
	 * The literals are from the enumeration {@link metamodel.LeaderboardTypes}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Board Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Board Type</em>' attribute.
	 * @see metamodel.LeaderboardTypes
	 * @see #setBoardType(LeaderboardTypes)
	 * @see metamodel.MetamodelPackage#getLeaderboard_BoardType()
	 * @model
	 * @generated
	 */
	LeaderboardTypes getBoardType();

	/**
	 * Sets the value of the '{@link metamodel.Leaderboard#getBoardType <em>Board Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Board Type</em>' attribute.
	 * @see metamodel.LeaderboardTypes
	 * @see #getBoardType()
	 * @generated
	 */
	void setBoardType(LeaderboardTypes value);

} // Leaderboard
