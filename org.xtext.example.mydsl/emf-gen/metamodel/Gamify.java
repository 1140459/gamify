/**
 */
package metamodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Gamify</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link metamodel.Gamify#getAchievement <em>Achievement</em>}</li>
 *   <li>{@link metamodel.Gamify#getSocialmechanic <em>Socialmechanic</em>}</li>
 *   <li>{@link metamodel.Gamify#getGamemechanic <em>Gamemechanic</em>}</li>
 *   <li>{@link metamodel.Gamify#getSystem <em>System</em>}</li>
 *   <li>{@link metamodel.Gamify#getName <em>Name</em>}</li>
 *   <li>{@link metamodel.Gamify#getTrigger <em>Trigger</em>}</li>
 * </ul>
 *
 * @see metamodel.MetamodelPackage#getGamify()
 * @model
 * @generated
 */
public interface Gamify extends EObject {
	/**
	 * Returns the value of the '<em><b>Achievement</b></em>' containment reference list.
	 * The list contents are of type {@link metamodel.Achievement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Achievement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Achievement</em>' containment reference list.
	 * @see metamodel.MetamodelPackage#getGamify_Achievement()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Achievement> getAchievement();

	/**
	 * Returns the value of the '<em><b>Socialmechanic</b></em>' containment reference list.
	 * The list contents are of type {@link metamodel.SocialMechanic}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Socialmechanic</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Socialmechanic</em>' containment reference list.
	 * @see metamodel.MetamodelPackage#getGamify_Socialmechanic()
	 * @model containment="true"
	 * @generated
	 */
	EList<SocialMechanic> getSocialmechanic();

	/**
	 * Returns the value of the '<em><b>Gamemechanic</b></em>' containment reference list.
	 * The list contents are of type {@link metamodel.GameMechanic}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Gamemechanic</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Gamemechanic</em>' containment reference list.
	 * @see metamodel.MetamodelPackage#getGamify_Gamemechanic()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<GameMechanic> getGamemechanic();

	/**
	 * Returns the value of the '<em><b>System</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>System</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System</em>' containment reference.
	 * @see #setSystem(metamodel.System)
	 * @see metamodel.MetamodelPackage#getGamify_System()
	 * @model containment="true"
	 * @generated
	 */
	metamodel.System getSystem();

	/**
	 * Sets the value of the '{@link metamodel.Gamify#getSystem <em>System</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>System</em>' containment reference.
	 * @see #getSystem()
	 * @generated
	 */
	void setSystem(metamodel.System value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see metamodel.MetamodelPackage#getGamify_Name()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link metamodel.Gamify#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Trigger</b></em>' containment reference list.
	 * The list contents are of type {@link metamodel.Trigger}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Trigger</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trigger</em>' containment reference list.
	 * @see metamodel.MetamodelPackage#getGamify_Trigger()
	 * @model containment="true"
	 * @generated
	 */
	EList<Trigger> getTrigger();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<Condition> getAllConditions();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<Action> getAllActions();

} // Gamify
