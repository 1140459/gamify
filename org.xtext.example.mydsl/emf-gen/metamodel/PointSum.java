/**
 */
package metamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Point Sum</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link metamodel.PointSum#getAmountToSatisfy <em>Amount To Satisfy</em>}</li>
 * </ul>
 *
 * @see metamodel.MetamodelPackage#getPointSum()
 * @model
 * @generated
 */
public interface PointSum extends Condition {
	/**
	 * Returns the value of the '<em><b>Amount To Satisfy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Amount To Satisfy</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Amount To Satisfy</em>' attribute.
	 * @see #setAmountToSatisfy(int)
	 * @see metamodel.MetamodelPackage#getPointSum_AmountToSatisfy()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Int"
	 * @generated
	 */
	int getAmountToSatisfy();

	/**
	 * Sets the value of the '{@link metamodel.PointSum#getAmountToSatisfy <em>Amount To Satisfy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Amount To Satisfy</em>' attribute.
	 * @see #getAmountToSatisfy()
	 * @generated
	 */
	void setAmountToSatisfy(int value);

} // PointSum
