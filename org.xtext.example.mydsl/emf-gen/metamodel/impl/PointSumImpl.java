/**
 */
package metamodel.impl;

import metamodel.MetamodelPackage;
import metamodel.PointSum;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Point Sum</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link metamodel.impl.PointSumImpl#getAmountToSatisfy <em>Amount To Satisfy</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PointSumImpl extends ConditionImpl implements PointSum {
	/**
	 * The default value of the '{@link #getAmountToSatisfy() <em>Amount To Satisfy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAmountToSatisfy()
	 * @generated
	 * @ordered
	 */
	protected static final int AMOUNT_TO_SATISFY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getAmountToSatisfy() <em>Amount To Satisfy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAmountToSatisfy()
	 * @generated
	 * @ordered
	 */
	protected int amountToSatisfy = AMOUNT_TO_SATISFY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PointSumImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodelPackage.Literals.POINT_SUM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getAmountToSatisfy() {
		return amountToSatisfy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAmountToSatisfy(int newAmountToSatisfy) {
		int oldAmountToSatisfy = amountToSatisfy;
		amountToSatisfy = newAmountToSatisfy;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.POINT_SUM__AMOUNT_TO_SATISFY, oldAmountToSatisfy, amountToSatisfy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodelPackage.POINT_SUM__AMOUNT_TO_SATISFY:
				return getAmountToSatisfy();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodelPackage.POINT_SUM__AMOUNT_TO_SATISFY:
				setAmountToSatisfy((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodelPackage.POINT_SUM__AMOUNT_TO_SATISFY:
				setAmountToSatisfy(AMOUNT_TO_SATISFY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodelPackage.POINT_SUM__AMOUNT_TO_SATISFY:
				return amountToSatisfy != AMOUNT_TO_SATISFY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (amountToSatisfy: ");
		result.append(amountToSatisfy);
		result.append(')');
		return result.toString();
	}

} //PointSumImpl
