/**
 */
package metamodel.impl;

import metamodel.MetamodelPackage;
import metamodel.Rank;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Rank</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RankImpl extends RewardImpl implements Rank {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RankImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodelPackage.Literals.RANK;
	}

} //RankImpl
