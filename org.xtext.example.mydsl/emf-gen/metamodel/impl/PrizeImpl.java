/**
 */
package metamodel.impl;

import metamodel.MetamodelPackage;
import metamodel.Prize;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Prize</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PrizeImpl extends RewardImpl implements Prize {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PrizeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodelPackage.Literals.PRIZE;
	}

} //PrizeImpl
