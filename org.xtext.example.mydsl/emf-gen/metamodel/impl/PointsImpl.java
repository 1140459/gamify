/**
 */
package metamodel.impl;

import metamodel.MetamodelPackage;
import metamodel.Points;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Points</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PointsImpl extends RewardImpl implements Points {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PointsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodelPackage.Literals.POINTS;
	}

} //PointsImpl
