/**
 */
package metamodel.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import metamodel.Achievement;
import metamodel.Action;
import metamodel.Condition;
import metamodel.GameMechanic;
import metamodel.Gamify;
import metamodel.MetamodelPackage;
import metamodel.SocialMechanic;
import metamodel.Trigger;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Gamify</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link metamodel.impl.GamifyImpl#getAchievement <em>Achievement</em>}</li>
 *   <li>{@link metamodel.impl.GamifyImpl#getSocialmechanic <em>Socialmechanic</em>}</li>
 *   <li>{@link metamodel.impl.GamifyImpl#getGamemechanic <em>Gamemechanic</em>}</li>
 *   <li>{@link metamodel.impl.GamifyImpl#getSystem <em>System</em>}</li>
 *   <li>{@link metamodel.impl.GamifyImpl#getName <em>Name</em>}</li>
 *   <li>{@link metamodel.impl.GamifyImpl#getTrigger <em>Trigger</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GamifyImpl extends MinimalEObjectImpl.Container implements Gamify {
	/**
	 * The cached value of the '{@link #getAchievement() <em>Achievement</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAchievement()
	 * @generated
	 * @ordered
	 */
	protected EList<Achievement> achievement;

	/**
	 * The cached value of the '{@link #getSocialmechanic() <em>Socialmechanic</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSocialmechanic()
	 * @generated
	 * @ordered
	 */
	protected EList<SocialMechanic> socialmechanic;

	/**
	 * The cached value of the '{@link #getGamemechanic() <em>Gamemechanic</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGamemechanic()
	 * @generated
	 * @ordered
	 */
	protected EList<GameMechanic> gamemechanic;

	/**
	 * The cached value of the '{@link #getSystem() <em>System</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystem()
	 * @generated
	 * @ordered
	 */
	protected metamodel.System system;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTrigger() <em>Trigger</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTrigger()
	 * @generated
	 * @ordered
	 */
	protected EList<Trigger> trigger;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GamifyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodelPackage.Literals.GAMIFY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Achievement> getAchievement() {
		if (achievement == null) {
			achievement = new EObjectContainmentEList<Achievement>(Achievement.class, this, MetamodelPackage.GAMIFY__ACHIEVEMENT);
		}
		return achievement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SocialMechanic> getSocialmechanic() {
		if (socialmechanic == null) {
			socialmechanic = new EObjectContainmentEList<SocialMechanic>(SocialMechanic.class, this, MetamodelPackage.GAMIFY__SOCIALMECHANIC);
		}
		return socialmechanic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GameMechanic> getGamemechanic() {
		if (gamemechanic == null) {
			gamemechanic = new EObjectContainmentEList<GameMechanic>(GameMechanic.class, this, MetamodelPackage.GAMIFY__GAMEMECHANIC);
		}
		return gamemechanic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public metamodel.System getSystem() {
		return system;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSystem(metamodel.System newSystem, NotificationChain msgs) {
		metamodel.System oldSystem = system;
		system = newSystem;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodelPackage.GAMIFY__SYSTEM, oldSystem, newSystem);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSystem(metamodel.System newSystem) {
		if (newSystem != system) {
			NotificationChain msgs = null;
			if (system != null)
				msgs = ((InternalEObject)system).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodelPackage.GAMIFY__SYSTEM, null, msgs);
			if (newSystem != null)
				msgs = ((InternalEObject)newSystem).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodelPackage.GAMIFY__SYSTEM, null, msgs);
			msgs = basicSetSystem(newSystem, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.GAMIFY__SYSTEM, newSystem, newSystem));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.GAMIFY__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Trigger> getTrigger() {
		if (trigger == null) {
			trigger = new EObjectContainmentEList<Trigger>(Trigger.class, this, MetamodelPackage.GAMIFY__TRIGGER);
		}
		return trigger;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Condition> getAllConditions() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Action> getAllActions() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodelPackage.GAMIFY__ACHIEVEMENT:
				return ((InternalEList<?>)getAchievement()).basicRemove(otherEnd, msgs);
			case MetamodelPackage.GAMIFY__SOCIALMECHANIC:
				return ((InternalEList<?>)getSocialmechanic()).basicRemove(otherEnd, msgs);
			case MetamodelPackage.GAMIFY__GAMEMECHANIC:
				return ((InternalEList<?>)getGamemechanic()).basicRemove(otherEnd, msgs);
			case MetamodelPackage.GAMIFY__SYSTEM:
				return basicSetSystem(null, msgs);
			case MetamodelPackage.GAMIFY__TRIGGER:
				return ((InternalEList<?>)getTrigger()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodelPackage.GAMIFY__ACHIEVEMENT:
				return getAchievement();
			case MetamodelPackage.GAMIFY__SOCIALMECHANIC:
				return getSocialmechanic();
			case MetamodelPackage.GAMIFY__GAMEMECHANIC:
				return getGamemechanic();
			case MetamodelPackage.GAMIFY__SYSTEM:
				return getSystem();
			case MetamodelPackage.GAMIFY__NAME:
				return getName();
			case MetamodelPackage.GAMIFY__TRIGGER:
				return getTrigger();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodelPackage.GAMIFY__ACHIEVEMENT:
				getAchievement().clear();
				getAchievement().addAll((Collection<? extends Achievement>)newValue);
				return;
			case MetamodelPackage.GAMIFY__SOCIALMECHANIC:
				getSocialmechanic().clear();
				getSocialmechanic().addAll((Collection<? extends SocialMechanic>)newValue);
				return;
			case MetamodelPackage.GAMIFY__GAMEMECHANIC:
				getGamemechanic().clear();
				getGamemechanic().addAll((Collection<? extends GameMechanic>)newValue);
				return;
			case MetamodelPackage.GAMIFY__SYSTEM:
				setSystem((metamodel.System)newValue);
				return;
			case MetamodelPackage.GAMIFY__NAME:
				setName((String)newValue);
				return;
			case MetamodelPackage.GAMIFY__TRIGGER:
				getTrigger().clear();
				getTrigger().addAll((Collection<? extends Trigger>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodelPackage.GAMIFY__ACHIEVEMENT:
				getAchievement().clear();
				return;
			case MetamodelPackage.GAMIFY__SOCIALMECHANIC:
				getSocialmechanic().clear();
				return;
			case MetamodelPackage.GAMIFY__GAMEMECHANIC:
				getGamemechanic().clear();
				return;
			case MetamodelPackage.GAMIFY__SYSTEM:
				setSystem((metamodel.System)null);
				return;
			case MetamodelPackage.GAMIFY__NAME:
				setName(NAME_EDEFAULT);
				return;
			case MetamodelPackage.GAMIFY__TRIGGER:
				getTrigger().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodelPackage.GAMIFY__ACHIEVEMENT:
				return achievement != null && !achievement.isEmpty();
			case MetamodelPackage.GAMIFY__SOCIALMECHANIC:
				return socialmechanic != null && !socialmechanic.isEmpty();
			case MetamodelPackage.GAMIFY__GAMEMECHANIC:
				return gamemechanic != null && !gamemechanic.isEmpty();
			case MetamodelPackage.GAMIFY__SYSTEM:
				return system != null;
			case MetamodelPackage.GAMIFY__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case MetamodelPackage.GAMIFY__TRIGGER:
				return trigger != null && !trigger.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case MetamodelPackage.GAMIFY___GET_ALL_CONDITIONS:
				return getAllConditions();
			case MetamodelPackage.GAMIFY___GET_ALL_ACTIONS:
				return getAllActions();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //GamifyImpl
