/**
 */
package metamodel.impl;

import java.util.Collection;

import metamodel.Achievement;
import metamodel.Condition;
import metamodel.MetamodelPackage;
import metamodel.Reward;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Achievement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link metamodel.impl.AchievementImpl#getCondition <em>Condition</em>}</li>
 *   <li>{@link metamodel.impl.AchievementImpl#getReward <em>Reward</em>}</li>
 *   <li>{@link metamodel.impl.AchievementImpl#getName <em>Name</em>}</li>
 *   <li>{@link metamodel.impl.AchievementImpl#getAchievementId <em>Achievement Id</em>}</li>
 *   <li>{@link metamodel.impl.AchievementImpl#isIsMilestone <em>Is Milestone</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AchievementImpl extends MinimalEObjectImpl.Container implements Achievement {
	/**
	 * The cached value of the '{@link #getCondition() <em>Condition</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCondition()
	 * @generated
	 * @ordered
	 */
	protected EList<Condition> condition;

	/**
	 * The cached value of the '{@link #getReward() <em>Reward</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReward()
	 * @generated
	 * @ordered
	 */
	protected EList<Reward> reward;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getAchievementId() <em>Achievement Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAchievementId()
	 * @generated
	 * @ordered
	 */
	protected static final int ACHIEVEMENT_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getAchievementId() <em>Achievement Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAchievementId()
	 * @generated
	 * @ordered
	 */
	protected int achievementId = ACHIEVEMENT_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #isIsMilestone() <em>Is Milestone</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsMilestone()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_MILESTONE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsMilestone() <em>Is Milestone</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsMilestone()
	 * @generated
	 * @ordered
	 */
	protected boolean isMilestone = IS_MILESTONE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AchievementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodelPackage.Literals.ACHIEVEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Condition> getCondition() {
		if (condition == null) {
			condition = new EObjectContainmentEList<Condition>(Condition.class, this, MetamodelPackage.ACHIEVEMENT__CONDITION);
		}
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Reward> getReward() {
		if (reward == null) {
			reward = new EObjectContainmentEList<Reward>(Reward.class, this, MetamodelPackage.ACHIEVEMENT__REWARD);
		}
		return reward;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.ACHIEVEMENT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getAchievementId() {
		return achievementId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAchievementId(int newAchievementId) {
		int oldAchievementId = achievementId;
		achievementId = newAchievementId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.ACHIEVEMENT__ACHIEVEMENT_ID, oldAchievementId, achievementId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsMilestone() {
		return isMilestone;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsMilestone(boolean newIsMilestone) {
		boolean oldIsMilestone = isMilestone;
		isMilestone = newIsMilestone;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.ACHIEVEMENT__IS_MILESTONE, oldIsMilestone, isMilestone));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodelPackage.ACHIEVEMENT__CONDITION:
				return ((InternalEList<?>)getCondition()).basicRemove(otherEnd, msgs);
			case MetamodelPackage.ACHIEVEMENT__REWARD:
				return ((InternalEList<?>)getReward()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodelPackage.ACHIEVEMENT__CONDITION:
				return getCondition();
			case MetamodelPackage.ACHIEVEMENT__REWARD:
				return getReward();
			case MetamodelPackage.ACHIEVEMENT__NAME:
				return getName();
			case MetamodelPackage.ACHIEVEMENT__ACHIEVEMENT_ID:
				return getAchievementId();
			case MetamodelPackage.ACHIEVEMENT__IS_MILESTONE:
				return isIsMilestone();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodelPackage.ACHIEVEMENT__CONDITION:
				getCondition().clear();
				getCondition().addAll((Collection<? extends Condition>)newValue);
				return;
			case MetamodelPackage.ACHIEVEMENT__REWARD:
				getReward().clear();
				getReward().addAll((Collection<? extends Reward>)newValue);
				return;
			case MetamodelPackage.ACHIEVEMENT__NAME:
				setName((String)newValue);
				return;
			case MetamodelPackage.ACHIEVEMENT__ACHIEVEMENT_ID:
				setAchievementId((Integer)newValue);
				return;
			case MetamodelPackage.ACHIEVEMENT__IS_MILESTONE:
				setIsMilestone((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodelPackage.ACHIEVEMENT__CONDITION:
				getCondition().clear();
				return;
			case MetamodelPackage.ACHIEVEMENT__REWARD:
				getReward().clear();
				return;
			case MetamodelPackage.ACHIEVEMENT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case MetamodelPackage.ACHIEVEMENT__ACHIEVEMENT_ID:
				setAchievementId(ACHIEVEMENT_ID_EDEFAULT);
				return;
			case MetamodelPackage.ACHIEVEMENT__IS_MILESTONE:
				setIsMilestone(IS_MILESTONE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodelPackage.ACHIEVEMENT__CONDITION:
				return condition != null && !condition.isEmpty();
			case MetamodelPackage.ACHIEVEMENT__REWARD:
				return reward != null && !reward.isEmpty();
			case MetamodelPackage.ACHIEVEMENT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case MetamodelPackage.ACHIEVEMENT__ACHIEVEMENT_ID:
				return achievementId != ACHIEVEMENT_ID_EDEFAULT;
			case MetamodelPackage.ACHIEVEMENT__IS_MILESTONE:
				return isMilestone != IS_MILESTONE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", achievementId: ");
		result.append(achievementId);
		result.append(", isMilestone: ");
		result.append(isMilestone);
		result.append(')');
		return result.toString();
	}

} //AchievementImpl
