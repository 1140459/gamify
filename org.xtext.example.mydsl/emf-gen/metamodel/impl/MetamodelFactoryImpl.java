/**
 */
package metamodel.impl;

import metamodel.Achievement;
import metamodel.Action;
import metamodel.ActionTypes;
import metamodel.Badge;
import metamodel.Condition;
import metamodel.ConditionTypes;
import metamodel.GameMechanic;
import metamodel.Gamify;
import metamodel.Leaderboard;
import metamodel.LeaderboardTypes;
import metamodel.MetamodelFactory;
import metamodel.MetamodelPackage;
import metamodel.Milestone;
import metamodel.Objective;
import metamodel.PointSum;
import metamodel.Points;
import metamodel.Prize;
import metamodel.Rank;
import metamodel.Restriction;
import metamodel.Reward;
import metamodel.RewardTypes;
import metamodel.Rule;
import metamodel.SocialMechanic;
import metamodel.SystemAttributes;
import metamodel.Trigger;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MetamodelFactoryImpl extends EFactoryImpl implements MetamodelFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MetamodelFactory init() {
		try {
			MetamodelFactory theMetamodelFactory = (MetamodelFactory)EPackage.Registry.INSTANCE.getEFactory(MetamodelPackage.eNS_URI);
			if (theMetamodelFactory != null) {
				return theMetamodelFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new MetamodelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetamodelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case MetamodelPackage.REWARD: return createReward();
			case MetamodelPackage.CONDITION: return createCondition();
			case MetamodelPackage.SOCIAL_MECHANIC: return createSocialMechanic();
			case MetamodelPackage.SYSTEM: return createSystem();
			case MetamodelPackage.GAME_MECHANIC: return createGameMechanic();
			case MetamodelPackage.GAMIFY: return createGamify();
			case MetamodelPackage.TRIGGER: return createTrigger();
			case MetamodelPackage.ACTION: return createAction();
			case MetamodelPackage.ACHIEVEMENT: return createAchievement();
			case MetamodelPackage.RULE: return createRule();
			case MetamodelPackage.RESTRICTION: return createRestriction();
			case MetamodelPackage.OBJECTIVE: return createObjective();
			case MetamodelPackage.RANK: return createRank();
			case MetamodelPackage.BADGE: return createBadge();
			case MetamodelPackage.POINTS: return createPoints();
			case MetamodelPackage.PRIZE: return createPrize();
			case MetamodelPackage.LEADERBOARD: return createLeaderboard();
			case MetamodelPackage.POINT_SUM: return createPointSum();
			case MetamodelPackage.MILESTONE: return createMilestone();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case MetamodelPackage.ACTION_TYPES:
				return createActionTypesFromString(eDataType, initialValue);
			case MetamodelPackage.CONDITION_TYPES:
				return createConditionTypesFromString(eDataType, initialValue);
			case MetamodelPackage.REWARD_TYPES:
				return createRewardTypesFromString(eDataType, initialValue);
			case MetamodelPackage.LEADERBOARD_TYPES:
				return createLeaderboardTypesFromString(eDataType, initialValue);
			case MetamodelPackage.SYSTEM_ATTRIBUTES:
				return createSystemAttributesFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case MetamodelPackage.ACTION_TYPES:
				return convertActionTypesToString(eDataType, instanceValue);
			case MetamodelPackage.CONDITION_TYPES:
				return convertConditionTypesToString(eDataType, instanceValue);
			case MetamodelPackage.REWARD_TYPES:
				return convertRewardTypesToString(eDataType, instanceValue);
			case MetamodelPackage.LEADERBOARD_TYPES:
				return convertLeaderboardTypesToString(eDataType, instanceValue);
			case MetamodelPackage.SYSTEM_ATTRIBUTES:
				return convertSystemAttributesToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reward createReward() {
		RewardImpl reward = new RewardImpl();
		return reward;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Condition createCondition() {
		ConditionImpl condition = new ConditionImpl();
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SocialMechanic createSocialMechanic() {
		SocialMechanicImpl socialMechanic = new SocialMechanicImpl();
		return socialMechanic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public metamodel.System createSystem() {
		SystemImpl system = new SystemImpl();
		return system;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GameMechanic createGameMechanic() {
		GameMechanicImpl gameMechanic = new GameMechanicImpl();
		return gameMechanic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Gamify createGamify() {
		GamifyImpl gamify = new GamifyImpl();
		return gamify;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Trigger createTrigger() {
		TriggerImpl trigger = new TriggerImpl();
		return trigger;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action createAction() {
		ActionImpl action = new ActionImpl();
		return action;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Achievement createAchievement() {
		AchievementImpl achievement = new AchievementImpl();
		return achievement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Rule createRule() {
		RuleImpl rule = new RuleImpl();
		return rule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Restriction createRestriction() {
		RestrictionImpl restriction = new RestrictionImpl();
		return restriction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Objective createObjective() {
		ObjectiveImpl objective = new ObjectiveImpl();
		return objective;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Rank createRank() {
		RankImpl rank = new RankImpl();
		return rank;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Badge createBadge() {
		BadgeImpl badge = new BadgeImpl();
		return badge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Points createPoints() {
		PointsImpl points = new PointsImpl();
		return points;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Prize createPrize() {
		PrizeImpl prize = new PrizeImpl();
		return prize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Leaderboard createLeaderboard() {
		LeaderboardImpl leaderboard = new LeaderboardImpl();
		return leaderboard;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PointSum createPointSum() {
		PointSumImpl pointSum = new PointSumImpl();
		return pointSum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Milestone createMilestone() {
		MilestoneImpl milestone = new MilestoneImpl();
		return milestone;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionTypes createActionTypesFromString(EDataType eDataType, String initialValue) {
		ActionTypes result = ActionTypes.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertActionTypesToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionTypes createConditionTypesFromString(EDataType eDataType, String initialValue) {
		ConditionTypes result = ConditionTypes.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertConditionTypesToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RewardTypes createRewardTypesFromString(EDataType eDataType, String initialValue) {
		RewardTypes result = RewardTypes.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRewardTypesToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeaderboardTypes createLeaderboardTypesFromString(EDataType eDataType, String initialValue) {
		LeaderboardTypes result = LeaderboardTypes.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLeaderboardTypesToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemAttributes createSystemAttributesFromString(EDataType eDataType, String initialValue) {
		SystemAttributes result = SystemAttributes.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSystemAttributesToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetamodelPackage getMetamodelPackage() {
		return (MetamodelPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static MetamodelPackage getPackage() {
		return MetamodelPackage.eINSTANCE;
	}

} //MetamodelFactoryImpl
