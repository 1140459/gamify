/**
 */
package metamodel;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Reward Types</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see metamodel.MetamodelPackage#getRewardTypes()
 * @model
 * @generated
 */
public enum RewardTypes implements Enumerator {
	/**
	 * The '<em><b>RANK</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RANK_VALUE
	 * @generated
	 * @ordered
	 */
	RANK(0, "RANK", "RANK"),

	/**
	 * The '<em><b>BADGE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BADGE_VALUE
	 * @generated
	 * @ordered
	 */
	BADGE(1, "BADGE", "BADGE"),

	/**
	 * The '<em><b>POINTS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #POINTS_VALUE
	 * @generated
	 * @ordered
	 */
	POINTS(2, "POINTS", "POINTS"),

	/**
	 * The '<em><b>PRIZE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PRIZE_VALUE
	 * @generated
	 * @ordered
	 */
	PRIZE(3, "PRIZE", "PRIZE"),

	/**
	 * The '<em><b>MILESTONE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MILESTONE_VALUE
	 * @generated
	 * @ordered
	 */
	MILESTONE(4, "MILESTONE", "MILESTONE");

	/**
	 * The '<em><b>RANK</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>RANK</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #RANK
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int RANK_VALUE = 0;

	/**
	 * The '<em><b>BADGE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BADGE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BADGE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BADGE_VALUE = 1;

	/**
	 * The '<em><b>POINTS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>POINTS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #POINTS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int POINTS_VALUE = 2;

	/**
	 * The '<em><b>PRIZE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PRIZE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PRIZE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PRIZE_VALUE = 3;

	/**
	 * The '<em><b>MILESTONE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MILESTONE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MILESTONE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MILESTONE_VALUE = 4;

	/**
	 * An array of all the '<em><b>Reward Types</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final RewardTypes[] VALUES_ARRAY =
		new RewardTypes[] {
			RANK,
			BADGE,
			POINTS,
			PRIZE,
			MILESTONE,
		};

	/**
	 * A public read-only list of all the '<em><b>Reward Types</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<RewardTypes> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Reward Types</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static RewardTypes get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			RewardTypes result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Reward Types</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static RewardTypes getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			RewardTypes result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Reward Types</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static RewardTypes get(int value) {
		switch (value) {
			case RANK_VALUE: return RANK;
			case BADGE_VALUE: return BADGE;
			case POINTS_VALUE: return POINTS;
			case PRIZE_VALUE: return PRIZE;
			case MILESTONE_VALUE: return MILESTONE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private RewardTypes(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //RewardTypes
