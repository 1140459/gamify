/**
 */
package metamodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trigger</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link metamodel.Trigger#getProgress <em>Progress</em>}</li>
 *   <li>{@link metamodel.Trigger#getConditions <em>Conditions</em>}</li>
 *   <li>{@link metamodel.Trigger#getActions <em>Actions</em>}</li>
 * </ul>
 *
 * @see metamodel.MetamodelPackage#getTrigger()
 * @model
 * @generated
 */
public interface Trigger extends EObject {
	/**
	 * Returns the value of the '<em><b>Progress</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Progress</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Progress</em>' attribute.
	 * @see #setProgress(int)
	 * @see metamodel.MetamodelPackage#getTrigger_Progress()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Int"
	 * @generated
	 */
	int getProgress();

	/**
	 * Sets the value of the '{@link metamodel.Trigger#getProgress <em>Progress</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Progress</em>' attribute.
	 * @see #getProgress()
	 * @generated
	 */
	void setProgress(int value);

	/**
	 * Returns the value of the '<em><b>Conditions</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conditions</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conditions</em>' reference.
	 * @see #setConditions(Gamify)
	 * @see metamodel.MetamodelPackage#getTrigger_Conditions()
	 * @model
	 * @generated
	 */
	Gamify getConditions();

	/**
	 * Sets the value of the '{@link metamodel.Trigger#getConditions <em>Conditions</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Conditions</em>' reference.
	 * @see #getConditions()
	 * @generated
	 */
	void setConditions(Gamify value);

	/**
	 * Returns the value of the '<em><b>Actions</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actions</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actions</em>' reference.
	 * @see #setActions(Gamify)
	 * @see metamodel.MetamodelPackage#getTrigger_Actions()
	 * @model
	 * @generated
	 */
	Gamify getActions();

	/**
	 * Sets the value of the '{@link metamodel.Trigger#getActions <em>Actions</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Actions</em>' reference.
	 * @see #getActions()
	 * @generated
	 */
	void setActions(Gamify value);

} // Trigger
