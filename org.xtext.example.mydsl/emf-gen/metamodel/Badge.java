/**
 */
package metamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Badge</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see metamodel.MetamodelPackage#getBadge()
 * @model
 * @generated
 */
public interface Badge extends Reward {
} // Badge
