/**
 */
package metamodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>System</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link metamodel.System#getAttributes <em>Attributes</em>}</li>
 * </ul>
 *
 * @see metamodel.MetamodelPackage#getSystem()
 * @model
 * @generated
 */
public interface System extends EObject {
	/**
	 * Returns the value of the '<em><b>Attributes</b></em>' attribute list.
	 * The list contents are of type {@link metamodel.SystemAttributes}.
	 * The literals are from the enumeration {@link metamodel.SystemAttributes}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attributes</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attributes</em>' attribute list.
	 * @see metamodel.SystemAttributes
	 * @see metamodel.MetamodelPackage#getSystem_Attributes()
	 * @model
	 * @generated
	 */
	EList<SystemAttributes> getAttributes();

} // System
