/**
 * generated by Xtext 2.16.0
 */
package org.xtext.gamify.dsl.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Random Reward</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.gamify.dsl.model.RandomReward#getDropRate <em>Drop Rate</em>}</li>
 *   <li>{@link org.xtext.gamify.dsl.model.RandomReward#isIsVisible <em>Is Visible</em>}</li>
 * </ul>
 *
 * @see org.xtext.gamify.dsl.model.ModelPackage#getRandomReward()
 * @model
 * @generated
 */
public interface RandomReward extends Reward
{
  /**
   * Returns the value of the '<em><b>Drop Rate</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Drop Rate</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Drop Rate</em>' attribute.
   * @see #setDropRate(double)
   * @see org.xtext.gamify.dsl.model.ModelPackage#getRandomReward_DropRate()
   * @model
   * @generated
   */
  double getDropRate();

  /**
   * Sets the value of the '{@link org.xtext.gamify.dsl.model.RandomReward#getDropRate <em>Drop Rate</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Drop Rate</em>' attribute.
   * @see #getDropRate()
   * @generated
   */
  void setDropRate(double value);

  /**
   * Returns the value of the '<em><b>Is Visible</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Is Visible</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Is Visible</em>' attribute.
   * @see #setIsVisible(boolean)
   * @see org.xtext.gamify.dsl.model.ModelPackage#getRandomReward_IsVisible()
   * @model
   * @generated
   */
  boolean isIsVisible();

  /**
   * Sets the value of the '{@link org.xtext.gamify.dsl.model.RandomReward#isIsVisible <em>Is Visible</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Is Visible</em>' attribute.
   * @see #isIsVisible()
   * @generated
   */
  void setIsVisible(boolean value);

} // RandomReward
