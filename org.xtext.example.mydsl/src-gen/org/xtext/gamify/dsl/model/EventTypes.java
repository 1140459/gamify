/**
 * generated by Xtext 2.16.0
 */
package org.xtext.gamify.dsl.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Event Types</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.xtext.gamify.dsl.model.ModelPackage#getEventTypes()
 * @model
 * @generated
 */
public enum EventTypes implements Enumerator
{
  /**
   * The '<em><b>CLICK</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #CLICK_VALUE
   * @generated
   * @ordered
   */
  CLICK(0, "CLICK", "CLICK"),

  /**
   * The '<em><b>HOVER</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #HOVER_VALUE
   * @generated
   * @ordered
   */
  HOVER(1, "HOVER", "HOVER"),

  /**
   * The '<em><b>VIEW</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #VIEW_VALUE
   * @generated
   * @ordered
   */
  VIEW(2, "VIEW", "VIEW"),

  /**
   * The '<em><b>DRAG</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #DRAG_VALUE
   * @generated
   * @ordered
   */
  DRAG(3, "DRAG", "DRAG"),

  /**
   * The '<em><b>DROP</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #DROP_VALUE
   * @generated
   * @ordered
   */
  DROP(4, "DROP", "DROP"),

  /**
   * The '<em><b>DSUCCESS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #DSUCCESS_VALUE
   * @generated
   * @ordered
   */
  DSUCCESS(5, "DSUCCESS", "DSUCCESS"),

  /**
   * The '<em><b>MSUCCESS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #MSUCCESS_VALUE
   * @generated
   * @ordered
   */
  MSUCCESS(6, "MSUCCESS", "MSUCCESS"),

  /**
   * The '<em><b>DFAILURE</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #DFAILURE_VALUE
   * @generated
   * @ordered
   */
  DFAILURE(7, "DFAILURE", "DFAILURE"),

  /**
   * The '<em><b>MFAILURE</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #MFAILURE_VALUE
   * @generated
   * @ordered
   */
  MFAILURE(8, "MFAILURE", "MFAILURE");

  /**
   * The '<em><b>CLICK</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>CLICK</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #CLICK
   * @model
   * @generated
   * @ordered
   */
  public static final int CLICK_VALUE = 0;

  /**
   * The '<em><b>HOVER</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>HOVER</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #HOVER
   * @model
   * @generated
   * @ordered
   */
  public static final int HOVER_VALUE = 1;

  /**
   * The '<em><b>VIEW</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>VIEW</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #VIEW
   * @model
   * @generated
   * @ordered
   */
  public static final int VIEW_VALUE = 2;

  /**
   * The '<em><b>DRAG</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>DRAG</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #DRAG
   * @model
   * @generated
   * @ordered
   */
  public static final int DRAG_VALUE = 3;

  /**
   * The '<em><b>DROP</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>DROP</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #DROP
   * @model
   * @generated
   * @ordered
   */
  public static final int DROP_VALUE = 4;

  /**
   * The '<em><b>DSUCCESS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>DSUCCESS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #DSUCCESS
   * @model
   * @generated
   * @ordered
   */
  public static final int DSUCCESS_VALUE = 5;

  /**
   * The '<em><b>MSUCCESS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>MSUCCESS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #MSUCCESS
   * @model
   * @generated
   * @ordered
   */
  public static final int MSUCCESS_VALUE = 6;

  /**
   * The '<em><b>DFAILURE</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>DFAILURE</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #DFAILURE
   * @model
   * @generated
   * @ordered
   */
  public static final int DFAILURE_VALUE = 7;

  /**
   * The '<em><b>MFAILURE</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>MFAILURE</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #MFAILURE
   * @model
   * @generated
   * @ordered
   */
  public static final int MFAILURE_VALUE = 8;

  /**
   * An array of all the '<em><b>Event Types</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static final EventTypes[] VALUES_ARRAY =
    new EventTypes[]
    {
      CLICK,
      HOVER,
      VIEW,
      DRAG,
      DROP,
      DSUCCESS,
      MSUCCESS,
      DFAILURE,
      MFAILURE,
    };

  /**
   * A public read-only list of all the '<em><b>Event Types</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static final List<EventTypes> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

  /**
   * Returns the '<em><b>Event Types</b></em>' literal with the specified literal value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param literal the literal.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static EventTypes get(String literal)
  {
    for (int i = 0; i < VALUES_ARRAY.length; ++i)
    {
      EventTypes result = VALUES_ARRAY[i];
      if (result.toString().equals(literal))
      {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>Event Types</b></em>' literal with the specified name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param name the name.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static EventTypes getByName(String name)
  {
    for (int i = 0; i < VALUES_ARRAY.length; ++i)
    {
      EventTypes result = VALUES_ARRAY[i];
      if (result.getName().equals(name))
      {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>Event Types</b></em>' literal with the specified integer value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the integer value.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static EventTypes get(int value)
  {
    switch (value)
    {
      case CLICK_VALUE: return CLICK;
      case HOVER_VALUE: return HOVER;
      case VIEW_VALUE: return VIEW;
      case DRAG_VALUE: return DRAG;
      case DROP_VALUE: return DROP;
      case DSUCCESS_VALUE: return DSUCCESS;
      case MSUCCESS_VALUE: return MSUCCESS;
      case DFAILURE_VALUE: return DFAILURE;
      case MFAILURE_VALUE: return MFAILURE;
    }
    return null;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final int value;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String name;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String literal;

  /**
   * Only this class can construct instances.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EventTypes(int value, String name, String literal)
  {
    this.value = value;
    this.name = name;
    this.literal = literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getValue()
  {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLiteral()
  {
    return literal;
  }

  /**
   * Returns the literal value of the enumerator, which is its string representation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    return literal;
  }
  
} //EventTypes
