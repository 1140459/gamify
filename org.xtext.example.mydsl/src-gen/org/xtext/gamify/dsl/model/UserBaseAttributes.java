/**
 * generated by Xtext 2.16.0
 */
package org.xtext.gamify.dsl.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>User Base Attributes</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.xtext.gamify.dsl.model.ModelPackage#getUserBaseAttributes()
 * @model
 * @generated
 */
public enum UserBaseAttributes implements Enumerator
{
  /**
   * The '<em><b>KILLERS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #KILLERS_VALUE
   * @generated
   * @ordered
   */
  KILLERS(0, "KILLERS", "KILLERS"),

  /**
   * The '<em><b>SOCIALIZERS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #SOCIALIZERS_VALUE
   * @generated
   * @ordered
   */
  SOCIALIZERS(1, "SOCIALIZERS", "SOCIALIZERS"),

  /**
   * The '<em><b>EXPLORERS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #EXPLORERS_VALUE
   * @generated
   * @ordered
   */
  EXPLORERS(2, "EXPLORERS", "EXPLORERS"),

  /**
   * The '<em><b>ACHIEVERS</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #ACHIEVERS_VALUE
   * @generated
   * @ordered
   */
  ACHIEVERS(3, "ACHIEVERS", "ACHIEVERS");

  /**
   * The '<em><b>KILLERS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>KILLERS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #KILLERS
   * @model
   * @generated
   * @ordered
   */
  public static final int KILLERS_VALUE = 0;

  /**
   * The '<em><b>SOCIALIZERS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>SOCIALIZERS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #SOCIALIZERS
   * @model
   * @generated
   * @ordered
   */
  public static final int SOCIALIZERS_VALUE = 1;

  /**
   * The '<em><b>EXPLORERS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>EXPLORERS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #EXPLORERS
   * @model
   * @generated
   * @ordered
   */
  public static final int EXPLORERS_VALUE = 2;

  /**
   * The '<em><b>ACHIEVERS</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>ACHIEVERS</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #ACHIEVERS
   * @model
   * @generated
   * @ordered
   */
  public static final int ACHIEVERS_VALUE = 3;

  /**
   * An array of all the '<em><b>User Base Attributes</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static final UserBaseAttributes[] VALUES_ARRAY =
    new UserBaseAttributes[]
    {
      KILLERS,
      SOCIALIZERS,
      EXPLORERS,
      ACHIEVERS,
    };

  /**
   * A public read-only list of all the '<em><b>User Base Attributes</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static final List<UserBaseAttributes> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

  /**
   * Returns the '<em><b>User Base Attributes</b></em>' literal with the specified literal value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param literal the literal.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static UserBaseAttributes get(String literal)
  {
    for (int i = 0; i < VALUES_ARRAY.length; ++i)
    {
      UserBaseAttributes result = VALUES_ARRAY[i];
      if (result.toString().equals(literal))
      {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>User Base Attributes</b></em>' literal with the specified name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param name the name.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static UserBaseAttributes getByName(String name)
  {
    for (int i = 0; i < VALUES_ARRAY.length; ++i)
    {
      UserBaseAttributes result = VALUES_ARRAY[i];
      if (result.getName().equals(name))
      {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>User Base Attributes</b></em>' literal with the specified integer value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the integer value.
   * @return the matching enumerator or <code>null</code>.
   * @generated
   */
  public static UserBaseAttributes get(int value)
  {
    switch (value)
    {
      case KILLERS_VALUE: return KILLERS;
      case SOCIALIZERS_VALUE: return SOCIALIZERS;
      case EXPLORERS_VALUE: return EXPLORERS;
      case ACHIEVERS_VALUE: return ACHIEVERS;
    }
    return null;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final int value;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String name;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String literal;

  /**
   * Only this class can construct instances.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private UserBaseAttributes(int value, String name, String literal)
  {
    this.value = value;
    this.name = name;
    this.literal = literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getValue()
  {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLiteral()
  {
    return literal;
  }

  /**
   * Returns the literal value of the enumerator, which is its string representation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    return literal;
  }
  
} //UserBaseAttributes
