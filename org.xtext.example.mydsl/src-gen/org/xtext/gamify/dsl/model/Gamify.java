/**
 * generated by Xtext 2.16.0
 */
package org.xtext.gamify.dsl.model;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Gamify</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.gamify.dsl.model.Gamify#getName <em>Name</em>}</li>
 *   <li>{@link org.xtext.gamify.dsl.model.Gamify#getSystem <em>System</em>}</li>
 *   <li>{@link org.xtext.gamify.dsl.model.Gamify#getGamedynamics <em>Gamedynamics</em>}</li>
 *   <li>{@link org.xtext.gamify.dsl.model.Gamify#getItems <em>Items</em>}</li>
 *   <li>{@link org.xtext.gamify.dsl.model.Gamify#getInformation <em>Information</em>}</li>
 * </ul>
 *
 * @see org.xtext.gamify.dsl.model.ModelPackage#getGamify()
 * @model
 * @generated
 */
public interface Gamify extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.xtext.gamify.dsl.model.ModelPackage#getGamify_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.xtext.gamify.dsl.model.Gamify#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>System</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>System</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>System</em>' containment reference.
   * @see #setSystem(org.xtext.gamify.dsl.model.System)
   * @see org.xtext.gamify.dsl.model.ModelPackage#getGamify_System()
   * @model containment="true"
   * @generated
   */
  org.xtext.gamify.dsl.model.System getSystem();

  /**
   * Sets the value of the '{@link org.xtext.gamify.dsl.model.Gamify#getSystem <em>System</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>System</em>' containment reference.
   * @see #getSystem()
   * @generated
   */
  void setSystem(org.xtext.gamify.dsl.model.System value);

  /**
   * Returns the value of the '<em><b>Gamedynamics</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.gamify.dsl.model.GameDynamic}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Gamedynamics</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Gamedynamics</em>' containment reference list.
   * @see org.xtext.gamify.dsl.model.ModelPackage#getGamify_Gamedynamics()
   * @model containment="true"
   * @generated
   */
  EList<GameDynamic> getGamedynamics();

  /**
   * Returns the value of the '<em><b>Items</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.gamify.dsl.model.Item}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Items</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Items</em>' containment reference list.
   * @see org.xtext.gamify.dsl.model.ModelPackage#getGamify_Items()
   * @model containment="true"
   * @generated
   */
  EList<Item> getItems();

  /**
   * Returns the value of the '<em><b>Information</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Information</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Information</em>' attribute.
   * @see #setInformation(String)
   * @see org.xtext.gamify.dsl.model.ModelPackage#getGamify_Information()
   * @model
   * @generated
   */
  String getInformation();

  /**
   * Sets the value of the '{@link org.xtext.gamify.dsl.model.Gamify#getInformation <em>Information</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Information</em>' attribute.
   * @see #getInformation()
   * @generated
   */
  void setInformation(String value);

} // Gamify
