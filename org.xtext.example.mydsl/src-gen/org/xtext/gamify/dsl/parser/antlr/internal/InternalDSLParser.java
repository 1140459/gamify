package org.xtext.gamify.dsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.gamify.dsl.services.DSLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDSLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_DOUBLE", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Gamify'", "'{'", "'Name:'", "'System'", "'GameDynamics'", "','", "'}'", "'Items'", "'Information:'", "'.'", "'Attributes:'", "'UserType:'", "'UserAttributes:'", "'GameDynamic'", "'DynamicType:'", "'GameMechanics'", "'Achievements'", "'GameMechanic'", "'MechanicType:'", "'Events'", "'Achievement'", "'Hidden:'", "'RequiredAchievements'", "'('", "')'", "'Rewards'", "'Conditions'", "'Event'", "'EventType:'", "'PointGain:'", "'TriggerConditions'", "'Restrictions'", "'Condition'", "'AmountRequired:'", "'ConditionType:'", "'PreRequirements'", "'Restriction'", "'RuleDescription:'", "'RestrictionType:'", "'Amount:'", "'true'", "'false'", "'RandomReward'", "'DropRate:'", "'IsVisible:'", "'Item:'", "'FixedReward'", "'Badge'", "'RewardDescription:'", "'BadgeLevel:'", "'BadgeImage:'", "'Points'", "'IsCollectible:'", "'Prize'", "'PrizeType:'", "'ESTABLISHED'", "'GROWING'", "'NEW'", "'EMPLOYEE'", "'CUSTOMER'", "'KILLERS'", "'SOCIALIZERS'", "'EXPLORERS'", "'ACHIEVERS'", "'TAKEHOLD'", "'CUSTOM'", "'TREASUREHUNT'", "'REVIEW'", "'BET'", "'PRIZESHOP'", "'LOTTERY'", "'PROFILE'", "'LEADERBOARD'", "'QUEST'", "'PROGRESSBAR'", "'COOPERATION'", "'OPPONENT'", "'COLLECTIBLE'", "'INVENTORY'", "'SOCIALGIFT'", "'AVATAR'", "'CLICK'", "'HOVER'", "'VIEW'", "'DRAG'", "'DROP'", "'DSUCCESS'", "'MSUCCESS'", "'DFAILURE'", "'MFAILURE'", "'EQUAL'", "'MORE'", "'LESS'", "'TIMELIMIT'", "'ACTIONLIMIT'", "'POINTLIMIT'", "'VIRTUAL'", "'PHYSICAL'"
    };
    public static final int T__50=50;
    public static final int T__59=59;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=5;
    public static final int RULE_INT=6;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__67=67;
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__64=64;
    public static final int T__65=65;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__91=91;
    public static final int T__100=100;
    public static final int T__92=92;
    public static final int T__93=93;
    public static final int T__102=102;
    public static final int T__94=94;
    public static final int T__101=101;
    public static final int T__90=90;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__99=99;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__95=95;
    public static final int T__96=96;
    public static final int T__97=97;
    public static final int T__98=98;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=9;
    public static final int RULE_DOUBLE=7;
    public static final int T__77=77;
    public static final int T__78=78;
    public static final int T__79=79;
    public static final int T__73=73;
    public static final int EOF=-1;
    public static final int T__74=74;
    public static final int T__75=75;
    public static final int T__76=76;
    public static final int T__80=80;
    public static final int T__81=81;
    public static final int T__82=82;
    public static final int T__83=83;
    public static final int RULE_WS=10;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__88=88;
    public static final int T__108=108;
    public static final int T__89=89;
    public static final int T__107=107;
    public static final int T__109=109;
    public static final int T__84=84;
    public static final int T__104=104;
    public static final int T__85=85;
    public static final int T__103=103;
    public static final int T__86=86;
    public static final int T__106=106;
    public static final int T__87=87;
    public static final int T__105=105;

    // delegates
    // delegators


        public InternalDSLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDSLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDSLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalDSL.g"; }



     	private DSLGrammarAccess grammarAccess;

        public InternalDSLParser(TokenStream input, DSLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Gamify";
       	}

       	@Override
       	protected DSLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleGamify"
    // InternalDSL.g:65:1: entryRuleGamify returns [EObject current=null] : iv_ruleGamify= ruleGamify EOF ;
    public final EObject entryRuleGamify() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGamify = null;


        try {
            // InternalDSL.g:65:47: (iv_ruleGamify= ruleGamify EOF )
            // InternalDSL.g:66:2: iv_ruleGamify= ruleGamify EOF
            {
             newCompositeNode(grammarAccess.getGamifyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGamify=ruleGamify();

            state._fsp--;

             current =iv_ruleGamify; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGamify"


    // $ANTLR start "ruleGamify"
    // InternalDSL.g:72:1: ruleGamify returns [EObject current=null] : (otherlv_0= 'Gamify' otherlv_1= '{' (otherlv_2= 'Name:' ( (lv_name_3_0= ruleEString ) ) )? (otherlv_4= 'System' ( (lv_system_5_0= ruleSystem ) ) )? otherlv_6= 'GameDynamics' otherlv_7= '{' ( (lv_gamedynamics_8_0= ruleGameDynamic ) ) (otherlv_9= ',' ( (lv_gamedynamics_10_0= ruleGameDynamic ) ) )* otherlv_11= '}' otherlv_12= 'Items' otherlv_13= '{' ( (lv_items_14_0= ruleItem ) ) (otherlv_15= ',' ( (lv_items_16_0= ruleItem ) ) )* otherlv_17= '}' (otherlv_18= 'Information:' ( (lv_information_19_0= RULE_STRING ) ) )? otherlv_20= '}' ) ;
    public final EObject ruleGamify() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token lv_information_19_0=null;
        Token otherlv_20=null;
        AntlrDatatypeRuleToken lv_name_3_0 = null;

        EObject lv_system_5_0 = null;

        EObject lv_gamedynamics_8_0 = null;

        EObject lv_gamedynamics_10_0 = null;

        EObject lv_items_14_0 = null;

        EObject lv_items_16_0 = null;



        	enterRule();

        try {
            // InternalDSL.g:78:2: ( (otherlv_0= 'Gamify' otherlv_1= '{' (otherlv_2= 'Name:' ( (lv_name_3_0= ruleEString ) ) )? (otherlv_4= 'System' ( (lv_system_5_0= ruleSystem ) ) )? otherlv_6= 'GameDynamics' otherlv_7= '{' ( (lv_gamedynamics_8_0= ruleGameDynamic ) ) (otherlv_9= ',' ( (lv_gamedynamics_10_0= ruleGameDynamic ) ) )* otherlv_11= '}' otherlv_12= 'Items' otherlv_13= '{' ( (lv_items_14_0= ruleItem ) ) (otherlv_15= ',' ( (lv_items_16_0= ruleItem ) ) )* otherlv_17= '}' (otherlv_18= 'Information:' ( (lv_information_19_0= RULE_STRING ) ) )? otherlv_20= '}' ) )
            // InternalDSL.g:79:2: (otherlv_0= 'Gamify' otherlv_1= '{' (otherlv_2= 'Name:' ( (lv_name_3_0= ruleEString ) ) )? (otherlv_4= 'System' ( (lv_system_5_0= ruleSystem ) ) )? otherlv_6= 'GameDynamics' otherlv_7= '{' ( (lv_gamedynamics_8_0= ruleGameDynamic ) ) (otherlv_9= ',' ( (lv_gamedynamics_10_0= ruleGameDynamic ) ) )* otherlv_11= '}' otherlv_12= 'Items' otherlv_13= '{' ( (lv_items_14_0= ruleItem ) ) (otherlv_15= ',' ( (lv_items_16_0= ruleItem ) ) )* otherlv_17= '}' (otherlv_18= 'Information:' ( (lv_information_19_0= RULE_STRING ) ) )? otherlv_20= '}' )
            {
            // InternalDSL.g:79:2: (otherlv_0= 'Gamify' otherlv_1= '{' (otherlv_2= 'Name:' ( (lv_name_3_0= ruleEString ) ) )? (otherlv_4= 'System' ( (lv_system_5_0= ruleSystem ) ) )? otherlv_6= 'GameDynamics' otherlv_7= '{' ( (lv_gamedynamics_8_0= ruleGameDynamic ) ) (otherlv_9= ',' ( (lv_gamedynamics_10_0= ruleGameDynamic ) ) )* otherlv_11= '}' otherlv_12= 'Items' otherlv_13= '{' ( (lv_items_14_0= ruleItem ) ) (otherlv_15= ',' ( (lv_items_16_0= ruleItem ) ) )* otherlv_17= '}' (otherlv_18= 'Information:' ( (lv_information_19_0= RULE_STRING ) ) )? otherlv_20= '}' )
            // InternalDSL.g:80:3: otherlv_0= 'Gamify' otherlv_1= '{' (otherlv_2= 'Name:' ( (lv_name_3_0= ruleEString ) ) )? (otherlv_4= 'System' ( (lv_system_5_0= ruleSystem ) ) )? otherlv_6= 'GameDynamics' otherlv_7= '{' ( (lv_gamedynamics_8_0= ruleGameDynamic ) ) (otherlv_9= ',' ( (lv_gamedynamics_10_0= ruleGameDynamic ) ) )* otherlv_11= '}' otherlv_12= 'Items' otherlv_13= '{' ( (lv_items_14_0= ruleItem ) ) (otherlv_15= ',' ( (lv_items_16_0= ruleItem ) ) )* otherlv_17= '}' (otherlv_18= 'Information:' ( (lv_information_19_0= RULE_STRING ) ) )? otherlv_20= '}'
            {
            otherlv_0=(Token)match(input,12,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getGamifyAccess().getGamifyKeyword_0());
            		
            otherlv_1=(Token)match(input,13,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getGamifyAccess().getLeftCurlyBracketKeyword_1());
            		
            // InternalDSL.g:88:3: (otherlv_2= 'Name:' ( (lv_name_3_0= ruleEString ) ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==14) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalDSL.g:89:4: otherlv_2= 'Name:' ( (lv_name_3_0= ruleEString ) )
                    {
                    otherlv_2=(Token)match(input,14,FOLLOW_5); 

                    				newLeafNode(otherlv_2, grammarAccess.getGamifyAccess().getNameKeyword_2_0());
                    			
                    // InternalDSL.g:93:4: ( (lv_name_3_0= ruleEString ) )
                    // InternalDSL.g:94:5: (lv_name_3_0= ruleEString )
                    {
                    // InternalDSL.g:94:5: (lv_name_3_0= ruleEString )
                    // InternalDSL.g:95:6: lv_name_3_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getGamifyAccess().getNameEStringParserRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_name_3_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getGamifyRule());
                    						}
                    						set(
                    							current,
                    							"name",
                    							lv_name_3_0,
                    							"org.xtext.gamify.dsl.DSL.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:113:3: (otherlv_4= 'System' ( (lv_system_5_0= ruleSystem ) ) )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==15) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalDSL.g:114:4: otherlv_4= 'System' ( (lv_system_5_0= ruleSystem ) )
                    {
                    otherlv_4=(Token)match(input,15,FOLLOW_3); 

                    				newLeafNode(otherlv_4, grammarAccess.getGamifyAccess().getSystemKeyword_3_0());
                    			
                    // InternalDSL.g:118:4: ( (lv_system_5_0= ruleSystem ) )
                    // InternalDSL.g:119:5: (lv_system_5_0= ruleSystem )
                    {
                    // InternalDSL.g:119:5: (lv_system_5_0= ruleSystem )
                    // InternalDSL.g:120:6: lv_system_5_0= ruleSystem
                    {

                    						newCompositeNode(grammarAccess.getGamifyAccess().getSystemSystemParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_7);
                    lv_system_5_0=ruleSystem();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getGamifyRule());
                    						}
                    						set(
                    							current,
                    							"system",
                    							lv_system_5_0,
                    							"org.xtext.gamify.dsl.DSL.System");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_6=(Token)match(input,16,FOLLOW_3); 

            			newLeafNode(otherlv_6, grammarAccess.getGamifyAccess().getGameDynamicsKeyword_4());
            		
            otherlv_7=(Token)match(input,13,FOLLOW_8); 

            			newLeafNode(otherlv_7, grammarAccess.getGamifyAccess().getLeftCurlyBracketKeyword_5());
            		
            // InternalDSL.g:146:3: ( (lv_gamedynamics_8_0= ruleGameDynamic ) )
            // InternalDSL.g:147:4: (lv_gamedynamics_8_0= ruleGameDynamic )
            {
            // InternalDSL.g:147:4: (lv_gamedynamics_8_0= ruleGameDynamic )
            // InternalDSL.g:148:5: lv_gamedynamics_8_0= ruleGameDynamic
            {

            					newCompositeNode(grammarAccess.getGamifyAccess().getGamedynamicsGameDynamicParserRuleCall_6_0());
            				
            pushFollow(FOLLOW_9);
            lv_gamedynamics_8_0=ruleGameDynamic();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getGamifyRule());
            					}
            					add(
            						current,
            						"gamedynamics",
            						lv_gamedynamics_8_0,
            						"org.xtext.gamify.dsl.DSL.GameDynamic");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDSL.g:165:3: (otherlv_9= ',' ( (lv_gamedynamics_10_0= ruleGameDynamic ) ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==17) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalDSL.g:166:4: otherlv_9= ',' ( (lv_gamedynamics_10_0= ruleGameDynamic ) )
            	    {
            	    otherlv_9=(Token)match(input,17,FOLLOW_8); 

            	    				newLeafNode(otherlv_9, grammarAccess.getGamifyAccess().getCommaKeyword_7_0());
            	    			
            	    // InternalDSL.g:170:4: ( (lv_gamedynamics_10_0= ruleGameDynamic ) )
            	    // InternalDSL.g:171:5: (lv_gamedynamics_10_0= ruleGameDynamic )
            	    {
            	    // InternalDSL.g:171:5: (lv_gamedynamics_10_0= ruleGameDynamic )
            	    // InternalDSL.g:172:6: lv_gamedynamics_10_0= ruleGameDynamic
            	    {

            	    						newCompositeNode(grammarAccess.getGamifyAccess().getGamedynamicsGameDynamicParserRuleCall_7_1_0());
            	    					
            	    pushFollow(FOLLOW_9);
            	    lv_gamedynamics_10_0=ruleGameDynamic();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getGamifyRule());
            	    						}
            	    						add(
            	    							current,
            	    							"gamedynamics",
            	    							lv_gamedynamics_10_0,
            	    							"org.xtext.gamify.dsl.DSL.GameDynamic");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            otherlv_11=(Token)match(input,18,FOLLOW_10); 

            			newLeafNode(otherlv_11, grammarAccess.getGamifyAccess().getRightCurlyBracketKeyword_8());
            		
            otherlv_12=(Token)match(input,19,FOLLOW_3); 

            			newLeafNode(otherlv_12, grammarAccess.getGamifyAccess().getItemsKeyword_9());
            		
            otherlv_13=(Token)match(input,13,FOLLOW_11); 

            			newLeafNode(otherlv_13, grammarAccess.getGamifyAccess().getLeftCurlyBracketKeyword_10());
            		
            // InternalDSL.g:202:3: ( (lv_items_14_0= ruleItem ) )
            // InternalDSL.g:203:4: (lv_items_14_0= ruleItem )
            {
            // InternalDSL.g:203:4: (lv_items_14_0= ruleItem )
            // InternalDSL.g:204:5: lv_items_14_0= ruleItem
            {

            					newCompositeNode(grammarAccess.getGamifyAccess().getItemsItemParserRuleCall_11_0());
            				
            pushFollow(FOLLOW_9);
            lv_items_14_0=ruleItem();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getGamifyRule());
            					}
            					add(
            						current,
            						"items",
            						lv_items_14_0,
            						"org.xtext.gamify.dsl.DSL.Item");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDSL.g:221:3: (otherlv_15= ',' ( (lv_items_16_0= ruleItem ) ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==17) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalDSL.g:222:4: otherlv_15= ',' ( (lv_items_16_0= ruleItem ) )
            	    {
            	    otherlv_15=(Token)match(input,17,FOLLOW_11); 

            	    				newLeafNode(otherlv_15, grammarAccess.getGamifyAccess().getCommaKeyword_12_0());
            	    			
            	    // InternalDSL.g:226:4: ( (lv_items_16_0= ruleItem ) )
            	    // InternalDSL.g:227:5: (lv_items_16_0= ruleItem )
            	    {
            	    // InternalDSL.g:227:5: (lv_items_16_0= ruleItem )
            	    // InternalDSL.g:228:6: lv_items_16_0= ruleItem
            	    {

            	    						newCompositeNode(grammarAccess.getGamifyAccess().getItemsItemParserRuleCall_12_1_0());
            	    					
            	    pushFollow(FOLLOW_9);
            	    lv_items_16_0=ruleItem();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getGamifyRule());
            	    						}
            	    						add(
            	    							current,
            	    							"items",
            	    							lv_items_16_0,
            	    							"org.xtext.gamify.dsl.DSL.Item");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            otherlv_17=(Token)match(input,18,FOLLOW_12); 

            			newLeafNode(otherlv_17, grammarAccess.getGamifyAccess().getRightCurlyBracketKeyword_13());
            		
            // InternalDSL.g:250:3: (otherlv_18= 'Information:' ( (lv_information_19_0= RULE_STRING ) ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==20) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalDSL.g:251:4: otherlv_18= 'Information:' ( (lv_information_19_0= RULE_STRING ) )
                    {
                    otherlv_18=(Token)match(input,20,FOLLOW_13); 

                    				newLeafNode(otherlv_18, grammarAccess.getGamifyAccess().getInformationKeyword_14_0());
                    			
                    // InternalDSL.g:255:4: ( (lv_information_19_0= RULE_STRING ) )
                    // InternalDSL.g:256:5: (lv_information_19_0= RULE_STRING )
                    {
                    // InternalDSL.g:256:5: (lv_information_19_0= RULE_STRING )
                    // InternalDSL.g:257:6: lv_information_19_0= RULE_STRING
                    {
                    lv_information_19_0=(Token)match(input,RULE_STRING,FOLLOW_14); 

                    						newLeafNode(lv_information_19_0, grammarAccess.getGamifyAccess().getInformationSTRINGTerminalRuleCall_14_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getGamifyRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"information",
                    							lv_information_19_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_20=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_20, grammarAccess.getGamifyAccess().getRightCurlyBracketKeyword_15());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGamify"


    // $ANTLR start "entryRuleEString"
    // InternalDSL.g:282:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalDSL.g:282:47: (iv_ruleEString= ruleEString EOF )
            // InternalDSL.g:283:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalDSL.g:289:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_ID_0= RULE_ID ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;


        	enterRule();

        try {
            // InternalDSL.g:295:2: (this_ID_0= RULE_ID )
            // InternalDSL.g:296:2: this_ID_0= RULE_ID
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            		current.merge(this_ID_0);
            	

            		newLeafNode(this_ID_0, grammarAccess.getEStringAccess().getIDTerminalRuleCall());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleFQNString"
    // InternalDSL.g:306:1: entryRuleFQNString returns [String current=null] : iv_ruleFQNString= ruleFQNString EOF ;
    public final String entryRuleFQNString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleFQNString = null;


        try {
            // InternalDSL.g:306:49: (iv_ruleFQNString= ruleFQNString EOF )
            // InternalDSL.g:307:2: iv_ruleFQNString= ruleFQNString EOF
            {
             newCompositeNode(grammarAccess.getFQNStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFQNString=ruleFQNString();

            state._fsp--;

             current =iv_ruleFQNString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFQNString"


    // $ANTLR start "ruleFQNString"
    // InternalDSL.g:313:1: ruleFQNString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID kw= '.' this_ID_2= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleFQNString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalDSL.g:319:2: ( (this_ID_0= RULE_ID kw= '.' this_ID_2= RULE_ID ) )
            // InternalDSL.g:320:2: (this_ID_0= RULE_ID kw= '.' this_ID_2= RULE_ID )
            {
            // InternalDSL.g:320:2: (this_ID_0= RULE_ID kw= '.' this_ID_2= RULE_ID )
            // InternalDSL.g:321:3: this_ID_0= RULE_ID kw= '.' this_ID_2= RULE_ID
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_15); 

            			current.merge(this_ID_0);
            		

            			newLeafNode(this_ID_0, grammarAccess.getFQNStringAccess().getIDTerminalRuleCall_0());
            		
            kw=(Token)match(input,21,FOLLOW_5); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getFQNStringAccess().getFullStopKeyword_1());
            		
            this_ID_2=(Token)match(input,RULE_ID,FOLLOW_2); 

            			current.merge(this_ID_2);
            		

            			newLeafNode(this_ID_2, grammarAccess.getFQNStringAccess().getIDTerminalRuleCall_2());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFQNString"


    // $ANTLR start "entryRuleItem"
    // InternalDSL.g:344:1: entryRuleItem returns [EObject current=null] : iv_ruleItem= ruleItem EOF ;
    public final EObject entryRuleItem() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleItem = null;


        try {
            // InternalDSL.g:344:45: (iv_ruleItem= ruleItem EOF )
            // InternalDSL.g:345:2: iv_ruleItem= ruleItem EOF
            {
             newCompositeNode(grammarAccess.getItemRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleItem=ruleItem();

            state._fsp--;

             current =iv_ruleItem; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleItem"


    // $ANTLR start "ruleItem"
    // InternalDSL.g:351:1: ruleItem returns [EObject current=null] : (this_Badge_0= ruleBadge | this_Points_1= rulePoints | this_Prize_2= rulePrize ) ;
    public final EObject ruleItem() throws RecognitionException {
        EObject current = null;

        EObject this_Badge_0 = null;

        EObject this_Points_1 = null;

        EObject this_Prize_2 = null;



        	enterRule();

        try {
            // InternalDSL.g:357:2: ( (this_Badge_0= ruleBadge | this_Points_1= rulePoints | this_Prize_2= rulePrize ) )
            // InternalDSL.g:358:2: (this_Badge_0= ruleBadge | this_Points_1= rulePoints | this_Prize_2= rulePrize )
            {
            // InternalDSL.g:358:2: (this_Badge_0= ruleBadge | this_Points_1= rulePoints | this_Prize_2= rulePrize )
            int alt6=3;
            switch ( input.LA(1) ) {
            case 59:
                {
                alt6=1;
                }
                break;
            case 63:
                {
                alt6=2;
                }
                break;
            case 65:
                {
                alt6=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalDSL.g:359:3: this_Badge_0= ruleBadge
                    {

                    			newCompositeNode(grammarAccess.getItemAccess().getBadgeParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Badge_0=ruleBadge();

                    state._fsp--;


                    			current = this_Badge_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalDSL.g:368:3: this_Points_1= rulePoints
                    {

                    			newCompositeNode(grammarAccess.getItemAccess().getPointsParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Points_1=rulePoints();

                    state._fsp--;


                    			current = this_Points_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalDSL.g:377:3: this_Prize_2= rulePrize
                    {

                    			newCompositeNode(grammarAccess.getItemAccess().getPrizeParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_Prize_2=rulePrize();

                    state._fsp--;


                    			current = this_Prize_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleItem"


    // $ANTLR start "entryRuleReward"
    // InternalDSL.g:389:1: entryRuleReward returns [EObject current=null] : iv_ruleReward= ruleReward EOF ;
    public final EObject entryRuleReward() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReward = null;


        try {
            // InternalDSL.g:389:47: (iv_ruleReward= ruleReward EOF )
            // InternalDSL.g:390:2: iv_ruleReward= ruleReward EOF
            {
             newCompositeNode(grammarAccess.getRewardRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleReward=ruleReward();

            state._fsp--;

             current =iv_ruleReward; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReward"


    // $ANTLR start "ruleReward"
    // InternalDSL.g:396:1: ruleReward returns [EObject current=null] : (this_RandomReward_0= ruleRandomReward | this_FixedReward_1= ruleFixedReward ) ;
    public final EObject ruleReward() throws RecognitionException {
        EObject current = null;

        EObject this_RandomReward_0 = null;

        EObject this_FixedReward_1 = null;



        	enterRule();

        try {
            // InternalDSL.g:402:2: ( (this_RandomReward_0= ruleRandomReward | this_FixedReward_1= ruleFixedReward ) )
            // InternalDSL.g:403:2: (this_RandomReward_0= ruleRandomReward | this_FixedReward_1= ruleFixedReward )
            {
            // InternalDSL.g:403:2: (this_RandomReward_0= ruleRandomReward | this_FixedReward_1= ruleFixedReward )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==54) ) {
                alt7=1;
            }
            else if ( (LA7_0==58) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalDSL.g:404:3: this_RandomReward_0= ruleRandomReward
                    {

                    			newCompositeNode(grammarAccess.getRewardAccess().getRandomRewardParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_RandomReward_0=ruleRandomReward();

                    state._fsp--;


                    			current = this_RandomReward_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalDSL.g:413:3: this_FixedReward_1= ruleFixedReward
                    {

                    			newCompositeNode(grammarAccess.getRewardAccess().getFixedRewardParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_FixedReward_1=ruleFixedReward();

                    state._fsp--;


                    			current = this_FixedReward_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReward"


    // $ANTLR start "entryRuleSystem"
    // InternalDSL.g:425:1: entryRuleSystem returns [EObject current=null] : iv_ruleSystem= ruleSystem EOF ;
    public final EObject entryRuleSystem() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSystem = null;


        try {
            // InternalDSL.g:425:47: (iv_ruleSystem= ruleSystem EOF )
            // InternalDSL.g:426:2: iv_ruleSystem= ruleSystem EOF
            {
             newCompositeNode(grammarAccess.getSystemRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSystem=ruleSystem();

            state._fsp--;

             current =iv_ruleSystem; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSystem"


    // $ANTLR start "ruleSystem"
    // InternalDSL.g:432:1: ruleSystem returns [EObject current=null] : ( () otherlv_1= '{' (otherlv_2= 'Attributes:' ( (lv_attributes_3_0= ruleSystemAttributes ) ) )? (otherlv_4= 'UserType:' ( (lv_UserType_5_0= ruleUserTypes ) ) )? (otherlv_6= 'UserAttributes:' ( (lv_UserAttributes_7_0= ruleUserBaseAttributes ) ) )? otherlv_8= '}' ) ;
    public final EObject ruleSystem() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Enumerator lv_attributes_3_0 = null;

        Enumerator lv_UserType_5_0 = null;

        Enumerator lv_UserAttributes_7_0 = null;



        	enterRule();

        try {
            // InternalDSL.g:438:2: ( ( () otherlv_1= '{' (otherlv_2= 'Attributes:' ( (lv_attributes_3_0= ruleSystemAttributes ) ) )? (otherlv_4= 'UserType:' ( (lv_UserType_5_0= ruleUserTypes ) ) )? (otherlv_6= 'UserAttributes:' ( (lv_UserAttributes_7_0= ruleUserBaseAttributes ) ) )? otherlv_8= '}' ) )
            // InternalDSL.g:439:2: ( () otherlv_1= '{' (otherlv_2= 'Attributes:' ( (lv_attributes_3_0= ruleSystemAttributes ) ) )? (otherlv_4= 'UserType:' ( (lv_UserType_5_0= ruleUserTypes ) ) )? (otherlv_6= 'UserAttributes:' ( (lv_UserAttributes_7_0= ruleUserBaseAttributes ) ) )? otherlv_8= '}' )
            {
            // InternalDSL.g:439:2: ( () otherlv_1= '{' (otherlv_2= 'Attributes:' ( (lv_attributes_3_0= ruleSystemAttributes ) ) )? (otherlv_4= 'UserType:' ( (lv_UserType_5_0= ruleUserTypes ) ) )? (otherlv_6= 'UserAttributes:' ( (lv_UserAttributes_7_0= ruleUserBaseAttributes ) ) )? otherlv_8= '}' )
            // InternalDSL.g:440:3: () otherlv_1= '{' (otherlv_2= 'Attributes:' ( (lv_attributes_3_0= ruleSystemAttributes ) ) )? (otherlv_4= 'UserType:' ( (lv_UserType_5_0= ruleUserTypes ) ) )? (otherlv_6= 'UserAttributes:' ( (lv_UserAttributes_7_0= ruleUserBaseAttributes ) ) )? otherlv_8= '}'
            {
            // InternalDSL.g:440:3: ()
            // InternalDSL.g:441:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getSystemAccess().getSystemAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,13,FOLLOW_16); 

            			newLeafNode(otherlv_1, grammarAccess.getSystemAccess().getLeftCurlyBracketKeyword_1());
            		
            // InternalDSL.g:451:3: (otherlv_2= 'Attributes:' ( (lv_attributes_3_0= ruleSystemAttributes ) ) )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==22) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalDSL.g:452:4: otherlv_2= 'Attributes:' ( (lv_attributes_3_0= ruleSystemAttributes ) )
                    {
                    otherlv_2=(Token)match(input,22,FOLLOW_17); 

                    				newLeafNode(otherlv_2, grammarAccess.getSystemAccess().getAttributesKeyword_2_0());
                    			
                    // InternalDSL.g:456:4: ( (lv_attributes_3_0= ruleSystemAttributes ) )
                    // InternalDSL.g:457:5: (lv_attributes_3_0= ruleSystemAttributes )
                    {
                    // InternalDSL.g:457:5: (lv_attributes_3_0= ruleSystemAttributes )
                    // InternalDSL.g:458:6: lv_attributes_3_0= ruleSystemAttributes
                    {

                    						newCompositeNode(grammarAccess.getSystemAccess().getAttributesSystemAttributesEnumRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_18);
                    lv_attributes_3_0=ruleSystemAttributes();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getSystemRule());
                    						}
                    						set(
                    							current,
                    							"attributes",
                    							lv_attributes_3_0,
                    							"org.xtext.gamify.dsl.DSL.SystemAttributes");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:476:3: (otherlv_4= 'UserType:' ( (lv_UserType_5_0= ruleUserTypes ) ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==23) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalDSL.g:477:4: otherlv_4= 'UserType:' ( (lv_UserType_5_0= ruleUserTypes ) )
                    {
                    otherlv_4=(Token)match(input,23,FOLLOW_19); 

                    				newLeafNode(otherlv_4, grammarAccess.getSystemAccess().getUserTypeKeyword_3_0());
                    			
                    // InternalDSL.g:481:4: ( (lv_UserType_5_0= ruleUserTypes ) )
                    // InternalDSL.g:482:5: (lv_UserType_5_0= ruleUserTypes )
                    {
                    // InternalDSL.g:482:5: (lv_UserType_5_0= ruleUserTypes )
                    // InternalDSL.g:483:6: lv_UserType_5_0= ruleUserTypes
                    {

                    						newCompositeNode(grammarAccess.getSystemAccess().getUserTypeUserTypesEnumRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_20);
                    lv_UserType_5_0=ruleUserTypes();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getSystemRule());
                    						}
                    						set(
                    							current,
                    							"UserType",
                    							lv_UserType_5_0,
                    							"org.xtext.gamify.dsl.DSL.UserTypes");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:501:3: (otherlv_6= 'UserAttributes:' ( (lv_UserAttributes_7_0= ruleUserBaseAttributes ) ) )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==24) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalDSL.g:502:4: otherlv_6= 'UserAttributes:' ( (lv_UserAttributes_7_0= ruleUserBaseAttributes ) )
                    {
                    otherlv_6=(Token)match(input,24,FOLLOW_21); 

                    				newLeafNode(otherlv_6, grammarAccess.getSystemAccess().getUserAttributesKeyword_4_0());
                    			
                    // InternalDSL.g:506:4: ( (lv_UserAttributes_7_0= ruleUserBaseAttributes ) )
                    // InternalDSL.g:507:5: (lv_UserAttributes_7_0= ruleUserBaseAttributes )
                    {
                    // InternalDSL.g:507:5: (lv_UserAttributes_7_0= ruleUserBaseAttributes )
                    // InternalDSL.g:508:6: lv_UserAttributes_7_0= ruleUserBaseAttributes
                    {

                    						newCompositeNode(grammarAccess.getSystemAccess().getUserAttributesUserBaseAttributesEnumRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_14);
                    lv_UserAttributes_7_0=ruleUserBaseAttributes();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getSystemRule());
                    						}
                    						set(
                    							current,
                    							"UserAttributes",
                    							lv_UserAttributes_7_0,
                    							"org.xtext.gamify.dsl.DSL.UserBaseAttributes");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_8=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getSystemAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSystem"


    // $ANTLR start "entryRuleGameDynamic"
    // InternalDSL.g:534:1: entryRuleGameDynamic returns [EObject current=null] : iv_ruleGameDynamic= ruleGameDynamic EOF ;
    public final EObject entryRuleGameDynamic() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGameDynamic = null;


        try {
            // InternalDSL.g:534:52: (iv_ruleGameDynamic= ruleGameDynamic EOF )
            // InternalDSL.g:535:2: iv_ruleGameDynamic= ruleGameDynamic EOF
            {
             newCompositeNode(grammarAccess.getGameDynamicRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGameDynamic=ruleGameDynamic();

            state._fsp--;

             current =iv_ruleGameDynamic; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGameDynamic"


    // $ANTLR start "ruleGameDynamic"
    // InternalDSL.g:541:1: ruleGameDynamic returns [EObject current=null] : (otherlv_0= 'GameDynamic' otherlv_1= '{' (otherlv_2= 'Name:' ( (lv_name_3_0= ruleEString ) ) )? (otherlv_4= 'DynamicType:' ( (lv_dynamicType_5_0= ruleDynamicTypes ) ) )? otherlv_6= 'GameMechanics' otherlv_7= '{' ( (lv_gamemechanics_8_0= ruleGameMechanic ) ) (otherlv_9= ',' ( (lv_gamemechanics_10_0= ruleGameMechanic ) ) )* otherlv_11= '}' otherlv_12= 'Achievements' otherlv_13= '{' ( (lv_achievements_14_0= ruleAchievement ) ) (otherlv_15= ',' ( (lv_achievements_16_0= ruleAchievement ) ) )* otherlv_17= '}' (otherlv_18= 'Information:' ( (lv_information_19_0= RULE_STRING ) ) )? otherlv_20= '}' ) ;
    public final EObject ruleGameDynamic() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token lv_information_19_0=null;
        Token otherlv_20=null;
        AntlrDatatypeRuleToken lv_name_3_0 = null;

        Enumerator lv_dynamicType_5_0 = null;

        EObject lv_gamemechanics_8_0 = null;

        EObject lv_gamemechanics_10_0 = null;

        EObject lv_achievements_14_0 = null;

        EObject lv_achievements_16_0 = null;



        	enterRule();

        try {
            // InternalDSL.g:547:2: ( (otherlv_0= 'GameDynamic' otherlv_1= '{' (otherlv_2= 'Name:' ( (lv_name_3_0= ruleEString ) ) )? (otherlv_4= 'DynamicType:' ( (lv_dynamicType_5_0= ruleDynamicTypes ) ) )? otherlv_6= 'GameMechanics' otherlv_7= '{' ( (lv_gamemechanics_8_0= ruleGameMechanic ) ) (otherlv_9= ',' ( (lv_gamemechanics_10_0= ruleGameMechanic ) ) )* otherlv_11= '}' otherlv_12= 'Achievements' otherlv_13= '{' ( (lv_achievements_14_0= ruleAchievement ) ) (otherlv_15= ',' ( (lv_achievements_16_0= ruleAchievement ) ) )* otherlv_17= '}' (otherlv_18= 'Information:' ( (lv_information_19_0= RULE_STRING ) ) )? otherlv_20= '}' ) )
            // InternalDSL.g:548:2: (otherlv_0= 'GameDynamic' otherlv_1= '{' (otherlv_2= 'Name:' ( (lv_name_3_0= ruleEString ) ) )? (otherlv_4= 'DynamicType:' ( (lv_dynamicType_5_0= ruleDynamicTypes ) ) )? otherlv_6= 'GameMechanics' otherlv_7= '{' ( (lv_gamemechanics_8_0= ruleGameMechanic ) ) (otherlv_9= ',' ( (lv_gamemechanics_10_0= ruleGameMechanic ) ) )* otherlv_11= '}' otherlv_12= 'Achievements' otherlv_13= '{' ( (lv_achievements_14_0= ruleAchievement ) ) (otherlv_15= ',' ( (lv_achievements_16_0= ruleAchievement ) ) )* otherlv_17= '}' (otherlv_18= 'Information:' ( (lv_information_19_0= RULE_STRING ) ) )? otherlv_20= '}' )
            {
            // InternalDSL.g:548:2: (otherlv_0= 'GameDynamic' otherlv_1= '{' (otherlv_2= 'Name:' ( (lv_name_3_0= ruleEString ) ) )? (otherlv_4= 'DynamicType:' ( (lv_dynamicType_5_0= ruleDynamicTypes ) ) )? otherlv_6= 'GameMechanics' otherlv_7= '{' ( (lv_gamemechanics_8_0= ruleGameMechanic ) ) (otherlv_9= ',' ( (lv_gamemechanics_10_0= ruleGameMechanic ) ) )* otherlv_11= '}' otherlv_12= 'Achievements' otherlv_13= '{' ( (lv_achievements_14_0= ruleAchievement ) ) (otherlv_15= ',' ( (lv_achievements_16_0= ruleAchievement ) ) )* otherlv_17= '}' (otherlv_18= 'Information:' ( (lv_information_19_0= RULE_STRING ) ) )? otherlv_20= '}' )
            // InternalDSL.g:549:3: otherlv_0= 'GameDynamic' otherlv_1= '{' (otherlv_2= 'Name:' ( (lv_name_3_0= ruleEString ) ) )? (otherlv_4= 'DynamicType:' ( (lv_dynamicType_5_0= ruleDynamicTypes ) ) )? otherlv_6= 'GameMechanics' otherlv_7= '{' ( (lv_gamemechanics_8_0= ruleGameMechanic ) ) (otherlv_9= ',' ( (lv_gamemechanics_10_0= ruleGameMechanic ) ) )* otherlv_11= '}' otherlv_12= 'Achievements' otherlv_13= '{' ( (lv_achievements_14_0= ruleAchievement ) ) (otherlv_15= ',' ( (lv_achievements_16_0= ruleAchievement ) ) )* otherlv_17= '}' (otherlv_18= 'Information:' ( (lv_information_19_0= RULE_STRING ) ) )? otherlv_20= '}'
            {
            otherlv_0=(Token)match(input,25,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getGameDynamicAccess().getGameDynamicKeyword_0());
            		
            otherlv_1=(Token)match(input,13,FOLLOW_22); 

            			newLeafNode(otherlv_1, grammarAccess.getGameDynamicAccess().getLeftCurlyBracketKeyword_1());
            		
            // InternalDSL.g:557:3: (otherlv_2= 'Name:' ( (lv_name_3_0= ruleEString ) ) )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==14) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalDSL.g:558:4: otherlv_2= 'Name:' ( (lv_name_3_0= ruleEString ) )
                    {
                    otherlv_2=(Token)match(input,14,FOLLOW_5); 

                    				newLeafNode(otherlv_2, grammarAccess.getGameDynamicAccess().getNameKeyword_2_0());
                    			
                    // InternalDSL.g:562:4: ( (lv_name_3_0= ruleEString ) )
                    // InternalDSL.g:563:5: (lv_name_3_0= ruleEString )
                    {
                    // InternalDSL.g:563:5: (lv_name_3_0= ruleEString )
                    // InternalDSL.g:564:6: lv_name_3_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getGameDynamicAccess().getNameEStringParserRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_23);
                    lv_name_3_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getGameDynamicRule());
                    						}
                    						set(
                    							current,
                    							"name",
                    							lv_name_3_0,
                    							"org.xtext.gamify.dsl.DSL.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:582:3: (otherlv_4= 'DynamicType:' ( (lv_dynamicType_5_0= ruleDynamicTypes ) ) )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==26) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalDSL.g:583:4: otherlv_4= 'DynamicType:' ( (lv_dynamicType_5_0= ruleDynamicTypes ) )
                    {
                    otherlv_4=(Token)match(input,26,FOLLOW_24); 

                    				newLeafNode(otherlv_4, grammarAccess.getGameDynamicAccess().getDynamicTypeKeyword_3_0());
                    			
                    // InternalDSL.g:587:4: ( (lv_dynamicType_5_0= ruleDynamicTypes ) )
                    // InternalDSL.g:588:5: (lv_dynamicType_5_0= ruleDynamicTypes )
                    {
                    // InternalDSL.g:588:5: (lv_dynamicType_5_0= ruleDynamicTypes )
                    // InternalDSL.g:589:6: lv_dynamicType_5_0= ruleDynamicTypes
                    {

                    						newCompositeNode(grammarAccess.getGameDynamicAccess().getDynamicTypeDynamicTypesEnumRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_25);
                    lv_dynamicType_5_0=ruleDynamicTypes();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getGameDynamicRule());
                    						}
                    						set(
                    							current,
                    							"dynamicType",
                    							lv_dynamicType_5_0,
                    							"org.xtext.gamify.dsl.DSL.DynamicTypes");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_6=(Token)match(input,27,FOLLOW_3); 

            			newLeafNode(otherlv_6, grammarAccess.getGameDynamicAccess().getGameMechanicsKeyword_4());
            		
            otherlv_7=(Token)match(input,13,FOLLOW_26); 

            			newLeafNode(otherlv_7, grammarAccess.getGameDynamicAccess().getLeftCurlyBracketKeyword_5());
            		
            // InternalDSL.g:615:3: ( (lv_gamemechanics_8_0= ruleGameMechanic ) )
            // InternalDSL.g:616:4: (lv_gamemechanics_8_0= ruleGameMechanic )
            {
            // InternalDSL.g:616:4: (lv_gamemechanics_8_0= ruleGameMechanic )
            // InternalDSL.g:617:5: lv_gamemechanics_8_0= ruleGameMechanic
            {

            					newCompositeNode(grammarAccess.getGameDynamicAccess().getGamemechanicsGameMechanicParserRuleCall_6_0());
            				
            pushFollow(FOLLOW_9);
            lv_gamemechanics_8_0=ruleGameMechanic();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getGameDynamicRule());
            					}
            					add(
            						current,
            						"gamemechanics",
            						lv_gamemechanics_8_0,
            						"org.xtext.gamify.dsl.DSL.GameMechanic");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDSL.g:634:3: (otherlv_9= ',' ( (lv_gamemechanics_10_0= ruleGameMechanic ) ) )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==17) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalDSL.g:635:4: otherlv_9= ',' ( (lv_gamemechanics_10_0= ruleGameMechanic ) )
            	    {
            	    otherlv_9=(Token)match(input,17,FOLLOW_26); 

            	    				newLeafNode(otherlv_9, grammarAccess.getGameDynamicAccess().getCommaKeyword_7_0());
            	    			
            	    // InternalDSL.g:639:4: ( (lv_gamemechanics_10_0= ruleGameMechanic ) )
            	    // InternalDSL.g:640:5: (lv_gamemechanics_10_0= ruleGameMechanic )
            	    {
            	    // InternalDSL.g:640:5: (lv_gamemechanics_10_0= ruleGameMechanic )
            	    // InternalDSL.g:641:6: lv_gamemechanics_10_0= ruleGameMechanic
            	    {

            	    						newCompositeNode(grammarAccess.getGameDynamicAccess().getGamemechanicsGameMechanicParserRuleCall_7_1_0());
            	    					
            	    pushFollow(FOLLOW_9);
            	    lv_gamemechanics_10_0=ruleGameMechanic();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getGameDynamicRule());
            	    						}
            	    						add(
            	    							current,
            	    							"gamemechanics",
            	    							lv_gamemechanics_10_0,
            	    							"org.xtext.gamify.dsl.DSL.GameMechanic");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

            otherlv_11=(Token)match(input,18,FOLLOW_27); 

            			newLeafNode(otherlv_11, grammarAccess.getGameDynamicAccess().getRightCurlyBracketKeyword_8());
            		
            otherlv_12=(Token)match(input,28,FOLLOW_3); 

            			newLeafNode(otherlv_12, grammarAccess.getGameDynamicAccess().getAchievementsKeyword_9());
            		
            otherlv_13=(Token)match(input,13,FOLLOW_28); 

            			newLeafNode(otherlv_13, grammarAccess.getGameDynamicAccess().getLeftCurlyBracketKeyword_10());
            		
            // InternalDSL.g:671:3: ( (lv_achievements_14_0= ruleAchievement ) )
            // InternalDSL.g:672:4: (lv_achievements_14_0= ruleAchievement )
            {
            // InternalDSL.g:672:4: (lv_achievements_14_0= ruleAchievement )
            // InternalDSL.g:673:5: lv_achievements_14_0= ruleAchievement
            {

            					newCompositeNode(grammarAccess.getGameDynamicAccess().getAchievementsAchievementParserRuleCall_11_0());
            				
            pushFollow(FOLLOW_9);
            lv_achievements_14_0=ruleAchievement();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getGameDynamicRule());
            					}
            					add(
            						current,
            						"achievements",
            						lv_achievements_14_0,
            						"org.xtext.gamify.dsl.DSL.Achievement");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDSL.g:690:3: (otherlv_15= ',' ( (lv_achievements_16_0= ruleAchievement ) ) )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==17) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalDSL.g:691:4: otherlv_15= ',' ( (lv_achievements_16_0= ruleAchievement ) )
            	    {
            	    otherlv_15=(Token)match(input,17,FOLLOW_28); 

            	    				newLeafNode(otherlv_15, grammarAccess.getGameDynamicAccess().getCommaKeyword_12_0());
            	    			
            	    // InternalDSL.g:695:4: ( (lv_achievements_16_0= ruleAchievement ) )
            	    // InternalDSL.g:696:5: (lv_achievements_16_0= ruleAchievement )
            	    {
            	    // InternalDSL.g:696:5: (lv_achievements_16_0= ruleAchievement )
            	    // InternalDSL.g:697:6: lv_achievements_16_0= ruleAchievement
            	    {

            	    						newCompositeNode(grammarAccess.getGameDynamicAccess().getAchievementsAchievementParserRuleCall_12_1_0());
            	    					
            	    pushFollow(FOLLOW_9);
            	    lv_achievements_16_0=ruleAchievement();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getGameDynamicRule());
            	    						}
            	    						add(
            	    							current,
            	    							"achievements",
            	    							lv_achievements_16_0,
            	    							"org.xtext.gamify.dsl.DSL.Achievement");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

            otherlv_17=(Token)match(input,18,FOLLOW_12); 

            			newLeafNode(otherlv_17, grammarAccess.getGameDynamicAccess().getRightCurlyBracketKeyword_13());
            		
            // InternalDSL.g:719:3: (otherlv_18= 'Information:' ( (lv_information_19_0= RULE_STRING ) ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==20) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalDSL.g:720:4: otherlv_18= 'Information:' ( (lv_information_19_0= RULE_STRING ) )
                    {
                    otherlv_18=(Token)match(input,20,FOLLOW_13); 

                    				newLeafNode(otherlv_18, grammarAccess.getGameDynamicAccess().getInformationKeyword_14_0());
                    			
                    // InternalDSL.g:724:4: ( (lv_information_19_0= RULE_STRING ) )
                    // InternalDSL.g:725:5: (lv_information_19_0= RULE_STRING )
                    {
                    // InternalDSL.g:725:5: (lv_information_19_0= RULE_STRING )
                    // InternalDSL.g:726:6: lv_information_19_0= RULE_STRING
                    {
                    lv_information_19_0=(Token)match(input,RULE_STRING,FOLLOW_14); 

                    						newLeafNode(lv_information_19_0, grammarAccess.getGameDynamicAccess().getInformationSTRINGTerminalRuleCall_14_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getGameDynamicRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"information",
                    							lv_information_19_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_20=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_20, grammarAccess.getGameDynamicAccess().getRightCurlyBracketKeyword_15());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGameDynamic"


    // $ANTLR start "entryRuleGameMechanic"
    // InternalDSL.g:751:1: entryRuleGameMechanic returns [EObject current=null] : iv_ruleGameMechanic= ruleGameMechanic EOF ;
    public final EObject entryRuleGameMechanic() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGameMechanic = null;


        try {
            // InternalDSL.g:751:53: (iv_ruleGameMechanic= ruleGameMechanic EOF )
            // InternalDSL.g:752:2: iv_ruleGameMechanic= ruleGameMechanic EOF
            {
             newCompositeNode(grammarAccess.getGameMechanicRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGameMechanic=ruleGameMechanic();

            state._fsp--;

             current =iv_ruleGameMechanic; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGameMechanic"


    // $ANTLR start "ruleGameMechanic"
    // InternalDSL.g:758:1: ruleGameMechanic returns [EObject current=null] : ( () otherlv_1= 'GameMechanic' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'MechanicType:' ( (lv_mechanicType_6_0= ruleGameMechanicTypes ) ) )? (otherlv_7= 'Events' otherlv_8= '{' ( (lv_events_9_0= ruleEvent ) ) (otherlv_10= ',' ( (lv_events_11_0= ruleEvent ) ) )* otherlv_12= '}' )? (otherlv_13= 'Information:' ( (lv_information_14_0= RULE_STRING ) ) )? otherlv_15= '}' ) ;
    public final EObject ruleGameMechanic() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token lv_information_14_0=null;
        Token otherlv_15=null;
        AntlrDatatypeRuleToken lv_name_4_0 = null;

        Enumerator lv_mechanicType_6_0 = null;

        EObject lv_events_9_0 = null;

        EObject lv_events_11_0 = null;



        	enterRule();

        try {
            // InternalDSL.g:764:2: ( ( () otherlv_1= 'GameMechanic' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'MechanicType:' ( (lv_mechanicType_6_0= ruleGameMechanicTypes ) ) )? (otherlv_7= 'Events' otherlv_8= '{' ( (lv_events_9_0= ruleEvent ) ) (otherlv_10= ',' ( (lv_events_11_0= ruleEvent ) ) )* otherlv_12= '}' )? (otherlv_13= 'Information:' ( (lv_information_14_0= RULE_STRING ) ) )? otherlv_15= '}' ) )
            // InternalDSL.g:765:2: ( () otherlv_1= 'GameMechanic' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'MechanicType:' ( (lv_mechanicType_6_0= ruleGameMechanicTypes ) ) )? (otherlv_7= 'Events' otherlv_8= '{' ( (lv_events_9_0= ruleEvent ) ) (otherlv_10= ',' ( (lv_events_11_0= ruleEvent ) ) )* otherlv_12= '}' )? (otherlv_13= 'Information:' ( (lv_information_14_0= RULE_STRING ) ) )? otherlv_15= '}' )
            {
            // InternalDSL.g:765:2: ( () otherlv_1= 'GameMechanic' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'MechanicType:' ( (lv_mechanicType_6_0= ruleGameMechanicTypes ) ) )? (otherlv_7= 'Events' otherlv_8= '{' ( (lv_events_9_0= ruleEvent ) ) (otherlv_10= ',' ( (lv_events_11_0= ruleEvent ) ) )* otherlv_12= '}' )? (otherlv_13= 'Information:' ( (lv_information_14_0= RULE_STRING ) ) )? otherlv_15= '}' )
            // InternalDSL.g:766:3: () otherlv_1= 'GameMechanic' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'MechanicType:' ( (lv_mechanicType_6_0= ruleGameMechanicTypes ) ) )? (otherlv_7= 'Events' otherlv_8= '{' ( (lv_events_9_0= ruleEvent ) ) (otherlv_10= ',' ( (lv_events_11_0= ruleEvent ) ) )* otherlv_12= '}' )? (otherlv_13= 'Information:' ( (lv_information_14_0= RULE_STRING ) ) )? otherlv_15= '}'
            {
            // InternalDSL.g:766:3: ()
            // InternalDSL.g:767:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getGameMechanicAccess().getGameMechanicAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,29,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getGameMechanicAccess().getGameMechanicKeyword_1());
            		
            otherlv_2=(Token)match(input,13,FOLLOW_29); 

            			newLeafNode(otherlv_2, grammarAccess.getGameMechanicAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalDSL.g:781:3: (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==14) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalDSL.g:782:4: otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,14,FOLLOW_5); 

                    				newLeafNode(otherlv_3, grammarAccess.getGameMechanicAccess().getNameKeyword_3_0());
                    			
                    // InternalDSL.g:786:4: ( (lv_name_4_0= ruleEString ) )
                    // InternalDSL.g:787:5: (lv_name_4_0= ruleEString )
                    {
                    // InternalDSL.g:787:5: (lv_name_4_0= ruleEString )
                    // InternalDSL.g:788:6: lv_name_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getGameMechanicAccess().getNameEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_30);
                    lv_name_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getGameMechanicRule());
                    						}
                    						set(
                    							current,
                    							"name",
                    							lv_name_4_0,
                    							"org.xtext.gamify.dsl.DSL.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:806:3: (otherlv_5= 'MechanicType:' ( (lv_mechanicType_6_0= ruleGameMechanicTypes ) ) )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==30) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalDSL.g:807:4: otherlv_5= 'MechanicType:' ( (lv_mechanicType_6_0= ruleGameMechanicTypes ) )
                    {
                    otherlv_5=(Token)match(input,30,FOLLOW_31); 

                    				newLeafNode(otherlv_5, grammarAccess.getGameMechanicAccess().getMechanicTypeKeyword_4_0());
                    			
                    // InternalDSL.g:811:4: ( (lv_mechanicType_6_0= ruleGameMechanicTypes ) )
                    // InternalDSL.g:812:5: (lv_mechanicType_6_0= ruleGameMechanicTypes )
                    {
                    // InternalDSL.g:812:5: (lv_mechanicType_6_0= ruleGameMechanicTypes )
                    // InternalDSL.g:813:6: lv_mechanicType_6_0= ruleGameMechanicTypes
                    {

                    						newCompositeNode(grammarAccess.getGameMechanicAccess().getMechanicTypeGameMechanicTypesEnumRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_32);
                    lv_mechanicType_6_0=ruleGameMechanicTypes();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getGameMechanicRule());
                    						}
                    						set(
                    							current,
                    							"mechanicType",
                    							lv_mechanicType_6_0,
                    							"org.xtext.gamify.dsl.DSL.GameMechanicTypes");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:831:3: (otherlv_7= 'Events' otherlv_8= '{' ( (lv_events_9_0= ruleEvent ) ) (otherlv_10= ',' ( (lv_events_11_0= ruleEvent ) ) )* otherlv_12= '}' )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==31) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalDSL.g:832:4: otherlv_7= 'Events' otherlv_8= '{' ( (lv_events_9_0= ruleEvent ) ) (otherlv_10= ',' ( (lv_events_11_0= ruleEvent ) ) )* otherlv_12= '}'
                    {
                    otherlv_7=(Token)match(input,31,FOLLOW_3); 

                    				newLeafNode(otherlv_7, grammarAccess.getGameMechanicAccess().getEventsKeyword_5_0());
                    			
                    otherlv_8=(Token)match(input,13,FOLLOW_33); 

                    				newLeafNode(otherlv_8, grammarAccess.getGameMechanicAccess().getLeftCurlyBracketKeyword_5_1());
                    			
                    // InternalDSL.g:840:4: ( (lv_events_9_0= ruleEvent ) )
                    // InternalDSL.g:841:5: (lv_events_9_0= ruleEvent )
                    {
                    // InternalDSL.g:841:5: (lv_events_9_0= ruleEvent )
                    // InternalDSL.g:842:6: lv_events_9_0= ruleEvent
                    {

                    						newCompositeNode(grammarAccess.getGameMechanicAccess().getEventsEventParserRuleCall_5_2_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_events_9_0=ruleEvent();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getGameMechanicRule());
                    						}
                    						add(
                    							current,
                    							"events",
                    							lv_events_9_0,
                    							"org.xtext.gamify.dsl.DSL.Event");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalDSL.g:859:4: (otherlv_10= ',' ( (lv_events_11_0= ruleEvent ) ) )*
                    loop18:
                    do {
                        int alt18=2;
                        int LA18_0 = input.LA(1);

                        if ( (LA18_0==17) ) {
                            alt18=1;
                        }


                        switch (alt18) {
                    	case 1 :
                    	    // InternalDSL.g:860:5: otherlv_10= ',' ( (lv_events_11_0= ruleEvent ) )
                    	    {
                    	    otherlv_10=(Token)match(input,17,FOLLOW_33); 

                    	    					newLeafNode(otherlv_10, grammarAccess.getGameMechanicAccess().getCommaKeyword_5_3_0());
                    	    				
                    	    // InternalDSL.g:864:5: ( (lv_events_11_0= ruleEvent ) )
                    	    // InternalDSL.g:865:6: (lv_events_11_0= ruleEvent )
                    	    {
                    	    // InternalDSL.g:865:6: (lv_events_11_0= ruleEvent )
                    	    // InternalDSL.g:866:7: lv_events_11_0= ruleEvent
                    	    {

                    	    							newCompositeNode(grammarAccess.getGameMechanicAccess().getEventsEventParserRuleCall_5_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_9);
                    	    lv_events_11_0=ruleEvent();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getGameMechanicRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"events",
                    	    								lv_events_11_0,
                    	    								"org.xtext.gamify.dsl.DSL.Event");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop18;
                        }
                    } while (true);

                    otherlv_12=(Token)match(input,18,FOLLOW_12); 

                    				newLeafNode(otherlv_12, grammarAccess.getGameMechanicAccess().getRightCurlyBracketKeyword_5_4());
                    			

                    }
                    break;

            }

            // InternalDSL.g:889:3: (otherlv_13= 'Information:' ( (lv_information_14_0= RULE_STRING ) ) )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==20) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalDSL.g:890:4: otherlv_13= 'Information:' ( (lv_information_14_0= RULE_STRING ) )
                    {
                    otherlv_13=(Token)match(input,20,FOLLOW_13); 

                    				newLeafNode(otherlv_13, grammarAccess.getGameMechanicAccess().getInformationKeyword_6_0());
                    			
                    // InternalDSL.g:894:4: ( (lv_information_14_0= RULE_STRING ) )
                    // InternalDSL.g:895:5: (lv_information_14_0= RULE_STRING )
                    {
                    // InternalDSL.g:895:5: (lv_information_14_0= RULE_STRING )
                    // InternalDSL.g:896:6: lv_information_14_0= RULE_STRING
                    {
                    lv_information_14_0=(Token)match(input,RULE_STRING,FOLLOW_14); 

                    						newLeafNode(lv_information_14_0, grammarAccess.getGameMechanicAccess().getInformationSTRINGTerminalRuleCall_6_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getGameMechanicRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"information",
                    							lv_information_14_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_15=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_15, grammarAccess.getGameMechanicAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGameMechanic"


    // $ANTLR start "entryRuleAchievement"
    // InternalDSL.g:921:1: entryRuleAchievement returns [EObject current=null] : iv_ruleAchievement= ruleAchievement EOF ;
    public final EObject entryRuleAchievement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAchievement = null;


        try {
            // InternalDSL.g:921:52: (iv_ruleAchievement= ruleAchievement EOF )
            // InternalDSL.g:922:2: iv_ruleAchievement= ruleAchievement EOF
            {
             newCompositeNode(grammarAccess.getAchievementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAchievement=ruleAchievement();

            state._fsp--;

             current =iv_ruleAchievement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAchievement"


    // $ANTLR start "ruleAchievement"
    // InternalDSL.g:928:1: ruleAchievement returns [EObject current=null] : (otherlv_0= 'Achievement' otherlv_1= '{' (otherlv_2= 'Name:' ( (lv_name_3_0= ruleEString ) ) )? (otherlv_4= 'Hidden:' ( (lv_hidden_5_0= ruleBoolean ) ) )? (otherlv_6= 'RequiredAchievements' otherlv_7= '(' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* otherlv_11= ')' )? (otherlv_12= 'Rewards' otherlv_13= '{' ( (lv_rewards_14_0= ruleReward ) ) (otherlv_15= ',' ( (lv_rewards_16_0= ruleReward ) ) )* otherlv_17= '}' )? otherlv_18= 'Conditions' otherlv_19= '{' ( (lv_conditions_20_0= ruleCondition ) ) (otherlv_21= ',' ( (lv_conditions_22_0= ruleCondition ) ) )* otherlv_23= '}' (otherlv_24= 'Information:' ( (lv_information_25_0= RULE_STRING ) ) )? otherlv_26= '}' ) ;
    public final EObject ruleAchievement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_23=null;
        Token otherlv_24=null;
        Token lv_information_25_0=null;
        Token otherlv_26=null;
        AntlrDatatypeRuleToken lv_name_3_0 = null;

        AntlrDatatypeRuleToken lv_hidden_5_0 = null;

        EObject lv_rewards_14_0 = null;

        EObject lv_rewards_16_0 = null;

        EObject lv_conditions_20_0 = null;

        EObject lv_conditions_22_0 = null;



        	enterRule();

        try {
            // InternalDSL.g:934:2: ( (otherlv_0= 'Achievement' otherlv_1= '{' (otherlv_2= 'Name:' ( (lv_name_3_0= ruleEString ) ) )? (otherlv_4= 'Hidden:' ( (lv_hidden_5_0= ruleBoolean ) ) )? (otherlv_6= 'RequiredAchievements' otherlv_7= '(' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* otherlv_11= ')' )? (otherlv_12= 'Rewards' otherlv_13= '{' ( (lv_rewards_14_0= ruleReward ) ) (otherlv_15= ',' ( (lv_rewards_16_0= ruleReward ) ) )* otherlv_17= '}' )? otherlv_18= 'Conditions' otherlv_19= '{' ( (lv_conditions_20_0= ruleCondition ) ) (otherlv_21= ',' ( (lv_conditions_22_0= ruleCondition ) ) )* otherlv_23= '}' (otherlv_24= 'Information:' ( (lv_information_25_0= RULE_STRING ) ) )? otherlv_26= '}' ) )
            // InternalDSL.g:935:2: (otherlv_0= 'Achievement' otherlv_1= '{' (otherlv_2= 'Name:' ( (lv_name_3_0= ruleEString ) ) )? (otherlv_4= 'Hidden:' ( (lv_hidden_5_0= ruleBoolean ) ) )? (otherlv_6= 'RequiredAchievements' otherlv_7= '(' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* otherlv_11= ')' )? (otherlv_12= 'Rewards' otherlv_13= '{' ( (lv_rewards_14_0= ruleReward ) ) (otherlv_15= ',' ( (lv_rewards_16_0= ruleReward ) ) )* otherlv_17= '}' )? otherlv_18= 'Conditions' otherlv_19= '{' ( (lv_conditions_20_0= ruleCondition ) ) (otherlv_21= ',' ( (lv_conditions_22_0= ruleCondition ) ) )* otherlv_23= '}' (otherlv_24= 'Information:' ( (lv_information_25_0= RULE_STRING ) ) )? otherlv_26= '}' )
            {
            // InternalDSL.g:935:2: (otherlv_0= 'Achievement' otherlv_1= '{' (otherlv_2= 'Name:' ( (lv_name_3_0= ruleEString ) ) )? (otherlv_4= 'Hidden:' ( (lv_hidden_5_0= ruleBoolean ) ) )? (otherlv_6= 'RequiredAchievements' otherlv_7= '(' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* otherlv_11= ')' )? (otherlv_12= 'Rewards' otherlv_13= '{' ( (lv_rewards_14_0= ruleReward ) ) (otherlv_15= ',' ( (lv_rewards_16_0= ruleReward ) ) )* otherlv_17= '}' )? otherlv_18= 'Conditions' otherlv_19= '{' ( (lv_conditions_20_0= ruleCondition ) ) (otherlv_21= ',' ( (lv_conditions_22_0= ruleCondition ) ) )* otherlv_23= '}' (otherlv_24= 'Information:' ( (lv_information_25_0= RULE_STRING ) ) )? otherlv_26= '}' )
            // InternalDSL.g:936:3: otherlv_0= 'Achievement' otherlv_1= '{' (otherlv_2= 'Name:' ( (lv_name_3_0= ruleEString ) ) )? (otherlv_4= 'Hidden:' ( (lv_hidden_5_0= ruleBoolean ) ) )? (otherlv_6= 'RequiredAchievements' otherlv_7= '(' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* otherlv_11= ')' )? (otherlv_12= 'Rewards' otherlv_13= '{' ( (lv_rewards_14_0= ruleReward ) ) (otherlv_15= ',' ( (lv_rewards_16_0= ruleReward ) ) )* otherlv_17= '}' )? otherlv_18= 'Conditions' otherlv_19= '{' ( (lv_conditions_20_0= ruleCondition ) ) (otherlv_21= ',' ( (lv_conditions_22_0= ruleCondition ) ) )* otherlv_23= '}' (otherlv_24= 'Information:' ( (lv_information_25_0= RULE_STRING ) ) )? otherlv_26= '}'
            {
            otherlv_0=(Token)match(input,32,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getAchievementAccess().getAchievementKeyword_0());
            		
            otherlv_1=(Token)match(input,13,FOLLOW_34); 

            			newLeafNode(otherlv_1, grammarAccess.getAchievementAccess().getLeftCurlyBracketKeyword_1());
            		
            // InternalDSL.g:944:3: (otherlv_2= 'Name:' ( (lv_name_3_0= ruleEString ) ) )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==14) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalDSL.g:945:4: otherlv_2= 'Name:' ( (lv_name_3_0= ruleEString ) )
                    {
                    otherlv_2=(Token)match(input,14,FOLLOW_5); 

                    				newLeafNode(otherlv_2, grammarAccess.getAchievementAccess().getNameKeyword_2_0());
                    			
                    // InternalDSL.g:949:4: ( (lv_name_3_0= ruleEString ) )
                    // InternalDSL.g:950:5: (lv_name_3_0= ruleEString )
                    {
                    // InternalDSL.g:950:5: (lv_name_3_0= ruleEString )
                    // InternalDSL.g:951:6: lv_name_3_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAchievementAccess().getNameEStringParserRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_35);
                    lv_name_3_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAchievementRule());
                    						}
                    						set(
                    							current,
                    							"name",
                    							lv_name_3_0,
                    							"org.xtext.gamify.dsl.DSL.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:969:3: (otherlv_4= 'Hidden:' ( (lv_hidden_5_0= ruleBoolean ) ) )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==33) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalDSL.g:970:4: otherlv_4= 'Hidden:' ( (lv_hidden_5_0= ruleBoolean ) )
                    {
                    otherlv_4=(Token)match(input,33,FOLLOW_36); 

                    				newLeafNode(otherlv_4, grammarAccess.getAchievementAccess().getHiddenKeyword_3_0());
                    			
                    // InternalDSL.g:974:4: ( (lv_hidden_5_0= ruleBoolean ) )
                    // InternalDSL.g:975:5: (lv_hidden_5_0= ruleBoolean )
                    {
                    // InternalDSL.g:975:5: (lv_hidden_5_0= ruleBoolean )
                    // InternalDSL.g:976:6: lv_hidden_5_0= ruleBoolean
                    {

                    						newCompositeNode(grammarAccess.getAchievementAccess().getHiddenBooleanParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_37);
                    lv_hidden_5_0=ruleBoolean();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAchievementRule());
                    						}
                    						set(
                    							current,
                    							"hidden",
                    							lv_hidden_5_0,
                    							"org.xtext.gamify.dsl.DSL.Boolean");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:994:3: (otherlv_6= 'RequiredAchievements' otherlv_7= '(' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* otherlv_11= ')' )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==34) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // InternalDSL.g:995:4: otherlv_6= 'RequiredAchievements' otherlv_7= '(' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* otherlv_11= ')'
                    {
                    otherlv_6=(Token)match(input,34,FOLLOW_38); 

                    				newLeafNode(otherlv_6, grammarAccess.getAchievementAccess().getRequiredAchievementsKeyword_4_0());
                    			
                    otherlv_7=(Token)match(input,35,FOLLOW_5); 

                    				newLeafNode(otherlv_7, grammarAccess.getAchievementAccess().getLeftParenthesisKeyword_4_1());
                    			
                    // InternalDSL.g:1003:4: ( ( ruleEString ) )
                    // InternalDSL.g:1004:5: ( ruleEString )
                    {
                    // InternalDSL.g:1004:5: ( ruleEString )
                    // InternalDSL.g:1005:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAchievementRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getAchievementAccess().getRequiredAchievementsAchievementCrossReference_4_2_0());
                    					
                    pushFollow(FOLLOW_39);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalDSL.g:1019:4: (otherlv_9= ',' ( ( ruleEString ) ) )*
                    loop23:
                    do {
                        int alt23=2;
                        int LA23_0 = input.LA(1);

                        if ( (LA23_0==17) ) {
                            alt23=1;
                        }


                        switch (alt23) {
                    	case 1 :
                    	    // InternalDSL.g:1020:5: otherlv_9= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_9=(Token)match(input,17,FOLLOW_5); 

                    	    					newLeafNode(otherlv_9, grammarAccess.getAchievementAccess().getCommaKeyword_4_3_0());
                    	    				
                    	    // InternalDSL.g:1024:5: ( ( ruleEString ) )
                    	    // InternalDSL.g:1025:6: ( ruleEString )
                    	    {
                    	    // InternalDSL.g:1025:6: ( ruleEString )
                    	    // InternalDSL.g:1026:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getAchievementRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getAchievementAccess().getRequiredAchievementsAchievementCrossReference_4_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_39);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop23;
                        }
                    } while (true);

                    otherlv_11=(Token)match(input,36,FOLLOW_40); 

                    				newLeafNode(otherlv_11, grammarAccess.getAchievementAccess().getRightParenthesisKeyword_4_4());
                    			

                    }
                    break;

            }

            // InternalDSL.g:1046:3: (otherlv_12= 'Rewards' otherlv_13= '{' ( (lv_rewards_14_0= ruleReward ) ) (otherlv_15= ',' ( (lv_rewards_16_0= ruleReward ) ) )* otherlv_17= '}' )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==37) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalDSL.g:1047:4: otherlv_12= 'Rewards' otherlv_13= '{' ( (lv_rewards_14_0= ruleReward ) ) (otherlv_15= ',' ( (lv_rewards_16_0= ruleReward ) ) )* otherlv_17= '}'
                    {
                    otherlv_12=(Token)match(input,37,FOLLOW_3); 

                    				newLeafNode(otherlv_12, grammarAccess.getAchievementAccess().getRewardsKeyword_5_0());
                    			
                    otherlv_13=(Token)match(input,13,FOLLOW_41); 

                    				newLeafNode(otherlv_13, grammarAccess.getAchievementAccess().getLeftCurlyBracketKeyword_5_1());
                    			
                    // InternalDSL.g:1055:4: ( (lv_rewards_14_0= ruleReward ) )
                    // InternalDSL.g:1056:5: (lv_rewards_14_0= ruleReward )
                    {
                    // InternalDSL.g:1056:5: (lv_rewards_14_0= ruleReward )
                    // InternalDSL.g:1057:6: lv_rewards_14_0= ruleReward
                    {

                    						newCompositeNode(grammarAccess.getAchievementAccess().getRewardsRewardParserRuleCall_5_2_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_rewards_14_0=ruleReward();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAchievementRule());
                    						}
                    						add(
                    							current,
                    							"rewards",
                    							lv_rewards_14_0,
                    							"org.xtext.gamify.dsl.DSL.Reward");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalDSL.g:1074:4: (otherlv_15= ',' ( (lv_rewards_16_0= ruleReward ) ) )*
                    loop25:
                    do {
                        int alt25=2;
                        int LA25_0 = input.LA(1);

                        if ( (LA25_0==17) ) {
                            alt25=1;
                        }


                        switch (alt25) {
                    	case 1 :
                    	    // InternalDSL.g:1075:5: otherlv_15= ',' ( (lv_rewards_16_0= ruleReward ) )
                    	    {
                    	    otherlv_15=(Token)match(input,17,FOLLOW_41); 

                    	    					newLeafNode(otherlv_15, grammarAccess.getAchievementAccess().getCommaKeyword_5_3_0());
                    	    				
                    	    // InternalDSL.g:1079:5: ( (lv_rewards_16_0= ruleReward ) )
                    	    // InternalDSL.g:1080:6: (lv_rewards_16_0= ruleReward )
                    	    {
                    	    // InternalDSL.g:1080:6: (lv_rewards_16_0= ruleReward )
                    	    // InternalDSL.g:1081:7: lv_rewards_16_0= ruleReward
                    	    {

                    	    							newCompositeNode(grammarAccess.getAchievementAccess().getRewardsRewardParserRuleCall_5_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_9);
                    	    lv_rewards_16_0=ruleReward();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getAchievementRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"rewards",
                    	    								lv_rewards_16_0,
                    	    								"org.xtext.gamify.dsl.DSL.Reward");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop25;
                        }
                    } while (true);

                    otherlv_17=(Token)match(input,18,FOLLOW_42); 

                    				newLeafNode(otherlv_17, grammarAccess.getAchievementAccess().getRightCurlyBracketKeyword_5_4());
                    			

                    }
                    break;

            }

            otherlv_18=(Token)match(input,38,FOLLOW_3); 

            			newLeafNode(otherlv_18, grammarAccess.getAchievementAccess().getConditionsKeyword_6());
            		
            otherlv_19=(Token)match(input,13,FOLLOW_43); 

            			newLeafNode(otherlv_19, grammarAccess.getAchievementAccess().getLeftCurlyBracketKeyword_7());
            		
            // InternalDSL.g:1112:3: ( (lv_conditions_20_0= ruleCondition ) )
            // InternalDSL.g:1113:4: (lv_conditions_20_0= ruleCondition )
            {
            // InternalDSL.g:1113:4: (lv_conditions_20_0= ruleCondition )
            // InternalDSL.g:1114:5: lv_conditions_20_0= ruleCondition
            {

            					newCompositeNode(grammarAccess.getAchievementAccess().getConditionsConditionParserRuleCall_8_0());
            				
            pushFollow(FOLLOW_9);
            lv_conditions_20_0=ruleCondition();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAchievementRule());
            					}
            					add(
            						current,
            						"conditions",
            						lv_conditions_20_0,
            						"org.xtext.gamify.dsl.DSL.Condition");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDSL.g:1131:3: (otherlv_21= ',' ( (lv_conditions_22_0= ruleCondition ) ) )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==17) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // InternalDSL.g:1132:4: otherlv_21= ',' ( (lv_conditions_22_0= ruleCondition ) )
            	    {
            	    otherlv_21=(Token)match(input,17,FOLLOW_43); 

            	    				newLeafNode(otherlv_21, grammarAccess.getAchievementAccess().getCommaKeyword_9_0());
            	    			
            	    // InternalDSL.g:1136:4: ( (lv_conditions_22_0= ruleCondition ) )
            	    // InternalDSL.g:1137:5: (lv_conditions_22_0= ruleCondition )
            	    {
            	    // InternalDSL.g:1137:5: (lv_conditions_22_0= ruleCondition )
            	    // InternalDSL.g:1138:6: lv_conditions_22_0= ruleCondition
            	    {

            	    						newCompositeNode(grammarAccess.getAchievementAccess().getConditionsConditionParserRuleCall_9_1_0());
            	    					
            	    pushFollow(FOLLOW_9);
            	    lv_conditions_22_0=ruleCondition();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAchievementRule());
            	    						}
            	    						add(
            	    							current,
            	    							"conditions",
            	    							lv_conditions_22_0,
            	    							"org.xtext.gamify.dsl.DSL.Condition");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);

            otherlv_23=(Token)match(input,18,FOLLOW_12); 

            			newLeafNode(otherlv_23, grammarAccess.getAchievementAccess().getRightCurlyBracketKeyword_10());
            		
            // InternalDSL.g:1160:3: (otherlv_24= 'Information:' ( (lv_information_25_0= RULE_STRING ) ) )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==20) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalDSL.g:1161:4: otherlv_24= 'Information:' ( (lv_information_25_0= RULE_STRING ) )
                    {
                    otherlv_24=(Token)match(input,20,FOLLOW_13); 

                    				newLeafNode(otherlv_24, grammarAccess.getAchievementAccess().getInformationKeyword_11_0());
                    			
                    // InternalDSL.g:1165:4: ( (lv_information_25_0= RULE_STRING ) )
                    // InternalDSL.g:1166:5: (lv_information_25_0= RULE_STRING )
                    {
                    // InternalDSL.g:1166:5: (lv_information_25_0= RULE_STRING )
                    // InternalDSL.g:1167:6: lv_information_25_0= RULE_STRING
                    {
                    lv_information_25_0=(Token)match(input,RULE_STRING,FOLLOW_14); 

                    						newLeafNode(lv_information_25_0, grammarAccess.getAchievementAccess().getInformationSTRINGTerminalRuleCall_11_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAchievementRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"information",
                    							lv_information_25_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_26=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_26, grammarAccess.getAchievementAccess().getRightCurlyBracketKeyword_12());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAchievement"


    // $ANTLR start "entryRuleEvent"
    // InternalDSL.g:1192:1: entryRuleEvent returns [EObject current=null] : iv_ruleEvent= ruleEvent EOF ;
    public final EObject entryRuleEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEvent = null;


        try {
            // InternalDSL.g:1192:46: (iv_ruleEvent= ruleEvent EOF )
            // InternalDSL.g:1193:2: iv_ruleEvent= ruleEvent EOF
            {
             newCompositeNode(grammarAccess.getEventRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEvent=ruleEvent();

            state._fsp--;

             current =iv_ruleEvent; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEvent"


    // $ANTLR start "ruleEvent"
    // InternalDSL.g:1199:1: ruleEvent returns [EObject current=null] : ( () otherlv_1= 'Event' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'EventType:' ( (lv_eventType_6_0= ruleEventTypes ) ) )? (otherlv_7= 'PointGain:' ( (lv_pointGain_8_0= RULE_INT ) ) )? (otherlv_9= 'TriggerConditions' otherlv_10= '(' ( ( ruleFQNString ) ) (otherlv_12= ',' ( ( ruleFQNString ) ) )* otherlv_14= ')' )? (otherlv_15= 'Restrictions' otherlv_16= '(' ( (lv_restriction_17_0= ruleRestriction ) ) (otherlv_18= ',' ( (lv_restriction_19_0= ruleRestriction ) ) )* otherlv_20= ')' )? (otherlv_21= 'Information:' ( (lv_information_22_0= RULE_STRING ) ) )? otherlv_23= '}' ) ;
    public final EObject ruleEvent() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token lv_pointGain_8_0=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_18=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        Token lv_information_22_0=null;
        Token otherlv_23=null;
        AntlrDatatypeRuleToken lv_name_4_0 = null;

        Enumerator lv_eventType_6_0 = null;

        EObject lv_restriction_17_0 = null;

        EObject lv_restriction_19_0 = null;



        	enterRule();

        try {
            // InternalDSL.g:1205:2: ( ( () otherlv_1= 'Event' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'EventType:' ( (lv_eventType_6_0= ruleEventTypes ) ) )? (otherlv_7= 'PointGain:' ( (lv_pointGain_8_0= RULE_INT ) ) )? (otherlv_9= 'TriggerConditions' otherlv_10= '(' ( ( ruleFQNString ) ) (otherlv_12= ',' ( ( ruleFQNString ) ) )* otherlv_14= ')' )? (otherlv_15= 'Restrictions' otherlv_16= '(' ( (lv_restriction_17_0= ruleRestriction ) ) (otherlv_18= ',' ( (lv_restriction_19_0= ruleRestriction ) ) )* otherlv_20= ')' )? (otherlv_21= 'Information:' ( (lv_information_22_0= RULE_STRING ) ) )? otherlv_23= '}' ) )
            // InternalDSL.g:1206:2: ( () otherlv_1= 'Event' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'EventType:' ( (lv_eventType_6_0= ruleEventTypes ) ) )? (otherlv_7= 'PointGain:' ( (lv_pointGain_8_0= RULE_INT ) ) )? (otherlv_9= 'TriggerConditions' otherlv_10= '(' ( ( ruleFQNString ) ) (otherlv_12= ',' ( ( ruleFQNString ) ) )* otherlv_14= ')' )? (otherlv_15= 'Restrictions' otherlv_16= '(' ( (lv_restriction_17_0= ruleRestriction ) ) (otherlv_18= ',' ( (lv_restriction_19_0= ruleRestriction ) ) )* otherlv_20= ')' )? (otherlv_21= 'Information:' ( (lv_information_22_0= RULE_STRING ) ) )? otherlv_23= '}' )
            {
            // InternalDSL.g:1206:2: ( () otherlv_1= 'Event' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'EventType:' ( (lv_eventType_6_0= ruleEventTypes ) ) )? (otherlv_7= 'PointGain:' ( (lv_pointGain_8_0= RULE_INT ) ) )? (otherlv_9= 'TriggerConditions' otherlv_10= '(' ( ( ruleFQNString ) ) (otherlv_12= ',' ( ( ruleFQNString ) ) )* otherlv_14= ')' )? (otherlv_15= 'Restrictions' otherlv_16= '(' ( (lv_restriction_17_0= ruleRestriction ) ) (otherlv_18= ',' ( (lv_restriction_19_0= ruleRestriction ) ) )* otherlv_20= ')' )? (otherlv_21= 'Information:' ( (lv_information_22_0= RULE_STRING ) ) )? otherlv_23= '}' )
            // InternalDSL.g:1207:3: () otherlv_1= 'Event' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'EventType:' ( (lv_eventType_6_0= ruleEventTypes ) ) )? (otherlv_7= 'PointGain:' ( (lv_pointGain_8_0= RULE_INT ) ) )? (otherlv_9= 'TriggerConditions' otherlv_10= '(' ( ( ruleFQNString ) ) (otherlv_12= ',' ( ( ruleFQNString ) ) )* otherlv_14= ')' )? (otherlv_15= 'Restrictions' otherlv_16= '(' ( (lv_restriction_17_0= ruleRestriction ) ) (otherlv_18= ',' ( (lv_restriction_19_0= ruleRestriction ) ) )* otherlv_20= ')' )? (otherlv_21= 'Information:' ( (lv_information_22_0= RULE_STRING ) ) )? otherlv_23= '}'
            {
            // InternalDSL.g:1207:3: ()
            // InternalDSL.g:1208:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getEventAccess().getEventAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,39,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getEventAccess().getEventKeyword_1());
            		
            otherlv_2=(Token)match(input,13,FOLLOW_44); 

            			newLeafNode(otherlv_2, grammarAccess.getEventAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalDSL.g:1222:3: (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==14) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalDSL.g:1223:4: otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,14,FOLLOW_5); 

                    				newLeafNode(otherlv_3, grammarAccess.getEventAccess().getNameKeyword_3_0());
                    			
                    // InternalDSL.g:1227:4: ( (lv_name_4_0= ruleEString ) )
                    // InternalDSL.g:1228:5: (lv_name_4_0= ruleEString )
                    {
                    // InternalDSL.g:1228:5: (lv_name_4_0= ruleEString )
                    // InternalDSL.g:1229:6: lv_name_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getEventAccess().getNameEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_45);
                    lv_name_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getEventRule());
                    						}
                    						set(
                    							current,
                    							"name",
                    							lv_name_4_0,
                    							"org.xtext.gamify.dsl.DSL.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:1247:3: (otherlv_5= 'EventType:' ( (lv_eventType_6_0= ruleEventTypes ) ) )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==40) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalDSL.g:1248:4: otherlv_5= 'EventType:' ( (lv_eventType_6_0= ruleEventTypes ) )
                    {
                    otherlv_5=(Token)match(input,40,FOLLOW_46); 

                    				newLeafNode(otherlv_5, grammarAccess.getEventAccess().getEventTypeKeyword_4_0());
                    			
                    // InternalDSL.g:1252:4: ( (lv_eventType_6_0= ruleEventTypes ) )
                    // InternalDSL.g:1253:5: (lv_eventType_6_0= ruleEventTypes )
                    {
                    // InternalDSL.g:1253:5: (lv_eventType_6_0= ruleEventTypes )
                    // InternalDSL.g:1254:6: lv_eventType_6_0= ruleEventTypes
                    {

                    						newCompositeNode(grammarAccess.getEventAccess().getEventTypeEventTypesEnumRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_47);
                    lv_eventType_6_0=ruleEventTypes();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getEventRule());
                    						}
                    						set(
                    							current,
                    							"eventType",
                    							lv_eventType_6_0,
                    							"org.xtext.gamify.dsl.DSL.EventTypes");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:1272:3: (otherlv_7= 'PointGain:' ( (lv_pointGain_8_0= RULE_INT ) ) )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==41) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // InternalDSL.g:1273:4: otherlv_7= 'PointGain:' ( (lv_pointGain_8_0= RULE_INT ) )
                    {
                    otherlv_7=(Token)match(input,41,FOLLOW_48); 

                    				newLeafNode(otherlv_7, grammarAccess.getEventAccess().getPointGainKeyword_5_0());
                    			
                    // InternalDSL.g:1277:4: ( (lv_pointGain_8_0= RULE_INT ) )
                    // InternalDSL.g:1278:5: (lv_pointGain_8_0= RULE_INT )
                    {
                    // InternalDSL.g:1278:5: (lv_pointGain_8_0= RULE_INT )
                    // InternalDSL.g:1279:6: lv_pointGain_8_0= RULE_INT
                    {
                    lv_pointGain_8_0=(Token)match(input,RULE_INT,FOLLOW_49); 

                    						newLeafNode(lv_pointGain_8_0, grammarAccess.getEventAccess().getPointGainINTTerminalRuleCall_5_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getEventRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"pointGain",
                    							lv_pointGain_8_0,
                    							"org.xtext.gamify.dsl.DSL.INT");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:1296:3: (otherlv_9= 'TriggerConditions' otherlv_10= '(' ( ( ruleFQNString ) ) (otherlv_12= ',' ( ( ruleFQNString ) ) )* otherlv_14= ')' )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==42) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalDSL.g:1297:4: otherlv_9= 'TriggerConditions' otherlv_10= '(' ( ( ruleFQNString ) ) (otherlv_12= ',' ( ( ruleFQNString ) ) )* otherlv_14= ')'
                    {
                    otherlv_9=(Token)match(input,42,FOLLOW_38); 

                    				newLeafNode(otherlv_9, grammarAccess.getEventAccess().getTriggerConditionsKeyword_6_0());
                    			
                    otherlv_10=(Token)match(input,35,FOLLOW_5); 

                    				newLeafNode(otherlv_10, grammarAccess.getEventAccess().getLeftParenthesisKeyword_6_1());
                    			
                    // InternalDSL.g:1305:4: ( ( ruleFQNString ) )
                    // InternalDSL.g:1306:5: ( ruleFQNString )
                    {
                    // InternalDSL.g:1306:5: ( ruleFQNString )
                    // InternalDSL.g:1307:6: ruleFQNString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getEventRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getEventAccess().getTriggerConditionsConditionCrossReference_6_2_0());
                    					
                    pushFollow(FOLLOW_39);
                    ruleFQNString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalDSL.g:1321:4: (otherlv_12= ',' ( ( ruleFQNString ) ) )*
                    loop32:
                    do {
                        int alt32=2;
                        int LA32_0 = input.LA(1);

                        if ( (LA32_0==17) ) {
                            alt32=1;
                        }


                        switch (alt32) {
                    	case 1 :
                    	    // InternalDSL.g:1322:5: otherlv_12= ',' ( ( ruleFQNString ) )
                    	    {
                    	    otherlv_12=(Token)match(input,17,FOLLOW_5); 

                    	    					newLeafNode(otherlv_12, grammarAccess.getEventAccess().getCommaKeyword_6_3_0());
                    	    				
                    	    // InternalDSL.g:1326:5: ( ( ruleFQNString ) )
                    	    // InternalDSL.g:1327:6: ( ruleFQNString )
                    	    {
                    	    // InternalDSL.g:1327:6: ( ruleFQNString )
                    	    // InternalDSL.g:1328:7: ruleFQNString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getEventRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getEventAccess().getTriggerConditionsConditionCrossReference_6_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_39);
                    	    ruleFQNString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop32;
                        }
                    } while (true);

                    otherlv_14=(Token)match(input,36,FOLLOW_50); 

                    				newLeafNode(otherlv_14, grammarAccess.getEventAccess().getRightParenthesisKeyword_6_4());
                    			

                    }
                    break;

            }

            // InternalDSL.g:1348:3: (otherlv_15= 'Restrictions' otherlv_16= '(' ( (lv_restriction_17_0= ruleRestriction ) ) (otherlv_18= ',' ( (lv_restriction_19_0= ruleRestriction ) ) )* otherlv_20= ')' )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==43) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // InternalDSL.g:1349:4: otherlv_15= 'Restrictions' otherlv_16= '(' ( (lv_restriction_17_0= ruleRestriction ) ) (otherlv_18= ',' ( (lv_restriction_19_0= ruleRestriction ) ) )* otherlv_20= ')'
                    {
                    otherlv_15=(Token)match(input,43,FOLLOW_38); 

                    				newLeafNode(otherlv_15, grammarAccess.getEventAccess().getRestrictionsKeyword_7_0());
                    			
                    otherlv_16=(Token)match(input,35,FOLLOW_51); 

                    				newLeafNode(otherlv_16, grammarAccess.getEventAccess().getLeftParenthesisKeyword_7_1());
                    			
                    // InternalDSL.g:1357:4: ( (lv_restriction_17_0= ruleRestriction ) )
                    // InternalDSL.g:1358:5: (lv_restriction_17_0= ruleRestriction )
                    {
                    // InternalDSL.g:1358:5: (lv_restriction_17_0= ruleRestriction )
                    // InternalDSL.g:1359:6: lv_restriction_17_0= ruleRestriction
                    {

                    						newCompositeNode(grammarAccess.getEventAccess().getRestrictionRestrictionParserRuleCall_7_2_0());
                    					
                    pushFollow(FOLLOW_39);
                    lv_restriction_17_0=ruleRestriction();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getEventRule());
                    						}
                    						add(
                    							current,
                    							"restriction",
                    							lv_restriction_17_0,
                    							"org.xtext.gamify.dsl.DSL.Restriction");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalDSL.g:1376:4: (otherlv_18= ',' ( (lv_restriction_19_0= ruleRestriction ) ) )*
                    loop34:
                    do {
                        int alt34=2;
                        int LA34_0 = input.LA(1);

                        if ( (LA34_0==17) ) {
                            alt34=1;
                        }


                        switch (alt34) {
                    	case 1 :
                    	    // InternalDSL.g:1377:5: otherlv_18= ',' ( (lv_restriction_19_0= ruleRestriction ) )
                    	    {
                    	    otherlv_18=(Token)match(input,17,FOLLOW_51); 

                    	    					newLeafNode(otherlv_18, grammarAccess.getEventAccess().getCommaKeyword_7_3_0());
                    	    				
                    	    // InternalDSL.g:1381:5: ( (lv_restriction_19_0= ruleRestriction ) )
                    	    // InternalDSL.g:1382:6: (lv_restriction_19_0= ruleRestriction )
                    	    {
                    	    // InternalDSL.g:1382:6: (lv_restriction_19_0= ruleRestriction )
                    	    // InternalDSL.g:1383:7: lv_restriction_19_0= ruleRestriction
                    	    {

                    	    							newCompositeNode(grammarAccess.getEventAccess().getRestrictionRestrictionParserRuleCall_7_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_39);
                    	    lv_restriction_19_0=ruleRestriction();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getEventRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"restriction",
                    	    								lv_restriction_19_0,
                    	    								"org.xtext.gamify.dsl.DSL.Restriction");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop34;
                        }
                    } while (true);

                    otherlv_20=(Token)match(input,36,FOLLOW_12); 

                    				newLeafNode(otherlv_20, grammarAccess.getEventAccess().getRightParenthesisKeyword_7_4());
                    			

                    }
                    break;

            }

            // InternalDSL.g:1406:3: (otherlv_21= 'Information:' ( (lv_information_22_0= RULE_STRING ) ) )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==20) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalDSL.g:1407:4: otherlv_21= 'Information:' ( (lv_information_22_0= RULE_STRING ) )
                    {
                    otherlv_21=(Token)match(input,20,FOLLOW_13); 

                    				newLeafNode(otherlv_21, grammarAccess.getEventAccess().getInformationKeyword_8_0());
                    			
                    // InternalDSL.g:1411:4: ( (lv_information_22_0= RULE_STRING ) )
                    // InternalDSL.g:1412:5: (lv_information_22_0= RULE_STRING )
                    {
                    // InternalDSL.g:1412:5: (lv_information_22_0= RULE_STRING )
                    // InternalDSL.g:1413:6: lv_information_22_0= RULE_STRING
                    {
                    lv_information_22_0=(Token)match(input,RULE_STRING,FOLLOW_14); 

                    						newLeafNode(lv_information_22_0, grammarAccess.getEventAccess().getInformationSTRINGTerminalRuleCall_8_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getEventRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"information",
                    							lv_information_22_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_23=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_23, grammarAccess.getEventAccess().getRightCurlyBracketKeyword_9());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEvent"


    // $ANTLR start "entryRuleCondition"
    // InternalDSL.g:1438:1: entryRuleCondition returns [EObject current=null] : iv_ruleCondition= ruleCondition EOF ;
    public final EObject entryRuleCondition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCondition = null;


        try {
            // InternalDSL.g:1438:50: (iv_ruleCondition= ruleCondition EOF )
            // InternalDSL.g:1439:2: iv_ruleCondition= ruleCondition EOF
            {
             newCompositeNode(grammarAccess.getConditionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCondition=ruleCondition();

            state._fsp--;

             current =iv_ruleCondition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCondition"


    // $ANTLR start "ruleCondition"
    // InternalDSL.g:1445:1: ruleCondition returns [EObject current=null] : ( () otherlv_1= 'Condition' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'AmountRequired:' ( (lv_amountRequired_6_0= RULE_INT ) ) )? (otherlv_7= 'ConditionType:' ( (lv_conditionType_8_0= ruleConditionTypes ) ) )? (otherlv_9= 'PreRequirements' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')' )? (otherlv_15= 'Information:' ( (lv_information_16_0= RULE_STRING ) ) )? otherlv_17= '}' ) ;
    public final EObject ruleCondition() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token lv_amountRequired_6_0=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token lv_information_16_0=null;
        Token otherlv_17=null;
        AntlrDatatypeRuleToken lv_name_4_0 = null;

        Enumerator lv_conditionType_8_0 = null;



        	enterRule();

        try {
            // InternalDSL.g:1451:2: ( ( () otherlv_1= 'Condition' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'AmountRequired:' ( (lv_amountRequired_6_0= RULE_INT ) ) )? (otherlv_7= 'ConditionType:' ( (lv_conditionType_8_0= ruleConditionTypes ) ) )? (otherlv_9= 'PreRequirements' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')' )? (otherlv_15= 'Information:' ( (lv_information_16_0= RULE_STRING ) ) )? otherlv_17= '}' ) )
            // InternalDSL.g:1452:2: ( () otherlv_1= 'Condition' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'AmountRequired:' ( (lv_amountRequired_6_0= RULE_INT ) ) )? (otherlv_7= 'ConditionType:' ( (lv_conditionType_8_0= ruleConditionTypes ) ) )? (otherlv_9= 'PreRequirements' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')' )? (otherlv_15= 'Information:' ( (lv_information_16_0= RULE_STRING ) ) )? otherlv_17= '}' )
            {
            // InternalDSL.g:1452:2: ( () otherlv_1= 'Condition' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'AmountRequired:' ( (lv_amountRequired_6_0= RULE_INT ) ) )? (otherlv_7= 'ConditionType:' ( (lv_conditionType_8_0= ruleConditionTypes ) ) )? (otherlv_9= 'PreRequirements' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')' )? (otherlv_15= 'Information:' ( (lv_information_16_0= RULE_STRING ) ) )? otherlv_17= '}' )
            // InternalDSL.g:1453:3: () otherlv_1= 'Condition' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'AmountRequired:' ( (lv_amountRequired_6_0= RULE_INT ) ) )? (otherlv_7= 'ConditionType:' ( (lv_conditionType_8_0= ruleConditionTypes ) ) )? (otherlv_9= 'PreRequirements' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')' )? (otherlv_15= 'Information:' ( (lv_information_16_0= RULE_STRING ) ) )? otherlv_17= '}'
            {
            // InternalDSL.g:1453:3: ()
            // InternalDSL.g:1454:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getConditionAccess().getConditionAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,44,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getConditionAccess().getConditionKeyword_1());
            		
            otherlv_2=(Token)match(input,13,FOLLOW_52); 

            			newLeafNode(otherlv_2, grammarAccess.getConditionAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalDSL.g:1468:3: (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==14) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // InternalDSL.g:1469:4: otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,14,FOLLOW_5); 

                    				newLeafNode(otherlv_3, grammarAccess.getConditionAccess().getNameKeyword_3_0());
                    			
                    // InternalDSL.g:1473:4: ( (lv_name_4_0= ruleEString ) )
                    // InternalDSL.g:1474:5: (lv_name_4_0= ruleEString )
                    {
                    // InternalDSL.g:1474:5: (lv_name_4_0= ruleEString )
                    // InternalDSL.g:1475:6: lv_name_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getConditionAccess().getNameEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_53);
                    lv_name_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getConditionRule());
                    						}
                    						set(
                    							current,
                    							"name",
                    							lv_name_4_0,
                    							"org.xtext.gamify.dsl.DSL.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:1493:3: (otherlv_5= 'AmountRequired:' ( (lv_amountRequired_6_0= RULE_INT ) ) )?
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==45) ) {
                alt38=1;
            }
            switch (alt38) {
                case 1 :
                    // InternalDSL.g:1494:4: otherlv_5= 'AmountRequired:' ( (lv_amountRequired_6_0= RULE_INT ) )
                    {
                    otherlv_5=(Token)match(input,45,FOLLOW_48); 

                    				newLeafNode(otherlv_5, grammarAccess.getConditionAccess().getAmountRequiredKeyword_4_0());
                    			
                    // InternalDSL.g:1498:4: ( (lv_amountRequired_6_0= RULE_INT ) )
                    // InternalDSL.g:1499:5: (lv_amountRequired_6_0= RULE_INT )
                    {
                    // InternalDSL.g:1499:5: (lv_amountRequired_6_0= RULE_INT )
                    // InternalDSL.g:1500:6: lv_amountRequired_6_0= RULE_INT
                    {
                    lv_amountRequired_6_0=(Token)match(input,RULE_INT,FOLLOW_54); 

                    						newLeafNode(lv_amountRequired_6_0, grammarAccess.getConditionAccess().getAmountRequiredINTTerminalRuleCall_4_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getConditionRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"amountRequired",
                    							lv_amountRequired_6_0,
                    							"org.xtext.gamify.dsl.DSL.INT");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:1517:3: (otherlv_7= 'ConditionType:' ( (lv_conditionType_8_0= ruleConditionTypes ) ) )?
            int alt39=2;
            int LA39_0 = input.LA(1);

            if ( (LA39_0==46) ) {
                alt39=1;
            }
            switch (alt39) {
                case 1 :
                    // InternalDSL.g:1518:4: otherlv_7= 'ConditionType:' ( (lv_conditionType_8_0= ruleConditionTypes ) )
                    {
                    otherlv_7=(Token)match(input,46,FOLLOW_55); 

                    				newLeafNode(otherlv_7, grammarAccess.getConditionAccess().getConditionTypeKeyword_5_0());
                    			
                    // InternalDSL.g:1522:4: ( (lv_conditionType_8_0= ruleConditionTypes ) )
                    // InternalDSL.g:1523:5: (lv_conditionType_8_0= ruleConditionTypes )
                    {
                    // InternalDSL.g:1523:5: (lv_conditionType_8_0= ruleConditionTypes )
                    // InternalDSL.g:1524:6: lv_conditionType_8_0= ruleConditionTypes
                    {

                    						newCompositeNode(grammarAccess.getConditionAccess().getConditionTypeConditionTypesEnumRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_56);
                    lv_conditionType_8_0=ruleConditionTypes();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getConditionRule());
                    						}
                    						set(
                    							current,
                    							"conditionType",
                    							lv_conditionType_8_0,
                    							"org.xtext.gamify.dsl.DSL.ConditionTypes");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:1542:3: (otherlv_9= 'PreRequirements' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')' )?
            int alt41=2;
            int LA41_0 = input.LA(1);

            if ( (LA41_0==47) ) {
                alt41=1;
            }
            switch (alt41) {
                case 1 :
                    // InternalDSL.g:1543:4: otherlv_9= 'PreRequirements' otherlv_10= '(' ( ( ruleEString ) ) (otherlv_12= ',' ( ( ruleEString ) ) )* otherlv_14= ')'
                    {
                    otherlv_9=(Token)match(input,47,FOLLOW_38); 

                    				newLeafNode(otherlv_9, grammarAccess.getConditionAccess().getPreRequirementsKeyword_6_0());
                    			
                    otherlv_10=(Token)match(input,35,FOLLOW_5); 

                    				newLeafNode(otherlv_10, grammarAccess.getConditionAccess().getLeftParenthesisKeyword_6_1());
                    			
                    // InternalDSL.g:1551:4: ( ( ruleEString ) )
                    // InternalDSL.g:1552:5: ( ruleEString )
                    {
                    // InternalDSL.g:1552:5: ( ruleEString )
                    // InternalDSL.g:1553:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getConditionRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getConditionAccess().getPreRequirementsConditionCrossReference_6_2_0());
                    					
                    pushFollow(FOLLOW_39);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalDSL.g:1567:4: (otherlv_12= ',' ( ( ruleEString ) ) )*
                    loop40:
                    do {
                        int alt40=2;
                        int LA40_0 = input.LA(1);

                        if ( (LA40_0==17) ) {
                            alt40=1;
                        }


                        switch (alt40) {
                    	case 1 :
                    	    // InternalDSL.g:1568:5: otherlv_12= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_12=(Token)match(input,17,FOLLOW_5); 

                    	    					newLeafNode(otherlv_12, grammarAccess.getConditionAccess().getCommaKeyword_6_3_0());
                    	    				
                    	    // InternalDSL.g:1572:5: ( ( ruleEString ) )
                    	    // InternalDSL.g:1573:6: ( ruleEString )
                    	    {
                    	    // InternalDSL.g:1573:6: ( ruleEString )
                    	    // InternalDSL.g:1574:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getConditionRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getConditionAccess().getPreRequirementsConditionCrossReference_6_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_39);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop40;
                        }
                    } while (true);

                    otherlv_14=(Token)match(input,36,FOLLOW_12); 

                    				newLeafNode(otherlv_14, grammarAccess.getConditionAccess().getRightParenthesisKeyword_6_4());
                    			

                    }
                    break;

            }

            // InternalDSL.g:1594:3: (otherlv_15= 'Information:' ( (lv_information_16_0= RULE_STRING ) ) )?
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( (LA42_0==20) ) {
                alt42=1;
            }
            switch (alt42) {
                case 1 :
                    // InternalDSL.g:1595:4: otherlv_15= 'Information:' ( (lv_information_16_0= RULE_STRING ) )
                    {
                    otherlv_15=(Token)match(input,20,FOLLOW_13); 

                    				newLeafNode(otherlv_15, grammarAccess.getConditionAccess().getInformationKeyword_7_0());
                    			
                    // InternalDSL.g:1599:4: ( (lv_information_16_0= RULE_STRING ) )
                    // InternalDSL.g:1600:5: (lv_information_16_0= RULE_STRING )
                    {
                    // InternalDSL.g:1600:5: (lv_information_16_0= RULE_STRING )
                    // InternalDSL.g:1601:6: lv_information_16_0= RULE_STRING
                    {
                    lv_information_16_0=(Token)match(input,RULE_STRING,FOLLOW_14); 

                    						newLeafNode(lv_information_16_0, grammarAccess.getConditionAccess().getInformationSTRINGTerminalRuleCall_7_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getConditionRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"information",
                    							lv_information_16_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_17=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_17, grammarAccess.getConditionAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCondition"


    // $ANTLR start "entryRuleRestriction"
    // InternalDSL.g:1626:1: entryRuleRestriction returns [EObject current=null] : iv_ruleRestriction= ruleRestriction EOF ;
    public final EObject entryRuleRestriction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRestriction = null;


        try {
            // InternalDSL.g:1626:52: (iv_ruleRestriction= ruleRestriction EOF )
            // InternalDSL.g:1627:2: iv_ruleRestriction= ruleRestriction EOF
            {
             newCompositeNode(grammarAccess.getRestrictionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRestriction=ruleRestriction();

            state._fsp--;

             current =iv_ruleRestriction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRestriction"


    // $ANTLR start "ruleRestriction"
    // InternalDSL.g:1633:1: ruleRestriction returns [EObject current=null] : ( () otherlv_1= 'Restriction' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'RuleDescription:' ( (lv_ruleDescription_6_0= RULE_STRING ) ) )? (otherlv_7= 'RestrictionType:' ( (lv_restrictionType_8_0= ruleRestrictionTypes ) ) )? (otherlv_9= 'Amount:' ( (lv_amount_10_0= RULE_INT ) ) )? (otherlv_11= 'Information:' ( (lv_information_12_0= RULE_STRING ) ) )? otherlv_13= '}' ) ;
    public final EObject ruleRestriction() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token lv_ruleDescription_6_0=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token lv_amount_10_0=null;
        Token otherlv_11=null;
        Token lv_information_12_0=null;
        Token otherlv_13=null;
        AntlrDatatypeRuleToken lv_name_4_0 = null;

        Enumerator lv_restrictionType_8_0 = null;



        	enterRule();

        try {
            // InternalDSL.g:1639:2: ( ( () otherlv_1= 'Restriction' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'RuleDescription:' ( (lv_ruleDescription_6_0= RULE_STRING ) ) )? (otherlv_7= 'RestrictionType:' ( (lv_restrictionType_8_0= ruleRestrictionTypes ) ) )? (otherlv_9= 'Amount:' ( (lv_amount_10_0= RULE_INT ) ) )? (otherlv_11= 'Information:' ( (lv_information_12_0= RULE_STRING ) ) )? otherlv_13= '}' ) )
            // InternalDSL.g:1640:2: ( () otherlv_1= 'Restriction' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'RuleDescription:' ( (lv_ruleDescription_6_0= RULE_STRING ) ) )? (otherlv_7= 'RestrictionType:' ( (lv_restrictionType_8_0= ruleRestrictionTypes ) ) )? (otherlv_9= 'Amount:' ( (lv_amount_10_0= RULE_INT ) ) )? (otherlv_11= 'Information:' ( (lv_information_12_0= RULE_STRING ) ) )? otherlv_13= '}' )
            {
            // InternalDSL.g:1640:2: ( () otherlv_1= 'Restriction' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'RuleDescription:' ( (lv_ruleDescription_6_0= RULE_STRING ) ) )? (otherlv_7= 'RestrictionType:' ( (lv_restrictionType_8_0= ruleRestrictionTypes ) ) )? (otherlv_9= 'Amount:' ( (lv_amount_10_0= RULE_INT ) ) )? (otherlv_11= 'Information:' ( (lv_information_12_0= RULE_STRING ) ) )? otherlv_13= '}' )
            // InternalDSL.g:1641:3: () otherlv_1= 'Restriction' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'RuleDescription:' ( (lv_ruleDescription_6_0= RULE_STRING ) ) )? (otherlv_7= 'RestrictionType:' ( (lv_restrictionType_8_0= ruleRestrictionTypes ) ) )? (otherlv_9= 'Amount:' ( (lv_amount_10_0= RULE_INT ) ) )? (otherlv_11= 'Information:' ( (lv_information_12_0= RULE_STRING ) ) )? otherlv_13= '}'
            {
            // InternalDSL.g:1641:3: ()
            // InternalDSL.g:1642:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getRestrictionAccess().getRestrictionAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,48,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getRestrictionAccess().getRestrictionKeyword_1());
            		
            otherlv_2=(Token)match(input,13,FOLLOW_57); 

            			newLeafNode(otherlv_2, grammarAccess.getRestrictionAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalDSL.g:1656:3: (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )?
            int alt43=2;
            int LA43_0 = input.LA(1);

            if ( (LA43_0==14) ) {
                alt43=1;
            }
            switch (alt43) {
                case 1 :
                    // InternalDSL.g:1657:4: otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,14,FOLLOW_5); 

                    				newLeafNode(otherlv_3, grammarAccess.getRestrictionAccess().getNameKeyword_3_0());
                    			
                    // InternalDSL.g:1661:4: ( (lv_name_4_0= ruleEString ) )
                    // InternalDSL.g:1662:5: (lv_name_4_0= ruleEString )
                    {
                    // InternalDSL.g:1662:5: (lv_name_4_0= ruleEString )
                    // InternalDSL.g:1663:6: lv_name_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getRestrictionAccess().getNameEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_58);
                    lv_name_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getRestrictionRule());
                    						}
                    						set(
                    							current,
                    							"name",
                    							lv_name_4_0,
                    							"org.xtext.gamify.dsl.DSL.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:1681:3: (otherlv_5= 'RuleDescription:' ( (lv_ruleDescription_6_0= RULE_STRING ) ) )?
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( (LA44_0==49) ) {
                alt44=1;
            }
            switch (alt44) {
                case 1 :
                    // InternalDSL.g:1682:4: otherlv_5= 'RuleDescription:' ( (lv_ruleDescription_6_0= RULE_STRING ) )
                    {
                    otherlv_5=(Token)match(input,49,FOLLOW_13); 

                    				newLeafNode(otherlv_5, grammarAccess.getRestrictionAccess().getRuleDescriptionKeyword_4_0());
                    			
                    // InternalDSL.g:1686:4: ( (lv_ruleDescription_6_0= RULE_STRING ) )
                    // InternalDSL.g:1687:5: (lv_ruleDescription_6_0= RULE_STRING )
                    {
                    // InternalDSL.g:1687:5: (lv_ruleDescription_6_0= RULE_STRING )
                    // InternalDSL.g:1688:6: lv_ruleDescription_6_0= RULE_STRING
                    {
                    lv_ruleDescription_6_0=(Token)match(input,RULE_STRING,FOLLOW_59); 

                    						newLeafNode(lv_ruleDescription_6_0, grammarAccess.getRestrictionAccess().getRuleDescriptionSTRINGTerminalRuleCall_4_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRestrictionRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"ruleDescription",
                    							lv_ruleDescription_6_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:1705:3: (otherlv_7= 'RestrictionType:' ( (lv_restrictionType_8_0= ruleRestrictionTypes ) ) )?
            int alt45=2;
            int LA45_0 = input.LA(1);

            if ( (LA45_0==50) ) {
                alt45=1;
            }
            switch (alt45) {
                case 1 :
                    // InternalDSL.g:1706:4: otherlv_7= 'RestrictionType:' ( (lv_restrictionType_8_0= ruleRestrictionTypes ) )
                    {
                    otherlv_7=(Token)match(input,50,FOLLOW_60); 

                    				newLeafNode(otherlv_7, grammarAccess.getRestrictionAccess().getRestrictionTypeKeyword_5_0());
                    			
                    // InternalDSL.g:1710:4: ( (lv_restrictionType_8_0= ruleRestrictionTypes ) )
                    // InternalDSL.g:1711:5: (lv_restrictionType_8_0= ruleRestrictionTypes )
                    {
                    // InternalDSL.g:1711:5: (lv_restrictionType_8_0= ruleRestrictionTypes )
                    // InternalDSL.g:1712:6: lv_restrictionType_8_0= ruleRestrictionTypes
                    {

                    						newCompositeNode(grammarAccess.getRestrictionAccess().getRestrictionTypeRestrictionTypesEnumRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_61);
                    lv_restrictionType_8_0=ruleRestrictionTypes();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getRestrictionRule());
                    						}
                    						set(
                    							current,
                    							"restrictionType",
                    							lv_restrictionType_8_0,
                    							"org.xtext.gamify.dsl.DSL.RestrictionTypes");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:1730:3: (otherlv_9= 'Amount:' ( (lv_amount_10_0= RULE_INT ) ) )?
            int alt46=2;
            int LA46_0 = input.LA(1);

            if ( (LA46_0==51) ) {
                alt46=1;
            }
            switch (alt46) {
                case 1 :
                    // InternalDSL.g:1731:4: otherlv_9= 'Amount:' ( (lv_amount_10_0= RULE_INT ) )
                    {
                    otherlv_9=(Token)match(input,51,FOLLOW_48); 

                    				newLeafNode(otherlv_9, grammarAccess.getRestrictionAccess().getAmountKeyword_6_0());
                    			
                    // InternalDSL.g:1735:4: ( (lv_amount_10_0= RULE_INT ) )
                    // InternalDSL.g:1736:5: (lv_amount_10_0= RULE_INT )
                    {
                    // InternalDSL.g:1736:5: (lv_amount_10_0= RULE_INT )
                    // InternalDSL.g:1737:6: lv_amount_10_0= RULE_INT
                    {
                    lv_amount_10_0=(Token)match(input,RULE_INT,FOLLOW_12); 

                    						newLeafNode(lv_amount_10_0, grammarAccess.getRestrictionAccess().getAmountINTTerminalRuleCall_6_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRestrictionRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"amount",
                    							lv_amount_10_0,
                    							"org.xtext.gamify.dsl.DSL.INT");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:1754:3: (otherlv_11= 'Information:' ( (lv_information_12_0= RULE_STRING ) ) )?
            int alt47=2;
            int LA47_0 = input.LA(1);

            if ( (LA47_0==20) ) {
                alt47=1;
            }
            switch (alt47) {
                case 1 :
                    // InternalDSL.g:1755:4: otherlv_11= 'Information:' ( (lv_information_12_0= RULE_STRING ) )
                    {
                    otherlv_11=(Token)match(input,20,FOLLOW_13); 

                    				newLeafNode(otherlv_11, grammarAccess.getRestrictionAccess().getInformationKeyword_7_0());
                    			
                    // InternalDSL.g:1759:4: ( (lv_information_12_0= RULE_STRING ) )
                    // InternalDSL.g:1760:5: (lv_information_12_0= RULE_STRING )
                    {
                    // InternalDSL.g:1760:5: (lv_information_12_0= RULE_STRING )
                    // InternalDSL.g:1761:6: lv_information_12_0= RULE_STRING
                    {
                    lv_information_12_0=(Token)match(input,RULE_STRING,FOLLOW_14); 

                    						newLeafNode(lv_information_12_0, grammarAccess.getRestrictionAccess().getInformationSTRINGTerminalRuleCall_7_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRestrictionRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"information",
                    							lv_information_12_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_13=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_13, grammarAccess.getRestrictionAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRestriction"


    // $ANTLR start "entryRuleBoolean"
    // InternalDSL.g:1786:1: entryRuleBoolean returns [String current=null] : iv_ruleBoolean= ruleBoolean EOF ;
    public final String entryRuleBoolean() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleBoolean = null;


        try {
            // InternalDSL.g:1786:47: (iv_ruleBoolean= ruleBoolean EOF )
            // InternalDSL.g:1787:2: iv_ruleBoolean= ruleBoolean EOF
            {
             newCompositeNode(grammarAccess.getBooleanRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBoolean=ruleBoolean();

            state._fsp--;

             current =iv_ruleBoolean.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBoolean"


    // $ANTLR start "ruleBoolean"
    // InternalDSL.g:1793:1: ruleBoolean returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'true' | kw= 'false' ) ;
    public final AntlrDatatypeRuleToken ruleBoolean() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalDSL.g:1799:2: ( (kw= 'true' | kw= 'false' ) )
            // InternalDSL.g:1800:2: (kw= 'true' | kw= 'false' )
            {
            // InternalDSL.g:1800:2: (kw= 'true' | kw= 'false' )
            int alt48=2;
            int LA48_0 = input.LA(1);

            if ( (LA48_0==52) ) {
                alt48=1;
            }
            else if ( (LA48_0==53) ) {
                alt48=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 48, 0, input);

                throw nvae;
            }
            switch (alt48) {
                case 1 :
                    // InternalDSL.g:1801:3: kw= 'true'
                    {
                    kw=(Token)match(input,52,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getBooleanAccess().getTrueKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalDSL.g:1807:3: kw= 'false'
                    {
                    kw=(Token)match(input,53,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getBooleanAccess().getFalseKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBoolean"


    // $ANTLR start "entryRuleRandomReward"
    // InternalDSL.g:1816:1: entryRuleRandomReward returns [EObject current=null] : iv_ruleRandomReward= ruleRandomReward EOF ;
    public final EObject entryRuleRandomReward() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRandomReward = null;


        try {
            // InternalDSL.g:1816:53: (iv_ruleRandomReward= ruleRandomReward EOF )
            // InternalDSL.g:1817:2: iv_ruleRandomReward= ruleRandomReward EOF
            {
             newCompositeNode(grammarAccess.getRandomRewardRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRandomReward=ruleRandomReward();

            state._fsp--;

             current =iv_ruleRandomReward; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRandomReward"


    // $ANTLR start "ruleRandomReward"
    // InternalDSL.g:1823:1: ruleRandomReward returns [EObject current=null] : ( () otherlv_1= 'RandomReward' otherlv_2= '{' (otherlv_3= 'DropRate:' ( (lv_dropRate_4_0= RULE_DOUBLE ) ) )? (otherlv_5= 'IsVisible:' ( (lv_isVisible_6_0= ruleBoolean ) ) )? (otherlv_7= 'Item:' ( ( ruleEString ) ) )? otherlv_9= '}' ) ;
    public final EObject ruleRandomReward() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_dropRate_4_0=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        AntlrDatatypeRuleToken lv_isVisible_6_0 = null;



        	enterRule();

        try {
            // InternalDSL.g:1829:2: ( ( () otherlv_1= 'RandomReward' otherlv_2= '{' (otherlv_3= 'DropRate:' ( (lv_dropRate_4_0= RULE_DOUBLE ) ) )? (otherlv_5= 'IsVisible:' ( (lv_isVisible_6_0= ruleBoolean ) ) )? (otherlv_7= 'Item:' ( ( ruleEString ) ) )? otherlv_9= '}' ) )
            // InternalDSL.g:1830:2: ( () otherlv_1= 'RandomReward' otherlv_2= '{' (otherlv_3= 'DropRate:' ( (lv_dropRate_4_0= RULE_DOUBLE ) ) )? (otherlv_5= 'IsVisible:' ( (lv_isVisible_6_0= ruleBoolean ) ) )? (otherlv_7= 'Item:' ( ( ruleEString ) ) )? otherlv_9= '}' )
            {
            // InternalDSL.g:1830:2: ( () otherlv_1= 'RandomReward' otherlv_2= '{' (otherlv_3= 'DropRate:' ( (lv_dropRate_4_0= RULE_DOUBLE ) ) )? (otherlv_5= 'IsVisible:' ( (lv_isVisible_6_0= ruleBoolean ) ) )? (otherlv_7= 'Item:' ( ( ruleEString ) ) )? otherlv_9= '}' )
            // InternalDSL.g:1831:3: () otherlv_1= 'RandomReward' otherlv_2= '{' (otherlv_3= 'DropRate:' ( (lv_dropRate_4_0= RULE_DOUBLE ) ) )? (otherlv_5= 'IsVisible:' ( (lv_isVisible_6_0= ruleBoolean ) ) )? (otherlv_7= 'Item:' ( ( ruleEString ) ) )? otherlv_9= '}'
            {
            // InternalDSL.g:1831:3: ()
            // InternalDSL.g:1832:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getRandomRewardAccess().getRandomRewardAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,54,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getRandomRewardAccess().getRandomRewardKeyword_1());
            		
            otherlv_2=(Token)match(input,13,FOLLOW_62); 

            			newLeafNode(otherlv_2, grammarAccess.getRandomRewardAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalDSL.g:1846:3: (otherlv_3= 'DropRate:' ( (lv_dropRate_4_0= RULE_DOUBLE ) ) )?
            int alt49=2;
            int LA49_0 = input.LA(1);

            if ( (LA49_0==55) ) {
                alt49=1;
            }
            switch (alt49) {
                case 1 :
                    // InternalDSL.g:1847:4: otherlv_3= 'DropRate:' ( (lv_dropRate_4_0= RULE_DOUBLE ) )
                    {
                    otherlv_3=(Token)match(input,55,FOLLOW_63); 

                    				newLeafNode(otherlv_3, grammarAccess.getRandomRewardAccess().getDropRateKeyword_3_0());
                    			
                    // InternalDSL.g:1851:4: ( (lv_dropRate_4_0= RULE_DOUBLE ) )
                    // InternalDSL.g:1852:5: (lv_dropRate_4_0= RULE_DOUBLE )
                    {
                    // InternalDSL.g:1852:5: (lv_dropRate_4_0= RULE_DOUBLE )
                    // InternalDSL.g:1853:6: lv_dropRate_4_0= RULE_DOUBLE
                    {
                    lv_dropRate_4_0=(Token)match(input,RULE_DOUBLE,FOLLOW_64); 

                    						newLeafNode(lv_dropRate_4_0, grammarAccess.getRandomRewardAccess().getDropRateDOUBLETerminalRuleCall_3_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRandomRewardRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"dropRate",
                    							lv_dropRate_4_0,
                    							"org.xtext.gamify.dsl.DSL.DOUBLE");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:1870:3: (otherlv_5= 'IsVisible:' ( (lv_isVisible_6_0= ruleBoolean ) ) )?
            int alt50=2;
            int LA50_0 = input.LA(1);

            if ( (LA50_0==56) ) {
                alt50=1;
            }
            switch (alt50) {
                case 1 :
                    // InternalDSL.g:1871:4: otherlv_5= 'IsVisible:' ( (lv_isVisible_6_0= ruleBoolean ) )
                    {
                    otherlv_5=(Token)match(input,56,FOLLOW_36); 

                    				newLeafNode(otherlv_5, grammarAccess.getRandomRewardAccess().getIsVisibleKeyword_4_0());
                    			
                    // InternalDSL.g:1875:4: ( (lv_isVisible_6_0= ruleBoolean ) )
                    // InternalDSL.g:1876:5: (lv_isVisible_6_0= ruleBoolean )
                    {
                    // InternalDSL.g:1876:5: (lv_isVisible_6_0= ruleBoolean )
                    // InternalDSL.g:1877:6: lv_isVisible_6_0= ruleBoolean
                    {

                    						newCompositeNode(grammarAccess.getRandomRewardAccess().getIsVisibleBooleanParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_65);
                    lv_isVisible_6_0=ruleBoolean();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getRandomRewardRule());
                    						}
                    						set(
                    							current,
                    							"isVisible",
                    							lv_isVisible_6_0,
                    							"org.xtext.gamify.dsl.DSL.Boolean");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:1895:3: (otherlv_7= 'Item:' ( ( ruleEString ) ) )?
            int alt51=2;
            int LA51_0 = input.LA(1);

            if ( (LA51_0==57) ) {
                alt51=1;
            }
            switch (alt51) {
                case 1 :
                    // InternalDSL.g:1896:4: otherlv_7= 'Item:' ( ( ruleEString ) )
                    {
                    otherlv_7=(Token)match(input,57,FOLLOW_5); 

                    				newLeafNode(otherlv_7, grammarAccess.getRandomRewardAccess().getItemKeyword_5_0());
                    			
                    // InternalDSL.g:1900:4: ( ( ruleEString ) )
                    // InternalDSL.g:1901:5: ( ruleEString )
                    {
                    // InternalDSL.g:1901:5: ( ruleEString )
                    // InternalDSL.g:1902:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getRandomRewardRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getRandomRewardAccess().getItemItemCrossReference_5_1_0());
                    					
                    pushFollow(FOLLOW_14);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_9=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_9, grammarAccess.getRandomRewardAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRandomReward"


    // $ANTLR start "entryRuleFixedReward"
    // InternalDSL.g:1925:1: entryRuleFixedReward returns [EObject current=null] : iv_ruleFixedReward= ruleFixedReward EOF ;
    public final EObject entryRuleFixedReward() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFixedReward = null;


        try {
            // InternalDSL.g:1925:52: (iv_ruleFixedReward= ruleFixedReward EOF )
            // InternalDSL.g:1926:2: iv_ruleFixedReward= ruleFixedReward EOF
            {
             newCompositeNode(grammarAccess.getFixedRewardRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFixedReward=ruleFixedReward();

            state._fsp--;

             current =iv_ruleFixedReward; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFixedReward"


    // $ANTLR start "ruleFixedReward"
    // InternalDSL.g:1932:1: ruleFixedReward returns [EObject current=null] : ( () otherlv_1= 'FixedReward' otherlv_2= '{' (otherlv_3= 'Item:' ( ( ruleEString ) ) )? otherlv_5= '}' ) ;
    public final EObject ruleFixedReward() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;


        	enterRule();

        try {
            // InternalDSL.g:1938:2: ( ( () otherlv_1= 'FixedReward' otherlv_2= '{' (otherlv_3= 'Item:' ( ( ruleEString ) ) )? otherlv_5= '}' ) )
            // InternalDSL.g:1939:2: ( () otherlv_1= 'FixedReward' otherlv_2= '{' (otherlv_3= 'Item:' ( ( ruleEString ) ) )? otherlv_5= '}' )
            {
            // InternalDSL.g:1939:2: ( () otherlv_1= 'FixedReward' otherlv_2= '{' (otherlv_3= 'Item:' ( ( ruleEString ) ) )? otherlv_5= '}' )
            // InternalDSL.g:1940:3: () otherlv_1= 'FixedReward' otherlv_2= '{' (otherlv_3= 'Item:' ( ( ruleEString ) ) )? otherlv_5= '}'
            {
            // InternalDSL.g:1940:3: ()
            // InternalDSL.g:1941:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getFixedRewardAccess().getFixedRewardAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,58,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getFixedRewardAccess().getFixedRewardKeyword_1());
            		
            otherlv_2=(Token)match(input,13,FOLLOW_65); 

            			newLeafNode(otherlv_2, grammarAccess.getFixedRewardAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalDSL.g:1955:3: (otherlv_3= 'Item:' ( ( ruleEString ) ) )?
            int alt52=2;
            int LA52_0 = input.LA(1);

            if ( (LA52_0==57) ) {
                alt52=1;
            }
            switch (alt52) {
                case 1 :
                    // InternalDSL.g:1956:4: otherlv_3= 'Item:' ( ( ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,57,FOLLOW_5); 

                    				newLeafNode(otherlv_3, grammarAccess.getFixedRewardAccess().getItemKeyword_3_0());
                    			
                    // InternalDSL.g:1960:4: ( ( ruleEString ) )
                    // InternalDSL.g:1961:5: ( ruleEString )
                    {
                    // InternalDSL.g:1961:5: ( ruleEString )
                    // InternalDSL.g:1962:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getFixedRewardRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getFixedRewardAccess().getItemItemCrossReference_3_1_0());
                    					
                    pushFollow(FOLLOW_14);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getFixedRewardAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFixedReward"


    // $ANTLR start "entryRuleBadge"
    // InternalDSL.g:1985:1: entryRuleBadge returns [EObject current=null] : iv_ruleBadge= ruleBadge EOF ;
    public final EObject entryRuleBadge() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBadge = null;


        try {
            // InternalDSL.g:1985:46: (iv_ruleBadge= ruleBadge EOF )
            // InternalDSL.g:1986:2: iv_ruleBadge= ruleBadge EOF
            {
             newCompositeNode(grammarAccess.getBadgeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBadge=ruleBadge();

            state._fsp--;

             current =iv_ruleBadge; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBadge"


    // $ANTLR start "ruleBadge"
    // InternalDSL.g:1992:1: ruleBadge returns [EObject current=null] : ( () otherlv_1= 'Badge' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'RewardDescription:' ( (lv_rewardDescription_6_0= RULE_STRING ) ) )? (otherlv_7= 'BadgeLevel:' ( (lv_badgeLevel_8_0= RULE_INT ) ) )? (otherlv_9= 'BadgeImage:' ( (lv_badgeImage_10_0= RULE_STRING ) ) )? (otherlv_11= 'Information:' ( (lv_information_12_0= RULE_STRING ) ) )? otherlv_13= '}' ) ;
    public final EObject ruleBadge() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token lv_rewardDescription_6_0=null;
        Token otherlv_7=null;
        Token lv_badgeLevel_8_0=null;
        Token otherlv_9=null;
        Token lv_badgeImage_10_0=null;
        Token otherlv_11=null;
        Token lv_information_12_0=null;
        Token otherlv_13=null;
        AntlrDatatypeRuleToken lv_name_4_0 = null;



        	enterRule();

        try {
            // InternalDSL.g:1998:2: ( ( () otherlv_1= 'Badge' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'RewardDescription:' ( (lv_rewardDescription_6_0= RULE_STRING ) ) )? (otherlv_7= 'BadgeLevel:' ( (lv_badgeLevel_8_0= RULE_INT ) ) )? (otherlv_9= 'BadgeImage:' ( (lv_badgeImage_10_0= RULE_STRING ) ) )? (otherlv_11= 'Information:' ( (lv_information_12_0= RULE_STRING ) ) )? otherlv_13= '}' ) )
            // InternalDSL.g:1999:2: ( () otherlv_1= 'Badge' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'RewardDescription:' ( (lv_rewardDescription_6_0= RULE_STRING ) ) )? (otherlv_7= 'BadgeLevel:' ( (lv_badgeLevel_8_0= RULE_INT ) ) )? (otherlv_9= 'BadgeImage:' ( (lv_badgeImage_10_0= RULE_STRING ) ) )? (otherlv_11= 'Information:' ( (lv_information_12_0= RULE_STRING ) ) )? otherlv_13= '}' )
            {
            // InternalDSL.g:1999:2: ( () otherlv_1= 'Badge' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'RewardDescription:' ( (lv_rewardDescription_6_0= RULE_STRING ) ) )? (otherlv_7= 'BadgeLevel:' ( (lv_badgeLevel_8_0= RULE_INT ) ) )? (otherlv_9= 'BadgeImage:' ( (lv_badgeImage_10_0= RULE_STRING ) ) )? (otherlv_11= 'Information:' ( (lv_information_12_0= RULE_STRING ) ) )? otherlv_13= '}' )
            // InternalDSL.g:2000:3: () otherlv_1= 'Badge' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'RewardDescription:' ( (lv_rewardDescription_6_0= RULE_STRING ) ) )? (otherlv_7= 'BadgeLevel:' ( (lv_badgeLevel_8_0= RULE_INT ) ) )? (otherlv_9= 'BadgeImage:' ( (lv_badgeImage_10_0= RULE_STRING ) ) )? (otherlv_11= 'Information:' ( (lv_information_12_0= RULE_STRING ) ) )? otherlv_13= '}'
            {
            // InternalDSL.g:2000:3: ()
            // InternalDSL.g:2001:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getBadgeAccess().getBadgeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,59,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getBadgeAccess().getBadgeKeyword_1());
            		
            otherlv_2=(Token)match(input,13,FOLLOW_66); 

            			newLeafNode(otherlv_2, grammarAccess.getBadgeAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalDSL.g:2015:3: (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )?
            int alt53=2;
            int LA53_0 = input.LA(1);

            if ( (LA53_0==14) ) {
                alt53=1;
            }
            switch (alt53) {
                case 1 :
                    // InternalDSL.g:2016:4: otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,14,FOLLOW_5); 

                    				newLeafNode(otherlv_3, grammarAccess.getBadgeAccess().getNameKeyword_3_0());
                    			
                    // InternalDSL.g:2020:4: ( (lv_name_4_0= ruleEString ) )
                    // InternalDSL.g:2021:5: (lv_name_4_0= ruleEString )
                    {
                    // InternalDSL.g:2021:5: (lv_name_4_0= ruleEString )
                    // InternalDSL.g:2022:6: lv_name_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getBadgeAccess().getNameEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_67);
                    lv_name_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getBadgeRule());
                    						}
                    						set(
                    							current,
                    							"name",
                    							lv_name_4_0,
                    							"org.xtext.gamify.dsl.DSL.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:2040:3: (otherlv_5= 'RewardDescription:' ( (lv_rewardDescription_6_0= RULE_STRING ) ) )?
            int alt54=2;
            int LA54_0 = input.LA(1);

            if ( (LA54_0==60) ) {
                alt54=1;
            }
            switch (alt54) {
                case 1 :
                    // InternalDSL.g:2041:4: otherlv_5= 'RewardDescription:' ( (lv_rewardDescription_6_0= RULE_STRING ) )
                    {
                    otherlv_5=(Token)match(input,60,FOLLOW_13); 

                    				newLeafNode(otherlv_5, grammarAccess.getBadgeAccess().getRewardDescriptionKeyword_4_0());
                    			
                    // InternalDSL.g:2045:4: ( (lv_rewardDescription_6_0= RULE_STRING ) )
                    // InternalDSL.g:2046:5: (lv_rewardDescription_6_0= RULE_STRING )
                    {
                    // InternalDSL.g:2046:5: (lv_rewardDescription_6_0= RULE_STRING )
                    // InternalDSL.g:2047:6: lv_rewardDescription_6_0= RULE_STRING
                    {
                    lv_rewardDescription_6_0=(Token)match(input,RULE_STRING,FOLLOW_68); 

                    						newLeafNode(lv_rewardDescription_6_0, grammarAccess.getBadgeAccess().getRewardDescriptionSTRINGTerminalRuleCall_4_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getBadgeRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"rewardDescription",
                    							lv_rewardDescription_6_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:2064:3: (otherlv_7= 'BadgeLevel:' ( (lv_badgeLevel_8_0= RULE_INT ) ) )?
            int alt55=2;
            int LA55_0 = input.LA(1);

            if ( (LA55_0==61) ) {
                alt55=1;
            }
            switch (alt55) {
                case 1 :
                    // InternalDSL.g:2065:4: otherlv_7= 'BadgeLevel:' ( (lv_badgeLevel_8_0= RULE_INT ) )
                    {
                    otherlv_7=(Token)match(input,61,FOLLOW_48); 

                    				newLeafNode(otherlv_7, grammarAccess.getBadgeAccess().getBadgeLevelKeyword_5_0());
                    			
                    // InternalDSL.g:2069:4: ( (lv_badgeLevel_8_0= RULE_INT ) )
                    // InternalDSL.g:2070:5: (lv_badgeLevel_8_0= RULE_INT )
                    {
                    // InternalDSL.g:2070:5: (lv_badgeLevel_8_0= RULE_INT )
                    // InternalDSL.g:2071:6: lv_badgeLevel_8_0= RULE_INT
                    {
                    lv_badgeLevel_8_0=(Token)match(input,RULE_INT,FOLLOW_69); 

                    						newLeafNode(lv_badgeLevel_8_0, grammarAccess.getBadgeAccess().getBadgeLevelINTTerminalRuleCall_5_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getBadgeRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"badgeLevel",
                    							lv_badgeLevel_8_0,
                    							"org.xtext.gamify.dsl.DSL.INT");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:2088:3: (otherlv_9= 'BadgeImage:' ( (lv_badgeImage_10_0= RULE_STRING ) ) )?
            int alt56=2;
            int LA56_0 = input.LA(1);

            if ( (LA56_0==62) ) {
                alt56=1;
            }
            switch (alt56) {
                case 1 :
                    // InternalDSL.g:2089:4: otherlv_9= 'BadgeImage:' ( (lv_badgeImage_10_0= RULE_STRING ) )
                    {
                    otherlv_9=(Token)match(input,62,FOLLOW_13); 

                    				newLeafNode(otherlv_9, grammarAccess.getBadgeAccess().getBadgeImageKeyword_6_0());
                    			
                    // InternalDSL.g:2093:4: ( (lv_badgeImage_10_0= RULE_STRING ) )
                    // InternalDSL.g:2094:5: (lv_badgeImage_10_0= RULE_STRING )
                    {
                    // InternalDSL.g:2094:5: (lv_badgeImage_10_0= RULE_STRING )
                    // InternalDSL.g:2095:6: lv_badgeImage_10_0= RULE_STRING
                    {
                    lv_badgeImage_10_0=(Token)match(input,RULE_STRING,FOLLOW_12); 

                    						newLeafNode(lv_badgeImage_10_0, grammarAccess.getBadgeAccess().getBadgeImageSTRINGTerminalRuleCall_6_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getBadgeRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"badgeImage",
                    							lv_badgeImage_10_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:2112:3: (otherlv_11= 'Information:' ( (lv_information_12_0= RULE_STRING ) ) )?
            int alt57=2;
            int LA57_0 = input.LA(1);

            if ( (LA57_0==20) ) {
                alt57=1;
            }
            switch (alt57) {
                case 1 :
                    // InternalDSL.g:2113:4: otherlv_11= 'Information:' ( (lv_information_12_0= RULE_STRING ) )
                    {
                    otherlv_11=(Token)match(input,20,FOLLOW_13); 

                    				newLeafNode(otherlv_11, grammarAccess.getBadgeAccess().getInformationKeyword_7_0());
                    			
                    // InternalDSL.g:2117:4: ( (lv_information_12_0= RULE_STRING ) )
                    // InternalDSL.g:2118:5: (lv_information_12_0= RULE_STRING )
                    {
                    // InternalDSL.g:2118:5: (lv_information_12_0= RULE_STRING )
                    // InternalDSL.g:2119:6: lv_information_12_0= RULE_STRING
                    {
                    lv_information_12_0=(Token)match(input,RULE_STRING,FOLLOW_14); 

                    						newLeafNode(lv_information_12_0, grammarAccess.getBadgeAccess().getInformationSTRINGTerminalRuleCall_7_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getBadgeRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"information",
                    							lv_information_12_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_13=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_13, grammarAccess.getBadgeAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBadge"


    // $ANTLR start "entryRulePoints"
    // InternalDSL.g:2144:1: entryRulePoints returns [EObject current=null] : iv_rulePoints= rulePoints EOF ;
    public final EObject entryRulePoints() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePoints = null;


        try {
            // InternalDSL.g:2144:47: (iv_rulePoints= rulePoints EOF )
            // InternalDSL.g:2145:2: iv_rulePoints= rulePoints EOF
            {
             newCompositeNode(grammarAccess.getPointsRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePoints=rulePoints();

            state._fsp--;

             current =iv_rulePoints; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePoints"


    // $ANTLR start "rulePoints"
    // InternalDSL.g:2151:1: rulePoints returns [EObject current=null] : ( () otherlv_1= 'Points' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'RewardDescription:' ( (lv_rewardDescription_6_0= RULE_STRING ) ) )? (otherlv_7= 'Amount:' ( (lv_amount_8_0= RULE_INT ) ) )? (otherlv_9= 'IsCollectible:' ( (lv_isCollectible_10_0= ruleBoolean ) ) )? (otherlv_11= 'Conditions' otherlv_12= '(' ( ( ruleEString ) ) (otherlv_14= ',' ( ( ruleEString ) ) )* otherlv_16= ')' )? (otherlv_17= 'Information:' ( (lv_information_18_0= RULE_STRING ) ) )? otherlv_19= '}' ) ;
    public final EObject rulePoints() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token lv_rewardDescription_6_0=null;
        Token otherlv_7=null;
        Token lv_amount_8_0=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token lv_information_18_0=null;
        Token otherlv_19=null;
        AntlrDatatypeRuleToken lv_name_4_0 = null;

        AntlrDatatypeRuleToken lv_isCollectible_10_0 = null;



        	enterRule();

        try {
            // InternalDSL.g:2157:2: ( ( () otherlv_1= 'Points' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'RewardDescription:' ( (lv_rewardDescription_6_0= RULE_STRING ) ) )? (otherlv_7= 'Amount:' ( (lv_amount_8_0= RULE_INT ) ) )? (otherlv_9= 'IsCollectible:' ( (lv_isCollectible_10_0= ruleBoolean ) ) )? (otherlv_11= 'Conditions' otherlv_12= '(' ( ( ruleEString ) ) (otherlv_14= ',' ( ( ruleEString ) ) )* otherlv_16= ')' )? (otherlv_17= 'Information:' ( (lv_information_18_0= RULE_STRING ) ) )? otherlv_19= '}' ) )
            // InternalDSL.g:2158:2: ( () otherlv_1= 'Points' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'RewardDescription:' ( (lv_rewardDescription_6_0= RULE_STRING ) ) )? (otherlv_7= 'Amount:' ( (lv_amount_8_0= RULE_INT ) ) )? (otherlv_9= 'IsCollectible:' ( (lv_isCollectible_10_0= ruleBoolean ) ) )? (otherlv_11= 'Conditions' otherlv_12= '(' ( ( ruleEString ) ) (otherlv_14= ',' ( ( ruleEString ) ) )* otherlv_16= ')' )? (otherlv_17= 'Information:' ( (lv_information_18_0= RULE_STRING ) ) )? otherlv_19= '}' )
            {
            // InternalDSL.g:2158:2: ( () otherlv_1= 'Points' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'RewardDescription:' ( (lv_rewardDescription_6_0= RULE_STRING ) ) )? (otherlv_7= 'Amount:' ( (lv_amount_8_0= RULE_INT ) ) )? (otherlv_9= 'IsCollectible:' ( (lv_isCollectible_10_0= ruleBoolean ) ) )? (otherlv_11= 'Conditions' otherlv_12= '(' ( ( ruleEString ) ) (otherlv_14= ',' ( ( ruleEString ) ) )* otherlv_16= ')' )? (otherlv_17= 'Information:' ( (lv_information_18_0= RULE_STRING ) ) )? otherlv_19= '}' )
            // InternalDSL.g:2159:3: () otherlv_1= 'Points' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'RewardDescription:' ( (lv_rewardDescription_6_0= RULE_STRING ) ) )? (otherlv_7= 'Amount:' ( (lv_amount_8_0= RULE_INT ) ) )? (otherlv_9= 'IsCollectible:' ( (lv_isCollectible_10_0= ruleBoolean ) ) )? (otherlv_11= 'Conditions' otherlv_12= '(' ( ( ruleEString ) ) (otherlv_14= ',' ( ( ruleEString ) ) )* otherlv_16= ')' )? (otherlv_17= 'Information:' ( (lv_information_18_0= RULE_STRING ) ) )? otherlv_19= '}'
            {
            // InternalDSL.g:2159:3: ()
            // InternalDSL.g:2160:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getPointsAccess().getPointsAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,63,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getPointsAccess().getPointsKeyword_1());
            		
            otherlv_2=(Token)match(input,13,FOLLOW_70); 

            			newLeafNode(otherlv_2, grammarAccess.getPointsAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalDSL.g:2174:3: (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )?
            int alt58=2;
            int LA58_0 = input.LA(1);

            if ( (LA58_0==14) ) {
                alt58=1;
            }
            switch (alt58) {
                case 1 :
                    // InternalDSL.g:2175:4: otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,14,FOLLOW_5); 

                    				newLeafNode(otherlv_3, grammarAccess.getPointsAccess().getNameKeyword_3_0());
                    			
                    // InternalDSL.g:2179:4: ( (lv_name_4_0= ruleEString ) )
                    // InternalDSL.g:2180:5: (lv_name_4_0= ruleEString )
                    {
                    // InternalDSL.g:2180:5: (lv_name_4_0= ruleEString )
                    // InternalDSL.g:2181:6: lv_name_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getPointsAccess().getNameEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_71);
                    lv_name_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPointsRule());
                    						}
                    						set(
                    							current,
                    							"name",
                    							lv_name_4_0,
                    							"org.xtext.gamify.dsl.DSL.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:2199:3: (otherlv_5= 'RewardDescription:' ( (lv_rewardDescription_6_0= RULE_STRING ) ) )?
            int alt59=2;
            int LA59_0 = input.LA(1);

            if ( (LA59_0==60) ) {
                alt59=1;
            }
            switch (alt59) {
                case 1 :
                    // InternalDSL.g:2200:4: otherlv_5= 'RewardDescription:' ( (lv_rewardDescription_6_0= RULE_STRING ) )
                    {
                    otherlv_5=(Token)match(input,60,FOLLOW_13); 

                    				newLeafNode(otherlv_5, grammarAccess.getPointsAccess().getRewardDescriptionKeyword_4_0());
                    			
                    // InternalDSL.g:2204:4: ( (lv_rewardDescription_6_0= RULE_STRING ) )
                    // InternalDSL.g:2205:5: (lv_rewardDescription_6_0= RULE_STRING )
                    {
                    // InternalDSL.g:2205:5: (lv_rewardDescription_6_0= RULE_STRING )
                    // InternalDSL.g:2206:6: lv_rewardDescription_6_0= RULE_STRING
                    {
                    lv_rewardDescription_6_0=(Token)match(input,RULE_STRING,FOLLOW_72); 

                    						newLeafNode(lv_rewardDescription_6_0, grammarAccess.getPointsAccess().getRewardDescriptionSTRINGTerminalRuleCall_4_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getPointsRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"rewardDescription",
                    							lv_rewardDescription_6_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:2223:3: (otherlv_7= 'Amount:' ( (lv_amount_8_0= RULE_INT ) ) )?
            int alt60=2;
            int LA60_0 = input.LA(1);

            if ( (LA60_0==51) ) {
                alt60=1;
            }
            switch (alt60) {
                case 1 :
                    // InternalDSL.g:2224:4: otherlv_7= 'Amount:' ( (lv_amount_8_0= RULE_INT ) )
                    {
                    otherlv_7=(Token)match(input,51,FOLLOW_48); 

                    				newLeafNode(otherlv_7, grammarAccess.getPointsAccess().getAmountKeyword_5_0());
                    			
                    // InternalDSL.g:2228:4: ( (lv_amount_8_0= RULE_INT ) )
                    // InternalDSL.g:2229:5: (lv_amount_8_0= RULE_INT )
                    {
                    // InternalDSL.g:2229:5: (lv_amount_8_0= RULE_INT )
                    // InternalDSL.g:2230:6: lv_amount_8_0= RULE_INT
                    {
                    lv_amount_8_0=(Token)match(input,RULE_INT,FOLLOW_73); 

                    						newLeafNode(lv_amount_8_0, grammarAccess.getPointsAccess().getAmountINTTerminalRuleCall_5_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getPointsRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"amount",
                    							lv_amount_8_0,
                    							"org.xtext.gamify.dsl.DSL.INT");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:2247:3: (otherlv_9= 'IsCollectible:' ( (lv_isCollectible_10_0= ruleBoolean ) ) )?
            int alt61=2;
            int LA61_0 = input.LA(1);

            if ( (LA61_0==64) ) {
                alt61=1;
            }
            switch (alt61) {
                case 1 :
                    // InternalDSL.g:2248:4: otherlv_9= 'IsCollectible:' ( (lv_isCollectible_10_0= ruleBoolean ) )
                    {
                    otherlv_9=(Token)match(input,64,FOLLOW_36); 

                    				newLeafNode(otherlv_9, grammarAccess.getPointsAccess().getIsCollectibleKeyword_6_0());
                    			
                    // InternalDSL.g:2252:4: ( (lv_isCollectible_10_0= ruleBoolean ) )
                    // InternalDSL.g:2253:5: (lv_isCollectible_10_0= ruleBoolean )
                    {
                    // InternalDSL.g:2253:5: (lv_isCollectible_10_0= ruleBoolean )
                    // InternalDSL.g:2254:6: lv_isCollectible_10_0= ruleBoolean
                    {

                    						newCompositeNode(grammarAccess.getPointsAccess().getIsCollectibleBooleanParserRuleCall_6_1_0());
                    					
                    pushFollow(FOLLOW_74);
                    lv_isCollectible_10_0=ruleBoolean();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPointsRule());
                    						}
                    						set(
                    							current,
                    							"isCollectible",
                    							lv_isCollectible_10_0,
                    							"org.xtext.gamify.dsl.DSL.Boolean");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:2272:3: (otherlv_11= 'Conditions' otherlv_12= '(' ( ( ruleEString ) ) (otherlv_14= ',' ( ( ruleEString ) ) )* otherlv_16= ')' )?
            int alt63=2;
            int LA63_0 = input.LA(1);

            if ( (LA63_0==38) ) {
                alt63=1;
            }
            switch (alt63) {
                case 1 :
                    // InternalDSL.g:2273:4: otherlv_11= 'Conditions' otherlv_12= '(' ( ( ruleEString ) ) (otherlv_14= ',' ( ( ruleEString ) ) )* otherlv_16= ')'
                    {
                    otherlv_11=(Token)match(input,38,FOLLOW_38); 

                    				newLeafNode(otherlv_11, grammarAccess.getPointsAccess().getConditionsKeyword_7_0());
                    			
                    otherlv_12=(Token)match(input,35,FOLLOW_5); 

                    				newLeafNode(otherlv_12, grammarAccess.getPointsAccess().getLeftParenthesisKeyword_7_1());
                    			
                    // InternalDSL.g:2281:4: ( ( ruleEString ) )
                    // InternalDSL.g:2282:5: ( ruleEString )
                    {
                    // InternalDSL.g:2282:5: ( ruleEString )
                    // InternalDSL.g:2283:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getPointsRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getPointsAccess().getConditionsConditionCrossReference_7_2_0());
                    					
                    pushFollow(FOLLOW_39);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalDSL.g:2297:4: (otherlv_14= ',' ( ( ruleEString ) ) )*
                    loop62:
                    do {
                        int alt62=2;
                        int LA62_0 = input.LA(1);

                        if ( (LA62_0==17) ) {
                            alt62=1;
                        }


                        switch (alt62) {
                    	case 1 :
                    	    // InternalDSL.g:2298:5: otherlv_14= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_14=(Token)match(input,17,FOLLOW_5); 

                    	    					newLeafNode(otherlv_14, grammarAccess.getPointsAccess().getCommaKeyword_7_3_0());
                    	    				
                    	    // InternalDSL.g:2302:5: ( ( ruleEString ) )
                    	    // InternalDSL.g:2303:6: ( ruleEString )
                    	    {
                    	    // InternalDSL.g:2303:6: ( ruleEString )
                    	    // InternalDSL.g:2304:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getPointsRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getPointsAccess().getConditionsConditionCrossReference_7_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_39);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop62;
                        }
                    } while (true);

                    otherlv_16=(Token)match(input,36,FOLLOW_12); 

                    				newLeafNode(otherlv_16, grammarAccess.getPointsAccess().getRightParenthesisKeyword_7_4());
                    			

                    }
                    break;

            }

            // InternalDSL.g:2324:3: (otherlv_17= 'Information:' ( (lv_information_18_0= RULE_STRING ) ) )?
            int alt64=2;
            int LA64_0 = input.LA(1);

            if ( (LA64_0==20) ) {
                alt64=1;
            }
            switch (alt64) {
                case 1 :
                    // InternalDSL.g:2325:4: otherlv_17= 'Information:' ( (lv_information_18_0= RULE_STRING ) )
                    {
                    otherlv_17=(Token)match(input,20,FOLLOW_13); 

                    				newLeafNode(otherlv_17, grammarAccess.getPointsAccess().getInformationKeyword_8_0());
                    			
                    // InternalDSL.g:2329:4: ( (lv_information_18_0= RULE_STRING ) )
                    // InternalDSL.g:2330:5: (lv_information_18_0= RULE_STRING )
                    {
                    // InternalDSL.g:2330:5: (lv_information_18_0= RULE_STRING )
                    // InternalDSL.g:2331:6: lv_information_18_0= RULE_STRING
                    {
                    lv_information_18_0=(Token)match(input,RULE_STRING,FOLLOW_14); 

                    						newLeafNode(lv_information_18_0, grammarAccess.getPointsAccess().getInformationSTRINGTerminalRuleCall_8_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getPointsRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"information",
                    							lv_information_18_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_19=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_19, grammarAccess.getPointsAccess().getRightCurlyBracketKeyword_9());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePoints"


    // $ANTLR start "entryRulePrize"
    // InternalDSL.g:2356:1: entryRulePrize returns [EObject current=null] : iv_rulePrize= rulePrize EOF ;
    public final EObject entryRulePrize() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrize = null;


        try {
            // InternalDSL.g:2356:46: (iv_rulePrize= rulePrize EOF )
            // InternalDSL.g:2357:2: iv_rulePrize= rulePrize EOF
            {
             newCompositeNode(grammarAccess.getPrizeRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePrize=rulePrize();

            state._fsp--;

             current =iv_rulePrize; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrize"


    // $ANTLR start "rulePrize"
    // InternalDSL.g:2363:1: rulePrize returns [EObject current=null] : ( () otherlv_1= 'Prize' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'RewardDescription:' ( (lv_rewardDescription_6_0= RULE_STRING ) ) )? (otherlv_7= 'PrizeType:' ( (lv_prizeType_8_0= rulePrizeTypes ) ) )? (otherlv_9= 'Information:' ( (lv_information_10_0= RULE_STRING ) ) )? otherlv_11= '}' ) ;
    public final EObject rulePrize() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token lv_rewardDescription_6_0=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token lv_information_10_0=null;
        Token otherlv_11=null;
        AntlrDatatypeRuleToken lv_name_4_0 = null;

        Enumerator lv_prizeType_8_0 = null;



        	enterRule();

        try {
            // InternalDSL.g:2369:2: ( ( () otherlv_1= 'Prize' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'RewardDescription:' ( (lv_rewardDescription_6_0= RULE_STRING ) ) )? (otherlv_7= 'PrizeType:' ( (lv_prizeType_8_0= rulePrizeTypes ) ) )? (otherlv_9= 'Information:' ( (lv_information_10_0= RULE_STRING ) ) )? otherlv_11= '}' ) )
            // InternalDSL.g:2370:2: ( () otherlv_1= 'Prize' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'RewardDescription:' ( (lv_rewardDescription_6_0= RULE_STRING ) ) )? (otherlv_7= 'PrizeType:' ( (lv_prizeType_8_0= rulePrizeTypes ) ) )? (otherlv_9= 'Information:' ( (lv_information_10_0= RULE_STRING ) ) )? otherlv_11= '}' )
            {
            // InternalDSL.g:2370:2: ( () otherlv_1= 'Prize' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'RewardDescription:' ( (lv_rewardDescription_6_0= RULE_STRING ) ) )? (otherlv_7= 'PrizeType:' ( (lv_prizeType_8_0= rulePrizeTypes ) ) )? (otherlv_9= 'Information:' ( (lv_information_10_0= RULE_STRING ) ) )? otherlv_11= '}' )
            // InternalDSL.g:2371:3: () otherlv_1= 'Prize' otherlv_2= '{' (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )? (otherlv_5= 'RewardDescription:' ( (lv_rewardDescription_6_0= RULE_STRING ) ) )? (otherlv_7= 'PrizeType:' ( (lv_prizeType_8_0= rulePrizeTypes ) ) )? (otherlv_9= 'Information:' ( (lv_information_10_0= RULE_STRING ) ) )? otherlv_11= '}'
            {
            // InternalDSL.g:2371:3: ()
            // InternalDSL.g:2372:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getPrizeAccess().getPrizeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,65,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getPrizeAccess().getPrizeKeyword_1());
            		
            otherlv_2=(Token)match(input,13,FOLLOW_75); 

            			newLeafNode(otherlv_2, grammarAccess.getPrizeAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalDSL.g:2386:3: (otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) ) )?
            int alt65=2;
            int LA65_0 = input.LA(1);

            if ( (LA65_0==14) ) {
                alt65=1;
            }
            switch (alt65) {
                case 1 :
                    // InternalDSL.g:2387:4: otherlv_3= 'Name:' ( (lv_name_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,14,FOLLOW_5); 

                    				newLeafNode(otherlv_3, grammarAccess.getPrizeAccess().getNameKeyword_3_0());
                    			
                    // InternalDSL.g:2391:4: ( (lv_name_4_0= ruleEString ) )
                    // InternalDSL.g:2392:5: (lv_name_4_0= ruleEString )
                    {
                    // InternalDSL.g:2392:5: (lv_name_4_0= ruleEString )
                    // InternalDSL.g:2393:6: lv_name_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getPrizeAccess().getNameEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_76);
                    lv_name_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPrizeRule());
                    						}
                    						set(
                    							current,
                    							"name",
                    							lv_name_4_0,
                    							"org.xtext.gamify.dsl.DSL.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:2411:3: (otherlv_5= 'RewardDescription:' ( (lv_rewardDescription_6_0= RULE_STRING ) ) )?
            int alt66=2;
            int LA66_0 = input.LA(1);

            if ( (LA66_0==60) ) {
                alt66=1;
            }
            switch (alt66) {
                case 1 :
                    // InternalDSL.g:2412:4: otherlv_5= 'RewardDescription:' ( (lv_rewardDescription_6_0= RULE_STRING ) )
                    {
                    otherlv_5=(Token)match(input,60,FOLLOW_13); 

                    				newLeafNode(otherlv_5, grammarAccess.getPrizeAccess().getRewardDescriptionKeyword_4_0());
                    			
                    // InternalDSL.g:2416:4: ( (lv_rewardDescription_6_0= RULE_STRING ) )
                    // InternalDSL.g:2417:5: (lv_rewardDescription_6_0= RULE_STRING )
                    {
                    // InternalDSL.g:2417:5: (lv_rewardDescription_6_0= RULE_STRING )
                    // InternalDSL.g:2418:6: lv_rewardDescription_6_0= RULE_STRING
                    {
                    lv_rewardDescription_6_0=(Token)match(input,RULE_STRING,FOLLOW_77); 

                    						newLeafNode(lv_rewardDescription_6_0, grammarAccess.getPrizeAccess().getRewardDescriptionSTRINGTerminalRuleCall_4_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getPrizeRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"rewardDescription",
                    							lv_rewardDescription_6_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:2435:3: (otherlv_7= 'PrizeType:' ( (lv_prizeType_8_0= rulePrizeTypes ) ) )?
            int alt67=2;
            int LA67_0 = input.LA(1);

            if ( (LA67_0==66) ) {
                alt67=1;
            }
            switch (alt67) {
                case 1 :
                    // InternalDSL.g:2436:4: otherlv_7= 'PrizeType:' ( (lv_prizeType_8_0= rulePrizeTypes ) )
                    {
                    otherlv_7=(Token)match(input,66,FOLLOW_78); 

                    				newLeafNode(otherlv_7, grammarAccess.getPrizeAccess().getPrizeTypeKeyword_5_0());
                    			
                    // InternalDSL.g:2440:4: ( (lv_prizeType_8_0= rulePrizeTypes ) )
                    // InternalDSL.g:2441:5: (lv_prizeType_8_0= rulePrizeTypes )
                    {
                    // InternalDSL.g:2441:5: (lv_prizeType_8_0= rulePrizeTypes )
                    // InternalDSL.g:2442:6: lv_prizeType_8_0= rulePrizeTypes
                    {

                    						newCompositeNode(grammarAccess.getPrizeAccess().getPrizeTypePrizeTypesEnumRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_12);
                    lv_prizeType_8_0=rulePrizeTypes();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPrizeRule());
                    						}
                    						set(
                    							current,
                    							"prizeType",
                    							lv_prizeType_8_0,
                    							"org.xtext.gamify.dsl.DSL.PrizeTypes");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDSL.g:2460:3: (otherlv_9= 'Information:' ( (lv_information_10_0= RULE_STRING ) ) )?
            int alt68=2;
            int LA68_0 = input.LA(1);

            if ( (LA68_0==20) ) {
                alt68=1;
            }
            switch (alt68) {
                case 1 :
                    // InternalDSL.g:2461:4: otherlv_9= 'Information:' ( (lv_information_10_0= RULE_STRING ) )
                    {
                    otherlv_9=(Token)match(input,20,FOLLOW_13); 

                    				newLeafNode(otherlv_9, grammarAccess.getPrizeAccess().getInformationKeyword_6_0());
                    			
                    // InternalDSL.g:2465:4: ( (lv_information_10_0= RULE_STRING ) )
                    // InternalDSL.g:2466:5: (lv_information_10_0= RULE_STRING )
                    {
                    // InternalDSL.g:2466:5: (lv_information_10_0= RULE_STRING )
                    // InternalDSL.g:2467:6: lv_information_10_0= RULE_STRING
                    {
                    lv_information_10_0=(Token)match(input,RULE_STRING,FOLLOW_14); 

                    						newLeafNode(lv_information_10_0, grammarAccess.getPrizeAccess().getInformationSTRINGTerminalRuleCall_6_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getPrizeRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"information",
                    							lv_information_10_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_11=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_11, grammarAccess.getPrizeAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrize"


    // $ANTLR start "ruleSystemAttributes"
    // InternalDSL.g:2492:1: ruleSystemAttributes returns [Enumerator current=null] : ( (enumLiteral_0= 'ESTABLISHED' ) | (enumLiteral_1= 'GROWING' ) | (enumLiteral_2= 'NEW' ) ) ;
    public final Enumerator ruleSystemAttributes() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;


        	enterRule();

        try {
            // InternalDSL.g:2498:2: ( ( (enumLiteral_0= 'ESTABLISHED' ) | (enumLiteral_1= 'GROWING' ) | (enumLiteral_2= 'NEW' ) ) )
            // InternalDSL.g:2499:2: ( (enumLiteral_0= 'ESTABLISHED' ) | (enumLiteral_1= 'GROWING' ) | (enumLiteral_2= 'NEW' ) )
            {
            // InternalDSL.g:2499:2: ( (enumLiteral_0= 'ESTABLISHED' ) | (enumLiteral_1= 'GROWING' ) | (enumLiteral_2= 'NEW' ) )
            int alt69=3;
            switch ( input.LA(1) ) {
            case 67:
                {
                alt69=1;
                }
                break;
            case 68:
                {
                alt69=2;
                }
                break;
            case 69:
                {
                alt69=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 69, 0, input);

                throw nvae;
            }

            switch (alt69) {
                case 1 :
                    // InternalDSL.g:2500:3: (enumLiteral_0= 'ESTABLISHED' )
                    {
                    // InternalDSL.g:2500:3: (enumLiteral_0= 'ESTABLISHED' )
                    // InternalDSL.g:2501:4: enumLiteral_0= 'ESTABLISHED'
                    {
                    enumLiteral_0=(Token)match(input,67,FOLLOW_2); 

                    				current = grammarAccess.getSystemAttributesAccess().getESTABLISHEDEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getSystemAttributesAccess().getESTABLISHEDEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalDSL.g:2508:3: (enumLiteral_1= 'GROWING' )
                    {
                    // InternalDSL.g:2508:3: (enumLiteral_1= 'GROWING' )
                    // InternalDSL.g:2509:4: enumLiteral_1= 'GROWING'
                    {
                    enumLiteral_1=(Token)match(input,68,FOLLOW_2); 

                    				current = grammarAccess.getSystemAttributesAccess().getGROWINGEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getSystemAttributesAccess().getGROWINGEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalDSL.g:2516:3: (enumLiteral_2= 'NEW' )
                    {
                    // InternalDSL.g:2516:3: (enumLiteral_2= 'NEW' )
                    // InternalDSL.g:2517:4: enumLiteral_2= 'NEW'
                    {
                    enumLiteral_2=(Token)match(input,69,FOLLOW_2); 

                    				current = grammarAccess.getSystemAttributesAccess().getNEWEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getSystemAttributesAccess().getNEWEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSystemAttributes"


    // $ANTLR start "ruleUserTypes"
    // InternalDSL.g:2527:1: ruleUserTypes returns [Enumerator current=null] : ( (enumLiteral_0= 'EMPLOYEE' ) | (enumLiteral_1= 'CUSTOMER' ) ) ;
    public final Enumerator ruleUserTypes() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalDSL.g:2533:2: ( ( (enumLiteral_0= 'EMPLOYEE' ) | (enumLiteral_1= 'CUSTOMER' ) ) )
            // InternalDSL.g:2534:2: ( (enumLiteral_0= 'EMPLOYEE' ) | (enumLiteral_1= 'CUSTOMER' ) )
            {
            // InternalDSL.g:2534:2: ( (enumLiteral_0= 'EMPLOYEE' ) | (enumLiteral_1= 'CUSTOMER' ) )
            int alt70=2;
            int LA70_0 = input.LA(1);

            if ( (LA70_0==70) ) {
                alt70=1;
            }
            else if ( (LA70_0==71) ) {
                alt70=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 70, 0, input);

                throw nvae;
            }
            switch (alt70) {
                case 1 :
                    // InternalDSL.g:2535:3: (enumLiteral_0= 'EMPLOYEE' )
                    {
                    // InternalDSL.g:2535:3: (enumLiteral_0= 'EMPLOYEE' )
                    // InternalDSL.g:2536:4: enumLiteral_0= 'EMPLOYEE'
                    {
                    enumLiteral_0=(Token)match(input,70,FOLLOW_2); 

                    				current = grammarAccess.getUserTypesAccess().getEMPLOYEEEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getUserTypesAccess().getEMPLOYEEEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalDSL.g:2543:3: (enumLiteral_1= 'CUSTOMER' )
                    {
                    // InternalDSL.g:2543:3: (enumLiteral_1= 'CUSTOMER' )
                    // InternalDSL.g:2544:4: enumLiteral_1= 'CUSTOMER'
                    {
                    enumLiteral_1=(Token)match(input,71,FOLLOW_2); 

                    				current = grammarAccess.getUserTypesAccess().getCUSTOMEREnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getUserTypesAccess().getCUSTOMEREnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUserTypes"


    // $ANTLR start "ruleUserBaseAttributes"
    // InternalDSL.g:2554:1: ruleUserBaseAttributes returns [Enumerator current=null] : ( (enumLiteral_0= 'KILLERS' ) | (enumLiteral_1= 'SOCIALIZERS' ) | (enumLiteral_2= 'EXPLORERS' ) | (enumLiteral_3= 'ACHIEVERS' ) ) ;
    public final Enumerator ruleUserBaseAttributes() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;


        	enterRule();

        try {
            // InternalDSL.g:2560:2: ( ( (enumLiteral_0= 'KILLERS' ) | (enumLiteral_1= 'SOCIALIZERS' ) | (enumLiteral_2= 'EXPLORERS' ) | (enumLiteral_3= 'ACHIEVERS' ) ) )
            // InternalDSL.g:2561:2: ( (enumLiteral_0= 'KILLERS' ) | (enumLiteral_1= 'SOCIALIZERS' ) | (enumLiteral_2= 'EXPLORERS' ) | (enumLiteral_3= 'ACHIEVERS' ) )
            {
            // InternalDSL.g:2561:2: ( (enumLiteral_0= 'KILLERS' ) | (enumLiteral_1= 'SOCIALIZERS' ) | (enumLiteral_2= 'EXPLORERS' ) | (enumLiteral_3= 'ACHIEVERS' ) )
            int alt71=4;
            switch ( input.LA(1) ) {
            case 72:
                {
                alt71=1;
                }
                break;
            case 73:
                {
                alt71=2;
                }
                break;
            case 74:
                {
                alt71=3;
                }
                break;
            case 75:
                {
                alt71=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 71, 0, input);

                throw nvae;
            }

            switch (alt71) {
                case 1 :
                    // InternalDSL.g:2562:3: (enumLiteral_0= 'KILLERS' )
                    {
                    // InternalDSL.g:2562:3: (enumLiteral_0= 'KILLERS' )
                    // InternalDSL.g:2563:4: enumLiteral_0= 'KILLERS'
                    {
                    enumLiteral_0=(Token)match(input,72,FOLLOW_2); 

                    				current = grammarAccess.getUserBaseAttributesAccess().getKILLERSEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getUserBaseAttributesAccess().getKILLERSEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalDSL.g:2570:3: (enumLiteral_1= 'SOCIALIZERS' )
                    {
                    // InternalDSL.g:2570:3: (enumLiteral_1= 'SOCIALIZERS' )
                    // InternalDSL.g:2571:4: enumLiteral_1= 'SOCIALIZERS'
                    {
                    enumLiteral_1=(Token)match(input,73,FOLLOW_2); 

                    				current = grammarAccess.getUserBaseAttributesAccess().getSOCIALIZERSEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getUserBaseAttributesAccess().getSOCIALIZERSEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalDSL.g:2578:3: (enumLiteral_2= 'EXPLORERS' )
                    {
                    // InternalDSL.g:2578:3: (enumLiteral_2= 'EXPLORERS' )
                    // InternalDSL.g:2579:4: enumLiteral_2= 'EXPLORERS'
                    {
                    enumLiteral_2=(Token)match(input,74,FOLLOW_2); 

                    				current = grammarAccess.getUserBaseAttributesAccess().getEXPLORERSEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getUserBaseAttributesAccess().getEXPLORERSEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalDSL.g:2586:3: (enumLiteral_3= 'ACHIEVERS' )
                    {
                    // InternalDSL.g:2586:3: (enumLiteral_3= 'ACHIEVERS' )
                    // InternalDSL.g:2587:4: enumLiteral_3= 'ACHIEVERS'
                    {
                    enumLiteral_3=(Token)match(input,75,FOLLOW_2); 

                    				current = grammarAccess.getUserBaseAttributesAccess().getACHIEVERSEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getUserBaseAttributesAccess().getACHIEVERSEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUserBaseAttributes"


    // $ANTLR start "ruleDynamicTypes"
    // InternalDSL.g:2597:1: ruleDynamicTypes returns [Enumerator current=null] : ( (enumLiteral_0= 'TAKEHOLD' ) | (enumLiteral_1= 'CUSTOM' ) | (enumLiteral_2= 'TREASUREHUNT' ) | (enumLiteral_3= 'REVIEW' ) | (enumLiteral_4= 'BET' ) | (enumLiteral_5= 'PRIZESHOP' ) | (enumLiteral_6= 'LOTTERY' ) | (enumLiteral_7= 'PROFILE' ) ) ;
    public final Enumerator ruleDynamicTypes() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;
        Token enumLiteral_6=null;
        Token enumLiteral_7=null;


        	enterRule();

        try {
            // InternalDSL.g:2603:2: ( ( (enumLiteral_0= 'TAKEHOLD' ) | (enumLiteral_1= 'CUSTOM' ) | (enumLiteral_2= 'TREASUREHUNT' ) | (enumLiteral_3= 'REVIEW' ) | (enumLiteral_4= 'BET' ) | (enumLiteral_5= 'PRIZESHOP' ) | (enumLiteral_6= 'LOTTERY' ) | (enumLiteral_7= 'PROFILE' ) ) )
            // InternalDSL.g:2604:2: ( (enumLiteral_0= 'TAKEHOLD' ) | (enumLiteral_1= 'CUSTOM' ) | (enumLiteral_2= 'TREASUREHUNT' ) | (enumLiteral_3= 'REVIEW' ) | (enumLiteral_4= 'BET' ) | (enumLiteral_5= 'PRIZESHOP' ) | (enumLiteral_6= 'LOTTERY' ) | (enumLiteral_7= 'PROFILE' ) )
            {
            // InternalDSL.g:2604:2: ( (enumLiteral_0= 'TAKEHOLD' ) | (enumLiteral_1= 'CUSTOM' ) | (enumLiteral_2= 'TREASUREHUNT' ) | (enumLiteral_3= 'REVIEW' ) | (enumLiteral_4= 'BET' ) | (enumLiteral_5= 'PRIZESHOP' ) | (enumLiteral_6= 'LOTTERY' ) | (enumLiteral_7= 'PROFILE' ) )
            int alt72=8;
            switch ( input.LA(1) ) {
            case 76:
                {
                alt72=1;
                }
                break;
            case 77:
                {
                alt72=2;
                }
                break;
            case 78:
                {
                alt72=3;
                }
                break;
            case 79:
                {
                alt72=4;
                }
                break;
            case 80:
                {
                alt72=5;
                }
                break;
            case 81:
                {
                alt72=6;
                }
                break;
            case 82:
                {
                alt72=7;
                }
                break;
            case 83:
                {
                alt72=8;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 72, 0, input);

                throw nvae;
            }

            switch (alt72) {
                case 1 :
                    // InternalDSL.g:2605:3: (enumLiteral_0= 'TAKEHOLD' )
                    {
                    // InternalDSL.g:2605:3: (enumLiteral_0= 'TAKEHOLD' )
                    // InternalDSL.g:2606:4: enumLiteral_0= 'TAKEHOLD'
                    {
                    enumLiteral_0=(Token)match(input,76,FOLLOW_2); 

                    				current = grammarAccess.getDynamicTypesAccess().getTAKEHOLDEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getDynamicTypesAccess().getTAKEHOLDEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalDSL.g:2613:3: (enumLiteral_1= 'CUSTOM' )
                    {
                    // InternalDSL.g:2613:3: (enumLiteral_1= 'CUSTOM' )
                    // InternalDSL.g:2614:4: enumLiteral_1= 'CUSTOM'
                    {
                    enumLiteral_1=(Token)match(input,77,FOLLOW_2); 

                    				current = grammarAccess.getDynamicTypesAccess().getCUSTOMEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getDynamicTypesAccess().getCUSTOMEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalDSL.g:2621:3: (enumLiteral_2= 'TREASUREHUNT' )
                    {
                    // InternalDSL.g:2621:3: (enumLiteral_2= 'TREASUREHUNT' )
                    // InternalDSL.g:2622:4: enumLiteral_2= 'TREASUREHUNT'
                    {
                    enumLiteral_2=(Token)match(input,78,FOLLOW_2); 

                    				current = grammarAccess.getDynamicTypesAccess().getTREASUREHUNTEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getDynamicTypesAccess().getTREASUREHUNTEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalDSL.g:2629:3: (enumLiteral_3= 'REVIEW' )
                    {
                    // InternalDSL.g:2629:3: (enumLiteral_3= 'REVIEW' )
                    // InternalDSL.g:2630:4: enumLiteral_3= 'REVIEW'
                    {
                    enumLiteral_3=(Token)match(input,79,FOLLOW_2); 

                    				current = grammarAccess.getDynamicTypesAccess().getREVIEWEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getDynamicTypesAccess().getREVIEWEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalDSL.g:2637:3: (enumLiteral_4= 'BET' )
                    {
                    // InternalDSL.g:2637:3: (enumLiteral_4= 'BET' )
                    // InternalDSL.g:2638:4: enumLiteral_4= 'BET'
                    {
                    enumLiteral_4=(Token)match(input,80,FOLLOW_2); 

                    				current = grammarAccess.getDynamicTypesAccess().getBETEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_4, grammarAccess.getDynamicTypesAccess().getBETEnumLiteralDeclaration_4());
                    			

                    }


                    }
                    break;
                case 6 :
                    // InternalDSL.g:2645:3: (enumLiteral_5= 'PRIZESHOP' )
                    {
                    // InternalDSL.g:2645:3: (enumLiteral_5= 'PRIZESHOP' )
                    // InternalDSL.g:2646:4: enumLiteral_5= 'PRIZESHOP'
                    {
                    enumLiteral_5=(Token)match(input,81,FOLLOW_2); 

                    				current = grammarAccess.getDynamicTypesAccess().getPRIZESHOPEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_5, grammarAccess.getDynamicTypesAccess().getPRIZESHOPEnumLiteralDeclaration_5());
                    			

                    }


                    }
                    break;
                case 7 :
                    // InternalDSL.g:2653:3: (enumLiteral_6= 'LOTTERY' )
                    {
                    // InternalDSL.g:2653:3: (enumLiteral_6= 'LOTTERY' )
                    // InternalDSL.g:2654:4: enumLiteral_6= 'LOTTERY'
                    {
                    enumLiteral_6=(Token)match(input,82,FOLLOW_2); 

                    				current = grammarAccess.getDynamicTypesAccess().getLOTTERYEnumLiteralDeclaration_6().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_6, grammarAccess.getDynamicTypesAccess().getLOTTERYEnumLiteralDeclaration_6());
                    			

                    }


                    }
                    break;
                case 8 :
                    // InternalDSL.g:2661:3: (enumLiteral_7= 'PROFILE' )
                    {
                    // InternalDSL.g:2661:3: (enumLiteral_7= 'PROFILE' )
                    // InternalDSL.g:2662:4: enumLiteral_7= 'PROFILE'
                    {
                    enumLiteral_7=(Token)match(input,83,FOLLOW_2); 

                    				current = grammarAccess.getDynamicTypesAccess().getPROFILEEnumLiteralDeclaration_7().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_7, grammarAccess.getDynamicTypesAccess().getPROFILEEnumLiteralDeclaration_7());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDynamicTypes"


    // $ANTLR start "ruleGameMechanicTypes"
    // InternalDSL.g:2672:1: ruleGameMechanicTypes returns [Enumerator current=null] : ( (enumLiteral_0= 'LEADERBOARD' ) | (enumLiteral_1= 'QUEST' ) | (enumLiteral_2= 'PROGRESSBAR' ) | (enumLiteral_3= 'COOPERATION' ) | (enumLiteral_4= 'OPPONENT' ) | (enumLiteral_5= 'COLLECTIBLE' ) | (enumLiteral_6= 'INVENTORY' ) | (enumLiteral_7= 'SOCIALGIFT' ) | (enumLiteral_8= 'AVATAR' ) ) ;
    public final Enumerator ruleGameMechanicTypes() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;
        Token enumLiteral_6=null;
        Token enumLiteral_7=null;
        Token enumLiteral_8=null;


        	enterRule();

        try {
            // InternalDSL.g:2678:2: ( ( (enumLiteral_0= 'LEADERBOARD' ) | (enumLiteral_1= 'QUEST' ) | (enumLiteral_2= 'PROGRESSBAR' ) | (enumLiteral_3= 'COOPERATION' ) | (enumLiteral_4= 'OPPONENT' ) | (enumLiteral_5= 'COLLECTIBLE' ) | (enumLiteral_6= 'INVENTORY' ) | (enumLiteral_7= 'SOCIALGIFT' ) | (enumLiteral_8= 'AVATAR' ) ) )
            // InternalDSL.g:2679:2: ( (enumLiteral_0= 'LEADERBOARD' ) | (enumLiteral_1= 'QUEST' ) | (enumLiteral_2= 'PROGRESSBAR' ) | (enumLiteral_3= 'COOPERATION' ) | (enumLiteral_4= 'OPPONENT' ) | (enumLiteral_5= 'COLLECTIBLE' ) | (enumLiteral_6= 'INVENTORY' ) | (enumLiteral_7= 'SOCIALGIFT' ) | (enumLiteral_8= 'AVATAR' ) )
            {
            // InternalDSL.g:2679:2: ( (enumLiteral_0= 'LEADERBOARD' ) | (enumLiteral_1= 'QUEST' ) | (enumLiteral_2= 'PROGRESSBAR' ) | (enumLiteral_3= 'COOPERATION' ) | (enumLiteral_4= 'OPPONENT' ) | (enumLiteral_5= 'COLLECTIBLE' ) | (enumLiteral_6= 'INVENTORY' ) | (enumLiteral_7= 'SOCIALGIFT' ) | (enumLiteral_8= 'AVATAR' ) )
            int alt73=9;
            switch ( input.LA(1) ) {
            case 84:
                {
                alt73=1;
                }
                break;
            case 85:
                {
                alt73=2;
                }
                break;
            case 86:
                {
                alt73=3;
                }
                break;
            case 87:
                {
                alt73=4;
                }
                break;
            case 88:
                {
                alt73=5;
                }
                break;
            case 89:
                {
                alt73=6;
                }
                break;
            case 90:
                {
                alt73=7;
                }
                break;
            case 91:
                {
                alt73=8;
                }
                break;
            case 92:
                {
                alt73=9;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 73, 0, input);

                throw nvae;
            }

            switch (alt73) {
                case 1 :
                    // InternalDSL.g:2680:3: (enumLiteral_0= 'LEADERBOARD' )
                    {
                    // InternalDSL.g:2680:3: (enumLiteral_0= 'LEADERBOARD' )
                    // InternalDSL.g:2681:4: enumLiteral_0= 'LEADERBOARD'
                    {
                    enumLiteral_0=(Token)match(input,84,FOLLOW_2); 

                    				current = grammarAccess.getGameMechanicTypesAccess().getLEADERBOARDEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getGameMechanicTypesAccess().getLEADERBOARDEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalDSL.g:2688:3: (enumLiteral_1= 'QUEST' )
                    {
                    // InternalDSL.g:2688:3: (enumLiteral_1= 'QUEST' )
                    // InternalDSL.g:2689:4: enumLiteral_1= 'QUEST'
                    {
                    enumLiteral_1=(Token)match(input,85,FOLLOW_2); 

                    				current = grammarAccess.getGameMechanicTypesAccess().getQUESTEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getGameMechanicTypesAccess().getQUESTEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalDSL.g:2696:3: (enumLiteral_2= 'PROGRESSBAR' )
                    {
                    // InternalDSL.g:2696:3: (enumLiteral_2= 'PROGRESSBAR' )
                    // InternalDSL.g:2697:4: enumLiteral_2= 'PROGRESSBAR'
                    {
                    enumLiteral_2=(Token)match(input,86,FOLLOW_2); 

                    				current = grammarAccess.getGameMechanicTypesAccess().getPROGRESSBAREnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getGameMechanicTypesAccess().getPROGRESSBAREnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalDSL.g:2704:3: (enumLiteral_3= 'COOPERATION' )
                    {
                    // InternalDSL.g:2704:3: (enumLiteral_3= 'COOPERATION' )
                    // InternalDSL.g:2705:4: enumLiteral_3= 'COOPERATION'
                    {
                    enumLiteral_3=(Token)match(input,87,FOLLOW_2); 

                    				current = grammarAccess.getGameMechanicTypesAccess().getCOOPERATIONEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getGameMechanicTypesAccess().getCOOPERATIONEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalDSL.g:2712:3: (enumLiteral_4= 'OPPONENT' )
                    {
                    // InternalDSL.g:2712:3: (enumLiteral_4= 'OPPONENT' )
                    // InternalDSL.g:2713:4: enumLiteral_4= 'OPPONENT'
                    {
                    enumLiteral_4=(Token)match(input,88,FOLLOW_2); 

                    				current = grammarAccess.getGameMechanicTypesAccess().getOPPONENTEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_4, grammarAccess.getGameMechanicTypesAccess().getOPPONENTEnumLiteralDeclaration_4());
                    			

                    }


                    }
                    break;
                case 6 :
                    // InternalDSL.g:2720:3: (enumLiteral_5= 'COLLECTIBLE' )
                    {
                    // InternalDSL.g:2720:3: (enumLiteral_5= 'COLLECTIBLE' )
                    // InternalDSL.g:2721:4: enumLiteral_5= 'COLLECTIBLE'
                    {
                    enumLiteral_5=(Token)match(input,89,FOLLOW_2); 

                    				current = grammarAccess.getGameMechanicTypesAccess().getCOLLECTIBLEEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_5, grammarAccess.getGameMechanicTypesAccess().getCOLLECTIBLEEnumLiteralDeclaration_5());
                    			

                    }


                    }
                    break;
                case 7 :
                    // InternalDSL.g:2728:3: (enumLiteral_6= 'INVENTORY' )
                    {
                    // InternalDSL.g:2728:3: (enumLiteral_6= 'INVENTORY' )
                    // InternalDSL.g:2729:4: enumLiteral_6= 'INVENTORY'
                    {
                    enumLiteral_6=(Token)match(input,90,FOLLOW_2); 

                    				current = grammarAccess.getGameMechanicTypesAccess().getINVENTORYEnumLiteralDeclaration_6().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_6, grammarAccess.getGameMechanicTypesAccess().getINVENTORYEnumLiteralDeclaration_6());
                    			

                    }


                    }
                    break;
                case 8 :
                    // InternalDSL.g:2736:3: (enumLiteral_7= 'SOCIALGIFT' )
                    {
                    // InternalDSL.g:2736:3: (enumLiteral_7= 'SOCIALGIFT' )
                    // InternalDSL.g:2737:4: enumLiteral_7= 'SOCIALGIFT'
                    {
                    enumLiteral_7=(Token)match(input,91,FOLLOW_2); 

                    				current = grammarAccess.getGameMechanicTypesAccess().getSOCIALGIFTEnumLiteralDeclaration_7().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_7, grammarAccess.getGameMechanicTypesAccess().getSOCIALGIFTEnumLiteralDeclaration_7());
                    			

                    }


                    }
                    break;
                case 9 :
                    // InternalDSL.g:2744:3: (enumLiteral_8= 'AVATAR' )
                    {
                    // InternalDSL.g:2744:3: (enumLiteral_8= 'AVATAR' )
                    // InternalDSL.g:2745:4: enumLiteral_8= 'AVATAR'
                    {
                    enumLiteral_8=(Token)match(input,92,FOLLOW_2); 

                    				current = grammarAccess.getGameMechanicTypesAccess().getAVATAREnumLiteralDeclaration_8().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_8, grammarAccess.getGameMechanicTypesAccess().getAVATAREnumLiteralDeclaration_8());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGameMechanicTypes"


    // $ANTLR start "ruleEventTypes"
    // InternalDSL.g:2755:1: ruleEventTypes returns [Enumerator current=null] : ( (enumLiteral_0= 'CLICK' ) | (enumLiteral_1= 'HOVER' ) | (enumLiteral_2= 'VIEW' ) | (enumLiteral_3= 'DRAG' ) | (enumLiteral_4= 'DROP' ) | (enumLiteral_5= 'DSUCCESS' ) | (enumLiteral_6= 'MSUCCESS' ) | (enumLiteral_7= 'DFAILURE' ) | (enumLiteral_8= 'MFAILURE' ) ) ;
    public final Enumerator ruleEventTypes() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;
        Token enumLiteral_6=null;
        Token enumLiteral_7=null;
        Token enumLiteral_8=null;


        	enterRule();

        try {
            // InternalDSL.g:2761:2: ( ( (enumLiteral_0= 'CLICK' ) | (enumLiteral_1= 'HOVER' ) | (enumLiteral_2= 'VIEW' ) | (enumLiteral_3= 'DRAG' ) | (enumLiteral_4= 'DROP' ) | (enumLiteral_5= 'DSUCCESS' ) | (enumLiteral_6= 'MSUCCESS' ) | (enumLiteral_7= 'DFAILURE' ) | (enumLiteral_8= 'MFAILURE' ) ) )
            // InternalDSL.g:2762:2: ( (enumLiteral_0= 'CLICK' ) | (enumLiteral_1= 'HOVER' ) | (enumLiteral_2= 'VIEW' ) | (enumLiteral_3= 'DRAG' ) | (enumLiteral_4= 'DROP' ) | (enumLiteral_5= 'DSUCCESS' ) | (enumLiteral_6= 'MSUCCESS' ) | (enumLiteral_7= 'DFAILURE' ) | (enumLiteral_8= 'MFAILURE' ) )
            {
            // InternalDSL.g:2762:2: ( (enumLiteral_0= 'CLICK' ) | (enumLiteral_1= 'HOVER' ) | (enumLiteral_2= 'VIEW' ) | (enumLiteral_3= 'DRAG' ) | (enumLiteral_4= 'DROP' ) | (enumLiteral_5= 'DSUCCESS' ) | (enumLiteral_6= 'MSUCCESS' ) | (enumLiteral_7= 'DFAILURE' ) | (enumLiteral_8= 'MFAILURE' ) )
            int alt74=9;
            switch ( input.LA(1) ) {
            case 93:
                {
                alt74=1;
                }
                break;
            case 94:
                {
                alt74=2;
                }
                break;
            case 95:
                {
                alt74=3;
                }
                break;
            case 96:
                {
                alt74=4;
                }
                break;
            case 97:
                {
                alt74=5;
                }
                break;
            case 98:
                {
                alt74=6;
                }
                break;
            case 99:
                {
                alt74=7;
                }
                break;
            case 100:
                {
                alt74=8;
                }
                break;
            case 101:
                {
                alt74=9;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 74, 0, input);

                throw nvae;
            }

            switch (alt74) {
                case 1 :
                    // InternalDSL.g:2763:3: (enumLiteral_0= 'CLICK' )
                    {
                    // InternalDSL.g:2763:3: (enumLiteral_0= 'CLICK' )
                    // InternalDSL.g:2764:4: enumLiteral_0= 'CLICK'
                    {
                    enumLiteral_0=(Token)match(input,93,FOLLOW_2); 

                    				current = grammarAccess.getEventTypesAccess().getCLICKEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getEventTypesAccess().getCLICKEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalDSL.g:2771:3: (enumLiteral_1= 'HOVER' )
                    {
                    // InternalDSL.g:2771:3: (enumLiteral_1= 'HOVER' )
                    // InternalDSL.g:2772:4: enumLiteral_1= 'HOVER'
                    {
                    enumLiteral_1=(Token)match(input,94,FOLLOW_2); 

                    				current = grammarAccess.getEventTypesAccess().getHOVEREnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getEventTypesAccess().getHOVEREnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalDSL.g:2779:3: (enumLiteral_2= 'VIEW' )
                    {
                    // InternalDSL.g:2779:3: (enumLiteral_2= 'VIEW' )
                    // InternalDSL.g:2780:4: enumLiteral_2= 'VIEW'
                    {
                    enumLiteral_2=(Token)match(input,95,FOLLOW_2); 

                    				current = grammarAccess.getEventTypesAccess().getVIEWEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getEventTypesAccess().getVIEWEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalDSL.g:2787:3: (enumLiteral_3= 'DRAG' )
                    {
                    // InternalDSL.g:2787:3: (enumLiteral_3= 'DRAG' )
                    // InternalDSL.g:2788:4: enumLiteral_3= 'DRAG'
                    {
                    enumLiteral_3=(Token)match(input,96,FOLLOW_2); 

                    				current = grammarAccess.getEventTypesAccess().getDRAGEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getEventTypesAccess().getDRAGEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalDSL.g:2795:3: (enumLiteral_4= 'DROP' )
                    {
                    // InternalDSL.g:2795:3: (enumLiteral_4= 'DROP' )
                    // InternalDSL.g:2796:4: enumLiteral_4= 'DROP'
                    {
                    enumLiteral_4=(Token)match(input,97,FOLLOW_2); 

                    				current = grammarAccess.getEventTypesAccess().getDROPEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_4, grammarAccess.getEventTypesAccess().getDROPEnumLiteralDeclaration_4());
                    			

                    }


                    }
                    break;
                case 6 :
                    // InternalDSL.g:2803:3: (enumLiteral_5= 'DSUCCESS' )
                    {
                    // InternalDSL.g:2803:3: (enumLiteral_5= 'DSUCCESS' )
                    // InternalDSL.g:2804:4: enumLiteral_5= 'DSUCCESS'
                    {
                    enumLiteral_5=(Token)match(input,98,FOLLOW_2); 

                    				current = grammarAccess.getEventTypesAccess().getDSUCCESSEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_5, grammarAccess.getEventTypesAccess().getDSUCCESSEnumLiteralDeclaration_5());
                    			

                    }


                    }
                    break;
                case 7 :
                    // InternalDSL.g:2811:3: (enumLiteral_6= 'MSUCCESS' )
                    {
                    // InternalDSL.g:2811:3: (enumLiteral_6= 'MSUCCESS' )
                    // InternalDSL.g:2812:4: enumLiteral_6= 'MSUCCESS'
                    {
                    enumLiteral_6=(Token)match(input,99,FOLLOW_2); 

                    				current = grammarAccess.getEventTypesAccess().getMSUCCESSEnumLiteralDeclaration_6().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_6, grammarAccess.getEventTypesAccess().getMSUCCESSEnumLiteralDeclaration_6());
                    			

                    }


                    }
                    break;
                case 8 :
                    // InternalDSL.g:2819:3: (enumLiteral_7= 'DFAILURE' )
                    {
                    // InternalDSL.g:2819:3: (enumLiteral_7= 'DFAILURE' )
                    // InternalDSL.g:2820:4: enumLiteral_7= 'DFAILURE'
                    {
                    enumLiteral_7=(Token)match(input,100,FOLLOW_2); 

                    				current = grammarAccess.getEventTypesAccess().getDFAILUREEnumLiteralDeclaration_7().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_7, grammarAccess.getEventTypesAccess().getDFAILUREEnumLiteralDeclaration_7());
                    			

                    }


                    }
                    break;
                case 9 :
                    // InternalDSL.g:2827:3: (enumLiteral_8= 'MFAILURE' )
                    {
                    // InternalDSL.g:2827:3: (enumLiteral_8= 'MFAILURE' )
                    // InternalDSL.g:2828:4: enumLiteral_8= 'MFAILURE'
                    {
                    enumLiteral_8=(Token)match(input,101,FOLLOW_2); 

                    				current = grammarAccess.getEventTypesAccess().getMFAILUREEnumLiteralDeclaration_8().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_8, grammarAccess.getEventTypesAccess().getMFAILUREEnumLiteralDeclaration_8());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEventTypes"


    // $ANTLR start "ruleConditionTypes"
    // InternalDSL.g:2838:1: ruleConditionTypes returns [Enumerator current=null] : ( (enumLiteral_0= 'EQUAL' ) | (enumLiteral_1= 'MORE' ) | (enumLiteral_2= 'LESS' ) ) ;
    public final Enumerator ruleConditionTypes() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;


        	enterRule();

        try {
            // InternalDSL.g:2844:2: ( ( (enumLiteral_0= 'EQUAL' ) | (enumLiteral_1= 'MORE' ) | (enumLiteral_2= 'LESS' ) ) )
            // InternalDSL.g:2845:2: ( (enumLiteral_0= 'EQUAL' ) | (enumLiteral_1= 'MORE' ) | (enumLiteral_2= 'LESS' ) )
            {
            // InternalDSL.g:2845:2: ( (enumLiteral_0= 'EQUAL' ) | (enumLiteral_1= 'MORE' ) | (enumLiteral_2= 'LESS' ) )
            int alt75=3;
            switch ( input.LA(1) ) {
            case 102:
                {
                alt75=1;
                }
                break;
            case 103:
                {
                alt75=2;
                }
                break;
            case 104:
                {
                alt75=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 75, 0, input);

                throw nvae;
            }

            switch (alt75) {
                case 1 :
                    // InternalDSL.g:2846:3: (enumLiteral_0= 'EQUAL' )
                    {
                    // InternalDSL.g:2846:3: (enumLiteral_0= 'EQUAL' )
                    // InternalDSL.g:2847:4: enumLiteral_0= 'EQUAL'
                    {
                    enumLiteral_0=(Token)match(input,102,FOLLOW_2); 

                    				current = grammarAccess.getConditionTypesAccess().getEQUALEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getConditionTypesAccess().getEQUALEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalDSL.g:2854:3: (enumLiteral_1= 'MORE' )
                    {
                    // InternalDSL.g:2854:3: (enumLiteral_1= 'MORE' )
                    // InternalDSL.g:2855:4: enumLiteral_1= 'MORE'
                    {
                    enumLiteral_1=(Token)match(input,103,FOLLOW_2); 

                    				current = grammarAccess.getConditionTypesAccess().getMOREEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getConditionTypesAccess().getMOREEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalDSL.g:2862:3: (enumLiteral_2= 'LESS' )
                    {
                    // InternalDSL.g:2862:3: (enumLiteral_2= 'LESS' )
                    // InternalDSL.g:2863:4: enumLiteral_2= 'LESS'
                    {
                    enumLiteral_2=(Token)match(input,104,FOLLOW_2); 

                    				current = grammarAccess.getConditionTypesAccess().getLESSEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getConditionTypesAccess().getLESSEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConditionTypes"


    // $ANTLR start "ruleRestrictionTypes"
    // InternalDSL.g:2873:1: ruleRestrictionTypes returns [Enumerator current=null] : ( (enumLiteral_0= 'TIMELIMIT' ) | (enumLiteral_1= 'ACTIONLIMIT' ) | (enumLiteral_2= 'POINTLIMIT' ) ) ;
    public final Enumerator ruleRestrictionTypes() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;


        	enterRule();

        try {
            // InternalDSL.g:2879:2: ( ( (enumLiteral_0= 'TIMELIMIT' ) | (enumLiteral_1= 'ACTIONLIMIT' ) | (enumLiteral_2= 'POINTLIMIT' ) ) )
            // InternalDSL.g:2880:2: ( (enumLiteral_0= 'TIMELIMIT' ) | (enumLiteral_1= 'ACTIONLIMIT' ) | (enumLiteral_2= 'POINTLIMIT' ) )
            {
            // InternalDSL.g:2880:2: ( (enumLiteral_0= 'TIMELIMIT' ) | (enumLiteral_1= 'ACTIONLIMIT' ) | (enumLiteral_2= 'POINTLIMIT' ) )
            int alt76=3;
            switch ( input.LA(1) ) {
            case 105:
                {
                alt76=1;
                }
                break;
            case 106:
                {
                alt76=2;
                }
                break;
            case 107:
                {
                alt76=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 76, 0, input);

                throw nvae;
            }

            switch (alt76) {
                case 1 :
                    // InternalDSL.g:2881:3: (enumLiteral_0= 'TIMELIMIT' )
                    {
                    // InternalDSL.g:2881:3: (enumLiteral_0= 'TIMELIMIT' )
                    // InternalDSL.g:2882:4: enumLiteral_0= 'TIMELIMIT'
                    {
                    enumLiteral_0=(Token)match(input,105,FOLLOW_2); 

                    				current = grammarAccess.getRestrictionTypesAccess().getTIMELIMITEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getRestrictionTypesAccess().getTIMELIMITEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalDSL.g:2889:3: (enumLiteral_1= 'ACTIONLIMIT' )
                    {
                    // InternalDSL.g:2889:3: (enumLiteral_1= 'ACTIONLIMIT' )
                    // InternalDSL.g:2890:4: enumLiteral_1= 'ACTIONLIMIT'
                    {
                    enumLiteral_1=(Token)match(input,106,FOLLOW_2); 

                    				current = grammarAccess.getRestrictionTypesAccess().getACTIONLIMITEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getRestrictionTypesAccess().getACTIONLIMITEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalDSL.g:2897:3: (enumLiteral_2= 'POINTLIMIT' )
                    {
                    // InternalDSL.g:2897:3: (enumLiteral_2= 'POINTLIMIT' )
                    // InternalDSL.g:2898:4: enumLiteral_2= 'POINTLIMIT'
                    {
                    enumLiteral_2=(Token)match(input,107,FOLLOW_2); 

                    				current = grammarAccess.getRestrictionTypesAccess().getPOINTLIMITEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getRestrictionTypesAccess().getPOINTLIMITEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRestrictionTypes"


    // $ANTLR start "rulePrizeTypes"
    // InternalDSL.g:2908:1: rulePrizeTypes returns [Enumerator current=null] : ( (enumLiteral_0= 'VIRTUAL' ) | (enumLiteral_1= 'PHYSICAL' ) ) ;
    public final Enumerator rulePrizeTypes() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalDSL.g:2914:2: ( ( (enumLiteral_0= 'VIRTUAL' ) | (enumLiteral_1= 'PHYSICAL' ) ) )
            // InternalDSL.g:2915:2: ( (enumLiteral_0= 'VIRTUAL' ) | (enumLiteral_1= 'PHYSICAL' ) )
            {
            // InternalDSL.g:2915:2: ( (enumLiteral_0= 'VIRTUAL' ) | (enumLiteral_1= 'PHYSICAL' ) )
            int alt77=2;
            int LA77_0 = input.LA(1);

            if ( (LA77_0==108) ) {
                alt77=1;
            }
            else if ( (LA77_0==109) ) {
                alt77=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 77, 0, input);

                throw nvae;
            }
            switch (alt77) {
                case 1 :
                    // InternalDSL.g:2916:3: (enumLiteral_0= 'VIRTUAL' )
                    {
                    // InternalDSL.g:2916:3: (enumLiteral_0= 'VIRTUAL' )
                    // InternalDSL.g:2917:4: enumLiteral_0= 'VIRTUAL'
                    {
                    enumLiteral_0=(Token)match(input,108,FOLLOW_2); 

                    				current = grammarAccess.getPrizeTypesAccess().getVIRTUALEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getPrizeTypesAccess().getVIRTUALEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalDSL.g:2924:3: (enumLiteral_1= 'PHYSICAL' )
                    {
                    // InternalDSL.g:2924:3: (enumLiteral_1= 'PHYSICAL' )
                    // InternalDSL.g:2925:4: enumLiteral_1= 'PHYSICAL'
                    {
                    enumLiteral_1=(Token)match(input,109,FOLLOW_2); 

                    				current = grammarAccess.getPrizeTypesAccess().getPHYSICALEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getPrizeTypesAccess().getPHYSICALEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrizeTypes"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x000000000001C000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000060000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x8800000000000000L,0x0000000000000002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000140000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000001C40000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000038L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000001840000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000000000L,0x00000000000000C0L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000001040000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000F00L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x000000000C004000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x000000000C000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000000000000L,0x00000000000FF000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x00000000C0144000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x00000000C0140000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000000000000L,0x000000001FF00000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000080140000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000006600004000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000006600000000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0030000000000000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000006400000000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000001000020000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000006000000000L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0440000000000000L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x00000F0000144000L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x00000F0000140000L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0000000000000000L,0x0000003FE0000000L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x00000E0000140000L});
    public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x00000C0000140000L});
    public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0000080000140000L});
    public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0000E00000144000L});
    public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0000E00000140000L});
    public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x0000C00000140000L});
    public static final BitSet FOLLOW_55 = new BitSet(new long[]{0x0000000000000000L,0x000001C000000000L});
    public static final BitSet FOLLOW_56 = new BitSet(new long[]{0x0000800000140000L});
    public static final BitSet FOLLOW_57 = new BitSet(new long[]{0x000E000000144000L});
    public static final BitSet FOLLOW_58 = new BitSet(new long[]{0x000E000000140000L});
    public static final BitSet FOLLOW_59 = new BitSet(new long[]{0x000C000000140000L});
    public static final BitSet FOLLOW_60 = new BitSet(new long[]{0x0000000000000000L,0x00000E0000000000L});
    public static final BitSet FOLLOW_61 = new BitSet(new long[]{0x0008000000140000L});
    public static final BitSet FOLLOW_62 = new BitSet(new long[]{0x0380000000040000L});
    public static final BitSet FOLLOW_63 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_64 = new BitSet(new long[]{0x0300000000040000L});
    public static final BitSet FOLLOW_65 = new BitSet(new long[]{0x0200000000040000L});
    public static final BitSet FOLLOW_66 = new BitSet(new long[]{0x7000000000144000L});
    public static final BitSet FOLLOW_67 = new BitSet(new long[]{0x7000000000140000L});
    public static final BitSet FOLLOW_68 = new BitSet(new long[]{0x6000000000140000L});
    public static final BitSet FOLLOW_69 = new BitSet(new long[]{0x4000000000140000L});
    public static final BitSet FOLLOW_70 = new BitSet(new long[]{0x1008004000144000L,0x0000000000000001L});
    public static final BitSet FOLLOW_71 = new BitSet(new long[]{0x1008004000140000L,0x0000000000000001L});
    public static final BitSet FOLLOW_72 = new BitSet(new long[]{0x0008004000140000L,0x0000000000000001L});
    public static final BitSet FOLLOW_73 = new BitSet(new long[]{0x0000004000140000L,0x0000000000000001L});
    public static final BitSet FOLLOW_74 = new BitSet(new long[]{0x0000004000140000L});
    public static final BitSet FOLLOW_75 = new BitSet(new long[]{0x1000000000144000L,0x0000000000000004L});
    public static final BitSet FOLLOW_76 = new BitSet(new long[]{0x1000000000140000L,0x0000000000000004L});
    public static final BitSet FOLLOW_77 = new BitSet(new long[]{0x0000000000140000L,0x0000000000000004L});
    public static final BitSet FOLLOW_78 = new BitSet(new long[]{0x0000000000000000L,0x0000300000000000L});

}