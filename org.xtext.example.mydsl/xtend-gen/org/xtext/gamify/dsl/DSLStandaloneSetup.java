/**
 * generated by Xtext 2.16.0
 */
package org.xtext.gamify.dsl;

import org.xtext.gamify.dsl.DSLStandaloneSetupGenerated;

/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
@SuppressWarnings("all")
public class DSLStandaloneSetup extends DSLStandaloneSetupGenerated {
  public static void doSetup() {
    new DSLStandaloneSetup().createInjectorAndDoEMFRegistration();
  }
}
